//
//  KKProgressManager.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 5/16/14.
//
//

#import <Foundation/Foundation.h>
#import "Progress.h"

@protocol ProgressObserverDelegate <NSObject>
@optional
- (void)idleProgressWithValue:(CGFloat)percents
                     stopFlag:(BOOL *)stopFlag;
- (void)idleProgressDidFinishWithValue:(CGFloat)value;
- (void)summaryProgressDidChangeValue:(CGFloat)summaryProgressValue;
- (void)progressDidFinishObserving:(Progress *)progress;
- (void)progress:(Progress *)progress didChangeValue:(CGFloat)progressValue;

@end

@interface ProgressObserver : NSObject
<
ProgressDelegate
>

@property (nonatomic, weak) id <ProgressObserverDelegate> delegate;

@property (nonatomic) CGFloat idlePercentsStepInterval;
@property (nonatomic) CGFloat idleInvocationTime;
@property (nonatomic) CGFloat idleProgressStartingValue;
@property (nonatomic) CGFloat idleProgressEndingValue;

+ (instancetype)sharedObserver;
- (void)observeProgress:(Progress *)progress
                partial:(BOOL)isPartial;

- (void)increaseSummaryProgressWithValue:(CGFloat)value;
- (void)startIdleProgressWithStartingValue:(CGFloat)startingValue
                               endingValue:(CGFloat)endingValue
                                 partially:(BOOL)isPartial;

@end
