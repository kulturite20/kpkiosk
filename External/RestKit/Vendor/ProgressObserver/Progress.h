//
//  Progress.h
//  RestKit
//
//  Created by Vadim Osovets on 5/16/14.
//  Copyright (c) 2014 RestKit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@class Progress;

@protocol ProgressDelegate <NSObject>
@required
- (void)progress:(Progress *)progress observesValue:(CGFloat)value;
@optional
- (void)progressDidStart:(Progress *)progress;
- (void)progressDidFinish:(Progress *)progress;

@end

@interface Progress : NSObject

@property (nonatomic, assign) BOOL isPartial;
@property (nonatomic, weak) id <ProgressDelegate> delegate;

- (id)initWithIdentifier:(NSString *)identifier
           startingValue:(CGFloat)startingValue
             endingValue:(CGFloat)endingValue;

- (NSString *)identifier;
- (CGFloat)currentValue;

- (void)subscribeForObserving:(CGFloat)value;

@end
