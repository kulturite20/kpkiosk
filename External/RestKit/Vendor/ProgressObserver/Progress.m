//
//  Progress.m
//  RestKit
//
//  Created by Vadim Osovets on 5/16/14.
//  Copyright (c) 2014 RestKit. All rights reserved.
//

#import "Progress.h"

@implementation Progress {
    NSString    *_identifier;
    CGFloat      _currentValue;
    
    CGFloat _startingValue;
    CGFloat _endingValue;
}

#pragma mark - Init

- (id)initWithIdentifier:(NSString *)identifier
           startingValue:(CGFloat)startingValue
             endingValue:(CGFloat)endingValue {
    if (self = [super init]) {
        _identifier = identifier;
        _startingValue = _currentValue = startingValue;
        _endingValue = endingValue;
    }
    return self;
}

#pragma mark - Public

- (void)subscribeForObserving:(CGFloat)value {
    if (_currentValue == _startingValue) {
        [_delegate progressDidStart:self];
    }
    if (_currentValue <= _endingValue) {
        _currentValue = (float)(value * (_endingValue - _startingValue));
        [_delegate progress:self observesValue:_currentValue + _startingValue];
    } else {
        [_delegate progressDidFinish:self];
    }
}

- (CGFloat)currentValue {
    return _currentValue;
}

- (NSString *)identifier {
    return _identifier;
}

@end
