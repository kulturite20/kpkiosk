//
//  KKProgressManager.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 5/16/14.
//
//

#import "ProgressObserver.h"

@interface ProgressObserver ()

@property (nonatomic) CGFloat idleProgress;
@property (nonatomic) BOOL stopMarker;
@property (nonatomic) BOOL isIdleProgressPartial;

@end

@implementation ProgressObserver

#pragma mark - Singleton

+ (instancetype)sharedObserver {
    static dispatch_once_t pred = 0;
    __strong static id _sharedObserver = nil;
    dispatch_once(&pred, ^{
        _sharedObserver = [[self alloc] init];
    });
    return _sharedObserver;
}

#pragma mark - Init

- (id)init {
    if (self = [super init]) {
        // By default
        _idleInvocationTime = 0.1;
        _idlePercentsStepInterval = 0.15;
    }
    return self;
}

#pragma mark - Public

- (void)increaseSummaryProgressWithValue:(CGFloat)value {
    [_delegate summaryProgressDidChangeValue:value];
}

- (void)observeProgress:(Progress *)progress
                partial:(BOOL)isPartial {
    progress.isPartial = isPartial;
    progress.delegate = self;
}

- (void)startIdleProgressWithStartingValue:(CGFloat)startingValue
                               endingValue:(CGFloat)endingValue
                                 partially:(BOOL)isPartial {
    if (_stopMarker || _idleProgress >= endingValue) {
        if ([_delegate respondsToSelector:@selector(idleProgressDidFinishWithValue:)]) {
            [_delegate idleProgressDidFinishWithValue:_idleProgress];
        }
        _idleProgress = 0.0f;
        _isIdleProgressPartial = NO;
        _stopMarker = NO;
        return;
    }
    _isIdleProgressPartial = isPartial;
    _idleProgress = startingValue;
    _idleProgress += _idlePercentsStepInterval;
    startingValue = _idleProgress;
    if ([_delegate respondsToSelector:@selector(idleProgressWithValue:stopFlag:)]) {
        [_delegate idleProgressWithValue:_idleProgress
                                stopFlag:&_stopMarker];
    }
    if (isPartial) {
        [_delegate summaryProgressDidChangeValue:_idleProgress];
    }
    __block long long delayInNanoSeconds = (long long)(_idleInvocationTime * NSEC_PER_SEC);
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInNanoSeconds);
    dispatch_after(popTime, dispatch_get_main_queue(), ^{
        [self startIdleProgressWithStartingValue:_idleProgress
                                     endingValue:endingValue
                                       partially:isPartial];
    });
}

#pragma mark - ProgressDelegate

- (void)progressDidStart:(Progress *)progress {
    if (progress.isPartial && _isIdleProgressPartial) {
        _stopMarker = YES;
    }
}

- (void)progressDidFinish:(Progress *)progress {
    if ([_delegate respondsToSelector:@selector(progressDidFinishObserving:)]) {
        [_delegate progressDidFinishObserving:progress];
    }
}

- (void)progress:(Progress *)progress observesValue:(CGFloat)value {
    [_delegate progress:progress didChangeValue:value];
    if (progress.isPartial) {
        [_delegate summaryProgressDidChangeValue:value];
    }
}

@end
