//
//  KKPopoverBackgroundView.h
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 8/8/13.
//
//

#import <UIKit/UIKit.h>

@interface KKPopoverBackgroundView : UIPopoverBackgroundView

@end
