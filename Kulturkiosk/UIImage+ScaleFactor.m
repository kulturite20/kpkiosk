//
//  UIImage+ScaleFactor.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 2/18/16.
//
//

#import "UIImage+ScaleFactor.h"

@implementation UIImage (ScaleFactor)

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
