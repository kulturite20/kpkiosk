//
//  KKLanguage.h
//  Kulturkiosk
//
//  Created by Rune Botten on 16.10.12.
//
//

#import <Foundation/Foundation.h>

typedef enum KKLanguageType {
  KKLanguageTypeEnglish   = 1,
  KKLanguageTypeFrench    = 2,
  KKLanguageTypeGerman    = 3,
  KKLanguageTypeSwedish   = 4,
  KKLanguageTypeSpanish   = 5,
  KKLanguageTypeNorwegian = 6,
} KKLanguageType;

@interface KKLanguage : NSObject

+ (NSArray *)availableLanguages;
+ (KKLanguageType)currentLanguage;
+ (NSString *)currentLanguageTitle;
+ (BOOL)setLanguage:(KKLanguageType)newLanguage;
+ (NSString *)localizedString:(NSString *)key;
+ (NSString *)contentForLanguage:(KKLanguageType)lang;

/** Return string description of given lang type, e.g. KKLanguageTypeEnglish would return @"en". */
+ (NSString *)descriptionForLanguageType:(KKLanguageType)type;

@end
