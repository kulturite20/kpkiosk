//
//  KKPresentationViewController.h
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 8/16/13.
//
//

#import <UIKit/UIKit.h>

@class Presentation;
@class Record;
@class MosaicPresentation;

@interface KKPresentationViewController : UIViewController

@property (strong, nonatomic) Presentation *presentation;
@property (nonatomic) Record *recordToPresent;

- (IBAction)dismissController:(id)sender;

@end
