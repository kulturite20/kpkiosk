//
//  KKAnnotation.m
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 04.06.13.
//
//

#import "KKAnnotation.h"
#import "KKAnnotationView.h"
#import "UIColor+HexColor.h"
#import <QuartzCore/QuartzCore.h>

#define KK_PIN_WIDTH   44.0f
#define KK_PIN_HEIGHT  66.0f
#define KK_PIN_POINT_X 25.0f
#define KK_PIN_POINT_Y 35.0f

@interface KKAnnotationView ()
- (void)updatePosition;

@property (nonatomic, weak) KKMapView *mapView;
@end

@implementation KKAnnotationView

- (id)initWithAnnotation:(KKAnnotation *)annotation onMapView:(KKMapView *)mapView {
  self = [super initWithFrame:CGRectZero];
  if (self) {
    self.mapView    = mapView;
    self.annotation = annotation;
    self.multipleTouchEnabled=YES;
    self.animating  = NO;
    self.exclusiveTouch = NO;
    self.opaque = NO;
    self.translatesAutoresizingMaskIntoConstraints = YES;
    self.layer.anchorPoint = CGPointMake(0.5, 1.0);
    [self updatePosition];
  }
  return self;
}

- (void)drawRect:(CGRect)rect
{
  self.backgroundColor = [UIColor clearColor];
  [super drawRect:rect];
  CGContextRef context = UIGraphicsGetCurrentContext();
  CGContextClearRect(context, rect);
  //// Bezier Drawing
  UIBezierPath* bezierPath = [UIBezierPath bezierPath];
  [bezierPath moveToPoint: CGPointMake((CGFloat)37.56, (CGFloat)6.44)];
  [bezierPath addCurveToPoint: CGPointMake((CGFloat)40.82, (CGFloat)33.41) controlPoint1: CGPointMake((CGFloat)44.87, (CGFloat)13.76) controlPoint2: CGPointMake((CGFloat)45.96, (CGFloat)24.95)];
  [bezierPath addLineToPoint: CGPointMake(22, 66)];
  [bezierPath addLineToPoint: CGPointMake((CGFloat)3.18, (CGFloat)33.41)];
  [bezierPath addCurveToPoint: CGPointMake(6.44f, 6.44f) controlPoint1: CGPointMake(-1.96f, 24.95f) controlPoint2: CGPointMake(-0.87f, 13.76f)];
  [bezierPath addCurveToPoint: CGPointMake(37.56f, 6.44f) controlPoint1: CGPointMake(15.04f, -2.15f) controlPoint2: CGPointMake(28.96f, -2.15f)];
  [bezierPath closePath];

  if (self.annotation.selected) {
    [[UIColor blackColor] setFill];
  }else{
    if (self.annotation) {
      [self.annotation.backgroundColor setFill];
    }else{
      [self.bgColor setFill];
    }
  }

  [bezierPath fill];
  
  [[UIColor whiteColor] set];
  
  
  if (self.annotation) {
    [self.annotation.code drawInRect:CGRectMake(8, 12, 30, 20) withFont:[UIFont fontWithName:@"MyriadPro-Bold" size:15] lineBreakMode:NSLineBreakByWordWrapping alignment:NSTextAlignmentCenter];
  }else if(self.code){
    [self.code drawInRect:CGRectMake(8, 12, 30, 20) withFont:[UIFont fontWithName:@"MyriadPro-Bold" size:15] lineBreakMode:NSLineBreakByWordWrapping alignment:NSTextAlignmentCenter];
  }

}


-(void)updatePosition{
  CGPoint point = self.annotation.point;
  
    //Position pin bottom center correctly;
  CGRect frame = self.frame;
  if (frame.origin.x == 0) {
    frame.origin.x = point.x - (KK_PIN_WIDTH / 2);
    frame.origin.y = point.y - ((KK_PIN_HEIGHT / 2) + 30);
  }
  
  if (!frame.size.width) {
    frame.size.width = KK_PIN_WIDTH;
    frame.size.height = KK_PIN_HEIGHT;
  }
  
  CGAffineTransform transform = CGAffineTransformMakeScale(1.0/self.mapView.zoomScale, 1.0/self.mapView.zoomScale);
  self.frame = frame;
  self.transform = transform;
  
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:@"contentSize"]) {
    [self updatePosition];
	}
  if ([keyPath isEqualToString:@"selected"]) {
    [self setNeedsDisplay];
  }
}


@end

#undef NA_PIN_WIDTH
#undef NA_PIN_HEIGHT
#undef NA_PIN_POINT_X
#undef NA_PIN_POINT_Y
