//
//  KKSlideShowPresentationCell.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 7/12/16.
//
//

#import "KKSlideShowPresentationCell.h"

#import "KKMediaItemViewController.h"

@implementation KKSlideShowPresentationCell {
    KKMediaItemViewController *_mediaPlayerController;
    
    __weak IBOutlet UIView *_videoContainer;
}

- (void)prepareForReuse {
    _mediaItem = nil;
    _mediaPlayerController = nil;
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(rootViewControllerDidDismiss:) name:@"SlideShowViewControllerDidDismiss" object:nil];
}

- (void)setMediaItem:(File *)mediaItem {
    _mediaItem = mediaItem;
    
    for (UIView *subview in [_videoContainer subviews]) {
        [subview removeFromSuperview];
    }
    
    _mediaPlayerController = [KKMediaItemViewController new];
    
    [_mediaPlayerController willMoveToParentViewController:self.rootViewController];
    [self.rootViewController addChildViewController:_mediaPlayerController];
    
    _mediaPlayerController.mediaItem = _mediaItem;
    
    _mediaPlayerController.isInSlideShowMode = YES;
    
    [_mediaPlayerController didMoveToParentViewController:self.rootViewController];
    
    [_videoContainer addSubview:_mediaPlayerController.view];
    
    CGRect videoFrame =  _videoContainer.frame;
    videoFrame.origin.x = videoFrame.origin.y = 0;
    
    _mediaPlayerController.view.frame = videoFrame;
    
    // use autolayout to center content properly
    UIView *containerView = _videoContainer;
    UIView *newSubview = _mediaPlayerController.view;
    newSubview.frame = CGRectZero;
    
    newSubview.translatesAutoresizingMaskIntoConstraints = NO;
    [containerView addSubview:newSubview];
    
    [containerView addConstraint:[NSLayoutConstraint constraintWithItem:newSubview
                                                              attribute:NSLayoutAttributeTop
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:containerView
                                                              attribute:NSLayoutAttributeTop
                                                             multiplier:1.0
                                                               constant:0.0]];
    
    [containerView addConstraint:[NSLayoutConstraint constraintWithItem:newSubview
                                                              attribute:NSLayoutAttributeLeading
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:containerView
                                                              attribute:NSLayoutAttributeLeading
                                                             multiplier:1.0
                                                               constant:0.0]];
    
    [containerView addConstraint:[NSLayoutConstraint constraintWithItem:newSubview
                                                              attribute:NSLayoutAttributeBottom
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:containerView
                                                              attribute:NSLayoutAttributeBottom
                                                             multiplier:1.0
                                                               constant:0.0]];
    
    [containerView addConstraint:[NSLayoutConstraint constraintWithItem:newSubview
                                                              attribute:NSLayoutAttributeTrailing
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:containerView
                                                              attribute:NSLayoutAttributeTrailing
                                                             multiplier:1.0
                                                               constant:0.0]];
    
    // if not doing this in `viewDidLoad` (e.g. you're doing this in
    // `viewDidAppear` or later), you can force `layoutIfNeeded` if you want
    // to look at `frame` values. Generally you don't need to do this unless
    // manually inspecting `frame` values or when changing constraints in a
    // `animations` block of `animateWithDuration`.
    
    [containerView layoutIfNeeded];
}

- (void)playMedia {
    if ([_mediaItem.type isEqualToString:@"video"]) {
        [_mediaPlayerController play];
    }
}

- (void)stopPlayingMedia {
    if ([_mediaItem.type isEqualToString:@"video"]) {
        [_mediaPlayerController pauseVideoPlayer];
    }
}

#pragma mark - Private

- (void)rootViewControllerDidDismiss:(NSNotification *)notification {
    if (_mediaPlayerController.isPlayingMediaItem) {
        [_mediaPlayerController pauseVideoPlayer];
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
