//
//  main.m
//  Kulturkiosk
//
//  Created by Anders Wiik on 21.05.12.
//  Copyright (c) 2012 Nordaaker Ltd.. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KKAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KKAppDelegate class]));
    }
}
