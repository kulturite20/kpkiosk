//
//  KKImageContainerViewController.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 1/29/16.
//
//

#import <UIKit/UIKit.h>

@interface KKImageContainerViewController : UIViewController

@property NSUInteger pageIndex;
@property (copy, nonatomic) void (^willhideScrollImageBars)(void);

// ONLY when we came from parent
@property CGRect parentRect;
@property BOOL imageVideoGalleryMode;

- (void)mediaItem:(File *)mediaItem;

@end
