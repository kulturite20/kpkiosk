//
//  NSString+Filesystem.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 9/22/16.
//
//

#import <Foundation/Foundation.h>

@interface NSString (Filesystem)

- (NSArray *)listOfFiles;

@end
