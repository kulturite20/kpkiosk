//
//  KKMapViewController.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 31.05.13.
//
//

#import "KKMapViewController.h"
#import "KKCalloutViewController.h"
#import "KKMapView.h"
#import "KKAnnotationView.h"
#import "KKAnnotation.h"
#import "KKCalloutView.h"
#import "UIColor+HexColor.h"
#import "UIImage+ScaleFactor.h"
#import "KKRecordContainerViewController.h"
#import "MapTitle+CoreDataClass.h"
#import "MapDescription+CoreDataClass.h"
#import "NSMutableString + FormatHTMLTags.h"
#import "KKCircleLineDrawer.h"
#import "Presentation.h"
#import "RecordImage.h"

@interface KKMapViewController () <KKMapViewDelegate, UIPopoverControllerDelegate>
@property (weak, nonatomic) IBOutlet KKMapView *mapView;
@property (weak, nonatomic) KKAnnotation *selectedAnnotation;
@property (strong, nonatomic) KKCalloutViewController *calloutView;
@property (nonatomic, assign) BOOL active;

@property (nonatomic, assign) CGFloat scaleFactor;
@property (nonatomic) BOOL needToAnnotateWithLinePoints;
@property (nonatomic) NSMutableArray<KKCircleLineDrawer *> *circleLineDrawerArray;

- (void) annotateMap;
-(void) languageChange;

@end

@implementation KKMapViewController

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    for(KKCircleLineDrawer *circleLineDrawer in self.circleLineDrawerArray){
        [self.mapView removeObserver:circleLineDrawer forKeyPath:@"contentSize"];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mapView.userInteractionEnabled = true;
    self.view.userInteractionEnabled = true;
    self.mapView.containerView.userInteractionEnabled = true;
    
    for (RecordDescription *recordDescription in self.presentation.records_decriptions) {
        if (recordDescription.map_image != nil) {
            self.needToAnnotateWithLinePoints = YES;
            break;
        }
    }
    
    self.navigationController.navigationBarHidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didTapCallout:)
                                                 name:@"didTapCallout"
                                               object:nil];
    
    UIImage *mapBackground = [self.presentation mapBackground];
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    CGSize mapSize = CGSizeMake(screenSize.width, screenSize.height - 60);
    
    NSLog(@"SIZEL: %@", NSStringFromCGSize(mapBackground.size));
    
    self.scaleFactor = mapSize.height / mapBackground.size.height;
    
    self.scaleFactor = 1;
    
//    self.scaleFactor = 1.0;
    
    if (mapBackground.size.width < 1024 && mapBackground != nil) {
        self.scaleFactor = 1046 / mapBackground.size.height;
        CGSize imageSize = CGSizeMake(floorf(mapBackground.size.width*self.scaleFactor), floorf(mapBackground.size.height*self.scaleFactor));
        //CGSize imageSize = CGSizeMake(1024, 708);
        mapBackground = [UIImage imageWithImage:[self.presentation mapBackground] scaledToSize:imageSize];
    }
//    
//    //TODO: Only for debug disabling zoom
//    [self.mapView displayMap:mapBackground shouldDisableZoom:false annotateWithLinePoints:self.needToAnnotateWithLinePoints];
    
    [self.mapView displayMap:mapBackground shouldDisableZoom:[self shouldDisableZoom] annotateWithLinePoints:self.needToAnnotateWithLinePoints];
    
    self.mapView.backgroundColor = [UIColor lightGrayColor];
    self.mapView.mapViewdelegate = self;
    
    
    if(self.presentation && mapBackground != nil) {
        // Separetion between lineDots and markers
//        if (self.needToAnnotateWithLinePoints) {
//            [self annotateMapWithLinePoints];
//        } else {
//            [self annotateMap];
//        }
//        self.calloutView = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil]  instantiateViewControllerWithIdentifier:@"CalloutView"];
        
        [self annotateMapWithLinePoints];
        [self annotateMap];
        
        self.calloutView = [[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil]  instantiateViewControllerWithIdentifier:@"CalloutView"];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(languageChange)
                                                 name:@"languageChange"
                                               object:nil];
}


- (void)languageChange {
    
    if (self.needToAnnotateWithLinePoints) {
        
        [self clearMapView];
        
        for (KKCircleLineDrawer *circleLineDrawer in self.circleLineDrawerArray) {
            [circleLineDrawer reloadCircleLineDrawer];
        }
        
    } else {
        
        [self.mapView removeAnnotations];
        [self annotateMap];
        
    }
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self didDeselectAnnotation: self.selectedAnnotation];
    self.active = NO;
}

- (void)viewWillAppear:(BOOL)animated{
    self.active=YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.presentation.isDefault && UIAppDelegate.needToShowDefaultRecord && self.presentation.defaultRecord) {
        KKRecordContainerViewController *recordContainer = [KKRecordContainerViewController new];
        recordContainer.record = self.presentation.defaultRecord;
        
        [self presentViewController:recordContainer
                           animated:YES
                         completion:NULL];
    }
}

#pragma mark - Maps

- (void) annotateMap {
    for (Record *record in [[self presentation] records]) {
        [self addAnnotationForRecord:record];
    }
}


- (void)annotateMapWithLinePoints {
    
    // Need for reload text in map image when changing language
    for (UIView *subview in [self.mapView.containerView subviews]) {
        if ([subview isKindOfClass:[UILabel class]] || [subview isKindOfClass:[UIButton class]]) {
            [subview removeFromSuperview];
        }
    }
    
    self.circleLineDrawerArray = [NSMutableArray new];
    
    for (RecordDescription *recordDescription in [self.presentation records_decriptions]) {
        MapImage *mapImage = recordDescription.map_image;
        
        if (mapImage != nil) {
            
            KKCircleLineDrawer *circleLineDrawer = [[KKCircleLineDrawer alloc] initWithMapView:self.mapView];
            
            [self.mapView addObserver:circleLineDrawer forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
            
            [circleLineDrawer creatCircleWith:recordDescription annotationOffset:self.mapView.annotationOffset scaleFactor:self.scaleFactor];
            
            [self.circleLineDrawerArray addObject:circleLineDrawer];
            
            
            // Handle tap on text, presenting record
            [circleLineDrawer setTapBlock:^(NSInteger recordID) {
                
                for (Record *record in [self.presentation records]) {
                    if ([record.identifier integerValue] == recordID) {
                        KKRecordContainerViewController *recordViewController = [KKRecordContainerViewController new];
                        
                        recordViewController.record = record;
                        
                        [self presentViewController:recordViewController animated:YES completion:nil];
                        
                        break;
                    }
                }
                
                
            }];
            
//            // TODO: only to debug FRAME
//            return;
            
        }
        
    }
    
}


- (void)addAnnotationForRecord:(Record *)record {
    
    RecordDescription *recordDescription = [record recordDescriptionWithPresentation:self.presentation];
    
    if (recordDescription.map_image.anchorOn == false) {
        
        NSInteger x = [recordDescription.x integerValue];
        NSInteger y = [recordDescription.y integerValue];
        
        CGPoint point = CGPointMake(x * self.mapView.annotationOffset, y * self.mapView.annotationOffset);
        
        KKAnnotation *annotation = [KKAnnotation annotationWithPoint:point];
        
        Content *content = [record contentForLanguage:[KKLanguage currentLanguage]];
        
        if (content) {
            annotation.title            = content.title;
            annotation.ingress          = content.desc;
            annotation.thumbnail        = [record.image thumbnailImage];
            annotation.code             = recordDescription.code;
            annotation.backgroundColor  = [UIColor colorWithHexColor:(recordDescription.color ? : @"#f59711")];
            
            annotation.record = record;
            
            [self.mapView addAnnotation:annotation
                               animated:NO];
        }
    }
}


/*
 - (void)highlightNode:(Nodes *)node
 {
 
 for (KKAnnotationView *annotationView in self.mapView.annotationViews) {
 KKAnnotation *annotation = annotationView.annotation;
 if ([annotation.tag intValue] == [node.item_id intValue]) {
 [self.mapView showCallOut: [annotationView.gestureRecognizers objectAtIndex:0]];
 }
 }
 }
 */

- (void)didTapCallout:(NSNotification *)notification {
    [self.selectedAnnotation setSelected:NO];
    [self.presentingPopover dismissPopoverAnimated:NO];
    Record *record = [notification valueForKeyPath:@"userInfo.record"];
    
    KKRecordContainerViewController *recordContainer = [KKRecordContainerViewController new];
    recordContainer.record = record;
    
    [self presentViewController:recordContainer
                       animated:YES
                     completion:NULL];
}

- (void)willSelectAnnotation:(KKAnnotation *)annotation
{
    if (self.selectedAnnotation != annotation)
    {
        [self.selectedAnnotation setSelected:NO];
        [self didDeselectAnnotation:self.selectedAnnotation];
    }
}

- (void)didSelectAnnotation:(KKAnnotation *)annotation
{
    self.selectedAnnotation = annotation;
    
    // Don't present annotations when we're not on screen.
    if(!self.active) return;
    
    UIPopoverController *popC = [[UIPopoverController alloc] initWithContentViewController:self.calloutView];
    popC.popoverBackgroundViewClass = [KKCalloutView class];
    popC.delegate = self;
    popC.passthroughViews = (NSArray*) self.mapView.annotationViews;
    
    if([self.presentingPopover isPopoverVisible]) {
        [self.presentingPopover dismissPopoverAnimated:NO];
    }
    self.presentingPopover = popC;
    
    CGFloat x = (CGFloat) round((self.mapView.zoomScale * annotation.point.x) - (45 / 2));
    CGFloat y = (CGFloat) round((self.mapView.zoomScale * annotation.point.y) - (70 / 2));
    
    CGRect frame = CGRectMake(x,y, 1, 1);
    
    [self.presentingPopover presentPopoverFromRect:frame inView:self.mapView permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
    [self.calloutView setAnnotation:annotation];
}


- (void)didDeselectAnnotation:(KKAnnotation *)annotation
{
    if([self.presentingPopover isPopoverVisible]) {
        [self.presentingPopover dismissPopoverAnimated:NO];
    }
    [self.selectedAnnotation setSelected:NO];
}

#pragma mark - Popover

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController {
    [self.selectedAnnotation setSelected:NO];
    
    return YES;
}

-(void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}

#pragma mark - Private

- (void)clearMapView {
    
    // Need for reload text in map image when changing language
    for (UIView *subview in [self.mapView.containerView subviews]) {
        if ([subview isKindOfClass:[UILabel class]] || [subview isKindOfClass:[UIButton class]]) {
            [subview removeFromSuperview];
        }
    }
    
}

- (BOOL)shouldDisableZoom {
    for (RecordDescription *recordDescription in [self.presentation records_decriptions]) {
        MapImage *mapImage = recordDescription.map_image;
        
        if (mapImage.anchorOn) {
            return true;
        }
        
    }
    
    return false;
    
}

@end
