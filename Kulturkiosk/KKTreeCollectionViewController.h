//
//  RBCollectionViewController.h
//  dasdas
//
//  Created by Rune Botten on 09.08.12.
//  Copyright (c) 2012 Rune Botten. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KKTreeCollectionViewController : UICollectionViewController

@property (weak) id delegate;
@property (strong) UINib *cellNib;


@end

@protocol RBCollectionViewControllerDelegate <NSObject>
//REF
/*
-(void) collectionViewController:(KKTreeCollectionViewController*)collectionViewController didSelectNode:(Nodes*)node atIndex:(NSInteger)index;
 */

@end
