//
//  KKEmbedSegue.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 6/11/13.
//
//

#import "KKCustomPushSegue.h"

@implementation KKCustomPushSegue

- (void)perform
{
  UIViewController *src = (UIViewController *) self.sourceViewController;
  UIViewController *dst = (UIViewController *) self.destinationViewController;

  for (UIViewController *viewController in src.childViewControllers) {
    [viewController removeFromParentViewController];
  }
  
  for (UIView *subview in src.view.subviews) {
    [subview removeFromSuperview];
  }
  [dst willMoveToParentViewController:src];
  [src addChildViewController:dst];
  [dst.view setFrame:CGRectMake(src.view.frame.origin.x,src.view.frame.origin.y, src.view.frame.size.width, src.view.frame.size.height)];
  [src.view addSubview:dst.view];
  [dst didMoveToParentViewController:src];
}


@end
