//
//  KKGalleryListViewController.m
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 04.11.13.
//
//

#import "KKGalleryListViewController.h"
#import "KKIndexCollectionViewCell.h"

#import "Record.h"
#import "Presentation.h"
#import "Content.h"

@interface KKGalleryListViewController ()

@end

@implementation KKGalleryListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //REF
    
    //  Content *content = [self.presentation contentForLanguage:[KKLanguage currentLanguage]];
    //  [self.titleLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:36]];
    //   self.titleLabel.text = content ? content.title : @"";
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.presentation.isDefault && UIAppDelegate.needToShowDefaultRecord && [self.presentation defaultRecord]) {
        NSInteger index = [self.sortedRecords indexOfObject:[self.presentation defaultRecord]];
        NSIndexPath *defaultRecordIndexPath = [NSIndexPath indexPathForItem:index inSection:0];
        [self collectionView:self.collectionView didSelectItemAtIndexPath:defaultRecordIndexPath];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIDeviceOrientation

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return [UIApplication sharedApplication].statusBarOrientation;
}

- (BOOL) shouldAutorotate {
    return  YES;
}

#pragma mark - UICollectionViewDelegate/Datasource

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    //REF
    
    Record *record = self.sortedRecords[indexPath.row];
    
    if([self.presentation.type isEqualToString:@"video_gallery"]) {
        [self performSegueWithIdentifier:@"presentVideo"
                                  sender:record];
    } else {
        [self performSegueWithIdentifier:@"presentImage"
                                  sender:record];
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    KKIndexCollectionViewCell* cell = (KKIndexCollectionViewCell*)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    if(![self.presentation.type isEqualToString:@"video_gallery"]) {
        cell.playButton.hidden=YES;
    }
    return cell;
}

#pragma mark - Private

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    id vc = segue.destinationViewController;
    if([[vc class] isSubclassOfClass: [KKPresentationViewController class]]) {
        [vc setRecordToPresent:sender];
        [vc setPresentation:self.presentation];
    }
}

@end
