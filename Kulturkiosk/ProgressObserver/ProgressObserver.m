//
//  KKProgressManager.m
//  Kulturkiosk
//
//  Created by Roman Ivchenko on 5/16/14.
//
//

#import "ProgressObserver.h"

@interface ProgressObserver ()

@property (nonatomic) CGFloat currentProgress;
@property (nonatomic) BOOL stopMarker;

@end

@implementation ProgressObserver

#pragma mark - Singleton

+ (instancetype)sharedObserver {
    static dispatch_once_t pred = 0;
    __strong static id _sharedObserver = nil;
    dispatch_once(&pred, ^{
        _sharedObserver = [[self alloc] init];
    });
    return _sharedObserver;
}

#pragma mark - Init

- (id)init {
    if (self = [super init]) {
        // By default
        _invocationTime = 0.25;
        _percentsStepInterval = 0.75;
    }
    return self;
}

#pragma mark - Public

- (void)increaseProgressWithValueInPercents:(CGFloat)percents {
    if ((100 - _currentProgress) > percents) {
        _currentProgress += percents;
    } else {
        _currentProgress = 100.0;
        _stopMarker = YES;
    }
}

- (void)observeProgress:(CGFloat)progress {
    if ([_delegate respondsToSelector:@selector(progressWithPercents:)]) {
        [_delegate progressWithPercents:progress * 100];
    }
}

#pragma mark - Private

- (void)startIdleProgress {
    if (_stopMarker) {
        return;
    }
    _currentProgress += _percentsStepInterval;
    [_delegate idleProgressWithPercents:_currentProgress
                               stopFlag:&_stopMarker];
    __block long long delayInNanoSeconds = (long long)(_invocationTime * NSEC_PER_SEC);
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInNanoSeconds);
    dispatch_after(popTime, dispatch_get_main_queue(), ^{
        [self startIdleProgress];
    });
}



@end
