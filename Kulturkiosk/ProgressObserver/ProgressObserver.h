//
//  KKProgressManager.h
//  Kulturkiosk
//
//  Created by Roman Ivchenko on 5/16/14.
//
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@protocol ProgressObserverDelegate <NSObject>

- (void)idleProgressWithPercents:(CGFloat)percents
                        stopFlag:(BOOL *)stopFlag;

- (void)progressWithPercents:(CGFloat)percents;

@end

@interface ProgressObserver : NSObject

@property (nonatomic, weak) id <ProgressObserverDelegate> delegate;

@property (nonatomic) CGFloat percentsStepInterval;
@property (nonatomic) CGFloat invocationTime;

+ (instancetype)sharedObserver;

- (void)increaseProgressWithValueInPercents:(CGFloat)percents;
- (void)observeProgress:(CGFloat)progress;
- (void)startIdleProgress;

@end
