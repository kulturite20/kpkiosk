//
//  KKLoadingViewController.h
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 14.06.13.
//
//

#import <UIKit/UIKit.h>

@interface KKLoadingViewController : UIViewController

@property (nonatomic) NSInteger totalResourcesCount;

- (void)loadedFile:(NSNotification *)notification;
//- (void)startShowingLoadingProgress:(NSNotification *)notification;

@end
