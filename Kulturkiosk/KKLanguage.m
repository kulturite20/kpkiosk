//
//  KKLanguage.m
//  Kulturkiosk
//
//  Created by Rune Botten on 16.10.12.
//
//

#import "KKLanguage.h"
#import "KKDataManager.h"

@implementation KKLanguage

+ (NSArray *)availableLanguages {
    
    NSMutableArray *languages   = [NSMutableArray array];
    KKDataManager  *dataManager = [KKDataManager sharedManager];
  
  if([dataManager hasContentForLanguage:KKLanguageTypeNorwegian])
    [languages addObject:@{@"title":@"Norsk",@"id":[NSNumber numberWithInt:KKLanguageTypeNorwegian], @"image" : [UIImage imageNamed:@"NO"]}];
  
  if([dataManager hasContentForLanguage:KKLanguageTypeSwedish])
    [languages addObject:@{@"title":@"Svenska",@"id":[NSNumber numberWithInt:KKLanguageTypeSwedish],@"image":[UIImage imageNamed:@"SE"]}];
  
  if([dataManager hasContentForLanguage:KKLanguageTypeEnglish])
    [languages addObject:@{@"title":@"English",@"id":[NSNumber numberWithInt:KKLanguageTypeEnglish],@"image":[UIImage imageNamed:@"GB"]}];
  
  if([dataManager hasContentForLanguage:KKLanguageTypeFrench])
    [languages addObject:@{@"title":@"Français",@"id":[NSNumber numberWithInt:KKLanguageTypeFrench],@"image":[UIImage imageNamed:@"FR"]}];
  
  if([dataManager hasContentForLanguage:KKLanguageTypeGerman])
    [languages addObject:@{@"title":@"Deutsch",@"id":[NSNumber numberWithInt:KKLanguageTypeGerman],@"image":[UIImage imageNamed:@"DE"]}];
  
  if([dataManager hasContentForLanguage:KKLanguageTypeSpanish])
    [languages addObject:@{@"title":@"Español",@"id":[NSNumber numberWithInt:KKLanguageTypeSpanish],@"image":[UIImage imageNamed:@"ES"]}];
    
  return languages;
}

// Return the current language. Default for this key is set in the app delegate on launch.
+(KKLanguageType) currentLanguage
{
  NSNumber *lang = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentLanguage"];
  return (KKLanguageType)[lang intValue];
}

+ (NSString *)contentForLanguage:(KKLanguageType)lang {
    NSString *language = nil;
    switch (lang) {
        case KKLanguageTypeEnglish: {
            language = @"en";
        }
            break;
        case KKLanguageTypeFrench: {
            language = @"fr";
        }
            break;
        case KKLanguageTypeGerman: {
            language = @"de";
        }
            break;
        case KKLanguageTypeSwedish: {
            language = @"sv";
        }
            break;
        case KKLanguageTypeSpanish: {
            language = @"es";
        }
            break;
        case KKLanguageTypeNorwegian: {
            language = @"no";
        }
            break;
            
        default:
            break;
    }
    
    return language;
}

+ (NSString *)currentLanguageTitle {
     NSString *language = nil;
     switch ([KKLanguage currentLanguage]) {
         case KKLanguageTypeEnglish: {
             language = @"English";
         }
             break;
         case KKLanguageTypeFrench: {
             language = @"Français";
         }
             break;
         case KKLanguageTypeGerman: {
             language = @"Deutsch";
         }
             break;
         case KKLanguageTypeSwedish: {
             language = @"Svenska";
         }
             break;
         case KKLanguageTypeSpanish: {
             language = @"Español";
         }
             break;
         case KKLanguageTypeNorwegian: {
             language = @"Norsk";
         }
             break;
         default:
             break;
     }
    
    return language;
}

+(BOOL) setLanguage:(KKLanguageType) newLanguage
{
  if([KKLanguage currentLanguage] != newLanguage) {
    [[NSUserDefaults standardUserDefaults] setInteger:newLanguage forKey:@"currentLanguage"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"languageChange" object:[NSNumber numberWithInt:newLanguage]];
    return YES;
  }
  return NO;
}

// Translate a string from the lang-%d.strings files where the int is the ID of the language (see KKLanguage above)
+(NSString*) localizedString:(NSString*) key
{
  KKLanguageType current = [KKLanguage currentLanguage];
  return [[NSBundle mainBundle] localizedStringForKey:key value:@"" table:[NSString stringWithFormat:@"lang-%d",current]];
}

+ (NSString *)descriptionForLanguageType:(KKLanguageType)type {
    NSString *languageTypes[] = {
        [KKLanguageTypeEnglish] = @"en",
        [KKLanguageTypeFrench] = @"fr",
        [KKLanguageTypeGerman] = @"gr",
        [KKLanguageTypeNorwegian] = @"no",
        [KKLanguageTypeSpanish] = @"sp",
        [KKLanguageTypeSwedish] = @"sw"
    };
    
    return languageTypes[type];
}

@end
