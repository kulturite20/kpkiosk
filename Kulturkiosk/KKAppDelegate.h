//
//  KKAppDelegate.h
//  Kulturkiosk
//
//  Created by Anders Wiik on 21.05.12.
//  Copyright (c) 2012 Nordaaker Ltd.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <RestKit/RestKit.h>
#import <RestKit/CoreData.h>
//#import <RestKit/NSManagedObjectContext+RKAdditions.h>
#import "NSManagedObject+ActiveRecord.h"
#import "UIAlertView+Blocks.h"
#import "KKTimerManager.h"
#import "Resource.h"
#import "Device.h"
#import "Content.h"
#import "Record.h"
#import "Presentation.h"
#import "Credit.h"
#import "File.h"
#import "Link.h"
#import "RecordImage.h"
#import "KKAssetsManager.h"

typedef enum {
    KKMasterAPIEnvironment,
    KKDefaultAPIEnvironment
} KKAPIEnvironment;

typedef enum {
    KKRunningMode,
    KKUpdatingMode,
    KKLaunchingMode,
    KKInitializingMode
} KKApplicationMode;

@interface KKAppDelegate : UIResponder <UIApplicationDelegate, KKTimerManagerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, assign) BOOL shouldRegisterTouch;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinatorForUpdate;

@property (nonatomic) KKAPIEnvironment apiEnvironment;
@property (nonatomic) KKApplicationMode applicationMode;
@property (assign)    BOOL hasNoSpace;
@property (assign)    BOOL needToShowDefaultRecord;

@property (assign) BOOL isInSlideShowMode;

- (void)dataBaseError:(NSString *)errorMassage;

- (NSURL *)applicationDocumentsDirectory;

- (void)stopIdleTimers;
- (void)startIdleTimers;
- (void)showUpdatingWarning;
- (void)emergencyReload;

@end
