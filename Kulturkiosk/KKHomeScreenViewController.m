//
//  KKHomeScreenViewController.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 27.05.13.
//
//

#import "KKHomeScreenViewController.h"
#import "KKNavigationViewController.h"
#import "KKHomeScreenItem.h"
#import "KKPopoverBackgroundView.h"
#import "KKTextContentViewController.h"
#import "KKVideoGalleryViewController.h"
#import "KKImageGalleryViewController.h"
#import "KKRecordContainerViewController.h"

@interface KKHomeScreenViewController ()

@property (nonatomic, strong) NSArray *presentations;
@property (weak, nonatomic) IBOutlet UIView *presentationsContainer;
@property (weak, nonatomic) IBOutlet UIImageView *background;
@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (strong, nonatomic) UIPopoverController *languageSelector;

@end

@implementation KKHomeScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    Device *device=[Device findFirst];
    self.presentations = [device.presentations sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"position" ascending:YES]]];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(languageChange)
                                                 name:@"languageChange"
                                               object:nil];
    NSArray *sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"position"
                                                               ascending:YES]];
    self.presentations = [self.presentations sortedArrayUsingDescriptors:sortDescriptors];
    for (Presentation *presentation in self.presentations) {
        KKHomeScreenItem *homeScreenItem = [[[NSBundle mainBundle] loadNibNamed:@"HomeScreenItem" owner:self options:nil] objectAtIndex:0];
        homeScreenItem.iconView.image = [presentation icon];
    
        //   [homeScreenItem.languages setFont:[UIFont fontWithName:@"MyriadPro-Italic" size:18]];
        NSArray *sortedArray = [[presentation.contents allObjects] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"language" ascending:YES]]];
        for (Content *contentItem in sortedArray) {
            if ([[contentItem title] length] > 0) {
                homeScreenItem.languages.text = [homeScreenItem.languages.text stringByAppendingFormat:@"%@\n",[contentItem title]];
            }
        }
        homeScreenItem.presentation = presentation;
        [homeScreenItem.button setTag:presentation.identifierValue];
        [homeScreenItem.button addTarget:self
                                  action:@selector(didSelectPresentation:)
                        forControlEvents:UIControlEventTouchUpInside];
        [homeScreenItem setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        [self.presentationsContainer addSubview:homeScreenItem];
    }
    
    self.background.image = [device idleScreenBackground];
    self.logo.image = [device idleScreenLogo];
}

- (void)languageChange
{
    if([self.languageSelector isPopoverVisible])
        [self.languageSelector dismissPopoverAnimated:YES];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    NSMutableDictionary *items = [NSMutableDictionary dictionary];
    int itemCount = 0;
    
    for (UIView *view in self.presentationsContainer.subviews) {
        [items setObject:view forKey:[NSString stringWithFormat:@"item%d", itemCount]];
        itemCount++;
    }
    
    NSMutableString *constraintString = [NSMutableString string];
    NSUInteger edgeSpacing = (self.presentationsContainer.frame.size.width - ((itemCount-1) * 10) - (160 * itemCount)) / 2;
    
    for (int i = 0; i < self.presentationsContainer.subviews.count; i++) {
        
        if (i == 0) {
            [constraintString appendFormat:@"|-%d-[%@(160)]", (int)edgeSpacing, [items allKeysForObject:self.presentationsContainer.subviews[i]][0]];
        } else {
            [constraintString appendFormat:@"-10-[%@(160)]", [items allKeysForObject:self.presentationsContainer.subviews[i]][0]];
        }
        
    }
    
    if(self.presentationsContainer.subviews.count) {
        [self.presentationsContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:constraintString options:NSLayoutFormatAlignAllCenterY metrics:nil views:items]];
        
        NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:items[@"item0"]
                                                                      attribute:NSLayoutAttributeCenterY
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.presentationsContainer
                                                                      attribute:NSLayoutAttributeCenterY
                                                                     multiplier:1
                                                                       constant:0];
        
        [self.presentationsContainer addConstraint:constraint];
        [self.presentationsContainer setNeedsLayout];
    }
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue isKindOfClass:[UIStoryboardPopoverSegue class]])
    {
        self.languageSelector = [(UIStoryboardPopoverSegue*)segue popoverController];
        self.languageSelector.popoverBackgroundViewClass = [KKPopoverBackgroundView class];
        
        CGFloat popoverHeight = ([[KKLanguage availableLanguages] count] > 3 ? 200 : 100);
        self.languageSelector.popoverContentSize = CGSizeMake(630, popoverHeight);
        
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didSelectPresentation:(id)sender {
    UIButton *button = (UIButton *)sender;
//    for (Presentation *presentation in self.presentations) {
//        if (presentation.identifierValue == button.tag
//            && [presentation.type isEqualToString:@"single"]) {
//            [self showSinglePresentation:presentation
//                              withSender:sender];
//            return;
//        }
//    }
    [self showInDefaultModeWithSender:button];
}

#pragma mark - Private

- (void)showSinglePresentation:(Presentation *)presentation
                    withSender:(id)sender {
    KKRecordContainerViewController *recordContainerViewController = [KKRecordContainerViewController new];
    recordContainerViewController.record = [presentation singleRecord];
    [self presentViewController:recordContainerViewController
                       animated:YES
                     completion:NULL];
}

- (void)showInDefaultModeWithSender:(id)sender {
    KKNavigationViewController *navController = (KKNavigationViewController *)self.presentingViewController;
    [navController setShouldEnterRestmode:NO];
    UIButton *button = (UIButton *)sender;
    
    for (KKHomeScreenItem *item in self.presentationsContainer.subviews) {
        if (button.tag == item.button.tag && [item.presentation singleRecord]) {
            [navController showSinglePresentationFromHomeScreen:item.presentation];
            [navController setShouldEnterRestmode:YES];
            [self dismissViewControllerAnimated:YES
                                     completion:^{
                                         [[NSNotificationCenter defaultCenter] postNotificationName:@"waitingModeDisabled"
                                                                                             object:nil
                                                                                           userInfo:nil];
                                     }];
            return;
        }
    }
    
    UIButton *targetbutton = (UIButton *)[[navController buttonContainer] viewWithTag:button.tag];
    [targetbutton sendActionsForControlEvents:UIControlEventTouchUpInside];
    [navController setShouldEnterRestmode:YES];
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 [[NSNotificationCenter defaultCenter] postNotificationName:@"waitingModeDisabled"
                                                                                     object:nil
                                                                                   userInfo:nil];
                             }];

//    KKNavigationViewController *navController = (KKNavigationViewController *)self.presentingViewController;
//    [navController setShouldEnterRestmode:NO];
//    UIButton *button = (UIButton*) sender;
//    UIButton *targetbutton = (UIButton *)[[navController buttonContainer] viewWithTag:button.tag];
//    [targetbutton sendActionsForControlEvents:UIControlEventTouchUpInside];
//    
//    [navController setShouldEnterRestmode:YES];
//    [self dismissViewControllerAnimated:YES
//                             completion:^{
//                                 [[NSNotificationCenter defaultCenter] postNotificationName:@"waitingModeDisabled"
//                                                                                     object:nil
//                                                                                   userInfo:nil];
//                             }];
}

@end
