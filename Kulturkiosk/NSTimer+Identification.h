//
//  NSTimer+Identification.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 4/17/14.
//
//

#import <Foundation/Foundation.h>

static NSString *const kTimerIdentifierKey  = @"identifier";
static NSString *const kTimerInitialTimeKey = @"initial_time";

@interface NSTimer (Identification)

- (NSString *)identifier;
- (NSInteger)initialTime;

@end
