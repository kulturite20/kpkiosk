//
//  NSMutableString + FormatHTMLTags.m
//  Kulturkiosk
//
//  Created by Dima Zinchenko on 3/18/17.
//
//

#import "NSMutableString + FormatHTMLTags.h"

@implementation NSMutableString (FormatHTMLTags)

+ (NSMutableString *)formatLineBreak:(NSString *)string {
    
    NSMutableString *mutableString = [[[string mutableCopy] stringByReplacingOccurrencesOfString:@"<br> "
                                                             withString:@"\n"] mutableCopy];
    
    return mutableString;
}

@end
