//
//  KKSlideShowPresentationCell.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 7/12/16.
//
//

#import <UIKit/UIKit.h>
#import "KKSlideShowPresentationViewController.h"

@interface KKSlideShowPresentationCell : UICollectionViewCell

@property (weak) KKSlideShowPresentationViewController *rootViewController;
@property (nonatomic) File *mediaItem;

- (void)playMedia;
- (void)stopPlayingMedia;

@end
