//
//  KKGalleryViewController.m
//  Kulturkiosk
//
//  Created by Rune Botten on 04.10.12.
//
//

#import "KKGalleryViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIColor+CustomColors.h"

@implementation KKGalleryViewController

- (NSInteger)indexOfItemView:(UIView *)view
{
  return view.tag;
}

- (void)didTap:(UITapGestureRecognizer *)tapGesture
{
  [self selectThumb:[self indexOfItemView:tapGesture.view]];
}

-(void) showInfo:(NSUInteger)index
{
  if(self.infoView) {

    NSDictionary *meta = [self.resourceDetails objectAtIndex:index];
    NSString *description = [meta valueForKey:@"description"],
    *credits = [meta valueForKey:@"credit"];
    self.infoView.descsLabel.text = description;
    self.infoView.creditsLabel.text = credits;
    
    if([description length] || [credits length])
      [self.infoView show];
    else
      [self.infoView hide];
  }
}

-(void) viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
    if (!self.thumbViews) {
        self.thumbViews = [NSMutableArray array];
    }
}

-(void) hideInfo
{
  if(self.infoView)
    [self.infoView hide];
}

-(void) selectThumb:(NSInteger) index
{
    [self showInfo:(NSUInteger)index];
    self.currentIndex = (NSUInteger)index;
    if ([self.thumbViews count] > 0) {
        [UIColor colorWithRed:223/255.0f green:187/255.0f blue:49/255.0f alpha:1];
        
        UIColor *selectedColor = [UIColor selectedColor];
        // Draw a border around the view
        UIView *container = [self.thumbViews objectAtIndex:(NSUInteger)index];
        container.layer.borderColor = selectedColor.CGColor;
        for(UIView *v in self.thumbViews) {
            if(v == container) {
                v.layer.borderWidth = 4.0f;
            } else {
                v.layer.borderWidth = 0.0f;
            }
        }
    }
}
@end
