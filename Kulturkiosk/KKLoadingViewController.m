//
//  KKLoadingViewController.m
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 14.06.13.
//
//

// Controllers
#import "KKLoadingViewController.h"

// Views
#import "YLProgressBar.h"

// Model
#import "File.h"
#import "ProgressObserver.h"

// Categories
#import "UIAlertView+Blocks.h"

@interface KKLoadingViewController ()
<
ProgressObserverDelegate
>

@end

@implementation KKLoadingViewController {
    __weak IBOutlet UIView          *_containerView;
    __weak IBOutlet UILabel         *_percentsTextLabel;
    __weak IBOutlet UILabel         *_loadingFileLabel;
    __weak IBOutlet UILabel         *_resourceNameLabel;
    __weak IBOutlet UILabel         *_pleaseWaitLabel;
    __weak IBOutlet YLProgressBar   *_progressBar;
    
                    NSInteger        _resourcesCounter;
                    CGFloat          _initialPercents;
                    BOOL             _isOldProject;
}

#pragma mark - View's lifecycle

- (void)viewDidLoad {
    CALayer *containerViewLayer = _containerView.layer;
    [containerViewLayer setMasksToBounds:YES];
    [containerViewLayer setCornerRadius:28.0];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(startShowingLoadingProgress:)
                                                 name:@"loadingStarted"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(startShowingInitializingProgress:)
                                                 name:@"initializingStarted"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadedFile:)
                                                 name:@"loadedFile"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(finishLoadingProgress)
                                                 name:@"finishLoadingProgress"
                                               object:nil];
    [ProgressObserver sharedObserver].delegate = self;
    _resourceNameLabel.hidden = YES;
    _loadingFileLabel.hidden  = YES;
    
    // Fonts setup
    _pleaseWaitLabel.font   = [UIFont fontWithName:@"MyriadPro-Cond" size:28];
    _resourceNameLabel.font = [UIFont fontWithName:@"MyriadPro-Regular" size:14];
    _loadingFileLabel.font  = [UIFont fontWithName:@"MyriadPro-Regular" size:14];
    [self setupProgressBar];
}

#pragma mark - Public

- (void)startShowingLoadingProgress:(NSNotification *)notification {
    _totalResourcesCount = [notification.userInfo[@"assetsCount"] integerValue];
}

- (void)loadedFile:(NSNotification*)notification {
    _resourceNameLabel.hidden = NO;
    _loadingFileLabel.hidden  = NO;

    CGFloat percents = (float)(100 - _initialPercents)/_totalResourcesCount * _resourcesCounter + _initialPercents;
    if (!percents) {
        percents = 0.0f;
    }
    _resourceNameLabel.text  = [NSString stringWithFormat:@"%@", notification.userInfo[@"fileName"]];
    if (percents >= 0) {
        _percentsTextLabel.text = [[NSString stringWithFormat:@"%.0f", percents] stringByAppendingString:@" %"];
    } else {
        _percentsTextLabel.text = [[NSString stringWithFormat:@"???"] stringByAppendingString:@" %"];
    }
    [_progressBar setProgress:percents >= 0 ? (float)percents/100 : 0.0f
                     animated:YES];
    _resourcesCounter++;
}


- (void)startShowingInitializingProgress:(NSNotification *)notification {
    [[ProgressObserver sharedObserver] startIdleProgressWithStartingValue:0.0
                                                              endingValue:11.0
                                                                partially:YES];
}

- (void)finishLoadingProgress {
    _isOldProject = YES;
    [ProgressObserver sharedObserver].idlePercentsStepInterval = 2.0f;
    [ProgressObserver sharedObserver].idleInvocationTime = 0.08f;
    [[ProgressObserver sharedObserver] startIdleProgressWithStartingValue:_initialPercents
                                                              endingValue:130.0f
                                                                partially:YES];
}

#pragma mark - ProgressObserverDelegate

- (void)idleProgressDidFinishWithValue:(CGFloat)value {
    if (_isOldProject) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"launchingFinished"
                                                            object:nil];
    }
}

- (void)progress:(Progress *)progress didChangeValue:(CGFloat)progressValue {
    //NSLog(@"Real progresso.. %.2f", progressValue);
}

- (void)summaryProgressDidChangeValue:(CGFloat)summaryProgressValue {
    if (!summaryProgressValue) {
        summaryProgressValue = 0.0f;
    }
    summaryProgressValue = summaryProgressValue > 100.0f ? 100.0f : summaryProgressValue;
    summaryProgressValue = summaryProgressValue < 0.0f ? 0.0f : summaryProgressValue;

    dispatch_async(dispatch_get_main_queue(), ^{
        [_progressBar setProgress:summaryProgressValue/100
                         animated:YES];
        if (summaryProgressValue >= 0) {
            _percentsTextLabel.text = [[NSString stringWithFormat:@"%.f", summaryProgressValue] stringByAppendingString:@" %"];
        } else {
            _percentsTextLabel.text = [[NSString stringWithFormat:@"???"] stringByAppendingString:@" %"];
        }
        
    });
    
    _initialPercents = summaryProgressValue;
}

#pragma mark - Private

- (void)setupProgressBar {
    NSArray *tintColors = @[[UIColor colorWithRed:50/255.0f green:149/255.0f blue:10/255.0f alpha:1.0f],
                            [UIColor colorWithRed:130/255.0f green:187/255.0f blue:29/255.0f alpha:1.0f]];
    
    _progressBar.progressTintColors       = tintColors;
    _progressBar.trackTintColor           = [UIColor blackColor];
    _progressBar.stripesOrientation       = YLProgressBarStripesOrientationRight;
    _progressBar.indicatorTextDisplayMode = YLProgressBarIndicatorTextDisplayModeNone;
}

#pragma mark - dealloc

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
