//
//  NSMutableString + FormatHTMLTags.h
//  Kulturkiosk
//
//  Created by Dima Zinchenko on 3/18/17.
//
//

#import <Foundation/Foundation.h>

@interface NSMutableString (FormatHTMLTags)

+ (NSMutableString *)formatLineBreak:(NSString *)string;

@end
