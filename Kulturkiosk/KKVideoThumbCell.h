//
//  KKVideoThumbCell.h
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 11.11.13.
//
//

#import <UIKit/UIKit.h>

@interface KKVideoThumbCell : UICollectionViewCell

@property (nonatomic) Resource *resource;


- (void)setThumbnail: (UIImage*)thumb;

@end
