//
//  RBCollectionViewController.m
//  dasdas
//
//  Created by Rune Botten on 09.08.12.
//  Copyright (c) 2012 Rune Botten. All rights reserved.
//

#import "KKTreeCollectionViewController.h"
#import "KKSelectableNodeCell.h"
#import "NSString+HTML.h"

#import <QuartzCore/QuartzCore.h>

@interface KKTreeCollectionViewController () <UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) NSMutableDictionary *imageCache;
@end

@implementation KKTreeCollectionViewController

@synthesize cellNib, delegate;


-(UICollectionView*) collectionView
{
  return (UICollectionView*) self.view;
}

-(NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
  return 1;
}

-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    //REF
    /*
  return (NSInteger)[self.nodes count];
     */
    return 1;
}

-(UICollectionViewCell*) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
  KKSelectableNodeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FolderCell" forIndexPath:indexPath];
  //REF
    /*
  Nodes *node = [self.nodes objectAtIndex:(NSUInteger)indexPath.row];
  Content *content = [node contentForLanguage:[KKLanguage currentLanguage]];
  
  cell.title.text = [[content.title stringByConvertingHTMLToPlainText] uppercaseString];
  
  UIImage *img = [node mainImage];
  if(!img) {
    NSArray *array = [node imagesWithType:KKFormatSmall];
    if([array count]) {
      File *imgFile = array[0];
      img = [UIImage imageWithContentsOfFile:imgFile.localUrl];
      [self.imageCache setObject:img forKey:node.identifier];
    }
  }
  
  cell.image.image = img;
  */
  return cell;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  
   
  // Set which NIB will be used to create cells
  self.cellNib =[UINib nibWithNibName:@"FolderCell" bundle:nil];
  [[self collectionView] registerNib:self.cellNib forCellWithReuseIdentifier:@"FolderCell"];
  
  self.imageCache = [NSMutableDictionary dictionaryWithCapacity:30];
}

- (void) dealloc
{
  self.imageCache = nil;
  self.cellNib = nil;
}

-(void) didReceiveMemoryWarning
{
  self.imageCache = nil;
  [super didReceiveMemoryWarning];
}

-(void) scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
  NSLog(@"Scrolling ended");
  [self notifyDelegate];
}

-(void) notifyDelegate
{
    //REF
    /*
  if(self.delegate && [self.delegate respondsToSelector:@selector(collectionViewController:didSelectNode:atIndex:)]) {
    [self.delegate collectionViewController:self didSelectNode:self.selectedNode atIndex:(NSInteger)[self.nodes indexOfObject:self.selectedNode]];
  }
     */
}

// Pass this info along to our delegate, if we have one
-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //REF
    /*
  self.selectedNode = [self.nodes objectAtIndex:indexPath.row];
     */
  
  // Scroll the item if its not fully visible
  if([self indexPathFullyVisible:indexPath]) {
    [self notifyDelegate];
  } else {
    [collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
  }
}

-(BOOL) indexPathFullyVisible:(NSIndexPath*) indexPath
{
  UICollectionViewLayoutAttributes *attr = [self.collectionView.collectionViewLayout layoutAttributesForItemAtIndexPath:indexPath];
  
  return attr.frame.origin.x >= self.collectionView.contentOffset.x && attr.frame.origin.x + attr.frame.size.width <= self.collectionView.contentOffset.x + self.collectionView.frame.size.width;
}



- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
  
  return CGSizeMake(300, 244);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
  return UIEdgeInsetsMake(55, 10, 55, 10);
}


@end
