//
//  KKImageVideoGalleryViewController.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 1/29/16.
//
//

#import "Resource.h"
#import "File.h"
#import "Credit.h"
#import "KKDataManager.h"
#import "ImageScrollView.h"

// Controllers
#import "KKImageVideoGalleryViewController.h"
#import "KKMediaItemViewController.h"
#import "KKImageContainerViewController.h"
#import "UIView+Autolayout.h"

@interface KKImageVideoGalleryViewController ()
<
UIGestureRecognizerDelegate,
UIPageViewControllerDataSource,
UIPageViewControllerDelegate
>

@end

@implementation KKImageVideoGalleryViewController {
    
    __weak IBOutlet UILabel *_titleLabel;
    __weak IBOutlet UIButton *_closeGalleryButton;
    
    __weak IBOutlet UIView       *_mediaContainer;
    
    __weak IBOutlet UIView       *_topBarContainer;
    __weak IBOutlet UIScrollView *_bottomBarThumbNailContainer;
    
    UIPageViewController *_pageViewController;
    
    NSInteger _currentMediaItemIndex;
    UIImage *_thumb;
    UIImageView *_thumbNail;
    
}

#pragma mark - UIViewController Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showVideoInFullscreen:)
                                                 name:@"showGalleryVideoFullscreen"
                                               object:nil];
    
    [self configureMediaContainer];
    
    [self configureMediaItemDetailsData];
    
    [self setupCreditView];
    
    [self configureThumbNails];

    [self.spinner stopAnimating];
    
    [self selectThumb:self.startGalleryWithIndex];
    
}

#pragma mark UIPageViewControllerDelegate

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed {
    KKImageContainerViewController *currentView = [pageViewController.viewControllers objectAtIndex:0];
    
    [self selectThumb:currentView.pageIndex];
    
    [_titleLabel setText:self.resourceDetails[currentView.pageIndex][@"title"]];
    
    [self configureTopBarForNewPage];
}

#pragma mark - UIPageViewControllerDataSource

- (KKImageContainerViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.resources count] == 0) || (index >= [self.resources count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    KKImageContainerViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"imageVideoContainer"];
    [pageContentViewController mediaItem:self.resources[index]];
    pageContentViewController.pageIndex = index;
    pageContentViewController.parentRect = _mediaContainer.frame;
    
    __weak typeof(self) wSelf = self;
    pageContentViewController.willhideScrollImageBars = ^() {
        [wSelf hideTopAndBottomBars:nil];
    };

    return pageContentViewController;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((KKImageContainerViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index - 1];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((KKImageContainerViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index + 1];
}

#pragma mark - Private methods

- (void)configureMediaContainer {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideTopAndBottomBars:)];
    tap.delegate = self;
    tap.numberOfTapsRequired = 1;
    [_mediaContainer addGestureRecognizer:tap];
    
    _pageViewController = [[UIPageViewController alloc]
                           initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll
                           navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
                           options:nil];
    
    _pageViewController.dataSource = self;
    _pageViewController.delegate = self;
    
    KKImageContainerViewController *startingViewController = [self viewControllerAtIndex:self.startGalleryWithIndex];
    NSArray *viewControllers = @[startingViewController];
    startingViewController.imageVideoGalleryMode = YES;
    
    [_pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];

    [_pageViewController willMoveToParentViewController:self];
    [self addChildViewController:_pageViewController];
    
    _pageViewController.view.frame = _mediaContainer.frame;
    
    NSLog(@"FRAME: %@", NSStringFromCGRect(_pageViewController.view.frame));
    
    [_mediaContainer addSubview:_pageViewController.view];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        NSLog(@"FRAME2: %@", NSStringFromCGRect(_mediaContainer.frame));
        
//        _pageViewController.view.frame = CGRectMake(0, 0, _mediaContainer.frame.size.width / 2, _mediaContainer.frame.size.height / 2);
        
//        [_pageViewController.view addToParentAndCentered:_mediaContainer];
    });
}

- (void)configureMediaItemDetailsData {
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    for (File *file in self.resources) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        
        NSString *descString = @"", *titleString = @"";
        
        KKLanguageType current = [KKLanguage currentLanguage];
        Content *content = [file contentForLanguage:current];
        
        NSMutableString *allCreditsString = [@"" mutableCopy];
        for (Credit *credit in file.credits) {
            if (credit.name.length > 0) {
                [allCreditsString appendFormat:@"%@: %@\n", [[KKDataManager sharedManager] localizedCreditTypeForTitle:credit.credit_type], credit.name];
            }
        }
        
        descString = content.desc;
        titleString = content.title;
        if ([allCreditsString length] > 0) {
            [dict setObject:allCreditsString
                     forKey:@"credit"];
        }
        if ([descString length] > 0) {
            [dict setObject:descString
                     forKey:@"description"];
        }
        if ([titleString length] > 0) {
            [dict setObject:titleString
                     forKey:@"title"];
        }
        [tempArray addObject:dict];
        
    }
    
    if([tempArray count] < 1) {
        return;
    }
    
    self.resourceDetails = [NSArray arrayWithArray:tempArray];
    
    _currentMediaItemIndex = self.startGalleryWithIndex;
    
    [_titleLabel setText:self.resourceDetails[_currentMediaItemIndex][@"title"]];
    
    [self configureTopBarForNewPage];
}

- (void)configureThumbNails {
    CGSize thumbSize = CGSizeMake(106, 80);
    NSUInteger offset = 10, horizontalMargin = 10, verticalMargin = 10;
    
    if (!self.thumbViews) {
        self.thumbViews = [NSMutableArray new];
    }
    for(NSUInteger i = 0; i < [self.resources count]; i++) {
        
        File *video = [self.resources objectAtIndex:i];
        
        if ([video.type isEqualToString:@"image"]) {
            NSString *thumbImagePath = [video pathToThumbnailItem];
            NSLog(@"%@",thumbImagePath);
            
            _thumb = [UIImage imageWithContentsOfFile:thumbImagePath];
            
            _thumbNail = [[UIImageView alloc] initWithImage:_thumb];
            
        } else {
            
            _thumb = [video thumbnailWithSize:&thumbSize];
            
            _thumbNail = [[UIImageView alloc] initWithImage:_thumb];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMidX(_thumbNail.frame) - 15, CGRectGetMidY(_thumbNail.frame) - 5, 30, 30)];
            imageView.image = [UIImage imageNamed:@"video_play_button.png"];
            
            [_thumbNail addSubview:imageView];
        }
        
        CGRect thumbFrame = CGRectMake(offset, verticalMargin/2, thumbSize.width, thumbSize.height);
        [_thumbNail setFrame:thumbFrame];
        offset += (thumbSize.width + horizontalMargin);
        
        [_thumbNail setContentMode:UIViewContentModeScaleAspectFill];
        [_thumbNail setAutoresizesSubviews:YES];
        [_thumbNail setClipsToBounds:YES];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTap:)];
        _thumbNail.tag = i;
        [_thumbNail addGestureRecognizer:tapGesture];
        [_thumbNail setUserInteractionEnabled:YES];
        
        [_bottomBarThumbNailContainer addSubview:_thumbNail];
        [self.thumbViews addObject:_thumbNail];
    }
    
    // Set the scroll view content size to fit all added thumbs (accumulated 'offset' variable), plus their height + vertical margin
    [_bottomBarThumbNailContainer setContentSize:CGSizeMake(offset, thumbSize.height + verticalMargin)];
}

- (void) setupCreditView {
    NSArray *array = [[UINib nibWithNibName:@"KKResourceCreditView" bundle:nil] instantiateWithOwner:self options:nil];
    for(id obj in array) {
        if([obj class] == [KKResourceCreditView class]) {
            self.infoView = (KKResourceCreditView*)obj;
        }
    }
    
    if(self.infoView) {
        //CGRect creditFrame = self.infoView.frame;
        CGRect creditFrame = CGRectMake(CGRectGetMinX(self.infoView.frame),
                             CGRectGetHeight(_topBarContainer.frame),
                             CGRectGetWidth(self.infoView.frame),
                             CGRectGetHeight(self.infoView.frame));
        // Hidden initially
        creditFrame.origin.x = -creditFrame.size.width;
        self.infoView.frame = creditFrame;
        [self.view addSubview:self.infoView];
    }
}

- (void)hideTopAndBottomBars:(UIGestureRecognizer *)tap {
    if (!_bottomBarThumbNailContainer.hidden) {
        _topBarContainer.hidden = YES;
        _bottomBarThumbNailContainer.hidden = YES;
        _closeGalleryButton.hidden = YES;
        [self.infoView hide];
    } else if (tap) {
        if (_titleLabel.text) {
            _topBarContainer.hidden = NO;
        } else {
            _topBarContainer.hidden = NO;
        }
        
        if (self.infoView.descsLabel.text || self.infoView.creditsLabel.text) {
            [self.infoView show];
        }
        
        _bottomBarThumbNailContainer.hidden = NO;
        _closeGalleryButton.hidden = NO;
    }
}

- (void)configureTopBarForNewPage {
    if (_bottomBarThumbNailContainer.hidden) {
        [self hideInfo];
    } else {
        if (_titleLabel.text || self.infoView.descsLabel.text) {
            _topBarContainer.hidden = NO;
            //[_closeGalleryButton setBackgroundImage:[UIImage imageNamed:@"header_close_icon.png"] forState:UIControlStateNormal];
        } else {
            _topBarContainer.hidden = NO;
            //[_closeGalleryButton setBackgroundImage:[UIImage imageNamed:@"close_icon_trans.png"] forState:UIControlStateNormal];
        }
        if (self.infoView.descsLabel.text || self.infoView.creditsLabel.text) {
            [self.infoView show];
        }
    }
}

- (IBAction)goBack:(id)sender {
    [self.view.window makeKeyAndVisible];
    [self setResources:nil];
    [UIAppDelegate startIdleTimers];
    UIAppDelegate.shouldRegisterTouch = YES;
    [self dismissViewControllerAnimated:YES
                             completion:NULL];
}

//- (IBAction)pauseButtonClicked:(id)sender {
//    [self startTimer];
//    [self.avPlayer pause];
//    [self syncPlayPauseButtons];
//}

- (void)didTap:(UITapGestureRecognizer *)tapGesture {
    [super didTap:tapGesture];
    
    [UIView animateWithDuration:0.3 animations:^{
        [_bottomBarThumbNailContainer scrollRectToVisible:tapGesture.view.frame animated:NO];
    } completion:^(BOOL finished) {
        NSInteger index = [self indexOfItemView:tapGesture.view];
        
        _currentMediaItemIndex = index;
        
        KKImageContainerViewController *manualSelectedViewController = [self viewControllerAtIndex:index];
        NSArray *viewControllers = @[manualSelectedViewController];
        
        __weak typeof(self) wSelf = self;
        __weak typeof(_titleLabel) wTitleLabel = _titleLabel;
        __weak typeof(_topBarContainer) wTopBarContainer = _topBarContainer;
        //__weak typeof(_closeGalleryButton) wCloseGalleryButton = _closeGalleryButton;
        
        [_pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:^(BOOL finished) {
            [wTitleLabel setText:wSelf.resourceDetails[index][@"title"]];
            
            if ([wSelf.infoView isHidden] && !wTitleLabel.text) {
                wTopBarContainer.hidden = NO;
                //wTopBarContainer.hidden = YES;
                //[wCloseGalleryButton setBackgroundImage:[UIImage imageNamed:@"close_icon_trans.png"] forState:UIControlStateNormal];
            } else if (wTitleLabel.text) {
                wTopBarContainer.hidden = NO;
                //[wCloseGalleryButton setBackgroundImage:[UIImage imageNamed:@"header_close_icon.png"] forState:UIControlStateNormal];
            }
        }];
    }];
}

- (void)showVideoInFullscreen:(NSNotification *)notification {
    
    File *videoFile = notification.userInfo[@"videoFile"];
    KKMediaItemViewController *videoController = [KKMediaItemViewController new];
    
    videoController.mediaItem = videoFile;
    videoController.isInFullscreenMode = YES;
    videoController.isInVideoGalleryMode = YES;
    videoController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:videoController
                       animated:YES
                     completion:NULL];
}


#pragma mark - Dealloc

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    UIAppDelegate.shouldRegisterTouch = YES;
}

@end
