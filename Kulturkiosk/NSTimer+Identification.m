//
//  NSTimer+Identification.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 4/17/14.
//
//

#import "NSTimer+Identification.h"

@implementation NSTimer (Identification)

- (NSString *)identifier {
    return self.userInfo[kTimerIdentifierKey];
}

- (NSInteger)initialTime {
    return [self.userInfo[kTimerInitialTimeKey] integerValue];
}

@end
