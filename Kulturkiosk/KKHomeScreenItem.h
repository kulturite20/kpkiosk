//
//  KKHomeScreenItem.h
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 6/26/13.
//
//

#import <UIKit/UIKit.h>

@interface KKHomeScreenItem : UIView
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *languages;
@property (weak, nonatomic) IBOutlet UIButton *button;

@property (strong, nonatomic) Presentation *presentation;

@end
