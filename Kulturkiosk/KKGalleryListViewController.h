//
//  KKGalleryListViewController.h
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 04.11.13.
//
//

#import "KKIndexViewController.h"

@interface KKGalleryListViewController : KKIndexViewController

@property (nonatomic) IBOutlet UILabel *titleLabel;

@end
