 //
//  KKCalloutViewController.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 6/14/13.
//
//

#import "KKCalloutViewController.h"
#import "KKAnnotation.h"

@interface KKCalloutViewController ()

@property (nonatomic, strong) KKAnnotation *annotation;
@property (nonatomic, strong) IBOutlet UIImageView *thumbnailView;
@property (nonatomic, strong) IBOutlet UILabel     *ingressLabel;
@property (nonatomic, strong) IBOutlet UILabel     *titleLabel;
@property (nonatomic, assign) CGPoint      position;
@property (weak, nonatomic) IBOutlet UIButton *timelineButton;

@end

@implementation KKCalloutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
 [super viewDidLoad];
  [self.titleLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:24]];
  [self.ingressLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:16]];
  
  UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                               action:@selector(didTapCallout)];
    self.thumbnailView.userInteractionEnabled = YES;
  [self.thumbnailView addGestureRecognizer:tapGesture];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setAnnotation:(KKAnnotation *)annotation
{
  if (_annotation != annotation) {
    
    _annotation = annotation;
    self.titleLabel.text    = @"";
    self.ingressLabel.text  = @"";
    self.thumbnailView.image = nil;
    self.position              = CGPointZero;
//    self.timelineButton.hidden = YES;
      
    self.position = annotation.point;
        self.titleLabel.text = annotation.title;
    
    if (annotation.ingress) {
      self.ingressLabel.text = annotation.ingress;
    }
    
    if (annotation.thumbnail) {
      self.thumbnailView.image = annotation.thumbnail;
      self.thumbnailView.hidden = NO;
    } else {
      self.thumbnailView.hidden = YES;
    }
      
  /*  NSArray *records = [Record objectsWithPredicate:[NSPredicate predicateWithFormat:@"node.item_id == %@ AND presentation != nil",self.annotation.tag]];
    for (Record *record in records) {
      if ([record.presentation.type isEqualToString:@"timeline"]){
        self.timelineButton.hidden = NO;
        self.timelineButton.tag = record.presentation.identifierValue;
        self.timelineButton.superview.tag = [record.node.item_id integerValue];
      }
    }
   */
  }
}

- (IBAction)highlightContentInPresentation:(id)sender
{
  UIButton *button = (UIButton*) sender;
  NSDictionary *userinfo = @{@"presentationId":[NSNumber numberWithInt:button.tag], @"nodeId":[NSNumber numberWithInt:button.superview.tag]};
  
  [[NSNotificationCenter defaultCenter] postNotificationName:@"highlightContentInPresentation" object:nil userInfo:userinfo];
  
}
- (IBAction)showContent:(id)sender {
  [[NSNotificationCenter defaultCenter] postNotificationName:@"didTapCallout"
                                                      object:nil
                                                    userInfo:@{@"record": self.annotation.record}];
}

- (void)didTapCallout {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"didTapCallout"
                                                        object:nil
                                                      userInfo:@{@"record": self.annotation.record}];
}

@end
