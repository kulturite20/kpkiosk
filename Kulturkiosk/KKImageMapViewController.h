//
//  KKImageMapViewController.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 2/28/17.
//
//

#import "KKPresentationViewController.h"

@interface KKImageMapViewController : KKPresentationViewController

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
- (void)setupRecordLines;

@end
