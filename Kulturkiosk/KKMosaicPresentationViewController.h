//
//  KKMosaicPresentationViewController.h
//  Kulturkiosk
//
//  Created by Vadim Osovets Zinchenko on 2/10/17.
//
//

#import <UIKit/UIKit.h>
#import "KKPresentationViewController.h"
#import "MosaicElementSize+CoreDataClass.h"
#import "MosaicTemplate+CoreDataClass.h"
#import "MosaicItem+CoreDataClass.h"
#import "KKImageMapViewController.h"
#import "KKMapViewController.h"

#import "KKNavigationViewController.h"

@interface KKMosaicPresentationViewController : KKPresentationViewController

@property (strong, nonatomic) KKImageMapViewController *imageMapViewController;
@property (strong, nonatomic) KKMapViewController *mapViewController;

@property (strong, nonatomic) MosaicPresentation *mosaicPresentation;
@property (strong, nonatomic) NSMutableArray *imageArray;
@property (strong, nonatomic) NSArray<MosaicItem *> *mosaicItems;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@property (weak, nonatomic) KKNavigationViewController *navigationKKController;

@end
