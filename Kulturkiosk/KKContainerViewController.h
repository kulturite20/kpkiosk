//
//  KKContainerViewController.h
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 13.06.13.
//
//

#import <UIKit/UIKit.h>
#import "KKPresentationViewController.h"

@interface KKContainerViewController : UIViewController

@property (nonatomic, strong) Presentation *presentation;

@end
