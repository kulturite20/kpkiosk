//
//  KKImagePresentationViewController.m
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 21.11.13.
//
//

#import "KKImagePresentationViewController.h"
#import "KKDataManager.h"
#import "Presentation.h"
#import "KKImageCell.h"
#import "Resource.h"
#import "Record.h"
#import "Block.h"
#import "Page.h"
#import "File.h"

@interface KKImagePresentationViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *imagetitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *imageDescriptionLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *imageThumbs;
@property (strong,nonatomic) NSArray *images;
@property (strong,nonatomic) UIWindow *externalWindow;

@end

@implementation KKImagePresentationViewController {
    CGRect _nonFullScreenImageContainerRect;
    
    BOOL _isFullscreenMode;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    _nonFullScreenImageContainerRect = _imageContainer.frame;
    
    [self.titleLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:36]];
    [self.descriptionLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:20]];
    self.descriptionLabel.numberOfLines = 0;
    [self.imagetitleLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:24]];
    [self.imageDescriptionLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:20]];
    self.imageDescriptionLabel.numberOfLines = 0;
    
    [self gatherAllImages];
    
    if([self.images count]) {
        [self loadImage: [self.images firstObject]];
    }
    
    Content *content = [self.recordToPresent contentForLanguage:[KKLanguage currentLanguage]];
    self.titleLabel.text = content.title;
    self.descriptionLabel.text = content.desc;
    [self.descriptionLabel sizeToFit];
}

- (void)loadImage:(File *)imageFile {
    
    self.imagetitleLabel.text = [imageFile contentForLanguage:[KKLanguage currentLanguage]].title;
    
    NSString *imageDescription = [imageFile contentForLanguage:[KKLanguage currentLanguage]].desc;
    NSMutableString *allCreditsString = [@"" mutableCopy];
    for (Credit *credit in imageFile.credits) {
        if (credit.name.length > 0) {
            [allCreditsString appendFormat:@"%@: %@\n", [[KKDataManager sharedManager] localizedCreditTypeForTitle:credit.credit_type], credit.name];
        }
    }
    NSString *finalDescription = [imageDescription stringByAppendingString:[NSString stringWithFormat:@"\n\n%@", allCreditsString]];
    
    self.imageDescriptionLabel.text = finalDescription;

    for (UIView *view  in self.imageContainer.subviews) {
        [view removeFromSuperview];
    }
    
    UIImage *resourceImage = [UIImage imageWithContentsOfFile:[imageFile pathToMainItem]];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:resourceImage];
    imgView.userInteractionEnabled = YES;
    imgView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    CGRect frame = self.imageContainer.bounds;

    imgView.frame = frame;
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    //[self.imageContainer addSubview:imgView];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(tapRecognized:)];
    [imgView addGestureRecognizer:tapRecognizer];
    
    if([[UIScreen screens] count] > 1) {
        UIScreen *externalDisplay = [[UIScreen screens] objectAtIndex:1];
        CGRect bounds = externalDisplay.bounds;
        self.externalWindow = [[UIWindow alloc] initWithFrame:bounds];
        self.externalWindow.screen = externalDisplay;
        self.externalWindow.hidden = NO;
        
        UIImageView *externalImg = [[UIImageView alloc] initWithImage:resourceImage];
        
        externalImg.frame = self.externalWindow.frame;
        externalImg.contentMode = UIViewContentModeScaleAspectFit;
        [self.externalWindow addSubview: externalImg];
    }

    //  [self.imageDescriptionLabel sizeToFit];
}

- (IBAction)dismissController:(id)sender {
    UIAppDelegate.needToShowDefaultRecord = NO;
    self.externalWindow.hidden = YES;
    
    [self dismissViewControllerAnimated:NO
                             completion:NULL];
}

#pragma mark - UIGestureRecognizer 

- (void)tapRecognized:(UITapGestureRecognizer *)tapRecognizer {
    _isFullscreenMode = !_isFullscreenMode;
    
    CGRect finalRect = _nonFullScreenImageContainerRect;
    
    if (_isFullscreenMode) {
        CGRect fullScreenRect = [UIApplication sharedApplication].keyWindow.bounds;
        
        finalRect = fullScreenRect;
    }
    
    // TODO: Const.
    [UIView animateWithDuration:0.25
                     animations:^{
                         _imageContainer.frame = finalRect;
                     } completion:NULL];
}

#pragma mark - Private

- (void)gatherAllImages {
    NSMutableArray *images = [NSMutableArray new];
    Content *content = [self.recordToPresent contentForLanguage:[KKLanguage currentLanguage]];
    for (Page *page in content.pages) {
        for (VideoBlock *block in [page videoBlocks]) {
            for (File *file in [(VideoBlock *)block files]) {
                if ([file.type isEqualToString:@"image"]) {
                    [images addObject:file];
                }
            }
        }
    }
    
    NSSortDescriptor *identifierSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"identifier"
                                                                               ascending:YES];
    self.images = [images sortedArrayUsingDescriptors:@[identifierSortDescriptor]];
}

#pragma mark - Collection view for bottom

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.images.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    KKImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"thumbCell"
                                                                  forIndexPath:indexPath];
    
    [cell updateWithImageFile:self.images[indexPath.row]];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self loadImage:[self.images objectAtIndex:indexPath.row]];
}

@end
