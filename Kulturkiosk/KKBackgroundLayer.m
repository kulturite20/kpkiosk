//
//  KKBackgroundLayer.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 6/12/13.
//
//

#import "KKBackgroundLayer.h"

@implementation KKBackgroundLayer


+ (CAGradientLayer *)blackGradient
{
  UIColor *colorOne = [UIColor colorWithWhite:0.25f alpha:1.0];
  UIColor *colorTwo = [UIColor colorWithWhite:0.01f alpha:1.0];
  
  NSArray *colors =  @[(id)colorOne.CGColor,(id)colorTwo.CGColor];
  
  NSNumber *stopOne = [NSNumber numberWithFloat:0.0];
  NSNumber *stopTwo = [NSNumber numberWithFloat:1.0];
  
  NSArray *locations =@[stopOne, stopTwo];
  CAGradientLayer *headerLayer = [CAGradientLayer layer];
  headerLayer.colors = colors;
  headerLayer.locations = locations;
  
  return headerLayer;
}
@end
