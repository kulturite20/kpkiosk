//
//  KKSlideShowViewController.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 7/11/16.
//
//

#import "KKSlideShowPresentationViewController.h"

#import "KKSlideShowPresentationCell.h"

#import "Page.h"
#import "VideoBlock.h"

@implementation KKSlideShowPresentationViewController {
    
    __weak IBOutlet UICollectionView *_collectionView;
    
    NSArray *_records;
    NSMutableArray *_medias;
    
    NSInteger _currentIndex;
    
    NSTimer *_slideTimer;
    
    BOOL _isContainsVideoRecords;
    
    __weak IBOutlet UIButton *_leftSecretButton;
    __weak IBOutlet UIButton *_rightSecretButton;
    
}

#pragma mark - Lifecycle.

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [UIAppDelegate stopIdleTimers];
    UIAppDelegate.shouldRegisterTouch = NO;
    
    _isContainsVideoRecords = false;
    
    // Observe Media Player
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(videoReachedEndNotification:)
                                                 name:@"MediaPlayerMediaItemDidReachEnd"
                                               object:nil];
    
    [self prepareContent];
    
    [self configureSecretButtons];
    [self configureSecretTapProtection];
    
    [self startTimer];
    
    // Disable user interaction for 'observe-only' presentation type.
    _collectionView.userInteractionEnabled = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(emergencyReload)
                                                 name:@"generatingThumbnailError"
                                               object:nil];
    
}

- (void)emergencyReload {
    [_slideTimer invalidate];
    
    _medias = nil;
    _records = nil;
    
    KKAppDelegate *appDelegate = (KKAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.isInSlideShowMode = YES;
    
    [appDelegate emergencyReload];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Private

- (void)prepareContent {
    _records = [self.presentation records];
    for (Record *record in _records) {
        [self fetchAllMediaByRecord:record];
    }
}

- (void)fetchAllMediaByRecord:(Record *)record {
    
    NSMutableArray *mediaArray = [NSMutableArray new];
    
    NSSortDescriptor *positionDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"position" ascending:YES];
    
    Content *content = [record contentForLanguage:[KKLanguage currentLanguage]];
    
    NSArray *pagesArray = [content.pages sortedArrayUsingDescriptors:@[positionDescriptor]];
    
    for (Page *page in pagesArray) {
        
        NSArray *pageBlocksArray = [[page videoBlocks] sortedArrayUsingDescriptors:@[positionDescriptor]];
        
        for (VideoBlock *block in pageBlocksArray) {
            
            NSMutableArray *blockMediaArray = [NSMutableArray array];
            
            for (File *file in [(VideoBlock *)block files]) {
                
                if ([file.type isEqualToString:@"video"] || [file.type isEqualToString:@"image"]) {
                    
                    //Check for a video containing.
                    if ([file.type isEqualToString:@"video"]) {
                        _isContainsVideoRecords = true;
                    }
                    
                    NSLog(@"%@",file.pathToOriginalItem);
                    [blockMediaArray addObject:file];
                }
            }
            
            NSArray *sortedArray = [blockMediaArray sortedArrayUsingDescriptors:@[positionDescriptor]];
            
            for (File *media in sortedArray) {
                [mediaArray addObject:media];
            }
        }
    }
    
    if (_medias == nil) {
        _medias = [NSMutableArray array];
    }
    
    [_medias addObjectsFromArray:mediaArray];
    
    [_collectionView reloadData];
    
    
        NSLog(@"Record ID: %@", record.identifier);
        for (File *media in _medias) {
            NSLog(@"---------------------------------");
            NSLog(@"Media name: %@", media.name);
            NSLog(@"Media identifier: %@", media.identifier);
            NSLog(@"Media u_id: %@", media.u_id);
            NSLog(@"Media position: %@", media.position);
            NSLog(@"---------------------------------");
        }
    
}

- (void)videoReachedEndNotification:(NSNotification *)notification {
    [self scrollToNextCell:notification];
}

- (void)closeSlideShowController {
    UIAppDelegate.needToShowDefaultRecord = NO;
    [UIAppDelegate startIdleTimers];
    UIAppDelegate.shouldRegisterTouch = YES;
    
    _medias = nil;
    [_collectionView reloadData];
    [self stopTimer];
    
    KKAppDelegate *appDelegate = (KKAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.isInSlideShowMode = YES;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SlideShowViewControllerDidDismiss" object:nil];
    
    [_navBarDelegate slideShowControllerDidDismiss:self];
}

#pragma mark - Secret Buttons Setup

- (void)configureSecretButtons {
    UITapGestureRecognizer *twiceTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapTwice:)];
    UITapGestureRecognizer *onceTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnce:)];
    
    onceTapGesture.numberOfTapsRequired = 1;
    twiceTapGesture.numberOfTapsRequired = 2;
    
    [onceTapGesture requireGestureRecognizerToFail:twiceTapGesture];
    [twiceTapGesture requireGestureRecognizerToFail:onceTapGesture];
    
    [_leftSecretButton addGestureRecognizer:twiceTapGesture];
    [_rightSecretButton addGestureRecognizer:onceTapGesture];

}

- (void)tapOnce:(UIGestureRecognizer *)gesture {
    _rightSecretButton.selected = !_rightSecretButton.selected;
}

- (void)tapTwice:(UIGestureRecognizer *)gesture {
    
    if (_leftSecretButton.isSelected && _rightSecretButton.isSelected) {
        [self closeSlideShowController];
    }
    
    _leftSecretButton.selected = !_leftSecretButton.selected;
}

- (void)configureSecretTapProtection {
    UITapGestureRecognizer *tapEveryWhere = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resetSecretButtonsState:)];
    [self.view addGestureRecognizer:tapEveryWhere];
}

- (void)resetSecretButtonsState:(UIGestureRecognizer *)gesture {
    _leftSecretButton.selected = NO;
    _rightSecretButton.selected = NO;
}

#pragma mark - NSTimer

- (void)startTimer {
    _slideTimer = [NSTimer scheduledTimerWithTimeInterval:4.0 target:self selector:@selector(scrollToNextCell:) userInfo:nil repeats:YES];
}

- (void)stopTimer {
    [_slideTimer invalidate];
}

- (void)scrollToNextCell:(id)sender {
    
    if (_currentIndex < _medias.count - 1) {
        [_collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:_currentIndex+1 inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    } else {
        _currentIndex = 0;
        
        if (_isContainsVideoRecords) {
            [self emergencyReload];
        } else {
            [_collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:_currentIndex inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        }
    }
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSLog(@"MEDIA count: %@", @(_medias.count));
    return _medias.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    KKSlideShowPresentationCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"slideShowCell" forIndexPath:indexPath];
    
    cell.rootViewController = self;

    [cell setMediaItem:[_medias objectAtIndex:indexPath.item]];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    CGFloat cellWidth =  [[UIScreen mainScreen] bounds].size.width;
    CGFloat cellHeight =  [[UIScreen mainScreen] bounds].size.height;
    
    return CGSizeMake(cellWidth, cellHeight);
}

#pragma mark - UICollectionView Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    _currentIndex = indexPath.item;
    
    [(KKSlideShowPresentationCell *)cell playMedia];
    
    File *currentMedia = [_medias objectAtIndex:indexPath.item];
    
    if ([currentMedia.type isEqualToString:@"video"]) {
        [self stopTimer];
    } else {
        if (![_slideTimer isValid]) {
            [self startTimer];
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    [(KKSlideShowPresentationCell *)cell stopPlayingMedia];
}

@end
