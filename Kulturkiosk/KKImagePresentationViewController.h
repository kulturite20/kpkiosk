//
//  KKImagePresentationViewController.h
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 05.11.13.
//
//

#import "KKIndexViewController.h"
#import "Record.h"

@class Resource;

@interface KKImagePresentationViewController : KKPresentationViewController <UICollectionViewDelegate, UICollectionViewDataSource>

@property (strong, nonatomic) IBOutlet UIView *imageContainer;
- (void)loadImage:(Resource *)image;
- (IBAction)dismissController:(id)sender;

@end
