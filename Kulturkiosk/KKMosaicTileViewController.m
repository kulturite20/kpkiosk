//
//  KKMosaicTileViewController.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 2/18/17.
//
//

#import "KKMosaicTileViewController.h"
#import "KKRecordContainerViewController.h"
#import "Record.h"
#import "MosaicItem+CoreDataClass.h"
#import "MosaicItemText+CoreDataClass.h"
#import "MosaicItemTextDescription+CoreDataClass.h"
#import "MosaicItemTextTitle+CoreDataClass.h"
#import "Presentation.h"
#import "NSMutableString + FormatHTMLTags.h"

#import "UIColor+HexColor.h"

@interface KKMosaicTileViewController ()

@property (strong, nonatomic) Record *record;

// Create property to check mosaicItem have record_id or not
@property (nonatomic) BOOL isPresentationTile;

// Create property of image_map presentation

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textContainerCenterXConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textContainerCenterYConstraint;
@property (weak, nonatomic) IBOutlet UIView *textContainer;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelCenterXConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelCenterYConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionLabelCenterXConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionLabelCenterYConstraint;

@end

@implementation KKMosaicTileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tileImageView.layer.cornerRadius = 5;
    self.tileImageView.layer.masksToBounds = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self setupText];
}




- (void)addMosaicItem:(MosaicItem*)mosaicItem {
    
    self.mosaicItem = mosaicItem;
    
    if ([self.mosaicItem.type isEqualToString:@"presentation"]) {
        self.isPresentationTile = YES;
    } else {
        self.isPresentationTile = NO;
    }

}


- (void)setupText {
    
    MosaicItemTextTitle *mosaicTitle;
    MosaicItemTextDescription *mosaicDescription;
    
    for (MosaicItemText *mosaicText in [self.mosaicItem text]) {
        if ([mosaicText.language isEqualToString:[mosaicText contentForLanguage:[KKLanguage currentLanguage]]]) {
            mosaicTitle = mosaicText.title;
            mosaicDescription = mosaicText.desriptionText;
        }
    }
    
    
    if ([mosaicDescription.placement isEqualToString:mosaicTitle.placement]) {
        
        //Using text container with title and description
        
        // Configure topTextLabel label
        self.textContainer.hidden = NO;
        
        self.topTextLabel.text = [NSMutableString formatLineBreak:[mosaicTitle.content mutableCopy]];
        self.topTextLabel.textColor = [UIColor colorWithHexColor:mosaicTitle.color];
        self.topTextLabel.font = [mosaicTitle fontWithSize];
        self.topTextLabel.textAlignment = [mosaicTitle textAlignment];
        
        NSLog(@"TEST Current top text label font is - %@", self.topTextLabel.font);
        
        self.bottomTextLabel.text = [NSMutableString formatLineBreak:[mosaicDescription.content mutableCopy]];
        self.bottomTextLabel.textColor = [UIColor colorWithHexColor:mosaicDescription.color];
        self.bottomTextLabel.font = [mosaicDescription fontWithSize];
        self.bottomTextLabel.textAlignment = [mosaicDescription textAlignment];
        
        NSLog(@"TEST Current bottom text label font is - %@", self.bottomTextLabel.font);
    
        
        if ([mosaicDescription.placement isEqualToString:@"top-left"]) {
            
            // Top left
            self.textContainerCenterYConstraint.constant = - (self.tileImageView.frame.size.height/2);
            self.textContainerCenterXConstraint.constant = - (self.tileImageView.frame.size.width/2);
            
        } else if ([mosaicDescription.placement isEqualToString:@"top-center"]) {
            
            // Top center
            self.textContainerCenterYConstraint.constant = -(self.tileImageView.frame.size.height);
            
        } else if ([mosaicDescription.placement isEqualToString:@"top-right"]) {
            
            // Top right
            self.textContainerCenterYConstraint.constant = -(self.tileImageView.frame.size.height/2);
            self.textContainerCenterXConstraint.constant = self.tileImageView.frame.size.width/2;
            
        } else if ([mosaicDescription.placement isEqualToString:@"middle-left"]) {
            
            // center - left
            self.textContainerCenterXConstraint.constant = -(self.tileImageView.frame.size.width/2);
            
        } else if ([mosaicDescription.placement isEqualToString:@"middle-center"]) {
            
            //default position.
            
        } else if ([mosaicDescription.placement isEqualToString:@"middle-right"]) {
            
            // center - right
            self.textContainerCenterXConstraint.constant = self.tileImageView.frame.size.width/2;
            
        } else if ([mosaicDescription.placement isEqualToString:@"bottom-left"]) {
            
            // bottom left
            self.textContainerCenterYConstraint.constant = self.tileImageView.frame.size.height/2;
            self.textContainerCenterXConstraint.constant = - (self.tileImageView.frame.size.width/2);
            
        } else if ([mosaicDescription.placement isEqualToString:@"bottom-right"]) {
            
            // Bottom right
            self.textContainerCenterYConstraint.constant = self.tileImageView.frame.size.height/2;
            self.textContainerCenterXConstraint.constant = self.tileImageView.frame.size.width/2;
            
        } else if ([mosaicDescription.placement isEqualToString:@"bottom-right"]) {
            
            //bottom center
            self.textContainerCenterYConstraint.constant = self.tileImageView.frame.size.height;
            
        }
        
        if (mosaicTitle.position > mosaicDescription.position && self.topTextLabel.text != nil && self.bottomTextLabel.text != nil) {
            NSAttributedString *titleAttrString = self.topTextLabel.attributedText;
            NSAttributedString *descriptionAttrString = self.bottomTextLabel.attributedText;
            self.topTextLabel.attributedText = descriptionAttrString;
            self.bottomTextLabel.attributedText = titleAttrString;
        }
        
        
    } else {
        
        // Using title label and description with different placements
        
        // Configure title label
        self.titleLabel.hidden = NO;
        
        self.titleLabel.text = [NSMutableString formatLineBreak:[mosaicTitle.content mutableCopy]];;
        self.titleLabel.textColor = [UIColor colorWithHexColor:mosaicTitle.color];
        self.titleLabel.font = [mosaicTitle fontWithSize];
        self.titleLabel.textAlignment = [mosaicTitle textAlignment];
        
        
        // Configure description label
        self.descriptionLabel.hidden = NO;
    
        self.descriptionLabel.text = [NSMutableString formatLineBreak:[mosaicDescription.content mutableCopy]];
        self.descriptionLabel.textColor = [UIColor colorWithHexColor:mosaicTitle.color];
        self.descriptionLabel.font = [mosaicDescription fontWithSize];
        self.descriptionLabel.textAlignment = [mosaicDescription textAlignment];
        
        //Configure placement for title
        
        if ([mosaicTitle.placement isEqualToString:@"top-left"]) {
            
            // Top left
            self.titleLabelCenterYConstraint.constant = - (self.tileImageView.frame.size.height/2);
            self.titleLabelCenterXConstraint.constant = - (self.tileImageView.frame.size.width/2);
            
        } else if ([mosaicTitle.placement isEqualToString:@"top-center"]) {
            
            // Top center
            self.titleLabelCenterYConstraint.constant = -(self.tileImageView.frame.size.height);
            
        } else if ([mosaicTitle.placement isEqualToString:@"top-right"]) {
            
            // Top right
            self.titleLabelCenterYConstraint.constant = -(self.tileImageView.frame.size.height/2);
            self.titleLabelCenterXConstraint.constant = self.tileImageView.frame.size.width/2;
            
        } else if ([mosaicTitle.placement isEqualToString:@"middle-left"]) {
            
            // center - left
            self.titleLabelCenterXConstraint.constant = -(self.tileImageView.frame.size.width/2);
            
        } else if ([mosaicTitle.placement isEqualToString:@"middle-center"]) {
            
            //default position.
            
        } else if ([mosaicTitle.placement isEqualToString:@"middle-right"]) {
            
            // center - right
            self.titleLabelCenterXConstraint.constant = self.tileImageView.frame.size.width/2;
            
        } else if ([mosaicTitle.placement isEqualToString:@"bottom-left"]) {
            
            //            // bottom left
            self.titleLabelCenterYConstraint.constant = self.tileImageView.frame.size.height/2;
            self.titleLabelCenterXConstraint.constant = - (self.tileImageView.frame.size.width/2);
            
        } else if ([mosaicTitle.placement isEqualToString:@"bottom-right"]) {
            
            // Bottom right
            self.titleLabelCenterYConstraint.constant = self.tileImageView.frame.size.height/2;
            self.titleLabelCenterXConstraint.constant = self.tileImageView.frame.size.width/2;
            
        } else if ([mosaicTitle.placement isEqualToString:@"bottom-right"]) {
            
            //bottom center
            self.titleLabelCenterYConstraint.constant = self.tileImageView.frame.size.height;
            
        }
        
        
        //Configure placement for description
        
        if ([mosaicDescription.placement isEqualToString:@"top-left"]) {
            
            // Top left
            self.descriptionLabelCenterYConstraint.constant = - (self.tileImageView.frame.size.height/2);
            self.descriptionLabelCenterXConstraint.constant = - (self.tileImageView.frame.size.width/2);
            
        } else if ([mosaicDescription.placement isEqualToString:@"top-center"]) {
            
            // Top center
            self.descriptionLabelCenterYConstraint.constant = -(self.tileImageView.frame.size.height);
            
        } else if ([mosaicDescription.placement isEqualToString:@"top-right"]) {
            
            // Top right
            self.descriptionLabelCenterYConstraint.constant = -(self.tileImageView.frame.size.height/2);
            self.descriptionLabelCenterXConstraint.constant = self.tileImageView.frame.size.width/2;
            
        } else if ([mosaicDescription.placement isEqualToString:@"middle-left"]) {
            
            // center - left
            self.descriptionLabelCenterXConstraint.constant = -(self.tileImageView.frame.size.width/2);
            
        } else if ([mosaicDescription.placement isEqualToString:@"middle-center"]) {
            
            //default position.
            
        } else if ([mosaicDescription.placement isEqualToString:@"middle-right"]) {
            
            // center - right
            self.descriptionLabelCenterXConstraint.constant = self.tileImageView.frame.size.width/2;
            
        } else if ([mosaicDescription.placement isEqualToString:@"bottom-left"]) {
            
            // bottom left
            self.descriptionLabelCenterYConstraint.constant = self.tileImageView.frame.size.height/2;
            self.descriptionLabelCenterXConstraint.constant = - (self.tileImageView.frame.size.width/2);
            
        } else if ([mosaicDescription.placement isEqualToString:@"bottom-right"]) {
            
            // Bottom right
            self.descriptionLabelCenterYConstraint.constant = self.tileImageView.frame.size.height/2;
            self.descriptionLabelCenterXConstraint.constant = self.tileImageView.frame.size.width/2;
            
        } else if ([mosaicDescription.placement isEqualToString:@"bottom-right"]) {
            
            //bottom center
            self.descriptionLabelCenterYConstraint.constant = self.tileImageView.frame.size.height;
            
        }

    
    }
    
    
    [self.view layoutIfNeeded];

    
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)tileButtonClicked:(UIButton *)sender {
    
    if (self.tapBlock != NULL) {
        self.tapBlock(self.mosaicItem, self.isPresentationTile);
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
