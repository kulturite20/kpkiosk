//
//  KKIndexCollectionViewCell.h
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 08.08.13.
//
//

#import <UIKit/UIKit.h>

@interface KKIndexCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) Record *record;
@property (weak, nonatomic) IBOutlet UIButton *mapButton;
@property (weak, nonatomic) IBOutlet UIButton *timelineButton;
@property (weak,nonatomic) IBOutlet UIImageView *playButton;

- (void)populateWithRecord:(Record*)_record
              presentation:(Presentation *)presentation;

@end
