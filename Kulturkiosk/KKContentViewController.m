//
//  KKContentViewController2.m
//  Kulturkiosk
//
//  Created by Rune Botten on 02.10.12.
//
//

#import "KKContentViewController.h"
#import "Resource.h"
#import "KKHeaderCollectionView.h"
#import "KKResourceCollectionViewCell.h"
#import "KKTimeLineViewController.h"
#import "KKImageGalleryViewController.h"
#import "KKVideoGalleryViewController.h"
#import "KKTextContentViewController.h"
#import "UIColor+HexColor.h"
#import "KKAnnotationView.h"

@interface KKContentViewController()
@property (assign) BOOL showingText;
@property (weak, nonatomic) IBOutlet KKAnnotationView *annotationview;
@property (weak, nonatomic) IBOutlet UIButton *mapButton;
@property (weak, nonatomic) IBOutlet UIButton *indexButton;
@property (weak, nonatomic) IBOutlet UIButton *timelineButton;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@end
@implementation KKContentViewController

- (void)dealloc
{
  self.presentation = nil;
}

-(UICollectionReusableView*) collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
  KKHeaderCollectionView *cell = (KKHeaderCollectionView*)[super collectionView:collectionView viewForSupplementaryElementOfKind:kind atIndexPath:indexPath];
    //REF
    /*
  cell.featureImage.image = [self.node headerImage];
  
  [cell.featureImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doReadMore)]];
  [cell.featureImage addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(doReadMore)]];
    
  cell.readMore.text = [[KKLanguage localizedString:@"Read more"] uppercaseString];
  cell.readMore.font = [UIFont fontWithName:@"MyriadPro-Bold" size:18];
     */
  return cell;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self reloadMetaData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if ([self.parentViewController isKindOfClass:[KKTimeLineViewController class]]) {
        self.closeButton.hidden = NO;
    } else {
        self.closeButton.hidden = YES;
    }
}

- (void)reloadMetaData {
//  NSArray *records = [Record objectsWithPredicate:[NSPredicate predicateWithFormat:@"node == %@ AND presentation != nil",self.node]];
//  NSArray *records = [self.presentation records];
    
  self.mapButton.hidden = YES;
  self.timelineButton.hidden = YES;
  self.indexButton.hidden = YES;
  //REF
    /*
  for (Record *record in records) {
    
    if ([record.presentation.type isEqualToString:@"map"]) {
      if (record.color != nil) {
        self.annotationview.bgColor = [UIColor colorWithHexColor:record.color];
        self.annotationview.code = record.code;
        [self.annotationview setNeedsDisplay];
      }
      self.mapButton.hidden = NO;
      self.mapButton.tag = record.presentation.identifierValue;
    }else if ([record.presentation.type isEqualToString:@"timeline"] && ![self.parentViewController isKindOfClass:[KKTimeLineViewController class]]){
      self.timelineButton.hidden = NO;
      self.timelineButton.tag = record.presentation.identifierValue;
    }else if ([record.presentation.type isEqualToString:@"index"]){
      self.indexButton.hidden = NO;
      self.indexButton.tag = record.presentation.identifierValue;
    }
  }
  */
  [self.annotationview setNeedsDisplay];
  
}

- (IBAction)highlightContentInPresentation:(id)sender
{
    //REF
    /*
  UIButton *button = (UIButton*) sender;
  
  NSDictionary *userinfo = @{@"presentationId":[NSNumber numberWithInt:button.tag], @"nodeId":self.node.item_id};
  
  [[NSNotificationCenter defaultCenter] postNotificationName:@"highlightContentInPresentation" object:nil userInfo:userinfo];
  */
}

-(void) didChangeLanguage:(NSNotification*) notification {
  [self.collectionView reloadData];
}

-(void) doReadMore
{
  // Both gesture recognizers might trigger, so this is just to stop them from both doing the
  // same work.
  if(self.showingText)
    return;
  self.showingText = YES;
  //REF
    /*
  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
  KKTextContentViewController *view = [storyboard instantiateViewControllerWithIdentifier:@"textView"];
  view.node = self.node;
  [self presentViewController:view animated:YES completion:^{
    self.showingText = NO;
  }];
     */
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
  KKResourceCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ResourceCell" forIndexPath:indexPath];
  
//  Resource *child = [self.childs objectAtIndex:(NSUInteger)indexPath.row];
    //REF
  /*
  if([child.type isEqualToString:KKResourceTypeImage]) {
    cell.title.text = [[KKLanguage localizedString:@"Photos"] uppercaseString];
    cell.title.font = [UIFont fontWithName:@"MyriadPro-Bold" size:18];
    
    File *file = [child imageWithFormat:KKFormatSmall];
    cell.image.image = [UIImage imageWithContentsOfFile:file.localUrl];
    
  } else if([child.type isEqualToString:KKResourceTypeVideo]) {
    
    File *v = [[child.files allObjects] objectAtIndex:0];

    CGSize thumbSize = CGSizeMake(cell.frame.size.width, cell.frame.size.height);
    UIImage *image = [v thumbnailWithSize:&thumbSize];
    
    cell.image.image = image;

    cell.title.text = [[KKLanguage localizedString:@"Videos"] uppercaseString];
    cell.title.font = [UIFont fontWithName:@"MyriadPro-Bold" size:18];
  }
  */
  return cell;
}
//REF
/*
-(void)setNode:(Nodes *)node
{
  _node = node;
  
  NSMutableArray *array = [NSMutableArray arrayWithCapacity:3];
  
  // Conditionally add video and image resources
  NSArray *vids = [self.node resourcesWithType:KKResourceTypeVideo
                                   forLanguage:[KKLanguage currentLanguage]];
  if([vids count]) {
    Resource *video = [vids objectAtIndex:0];
    [array addObject:video];
  }
  
  NSArray *images = [self.node imagesWithType:KKFormatHeader];
  if(images.count != 0)
  {
    File *f = [images objectAtIndex:0];
    Resource *image = f.resource;
    [array addObject:image];
  }
  self.childs = array;
  
  
  [self.collectionView reloadData];
  [self reloadMetaData];
}
 */

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
//  Resource *child = [self.childs objectAtIndex:(NSUInteger)indexPath.row];
//  UIViewController *vc = nil;
//  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
  //REF
    /*
  if([child.type isEqualToString:KKResourceTypeImage]) {
    KKImageGalleryViewController *view = [storyboard instantiateViewControllerWithIdentifier:@"imageGalleryView"];
    
    NSArray *imageResources = [self.node resourcesWithType:KKResourceTypeImage forLanguage:[KKLanguage currentLanguage]];
    NSArray *sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"page_id" ascending:YES],[NSSortDescriptor sortDescriptorWithKey:@"position" ascending:YES]];
    view.resources = [imageResources sortedArrayUsingDescriptors:sortDescriptors];
    vc = view;
    
  } else if([child.type isEqualToString:KKResourceTypeVideo]) {
    KKVideoGalleryViewController *view = [storyboard instantiateViewControllerWithIdentifier:@"videoGalleryView"];
    NSArray *videoResources = [self.node resourcesWithType:KKResourceTypeVideo forLanguage:[KKLanguage currentLanguage]];
    view.resources = [videoResources sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"identifier" ascending:YES]]];
    vc = view;
  }
  
  Content *c = [self.node contentForLanguage:[KKLanguage currentLanguage]];
  vc.title = c.title;
  [self presentViewController:vc animated:YES completion:nil];
     */
}

- (IBAction)closeButtonClicked:(id)sender {
    if ([self.parentViewController isKindOfClass:[KKTimeLineViewController class]]) {
        KKTimeLineViewController *timelineView = (KKTimeLineViewController *)self.parentViewController;
        [timelineView restoreScreen];
    }
}


@end
