//
//  KKAppDelegate.m
//  Kulturkiosk
//
//  Created by Anders Wiik on 21.05.12.
//  Copyright (c) 2012 Nordaaker Ltd.. All rights reserved.
//

#import <objc/runtime.h>

#import "KKAppDelegate.h"
#import "KKWarningView.h"
#import "KKWindow.h"
#import "KKUpdatingWarningView.h"
#import "KKLanguage.h"
#import "KKLoadingViewController.h"
#import "KKNavigationViewController.h"
#import "KKMapViewController.h"
#import "KKVideoPresentationViewController.h"
#import "KKHomeScreenViewController.h"
#import "KKMemoryAlertView.h"

// Fake datamodel
#import "TestContent.h"
#import "TestBlock.h"
#import "TestFile.h"
#import "TestPage.h"
#import "TestAudioBlock.h"
#import "TestTextBlock.h"
#import "TestVideoBlock.h"

//For testing
#import "KKRecordContainerViewController.h"

#import "NSString+Filesystem.h"

#import "KKConstants.h"

//static NSString *const kAPIMasterHostAdress  = @"https://dev-api.kulturpunkt.org";
//static NSString *const kAPIDefaultHostAdress = @"https://dev-api.kulturpunkt.org";

static NSString *const kAPIMasterHostAdress  = @"https://api.kulturpunkt.org";
static NSString *const kAPIDefaultHostAdress = @"https://api.kulturpunkt.org";

static NSString *const kIdleTimerName         = @"idleTimer";
static NSString *const kHomeScreenTimerName   = @"homeScreenTimer";
static NSString *const kUpdatingTimerName     = @"updatingTimer";
static NSString *const kCommitingUpdatesTimer = @"commitingUpdatesTimer";
static NSString *const kDataDownoadedKey      = @"kDataDownoadedKey";

//static NSInteger const kAPIDeviceID = 174;
//static NSInteger const kAPIDeviceID = 176;
// TODO: only to test https://kulturit.atlassian.net/browse/KPK-95
//static NSInteger const kAPIDeviceID = 195;

//static NSInteger const kAPIDeviceID = 200;

// Test placers on prod.
//static NSInteger const kAPIDeviceID = 164;

static NSInteger const kAPIDeviceID = 1;

// For image map testing
//static NSInteger const kAPIDeviceID = 194;
//static NSInteger const kAPIDeviceID = 199;
//static NSInteger const kAPIDeviceID = 200;
//static NSInteger const kAPIDeviceID = 201;

// Image-map markers with lines
//static NSInteger const kAPIDeviceID = 202;

//static NSInteger const kAPIDeviceID = 1;


//static NSInteger const kAPIDeviceID = 34;

//For zoom testing (on production)
//static NSInteger const kAPIDeviceID = 184;

//For dots and lines testing (on dev api)
//static NSInteger const kAPIDeviceID = 178;

//Fonts testing (developer api)
//static NSInteger const kAPIDeviceID = 193;
//static NSInteger const kAPIDeviceID = 194;

// check for update timer timeout
// 900 by default

static NSInteger const kTimeToUpdateContent = 900;

@interface KKAppDelegate()

@property (nonatomic, assign) NSInteger inactivityTimeout;
@property (nonatomic, strong) AFHTTPClient *client;
@property (nonatomic, strong) RKManagedObjectStore *tempStore;
@property (nonatomic, strong) NSString *lastUpdateTimeValue;
@property (nonatomic, strong) UIAlertView *reachabilityAlertView;

@property (assign) BOOL isChangedFromLastLaunch;
@property (assign) BOOL isTimeToUpdateContent;

// Indicates we wait until we update current data set.
@property (assign) BOOL isMovedInWaitingMode;

@property (assign) BOOL needToShowLoadingView;
@property (assign) BOOL newProject;

@end

@implementation KKAppDelegate

@synthesize managedObjectContext                 = __managedObjectContext;
@synthesize managedObjectModel                   = __managedObjectModel;
@synthesize persistentStoreCoordinator           = __persistentStoreCoordinator;
@synthesize persistentStoreCoordinatorForUpdate  = _persistentStoreCoordinatorForUpdate;
@synthesize window                               = _window;

#pragma mark - AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // Setting up the environment
    
    self.apiEnvironment = KKMasterAPIEnvironment;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    // Register the preference defaults early.
    [defaults registerDefaults:@{
                                     @"device_id" : @(kAPIDeviceID),
                                     @"currentLanguage" : [NSNumber numberWithInt:KKLanguageTypeNorwegian],
                                     @"api_host" : (self.apiEnvironment == KKMasterAPIEnvironment) ? kAPIMasterHostAdress : kAPIDefaultHostAdress,
                                     @"inactivity_timer": @1
                                     }];
        
        // transfer the current version number into the defaults so that this correct value will be displayed when the user visit settings page later
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [defaults setObject:version forKey:@"version"];
    
    
    [self subscribeToNotifications];
    
    // Load the configured project
    NSInteger project_id   = [defaults integerForKey:@"device_id"],
    
    last_project = [defaults integerForKey:@"last_project"];
    
    NSLog(@"\n\n \t ID: %@\n\n", @(project_id));
    
    self.newProject = project_id != last_project;
    // Has the user switched project via system settings since last launch?
    if (self.newProject) {
        _applicationMode = KKInitializingMode;
    } else {
        _applicationMode = KKLaunchingMode;
    }
    
    NSString *apiHost = [[NSUserDefaults standardUserDefaults] stringForKey:@"api_host"];
    _client = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:apiHost]];
    
    [self setupReachability];
    
    [self setupUI];
    
    [KKTimerManager sharedManager].delegate = self;
    [[KKTimerManager sharedManager] startTimerWithIdentifier:kUpdatingTimerName
                                                     timeout:kTimeToUpdateContent];
    _isChangedFromLastLaunch = YES;
    _hasNoSpace              = NO;
    _needToShowLoadingView   = NO;
    
    [defaults synchronize];
    
    return YES;
}

- (void)setupUI {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    KKLoadingViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"LoadingView"];
    self.window = [[KKWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = viewController;
    
    self.window.opaque = YES;
    self.window.backgroundColor = [UIColor blackColor];
    [self.window makeKeyAndVisible];
}

- (void)subscribeToNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showMemoryWarning)
                                                 name:@"notEnoughSpace"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dismissMemoryWarning)
                                                 name:@"enoughSpace"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(doneLoading)
                                                 name:@"launchingFinished"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadingInitialized:)
                                                 name:@"loadingInitialized"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(initializingFinished)
                                                 name:@"initializingFinished"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movedInWaitingMode)
                                                 name:@"waitingModeEnabled"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didQuitWaitingMode)
                                                 name:@"waitingModeDisabled"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updatesFinished)
                                                 name:@"loadingUpdatesFinished"
                                               object:nil];
    
}

- (void)setupReachability {
    // Checking reachability
    __weak typeof(self) weakSelf = self;
    
    [_client setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        BOOL isDataDownloaded = [[NSUserDefaults standardUserDefaults] boolForKey:kDataDownoadedKey];
        
        if (status == AFNetworkReachabilityStatusReachableViaWiFi || status ==
            AFNetworkReachabilityStatusReachableViaWWAN) {
            // internet YES
            // need downloading? - >YES: download
            // no data OR device id was changed
            // need downloading? ->NO: skip
            if ([weakSelf.reachabilityAlertView isVisible]) {
                [weakSelf.reachabilityAlertView dismissWithClickedButtonIndex:0
                                                                     animated:YES];
            }
            
            if ([weakSelf.window.rootViewController isKindOfClass:[KKLoadingViewController class]]) {
                if(weakSelf.newProject) {
                    [defaults setBool:NO forKey:kDataDownoadedKey];
                    [defaults synchronize];
                    
                    [weakSelf deleteDefaultCachedData];
                }
                
                // TODO: to Vadim force showign home screen
                //#warning ONLY for testing!!!
//                if (isDataDownloaded == YES){
//                    weakSelf.applicationMode = KKInitializingMode;
//                    [weakSelf setupRestKit];
//                    [weakSelf mappingDidFinished];
//                    
//                    return;
//                }
                
                // Before each launch of this method we have to make hard reset of buffer persistent store
                [weakSelf resetBufferDatabase];
                [weakSelf setupRestKit];
                [weakSelf loadProjectFromNetwork];
            }
            
        } else if (status == AFNetworkReachabilityStatusNotReachable) {
            // internet NO
            // do we have data to show? ->YES: show
            // if we just launched
            // do we have data to show? ->NO: show error alert and retry.
            
            if (isDataDownloaded == NO) {
                [weakSelf showRetryAlertWithBackUpMode:NO];
            } else if ([weakSelf.window.rootViewController isKindOfClass:[KKLoadingViewController class]]) {
                if (weakSelf.newProject) {
                    [weakSelf showRetryAlertWithBackUpMode:YES];
                } else {
                    weakSelf.applicationMode = KKInitializingMode;
                    [weakSelf setupRestKit];
                    [weakSelf mappingDidFinished];
                }
            }
        }
    }];

}

#pragma mark - Core Data stack

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *)managedObjectContext {
    if (__managedObjectContext != nil)
    {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil)
    {
        __managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return __managedObjectContext;
}

/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *)managedObjectModel
{
    if (__managedObjectModel != nil)
    {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return __managedObjectModel;
}

/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (__persistentStoreCoordinator != nil) {
        return __persistentStoreCoordinator;
    }
    
    NSError *error = nil;
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:[KKConstants mainStoreURL] options:options error:&error])
    {
        
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        //abort();
    }
    
    return __persistentStoreCoordinator;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinatorForUpdate {
    if (_persistentStoreCoordinatorForUpdate != nil) {
        return _persistentStoreCoordinatorForUpdate;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Kulturkiosk_temp.sqlite"];
    
    NSError *error = nil;
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    _persistentStoreCoordinatorForUpdate = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinatorForUpdate addPersistentStoreWithType:NSSQLiteStoreType
                                                            configuration:nil
                                                                      URL:storeURL
                                                                  options:options
                                                                    error:&error])
    {
        
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        //abort();
    }
    
    return _persistentStoreCoordinatorForUpdate;
}


#pragma mark - Application's Documents directory

/**
 Returns the URL to the application's Documents directory.
 */
- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - RestKit stack

- (void)setupRestKit {
    /** Creating & initializing RestKit object store */
    NSManagedObjectModel *objectModel = [self managedObjectModel];
    RKManagedObjectStore *objectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:objectModel];
    
    NSError *error;
    NSString *path;
    if (_applicationMode == KKInitializingMode) {
        path = [[KKConstants mainStoreURL] path];
    } else if (_applicationMode == KKUpdatingMode || _applicationMode == KKLaunchingMode) {
        path = [[KKConstants tempStoreURL] path];
    }
    
    [objectStore addSQLitePersistentStoreAtPath:path
                         fromSeedDatabaseAtPath:nil
                              withConfiguration:nil
                                        options:@{NSInferMappingModelAutomaticallyOption       :
                                                      @YES,
                                                  NSMigratePersistentStoresAutomaticallyOption : @YES}
                                          error:&error];
    // Reset the persistant store when the data model changes
    if (error) {
        NSLog(@"Couldn't add a persistent store due to error: %@", [error localizedDescription]);
    }
    
    if (_applicationMode == KKInitializingMode) {
        [RKManagedObjectStore setDefaultStore:objectStore];
        [RKManagedObjectStore defaultStore].managedObjectCache = [RKFetchRequestManagedObjectCache new];
    } else if (_applicationMode == KKUpdatingMode || _applicationMode == KKLaunchingMode) {
        objectStore.managedObjectCache = [RKFetchRequestManagedObjectCache new];
        _tempStore = objectStore;
    }
    
    [objectStore createManagedObjectContexts];
    
    /** Creating & initializing RestKit global object manager */
    
    NSURL *baseURL = [NSURL URLWithString:[[NSUserDefaults standardUserDefaults]
                                           valueForKey:@"api_host"]];
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:baseURL];
    [client setDefaultHeader:@"Accept"
                       value:RKMIMETypeJSON];
    
    RKObjectManager *objectManager = [[RKObjectManager alloc] initWithHTTPClient:client];
    
    if (_applicationMode == KKInitializingMode) {
        objectManager.managedObjectStore = [RKManagedObjectStore defaultStore];
    } else if (_applicationMode == KKUpdatingMode || _applicationMode == KKLaunchingMode) {
        objectManager.managedObjectStore = _tempStore;
    }
    
    [RKObjectManager setSharedManager:objectManager];
    
    // Enable automatic network activity indicator management
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    
//    RKLogConfigureByName("RestKit/Network", RKLogLevelOff);
//    RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelOff);
//    
    //RKLogConfigureByName("RestKit/CoreData", RKLogLevelOff);
    //RKLogConfigureByName("RestKit", RKLogLevelDebug);
    
    /*!
     Map to a target object class -- just as you would for a non-persistent class. The entity is resolved
     for you using the Active Record pattern where the class name corresponds to the entity name within Core Data.
     */
    
    //MosaicPresentation
    RKEntityMapping *mosaicPresentationMapping = [RKEntityMapping
                                          mappingForEntityForName:@"MosaicPresentation"
                                          inManagedObjectStore:objectStore];
    
    mosaicPresentationMapping.identificationAttributes = @[@"identifier"];
    [mosaicPresentationMapping addAttributeMappingsFromDictionary:@{@"id"   : @"identifier",
                                                            @"type"         : @"type",
                                                            @"updated_at"   : @"updated_at"}];
    
    
    //MosaicElementSize
    RKEntityMapping *mosaicElementSizeMapping = [RKEntityMapping
                                      mappingForEntityForName:@"MosaicElementSize"
                                      inManagedObjectStore:objectStore];
    //mosaicElementSizeMapping.identificationAttributes = @[@"identifier"];
    [mosaicElementSizeMapping addAttributeMappingsFromDictionary:@{//@"id" : @"identifier",
                                                            @"@metadata.mapping.collectionIndex" :  @"position",
                                                            @"x"  : @"x",
                                                            @"y"  : @"y"}];
    
    //MosaicTemplate
    RKEntityMapping *mosaicTemplateMapping = [RKEntityMapping
                                       mappingForEntityForName:@"MosaicTemplate"
                                       inManagedObjectStore:objectStore];
    
    //mosaicTemplateMapping.identificationAttributes = @[@"identifier"];
    [mosaicTemplateMapping addAttributeMappingsFromDictionary:@{//@"id"      : @"identifier",
                                                        @"width"    : @"width",
                                                        @"height"   : @"height"}];
    
    
    //MosaicItem
    RKEntityMapping *mosaicItemMapping = [RKEntityMapping
                                       mappingForEntityForName:@"MosaicItem"
                                       inManagedObjectStore:objectStore];
    
    mosaicItemMapping.identificationAttributes = @[@"u_id"];
    [mosaicItemMapping addAttributeMappingsFromDictionary:@{@"id"           : @"identifier",
                                                         @"u_id"            : @"u_id",
                                                         @"type"            : @"type",
                                                         @"presentation_id" : @"presentationID",
                                                         @"record_id"       : @"record_id",
                                                         @"position"        : @"position",
                                                         @"image"           : @"imageUrl"}];
    
    //MosaicItemText
    RKEntityMapping *mosaicItemTextMapping = [RKEntityMapping
                                       mappingForEntityForName:@"MosaicItemText"
                                       inManagedObjectStore:objectStore];
    
    [mosaicItemTextMapping addAttributeMappingFromKeyOfRepresentationToAttribute:@"language"];
    
   // mosaicItemTextMapping.identificationAttributes = @[@"language"];
    
    mosaicItemTextMapping.forceCollectionMapping = YES;
        
    
    //MosaicItemTextDescription
    RKEntityMapping *mosaicItemTextDescriptionMapping = [RKEntityMapping
                                   mappingForEntityForName:@"MosaicItemTextDescription"
                                   inManagedObjectStore:objectStore];
    
    [mosaicItemTextDescriptionMapping addAttributeMappingsFromDictionary:@{
                                                                    @"font-size"       : @"font_size",
                                                                    @"position"        : @"position",
                                                                    @"placement"       : @"placement",
                                                                    @"font"            : @"font",
                                                                    @"color"           : @"color",
                                                                    @"content"         : @"content",
                                                                    @"alignment"       : @"alignment"}];
    
    //MosaicItemTextTitle
    RKEntityMapping *mosaicItemTextTitleMapping = [RKEntityMapping
                                                  mappingForEntityForName:@"MosaicItemTextTitle"
                                                  inManagedObjectStore:objectStore];
    
    [mosaicItemTextTitleMapping addAttributeMappingsFromDictionary:@{
                                                                    @"font-size"            : @"size",
                                                                    @"position"        : @"position",
                                                                    @"placement"       : @"placement",
                                                                    @"font"            : @"font",
                                                                    @"color"           : @"color",
                                                                    @"content"         : @"content",
                                                                    @"alignment"       : @"alignment"}];
    
    
    
    // Record
    RKEntityMapping *recordMapping = [RKEntityMapping
                                      mappingForEntityForName:@"Record"
                                      inManagedObjectStore:objectStore];
    
    recordMapping.identificationAttributes = @[@"identifier"];
    [recordMapping addAttributeMappingsFromDictionary:@{@"id"     : @"identifier",
                                                        @"title"  : @"title",
                                                        @"type"   : @"type"}];
    
    // Credit
    RKEntityMapping *creditMapping = [RKEntityMapping
                                      mappingForEntityForName:@"Credit"
                                      inManagedObjectStore:objectStore];
    creditMapping.identificationAttributes = @[@"identifier", @"parent_id"];
    [creditMapping addAttributeMappingsFromDictionary:@{@"@parent.u_id" : @"parent_id",
                                                        @"id" : @"identifier",
                                                        @"name" : @"name",
                                                        @"type" : @"credit_type"}];
    
    // Files
    RKEntityMapping *fileMapping = [RKEntityMapping
                                    mappingForEntityForName:@"File"
                                    inManagedObjectStore:objectStore];
    fileMapping.identificationAttributes = @[@"identifier", @"u_id", @"name"];
    [fileMapping addAttributeMappingsFromDictionary:@{@"u_id"     : @"u_id",
                                                      @"id"       : @"identifier",
                                                      @"filename" : @"name",
                                                      @"type"     : @"type",
                                                      @"position" : @"position"
                                                      }];
    
    // Links
    RKEntityMapping *linkMapping = [RKEntityMapping
                                    mappingForEntityForName:@"Link"
                                    inManagedObjectStore:objectStore];
    linkMapping.identificationAttributes = @[@"u_id", @"url"];
    [linkMapping addAttributeMappingsFromDictionary:@{@"@parent.u_id" : @"u_id",
                                                      @"url"          : @"url",
                                                      @"url2"         : @"url2",
                                                      @"type"         : @"type"}];
    
    // Content
    RKEntityMapping *contentMapping = [RKEntityMapping
                                       mappingForEntityForName:@"Content"
                                       inManagedObjectStore:objectStore];
    
    [contentMapping addAttributeMappingFromKeyOfRepresentationToAttribute:@"language"];
    
//    RKLogConfigureByName("RestKit", RKLogLevelWarning);
//    RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelTrace);
//    RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
//    
//    contentMapping.identificationAttributes = @[/*@"language", @"title", */@"parent_id"];
    contentMapping.forceCollectionMapping = YES;
    [contentMapping addAttributeMappingsFromDictionary:@{@"(language).title"       : @"title",
                                                         @"(language).description" : @"desc"
//                                                        @"@parent.u_id"           : @"parent_id"
                                                         }];
    
    // Presentation
    RKEntityMapping *presentationMapping = [RKEntityMapping
                                            mappingForEntityForName:@"Presentation"
                                            inManagedObjectStore:objectStore];
    presentationMapping.identificationAttributes = @[@"identifier"];
    [presentationMapping addAttributeMappingsFromDictionary:@{@"id"       : @"identifier",
                                                              @"name"     : @"name",
                                                              @"type"     : @"type",
                                                              @"position" : @"position"}];
    // Device
    RKEntityMapping *deviceMapping = [RKEntityMapping
                                      mappingForEntityForName:@"Device"
                                      inManagedObjectStore:objectStore];
    
    deviceMapping.identificationAttributes = @[@"identifier"];
    [deviceMapping addAttributeMappingsFromDictionary:@{
                                                        @"id"                   : @"identifier",
                                                        @"name"                 : @"name",
                                                        @"reset_time"           : @"resetTime",
                                                        @"idle_screen_active"   : @"idleScreenActive",
                                                        @"idle_screen_title"    : @"idleScreenTitle",
                                                        @"default_presentation" : @"defaultPresentation",
                                                        @"default_point"        : @"defaultPoint"}];
    
    // RecordDescription
    RKEntityMapping *recordDescriptionMapping = [RKEntityMapping mappingForEntityForName:@"RecordDescription"
                                                                    inManagedObjectStore:objectStore];
    recordDescriptionMapping.identificationAttributes = @[@"record_id", @"u_id"];
    [recordDescriptionMapping addAttributeMappingsFromDictionary:@{@"record_id"      : @"record_id",
                                                                   @"u_id"           : @"u_id",
                                                                   @"x"              : @"x",
                                                                   @"y"              : @"y",
                                                                   @"position"       : @"position",
                                                                   @"code"           : @"code",
                                                                   @"color"          : @"color"}];
    
    // RecordImage
    RKEntityMapping *recordImageMapping = [RKEntityMapping mappingForEntityForName:@"RecordImage"
                                                              inManagedObjectStore:objectStore];
    recordImageMapping.identificationAttributes = @[@"identifier"];
    [recordImageMapping addAttributeMappingsFromDictionary:@{@"id"   : @"identifier",
                                                             @"name" : @"name"}];
    
    // MapImage
    RKEntityMapping *mapImageMapping = [RKEntityMapping mappingForEntityForName:@"MapImage"
                                                              inManagedObjectStore:objectStore];
    //mapImageMapping.identificationAttributes = @[@"identifier"];
    [mapImageMapping addAttributeMappingsFromDictionary:@{@"anchorOn"       : @"anchorOn",
                                                          @"anchor_x"       : @"anchor_x",
                                                          @"anchor_y"       : @"anchor_y",
                                                          @"map_x"          : @"map_x",
                                                          @"map_y"          : @"map_y",
                                                          @"text_placement" : @"text_placement"}];
    
    //MapText
    RKEntityMapping *mapTextMapping = [RKEntityMapping
                                              mappingForEntityForName:@"MapText"
                                              inManagedObjectStore:objectStore];
    
    [mapTextMapping addAttributeMappingFromKeyOfRepresentationToAttribute:@"language"];
    
    mapTextMapping.forceCollectionMapping = YES;
    
    //MosaicItemTextDescription
    RKEntityMapping *mapDescriptionMapping = [RKEntityMapping
                                                         mappingForEntityForName:@"MapDescription"
                                                         inManagedObjectStore:objectStore];
    
    [mapDescriptionMapping addAttributeMappingsFromDictionary:@{ @"font-size"       : @"font_size",
                                                                 @"position"        : @"position",
                                                                 @"placement"       : @"placement",
                                                                 @"font"            : @"font",
                                                                 @"color"           : @"color",
                                                                 @"content"         : @"content",
                                                                 @"alignment"       : @"alignment"}];
    
    //MosaicItemTextTitle
    RKEntityMapping *mapTitleMapping = [RKEntityMapping
                                                   mappingForEntityForName:@"MapTitle"
                                                   inManagedObjectStore:objectStore];
    
    [mapTitleMapping addAttributeMappingsFromDictionary:@{ @"font-size"       : @"size",
                                                           @"position"        : @"position",
                                                           @"placement"       : @"placement",
                                                           @"font"            : @"font",
                                                           @"color"           : @"color",
                                                           @"content"         : @"content",
                                                           @"alignment"       : @"alignment"}];
    
    
    
    // VideoBlocks
    RKEntityMapping *videoBlockMapping = [RKEntityMapping mappingForEntityForName:@"VideoBlock"
                                                             inManagedObjectStore:objectStore];
    videoBlockMapping.identificationAttributes = @[@"identifier"];
    [videoBlockMapping addAttributeMappingsFromDictionary:@{@"id"       : @"identifier",
                                                            @"type"     : @"type",
                                                            @"position" : @"position"}];
    
    
    // AudioBlocks
    RKEntityMapping *audioBlockMapping = [RKEntityMapping mappingForEntityForName:@"AudioBlock"
                                                             inManagedObjectStore:objectStore];
    audioBlockMapping.identificationAttributes = @[@"identifier"];
    [audioBlockMapping addAttributeMappingsFromDictionary:@{@"id"       : @"identifier",
                                                            @"type"     : @"type",
                                                            @"position" : @"position"}];
    
    
    // TextBlocks
    RKEntityMapping *textBlockMapping = [RKEntityMapping mappingForEntityForName:@"TextBlock"
                                                            inManagedObjectStore:objectStore];
    textBlockMapping.identificationAttributes = @[@"identifier"];
    [textBlockMapping addAttributeMappingsFromDictionary:@{@"id"       : @"identifier",
                                                           @"type"     : @"type",
                                                           @"data"     : @"text",
                                                           @"position" : @"position"}];
    
    // Pages
    RKEntityMapping *pageMapping = [RKEntityMapping mappingForEntityForName:@"Page"
                                                       inManagedObjectStore:objectStore];
    
    pageMapping.identificationAttributes = @[@"identifier"];
    [pageMapping addAttributeMappingsFromDictionary:@{@"id"         : @"identifier",
                                                      @"title"      : @"title",
                                                      @"position"   : @"position"}];
    
    //Resources
    RKEntityMapping *resourceMapping = [RKEntityMapping mappingForEntityForName:@"Resource"
                                                           inManagedObjectStore:objectStore];
    
    resourceMapping.identificationAttributes = @[@"identifier", @"u_id"];
    [resourceMapping addAttributeMappingsFromDictionary:@{@"id"    : @"identifier",
                                                          @"u_id"  : @"u_id",
                                                          @"name"  : @"name",
                                                          @"type"  : @"type"}];
    
    /** Response descriptors */
    
    // Record descriptor
    RKResponseDescriptor *recordResponseDescriptor = [RKResponseDescriptor
                                                      responseDescriptorWithMapping:recordMapping
                                                      method:RKRequestMethodAny
                                                      pathPattern:nil
                                                      keyPath:@"records"
                                                      statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [[RKObjectManager sharedManager] addResponseDescriptor:recordResponseDescriptor];
    
    // Presentation descriptor
    RKResponseDescriptor *presentationResponseDescriptor = [RKResponseDescriptor
                                                            responseDescriptorWithMapping:presentationMapping
                                                            method:RKRequestMethodAny
                                                            pathPattern:nil
                                                            keyPath:@"presentations"
                                                            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [[RKObjectManager sharedManager] addResponseDescriptor:presentationResponseDescriptor];
    
    
    // mosaicPresentationMapping descriptor
    RKResponseDescriptor *presentationMosaicResponseDescriptor = [RKResponseDescriptor
                                                            responseDescriptorWithMapping:mosaicPresentationMapping
                                                            method:RKRequestMethodAny
                                                            pathPattern:nil
                                                            keyPath:@"presentations"
                                                            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [[RKObjectManager sharedManager] addResponseDescriptor:presentationMosaicResponseDescriptor];
    
    
    // Device descriptor
    RKResponseDescriptor *deviceResponseDescriptor = [RKResponseDescriptor
                                                      responseDescriptorWithMapping:deviceMapping
                                                      method:RKRequestMethodAny
                                                      pathPattern:nil
                                                      keyPath:@"device"
                                                      statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [[RKObjectManager sharedManager] addResponseDescriptor:deviceResponseDescriptor];
    
    /** Mapping properties */
    
    // Blocks properties
    RKDynamicMapping* blockDynamicMapping = [RKDynamicMapping new];
    
    [videoBlockMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"data"
                                                                                      toKeyPath:@"files" withMapping:fileMapping]];
    [audioBlockMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"data"
                                                                                      toKeyPath:@"files" withMapping:fileMapping]];
    
    [blockDynamicMapping setObjectMappingForRepresentationBlock:^RKObjectMapping *(id representation) {
        if ([[representation valueForKey:@"type"] isEqualToString:@"text"]) {
            return textBlockMapping;
        } else if ([[representation valueForKey:@"type"] isEqualToString:@"audio"]) {
            return audioBlockMapping;
        } else if ([[representation valueForKey:@"type"] isEqualToString:@"image_video"]) {
            return videoBlockMapping;
        }
        return nil;
    }];
    
    RKDynamicMapping* dynamicMapping = [RKDynamicMapping new];
    
    [dynamicMapping setObjectMappingForRepresentationBlock:^RKObjectMapping *(id representation) {
        if ([[representation objectForKey:@"type"] isEqualToString:@"mosaic"]) {
            return mosaicPresentationMapping;
        } else {
            return presentationMapping;
        }
        
       return nil;
    }];

    
    RKResponseDescriptor *blockResponseDescriptor = [RKResponseDescriptor
                                                     responseDescriptorWithMapping:blockDynamicMapping
                                                     method:RKRequestMethodAny
                                                     pathPattern:nil
                                                     keyPath:@"data"
                                                     statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    [[RKObjectManager sharedManager] addResponseDescriptor:blockResponseDescriptor];
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor
                                                     responseDescriptorWithMapping:dynamicMapping
                                                     method:RKRequestMethodAny
                                                     pathPattern:nil
                                                     keyPath:@"presentations"
                                                     statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    

    [[RKObjectManager sharedManager] addResponseDescriptor:responseDescriptor];
    
    // Record properties
    [recordMapping addPropertyMapping:[RKRelationshipMapping
                                       relationshipMappingFromKeyPath:@"contents"
                                       toKeyPath:@"contents"
                                       withMapping:contentMapping]];
    
    [recordMapping addPropertyMapping:[RKRelationshipMapping
                                       relationshipMappingFromKeyPath:@"image"
                                       toKeyPath:@"image"
                                       withMapping:recordImageMapping]];
    
    // Presentation properties
    [presentationMapping addPropertyMapping:[RKRelationshipMapping
                                             relationshipMappingFromKeyPath:@"children"
                                             toKeyPath:@"records_decriptions"
                                             withMapping:recordDescriptionMapping]];
    [presentationMapping addPropertyMapping:[RKRelationshipMapping
                                             relationshipMappingFromKeyPath:@"contents"
                                             toKeyPath:@"contents"
                                             withMapping:contentMapping]];
    
    [presentationMapping addPropertyMapping:[RKRelationshipMapping
                                             relationshipMappingFromKeyPath:@"resources"
                                             toKeyPath:@"resources"
                                             withMapping:resourceMapping]];
    
    //MosaicItemText properties
    [mosaicItemTextMapping addPropertyMapping:[RKRelationshipMapping
                                           relationshipMappingFromKeyPath:@"(language).title"
                                           toKeyPath:@"title"
                                           withMapping:mosaicItemTextTitleMapping]];
    
    [mosaicItemTextMapping addPropertyMapping:[RKRelationshipMapping
                                               relationshipMappingFromKeyPath:@"(language).description"
                                               toKeyPath:@"desriptionText"
                                               withMapping:mosaicItemTextDescriptionMapping]];
    
    //MapText properties
    [mapTextMapping addPropertyMapping:[RKRelationshipMapping
                                               relationshipMappingFromKeyPath:@"(language).title"
                                               toKeyPath:@"mapTitle"
                                               withMapping:mapTitleMapping]];
    
    [mapTextMapping addPropertyMapping:[RKRelationshipMapping
                                               relationshipMappingFromKeyPath:@"(language).description"
                                               toKeyPath:@"mapDescription"
                                               withMapping:mapDescriptionMapping]];
    
    //MosaicItem properties
    [mosaicItemMapping addPropertyMapping:[RKRelationshipMapping
                                           relationshipMappingFromKeyPath:@"text"
                                           toKeyPath:@"text"
                                           withMapping:mosaicItemTextMapping]];
    
    // MosaicTemplate properties
    [mosaicTemplateMapping addPropertyMapping:[RKRelationshipMapping
                                                   relationshipMappingFromKeyPath:@"elementSizes"
                                                   toKeyPath:@"elementSizes"
                                                   withMapping:mosaicElementSizeMapping]];
    
    // MosaicPresentation properties
    [mosaicPresentationMapping addPropertyMapping:[RKRelationshipMapping
                                             relationshipMappingFromKeyPath:@"children"
                                             toKeyPath:@"mosaic_items"
                                             withMapping:mosaicItemMapping]];
    
    // TODO: REC
    [mosaicPresentationMapping addPropertyMapping:[RKRelationshipMapping
                                             relationshipMappingFromKeyPath:@"contents"
                                             toKeyPath:@"contents"
                                             withMapping:contentMapping]];
    
    [mosaicPresentationMapping addPropertyMapping:[RKRelationshipMapping
                                                   relationshipMappingFromKeyPath:@"template"
                                                   toKeyPath:@"presentation_template"
                                                   withMapping:mosaicTemplateMapping]];
    
    [mosaicPresentationMapping addPropertyMapping:[RKRelationshipMapping
                                             relationshipMappingFromKeyPath:@"resources"
                                             toKeyPath:@"resources"
                                             withMapping:resourceMapping]];
    
    // RecordDescription properties
    [recordDescriptionMapping addPropertyMapping:[RKRelationshipMapping
                                                   relationshipMappingFromKeyPath:@"map_image"
                                                   toKeyPath:@"map_image"
                                                   withMapping:mapImageMapping]];
    
    [recordDescriptionMapping addPropertyMapping:[RKRelationshipMapping
                                                  relationshipMappingFromKeyPath:@"text"
                                                  toKeyPath:@"map_text"
                                                  withMapping:mapTextMapping]];
    
     //Devices properties
    [deviceMapping addPropertyMapping:[RKRelationshipMapping
                                       relationshipMappingFromKeyPath:@"presentations"
                                       toKeyPath:@"presentations"
                                       withMapping:dynamicMapping]];
    
    [deviceMapping addPropertyMapping:[RKRelationshipMapping
                                       relationshipMappingFromKeyPath:@"resources"
                                       toKeyPath:@"resources"
                                       withMapping:resourceMapping]];
    
    // Content properties
    [contentMapping addPropertyMapping:[RKRelationshipMapping
                                        relationshipMappingFromKeyPath:@"(language).pages"
                                        toKeyPath:@"pages"
                                        withMapping:pageMapping]];
    // Pages properties
    [pageMapping addPropertyMapping:[RKRelationshipMapping
                                     relationshipMappingFromKeyPath:@"blocks"
                                     toKeyPath:@"blocks"
                                     withMapping:blockDynamicMapping]];
    
    // RecordImage properties
    [recordImageMapping addPropertyMapping:[RKRelationshipMapping
                                            relationshipMappingFromKeyPath:@"urls"
                                            toKeyPath:@"links"
                                            withMapping:linkMapping]];
    
    // Files properties
    [fileMapping addPropertyMapping:[RKRelationshipMapping
                                     relationshipMappingFromKeyPath:@"credits"
                                     toKeyPath:@"credits"
                                     withMapping:creditMapping]];
    [fileMapping addPropertyMapping:[RKRelationshipMapping
                                     relationshipMappingFromKeyPath:@"url"
                                     toKeyPath:@"links"
                                     withMapping:linkMapping]];
    
    [fileMapping addPropertyMapping:[RKRelationshipMapping
                                     relationshipMappingFromKeyPath:@"contents"
                                     toKeyPath:@"contents"
                                     withMapping:contentMapping]];

    
    // Resourse properties
    [resourceMapping addPropertyMapping:[RKRelationshipMapping
                                         relationshipMappingFromKeyPath:@"urls"
                                         toKeyPath:@"links"
                                         withMapping:linkMapping]];
    
}

- (void)loadProjectFromNetwork {
    // Way to get and store JSON response for current presentation
    [[NSNotificationCenter defaultCenter] postNotificationName:@"initializingStarted"
                                                        object:nil];
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
     [_client getPath:[self pathForMapping]
           parameters:nil
              success:^(AFHTTPRequestOperation *operation, id responseObject) {
    
                  NSError *parseError = nil;
                  NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:(NSData *)responseObject
                                                                           options:kNilOptions
                                                                             error:&parseError];
                  NSLog(@"LOAD URL: %@", [operation.request.URL absoluteString]);
                  NSLog(@"%s: %@", __FUNCTION__, jsonDict);
                  
                  if (parseError) {
                      NSLog(@"Error getting JSON from server: %@", [parseError localizedDescription]);
                      [self mappingDidFailed];
                      return;
                  }
                  
                  _lastUpdateTimeValue = jsonDict[@"last_update"];
                  NSString *previousUpdateTimeValue = (NSString *)[defaults objectForKey:@"previous_update_time"];
     
                  if ([previousUpdateTimeValue isEqualToString:_lastUpdateTimeValue] && !self.newProject) {
                      _isChangedFromLastLaunch = NO;
                  } else {
                      _isChangedFromLastLaunch = YES;
                  }
                  
                  [[RKObjectManager sharedManager] getObjectsAtPath:[self pathForMapping]
                                                         parameters:nil
                                                            success:^(RKObjectRequestOperation *operation,
                                                                      RKMappingResult *mappingResult) {
                                                                NSLog(@"Mapping has finished successfully");
                                                                [self mappingDidFinished];
                                                            } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                                [self mappingDidFailed];
                                                                NSLog(@"Mapping was failed with error: %@", [error localizedDescription]);
                                                            }];
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  NSLog(@"Error getting JSON from server: %@", [error localizedDescription]);
                  [self mappingDidFailed];
              }];
}

/**
 Invoked when the mapping has finished
 */

- (void)mappingDidFinished {
    
    NSArray *linksForAssets = [self linksForAssetsUsingMode:_applicationMode];
    KKAssetsManager *assetsManager = [KKAssetsManager sharedManager];
    
    if (!_isChangedFromLastLaunch) {
        // That means we rolled back to the last version that we can show
        if (_hasNoSpace) {
            [self dismissMemoryWarning];
            if (_applicationMode == KKLaunchingMode) {
                [self commitUpdatesWithCompletionBlock:^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"finishLoadingProgress"
                                                                        object:nil];
                }];
            } else if (_applicationMode == KKUpdatingMode) {
                [self updatesFinished];
            }
        } else {
            [self commitUpdatesWithCompletionBlock:^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"finishLoadingProgress"
                                                                    object:nil];
            }];
        }
        return;
    }
    
    switch (_applicationMode) {
        case KKLaunchingMode: {
            assetsManager.mode = AssetsManagerLaunchingMode;
            [assetsManager loadAssets:linksForAssets
                  withCompletionBlock:^{
                      [self commitUpdatesWithCompletionBlock:^{
                          [[NSNotificationCenter defaultCenter] postNotificationName:@"launchingFinished"
                                                                              object:nil];
                      }];
                  }];
        }
            break;
        case KKUpdatingMode: {
            assetsManager.mode = AssetsManagerUpdatingMode;
            [assetsManager loadAssets:linksForAssets
                  withCompletionBlock:^{
                      if (assetsManager.mode == AssetsManagerLaunchingMode) {
                          [self commitUpdatesWithCompletionBlock:^{
                              [[NSNotificationCenter defaultCenter] postNotificationName:@"launchingFinished"
                                                                                  object:nil];
                          }];
                      } else {
                          
                          _isMovedInWaitingMode = YES;
                          [[NSNotificationCenter defaultCenter] postNotificationName:@"loadingUpdatesFinished"
                                                                              object:nil];
                      }
                  }];
        }
            break;
        case KKInitializingMode: {
            [assetsManager loadAssets:linksForAssets
                  withCompletionBlock:^{
                      [[NSNotificationCenter defaultCenter] postNotificationName:@"initializingFinished"
                                                                          object:nil];
                  }];
        }
            break;
            
        default:
            break;
    }
}

/**
 * Sent when mapping was failed due to an error
 */
- (void)mappingDidFailed {
    [UIAlertView showWithTitle:NSLocalizedString(@"Det har skjedd en feil!",@"")
                       message:NSLocalizedString(@"Sjekk at du har satt riktig Utstillings-ID i innstillinger",@"")
             cancelButtonTitle:@"Retry"
             otherButtonTitles:nil
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          [self loadProjectFromNetwork];
                        }];
}

- (void)initializingFinished {
    
    [self setDefaultLanguage];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"initializingFinished"
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self.window.rootViewController
                                                    name:@"loadedFile"
                                                  object:nil];
    NSError *error;
    // Save the database, so all the downloaded data is retained
    [[RKObjectManager sharedManager].managedObjectStore.mainQueueManagedObjectContext saveToPersistentStore:&error];
    if (error) {
        NSLog(@"Saving to persistent store finished with error: %@", [error localizedDescription]);
    }
    // Remember what project the database contains, so we can delete if it changes
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:[defaults integerForKey:@"device_id"]
                  forKey:@"last_project"];
    [defaults setBool:YES
               forKey:kDataDownoadedKey];
    [defaults setObject:_lastUpdateTimeValue
                 forKey:@"previous_update_time"];
    [defaults synchronize];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    UIViewController *view = [storyboard instantiateInitialViewController];
    self.window.rootViewController = view;
    
    Device *device = [Device objectWithPredicate: [NSPredicate predicateWithFormat: @"identifier == %@" argumentArray:@[[[NSUserDefaults standardUserDefaults] objectForKey: @"device_id"]]]];
    
    if (device.idleScreenActiveValue) {
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"HomeScreen"];
        [view presentViewController:viewController
                           animated:YES
                         completion:^{
                             _isMovedInWaitingMode = YES;
                         }];
    }
    
//    self.inactivityTimeout = [device.resetTime integerValue];
    self.inactivityTimeout = 60;
    
    self.shouldRegisterTouch     = YES;
    self.needToShowDefaultRecord = YES;
    
    _applicationMode = KKRunningMode;
    
    [[KKTimerManager sharedManager] restartTimerWithIdentifier:kUpdatingTimerName];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(restartIdleTimers)
                                                 name:UserDidTouchWindowNotification
                                               object:nil];
    [self startIdleTimers];
}

- (void)doneLoading {
    
    Device *device = [Device objectWithPredicate: [NSPredicate predicateWithFormat: @"identifier == %@" argumentArray:@[[[NSUserDefaults standardUserDefaults] objectForKey: @"device_id"]]]];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    UIViewController *initialView = [storyboard instantiateInitialViewController];
    self.window.rootViewController = initialView;
    if (device.idleScreenActiveValue) {
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"HomeScreen"];
        [initialView presentViewController:viewController
                                  animated:YES
                                completion:^{
                                    _isMovedInWaitingMode = YES;
                                }];
    }
    
    [self setDefaultLanguage];
    self.inactivityTimeout = 60;
//    self.inactivityTimeout = [device.resetTime integerValue];
    
    self.shouldRegisterTouch     = YES;
    self.needToShowDefaultRecord = YES;
    
    [[KKTimerManager sharedManager] restartTimerWithIdentifier:kUpdatingTimerName];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(restartIdleTimers)
                                                 name:UserDidTouchWindowNotification
                                               object:nil];
    [self startIdleTimers];
}

-(void)deleteDefaultCachedData {
    // Delete current database to make room for different data
    NSLog(@"Deleting previous database");
    NSPersistentStoreCoordinator *storeCoordinator = [self persistentStoreCoordinator];
    NSArray *stores = [storeCoordinator persistentStores];
    for (NSPersistentStore *store in stores) {
        NSError *error = nil;
        [storeCoordinator removePersistentStore:store error:&error];
        if (error) {
            NSLog(@"Error: %@", [error localizedDescription]);
            error = nil;
        }
        [[NSFileManager defaultManager] removeItemAtPath:store.URL.path error:&error];
        if (error) {
            NSLog(@"Error: %@", [error localizedDescription]);
            error = nil;
        }
        [[NSFileManager defaultManager] removeItemAtPath:[store.URL.path stringByAppendingString:@"-shm"] error:&error];
        if (error) {
            NSLog(@"Error: %@", [error localizedDescription]);
            error = nil;
        }
        [[NSFileManager defaultManager] removeItemAtPath:[store.URL.path stringByAppendingString:@"-wal"] error:&error];
        if (error) {
            NSLog(@"Error: %@", [error localizedDescription]);
            error = nil;
        }
    }
    
    // Delete the cache dir
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *dir = [paths objectAtIndex:0]; // Get caches folder
    NSString *cacheDir = [dir stringByAppendingPathComponent:@"DownloadedMedia"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:cacheDir]) {
        NSLog(@"Deleting %@", cacheDir);
        [[NSFileManager defaultManager] removeItemAtPath:cacheDir error:nil];
    }
}

#pragma mark - Idle timers

- (void)startIdleTimers {
    [[KKTimerManager sharedManager] startTimerWithIdentifier:kIdleTimerName
                                                     timeout:self.inactivityTimeout - 10];
    [[KKTimerManager sharedManager] startTimerWithIdentifier:kHomeScreenTimerName
                                                     timeout:self.inactivityTimeout];
}

- (void)stopIdleTimers {
    [[KKTimerManager sharedManager] stopTimerWithIdentifier:kIdleTimerName];
    [[KKTimerManager sharedManager] stopTimerWithIdentifier:kHomeScreenTimerName];
}

- (void)restartIdleTimers {
    [[KKTimerManager sharedManager] restartTimerWithIdentifier:kIdleTimerName];
    [[KKTimerManager sharedManager] restartTimerWithIdentifier:kHomeScreenTimerName];
}

#pragma mark - KKTimerManager delegate

- (void)timerTimeoutFinished:(NSTimer *)timer {
    if ([[timer identifier] isEqualToString:kIdleTimerName]) {
        [self showIdleWarningScreen];
        return;
    }
    if ([[timer identifier] isEqualToString:kHomeScreenTimerName]) {
        [self showHomeScreen];
        return;
    }
    if ([[timer identifier] isEqualToString:kUpdatingTimerName]) {
        switch (_applicationMode) {
            case KKUpdatingMode: {
                if (_hasNoSpace) {
                    [self checkForUpdateWithCompletion:^(BOOL needToUpdate) {
                        if (needToUpdate) {
                            [self updateData];
                        } else {
                            //For testing
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"updateIsNotNeeded"
                                                                                object:nil];
                            NSLog(@"Update is not needed");
                        }
                    }];
                } else {
                    NSLog(@"Content is updating now... no need to check updates");
                }
            }
                break;
            case KKLaunchingMode: {
                if (_hasNoSpace) {
                    // Before each launch of this method we have to make hard reset of buffer persistent store
                    [self resetBufferDatabase];
                    [self setupRestKit];
                    [self loadProjectFromNetwork];
                } else {
                    NSLog(@"Content is downloading now... no need to check updates");
                }
            }
                break;
            case KKRunningMode: {
                [self checkForUpdateWithCompletion:^(BOOL needToUpdate) {
                    if (needToUpdate) {
                        [self updateData];
                    } else {
                        //For testing
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"updateIsNotNeeded"
                                                                            object:nil];
                        NSLog(@"Update is not needed");
                    }
                }];
            }
                break;
                
            default:
                break;
        }
        [[KKTimerManager sharedManager] startTimerWithIdentifier:kUpdatingTimerName
                                                         timeout:kTimeToUpdateContent];
        //For testing
        [[NSNotificationCenter defaultCenter] postNotificationName:@"updatingTimerFired"
                                                            object:nil];
        return;
    }
    
    if ([[timer identifier] isEqualToString:kCommitingUpdatesTimer]) {
        if (_hasNoSpace) {
            [self dismissMemoryWarning];
        }
        Device *device=[Device objectWithPredicate: [NSPredicate predicateWithFormat: @"identifier == %@" argumentArray:@[[[NSUserDefaults standardUserDefaults] objectForKey: @"device_id"]]]];
        
        [self dismissUpdatingWarningWithCompletionBlock:^{
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            UIViewController *initialView = [storyboard instantiateInitialViewController];
            self.window.rootViewController = initialView;
            if (device.idleScreenActiveValue) {
                UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"HomeScreen"];
                [initialView presentViewController:viewController
                                          animated:NO
                                        completion:^{
                                            _isMovedInWaitingMode = YES;
                                        }];
            }
        }];
        
        
        [self setDefaultLanguage];
//        self.inactivityTimeout = [device.resetTime integerValue];
        self.inactivityTimeout = 60;
        [[KKTimerManager sharedManager] startTimerWithIdentifier:kUpdatingTimerName
                                                         timeout:kTimeToUpdateContent];
        return;
    }
}

#pragma mark - Languages

- (void)setDefaultLanguage {
    NSArray *availableLanguages = [KKLanguage availableLanguages];
    if ([availableLanguages count]) {
        NSDictionary *firstLanguage = [availableLanguages objectAtIndex:0];
        KKLanguageType defaultLanguage = (KKLanguageType)[[firstLanguage objectForKey:@"id"] intValue];
        [KKLanguage setLanguage:defaultLanguage];
        DLog(@"Language set to %@", [firstLanguage valueForKey:@"title"]);
    }
}

#pragma mark - Updating

- (void)checkForUpdateWithCompletion:(void (^)(BOOL needToUpdate))completionBlock {
    [_client getPath:[self pathForMapping]
          parameters:nil
             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                 NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:(NSData *)responseObject
                                                                          options:kNilOptions
                                                                            error:nil];
                 
                 NSLog(@"%s: %@", __FUNCTION__, jsonDict);
                 
                 if ([_lastUpdateTimeValue isEqualToString:jsonDict[@"last_update"]]) {
                     completionBlock(NO);
                 } else {
                     completionBlock(YES);
                 }
             } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 completionBlock(NO);
             }];
}

- (void)updateData {
    NSLog(@"Updating data... JSONs are not equal");
    _applicationMode = KKUpdatingMode;
    // Before each launch of this method we have to make hard reset of buffer persistent store
    [self resetBufferDatabase];
    [self setupRestKit];
    [self loadProjectFromNetwork];
}

- (void)updatesFinished {
    _isTimeToUpdateContent = YES;
    if (_isMovedInWaitingMode) {
        [self showUpdatingWarning];
        [self commitUpdatesWithCompletionBlock:^{
            //Showing alert about updating for 2 secs
            [[KKTimerManager sharedManager] startTimerWithIdentifier:kCommitingUpdatesTimer
                                                             timeout:2];
        }];
    }
}

- (BOOL)migratePersistentStore:(NSPersistentStoreCoordinator *)storeCoordinator toStoreURL:(NSURL *)storeURL {
    
    BOOL res = NO;
    
    // grab the current store
    NSPersistentStore *currentStore = storeCoordinator.persistentStores.lastObject;
    
    if (currentStore == nil) {
        NSLog(@"Couldn't get currentStore, from store Coordinator: %@", storeCoordinator.persistentStores);
        return res;
    }
    
    // setup new options dictionary if necessary
    NSError *error = nil;
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    
    
    // before migration: check destination url and remove previously created resources if something still here:
    // temporary move persistent store to location
    
    NSFileManager   *fileManager   = [NSFileManager defaultManager];
    
    
    if ([fileManager fileExistsAtPath:[storeURL path]]) {
        // TODO: try to use remove URL
        [fileManager removeItemAtPath:[storeURL path]
                                error:nil];
        [fileManager removeItemAtPath:[[storeURL path] stringByAppendingString:@"-shm"]
                                error:nil];
        [fileManager removeItemAtPath:[[storeURL path] stringByAppendingString:@"-wal"]
                                error:nil];
    }
    
    // migrate current store to new URL
    [storeCoordinator migratePersistentStore:currentStore toURL:storeURL
                                     options:options withType:NSSQLiteStoreType error:&error];
    
    if (error) {
        NSLog(@"Error: %@", error);
    } else {
        NSLog(@"Successufy migrated");
        res = YES;
    }
    
    return res;
}

- (BOOL)updateDatabase {
    
    BOOL res = NO;
    
    NSFileManager   *fileManager   = [NSFileManager defaultManager];
    
    // move store folder
    
    NSURL *mainStoreURL = [KKConstants mainStoreURL];
//    NSURL *tempStoreURL = [KKConstants tempStoreURL];
    
//    NSError *error = nil;
//    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
//                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
//                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    [RKApplicationDataDirectory() listOfFiles];
    
    NSLog(@"STORE URL1: %@", [[[[[RKManagedObjectStore defaultStore] persistentStoreCoordinator] persistentStores] firstObject] URL]);
    
    // migrate current store to existing store
    res = [self migratePersistentStore:[RKManagedObjectStore defaultStore].persistentStoreCoordinator
                            toStoreURL:mainStoreURL];
    
    NSLog(@"STORE URL2: %@", [[[[[RKManagedObjectStore defaultStore] persistentStoreCoordinator] persistentStores] firstObject] URL]);
    
    [RKApplicationDataDirectory() listOfFiles];
    
    
    if (!res) {
        return res;
    }
    
    // clean up system disk
    
    KKAssetsManager *assetsManager = [KKAssetsManager sharedManager];
    
    if (_isChangedFromLastLaunch) {
        [assetsManager deleteUnusedResources];
    }
    
    NSLog(@"Temp folder: %@", [assetsManager pathToTempFolder]);
    
    // Moving resources
    
    NSArray *newResources = [fileManager contentsOfDirectoryAtPath:[assetsManager pathToTempFolder]
                                                             error:nil];
    //Create folder
    if (![[NSFileManager defaultManager] fileExistsAtPath:[assetsManager pathToDefaultFolder]]) {
        NSError *error;
        [[NSFileManager defaultManager] createDirectoryAtPath:[assetsManager pathToDefaultFolder]
                                  withIntermediateDirectories:NO
                                                   attributes:nil
                                                        error:&error];
        if (error) {
            NSLog(@"Error while creating new saving folder: %@",[error localizedDescription]);
            res = NO;
        }
    }
    
    for (NSString *file in newResources) {
        [fileManager moveItemAtPath:[[assetsManager pathToTempFolder] stringByAppendingPathComponent:file]
                             toPath:[[assetsManager pathToDefaultFolder] stringByAppendingPathComponent:file]
                              error:nil];
    }
    
    [fileManager removeItemAtPath:[assetsManager pathToTempFolder]
                            error:nil];
    
    return res;
}

- (void)commitUpdatesWithCompletionBlock:(void (^)())completionBlock {
    
    // save main persistens store for next usage
    RKManagedObjectStore *mainStore = [RKManagedObjectStore defaultStore];
    
    BOOL res = [self migratePersistentStore:mainStore.persistentStoreCoordinator
                            toStoreURL:[KKConstants mainSlaveStoreURL]];
    
    if (!res) {
        NSLog(@"Error: can't save main persistent store to continue safety operations.");
        
        return;
    }
    
    //Replacing persistent stores, temporary storage becomes default, default storage is cleaning up
    [RKManagedObjectStore setDefaultStore:_tempStore];
    [RKObjectManager sharedManager].managedObjectStore = [RKManagedObjectStore defaultStore];
    
    NSError *savingDBError;
    
    // Save the database, so all the downloaded data is retained
    [[RKObjectManager sharedManager].managedObjectStore.mainQueueManagedObjectContext saveToPersistentStore:&savingDBError];
    
    if (savingDBError) {
        NSLog(@"Saving to persistent store finished with error: %@", [savingDBError localizedDescription]);
        
        // TODODEV: recovery from this state
        
        BOOL res = [self migratePersistentStore:mainStore.persistentStoreCoordinator
                                     toStoreURL:[KKConstants mainStoreURL]];
        
        if (!res) {
            NSLog(@"Error: can't recovery main persistent store.");
        } else {
            [RKManagedObjectStore setDefaultStore:mainStore];
            [RKObjectManager sharedManager].managedObjectStore = [RKManagedObjectStore defaultStore];
        }
        
    } else {
        
        [self updateDatabase];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:_lastUpdateTimeValue
                     forKey:@"previous_update_time"];
        [defaults synchronize];
        
        _tempStore = nil;
        _persistentStoreCoordinatorForUpdate = nil;
        _applicationMode = KKRunningMode;
        _isTimeToUpdateContent = NO;
    }
    
    completionBlock();
}

- (NSArray *)linksForAssetsUsingMode:(KKApplicationMode)mode {
    
    NSMutableArray *files = [NSMutableArray new];
    KKAssetsManager *assetsManager = [KKAssetsManager sharedManager];
    RKManagedObjectStore *currentStore = [RKManagedObjectStore defaultStore];
    
    NSLog(@"Started counting assets");
    if (mode == KKUpdatingMode || mode == KKLaunchingMode) {
        // Replacing persistant stores in order to get files info for downloading new resources
        // Setting default storage to the new one
        [RKManagedObjectStore setDefaultStore:_tempStore];
    }
    
    for (File *file in [File allObjects]) {
        for (Link *link in [file links]) {
            NSString *fileName = [NSString stringWithFormat:@"%@-%@-%@",
                                  link.type,
                                  [[NSURL URLWithString:link.url] lastPathComponent],
                                  link.file.name];
            link.localUrl = [[assetsManager pathToDefaultFolder] stringByAppendingPathComponent:fileName];
            NSDictionary *fileData = nil;
            // Temp
            if ([link.type isEqualToString:@"original"] && [link.file.type isEqualToString:@"image"]) {
                fileData = @{@"fileName"    : fileName,
                             @"fileURL"     : [self checkURL:link.url inLink:link],
                             @"fileURLTemp" : [self checkURL:link.url2 inLink:link]};
            } else {
                fileData = @{@"fileName" : fileName,
                             @"fileURL"  : [self checkURL:link.url inLink:link]};
            }
            [files addObject:fileData];
        }
    }
    
    for (RecordImage *recordImage in [RecordImage allObjects]) {
        for (Link *link in [recordImage links]) {
            NSString *fileName = [NSString stringWithFormat:@"%@-%@-%@",
                                  link.type,
                                  [[NSURL URLWithString:link.url] lastPathComponent],
                                  link.record_image.name];
            link.localUrl = [[assetsManager pathToDefaultFolder] stringByAppendingPathComponent:fileName];
            NSDictionary *fileData = nil;
            if ([link.type isEqualToString:@"original"]) {
                fileData = @{@"fileName"    : fileName,
                             @"fileURL"     : [self checkURL:link.url inLink:link],
                             @"fileURLTemp" : [self checkURL:link.url2 inLink:link]};
            } else {
                fileData = @{@"fileName" : fileName,
                             @"fileURL"  : [self checkURL:link.url inLink:link]};
            }
            [files addObject:fileData];
            
        }
    }
    
    for (Resource *resourse in [Resource allObjects]) {
        for (Link *link in [resourse links]) {
            NSString *fileName = [NSString stringWithFormat:@"%@-%@-%@",
                                  link.type,
                                  [[NSURL URLWithString:link.url] lastPathComponent],
                                  link.resource.name];
            link.localUrl = [[assetsManager pathToDefaultFolder] stringByAppendingPathComponent:fileName];
            NSDictionary *fileData = nil;
            if ([link.type isEqualToString:@"original"]) {
                fileData = @{@"fileName"    : fileName,
                             @"fileURL"     : [self checkURL:link.url inLink:link],
                             @"fileURLTemp" : [self checkURL:link.url2 inLink:link]};
            } else {
                fileData = @{@"fileName" : fileName,
                             @"fileURL"  : [self checkURL:link.url inLink:link]};
            }
            [files addObject:fileData];
        }
    }
    
    
    if (mode == KKUpdatingMode || mode == KKLaunchingMode) {
        [RKManagedObjectStore setDefaultStore:currentStore];
    }
    NSLog(@"Finished counting assets");
    
    return files;
}

- (NSString *)checkURL:(NSString *)url inLink:(Link *)link {

    if (url && [url length] > 0) {
        return url;
    }
    
    // TODO: ask Jon to fix database for this case
    return @"https://www.google.com.ua/search?q=picture&client=firefox-b&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiIluzXiIbSAhXoFJoKHZ6bCjkQ_AUICCgB#";
        
    [self dataBaseError:NSLocalizedString(@"Error occurred during receiving database! Found empty URL",@"")];
    
    NSLog(@"URL is empty by u_id: %@", link.u_id);

    return @"";
 }

- (void) dataBaseError:(NSString *)errorMassage {
    
    __weak typeof(self) weakSelf = self;

    [UIAlertView showWithTitle:NSLocalizedString(@"Failed!",@"")
                       message:errorMassage
             cancelButtonTitle:NSLocalizedString(@"Ok",@"")
             otherButtonTitles:nil
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          if ([weakSelf.window.rootViewController isKindOfClass:[KKLoadingViewController class]]) {
                              
                              [self showRetryAlertWithBackUpMode:YES];
                              
                          } else {
                              
                              [weakSelf showHomeScreen];
                              
                          }
                      }];
}

- (void)resetBufferDatabase {
    NSLog(@"Reseting buffer database");
    NSPersistentStoreCoordinator *storeCoordinator = [self persistentStoreCoordinatorForUpdate];
    NSArray *stores = [storeCoordinator persistentStores];
    
    for (NSPersistentStore *store in stores) {
        [storeCoordinator removePersistentStore:store error:nil];
        [[NSFileManager defaultManager] removeItemAtPath:store.URL.path error:nil];
    }
    
    [[NSFileManager defaultManager] removeItemAtPath:[RKApplicationDataDirectory()
                                                      stringByAppendingPathComponent:@"Kulturkiosk_temp.sqlite"]
                                               error:nil];
    [[NSFileManager defaultManager] removeItemAtPath:[RKApplicationDataDirectory()
                                                      stringByAppendingPathComponent:@"Kulturkiosk_temp.sqlite-shm"]
                                               error:nil];
    [[NSFileManager defaultManager] removeItemAtPath:[RKApplicationDataDirectory()
                                                      stringByAppendingPathComponent:@"Kulturkiosk_temp.sqlite-wal"]
                                               error:nil];
    
    // Delete the cache dir
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *dir = [paths objectAtIndex:0]; // Get caches folder
    NSString *cacheDir = [dir stringByAppendingPathComponent:@"TemporaryDownloadedMedia"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:cacheDir]) {
        NSLog(@"Deleting %@", cacheDir);
        [[NSFileManager defaultManager] removeItemAtPath:cacheDir error:nil];
    }
    
    _tempStore = nil;
    _persistentStoreCoordinatorForUpdate = nil;
}

#pragma mark - Private

- (void)showRetryAlertWithBackUpMode:(BOOL)ableToBackUp {
    NSInteger last_project = [[NSUserDefaults standardUserDefaults] integerForKey:@"last_project"];
    NSInteger current_project = [[NSUserDefaults standardUserDefaults] integerForKey:@"device_id"];
    
    __weak typeof(self) weakSelf = self;
    __block void(^retryBlock)(void) = ^{
        
        _reachabilityAlertView = [UIAlertView showWithTitle:ableToBackUp ? @"Enheten er ikke koblet til Internett." :
                                  @"Missing network connection?"
                                                    message:ableToBackUp ? [NSString stringWithFormat:@"Enhenten viser innhold fra id: %ld. Enheten må være koblet til Internett og appen må restartes for å vise innhold for id: %ld.",(long)current_project , (long)last_project] : @"Must be able to connect to backend to load new project."
                                          cancelButtonTitle:ableToBackUp ? @"Prøv igjen" : @"Retry"
                                          otherButtonTitles:ableToBackUp ? @[@"Bruk forrige"] : nil
                                                   tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                                       if (buttonIndex == 0) {
                                                           dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                                               if (weakSelf.client.networkReachabilityStatus == AFNetworkReachabilityStatusNotReachable) {
                                                                   [weakSelf showRetryAlertWithBackUpMode:ableToBackUp];
                                                               }
                                                           });
                                                       } else {
                                                           weakSelf.newProject = NO;
                                                           [[NSUserDefaults standardUserDefaults] setInteger:last_project
                                                                                                      forKey:@"device_id"];
                                                           // Before each launch of this method we have to make hard reset of buffer persistent store
                                                           weakSelf.applicationMode = KKInitializingMode;
                                                           [weakSelf setupRestKit];
                                                           [weakSelf mappingDidFinished];
                                                       }
                                                   }];
    };
    
    retryBlock();
}

- (NSString *)pathForMapping {
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSInteger device   = [defaults integerForKey:@"device_id"];
    // NSString *path = [NSString stringWithFormat:@"device/%ld/load_view",(long)device];
    NSString *path = [NSString stringWithFormat:@"kiosk/device/%ld",(long)device];
    
    return path;
}

- (void)showHomeScreen {
    KKNavigationViewController *navigationViewController = (KKNavigationViewController*) self.window.rootViewController;
    UIViewController *vc = nil;
    
    if (navigationViewController.activeViewController.presentedViewController) {
        vc = navigationViewController.activeViewController.presentedViewController;
    } else {
        vc = self.window.rootViewController;
    }
    
    if (![navigationViewController.presentedViewController isKindOfClass:[KKHomeScreenViewController class]]) {
    // Dismiss idlescreen
        UIView *warningViewContainer = [_window.subviews lastObject];
        KKWarningView *warningView = [warningViewContainer.subviews lastObject];
        [UIView animateWithDuration:0.35
                         animations:^{
                             warningView.alpha = 0.0;
                         }
                         completion:^(BOOL finished) {
                             if (finished) {
                                 [warningView removeFromSuperview];
                             }
                         }];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:TimerDidEndNotification
                                                        object:nil];
    _needToShowDefaultRecord = YES;
    _isMovedInWaitingMode = YES;
}

- (void)showIdleWarningScreen {
    KKNavigationViewController *navigationViewController = (KKNavigationViewController*) self.window.rootViewController;
    
    if ([navigationViewController.presentedViewController isKindOfClass:[KKHomeScreenViewController class]]) {
        return;
    }
    
    KKWarningView *warningView = [[[NSBundle mainBundle] loadNibNamed:@"KKWarningView" owner:self options:0] objectAtIndex:0];
    
    warningView.warningTitle.text = [KKLanguage localizedString:@"TimeoutTitle"];
    warningView.warningMessage.text = [KKLanguage localizedString:@"TimeoutMessage"];
    warningView.alpha = 0.0;
    UIView *view = [_window.subviews lastObject];
    warningView.frame = UIScreen.mainScreen.bounds;
    [view addSubview:warningView];
    [UIView animateWithDuration:0.35
                     animations:^{
                         warningView.alpha = 1.0;
                     }];
     
/*
    KKNavigationViewController *navigationViewController = (KKNavigationViewController*) self.window.rootViewController;
    UIViewController *vc = nil;
    
    if ([navigationViewController.presentedViewController isKindOfClass:[KKHomeScreenViewController class]]) {
        return;
    }
    
    if (navigationViewController.activeViewController.presentedViewController) {
        vc = navigationViewController.activeViewController.presentedViewController;
    } else {
        vc = self.window.rootViewController;
    }
    
    if ([navigationViewController.activeViewController isKindOfClass: [KKMapViewController class]] && [[(KKMapViewController*)navigationViewController.activeViewController presentingPopover] isPopoverVisible]) {
        [[(KKMapViewController*)navigationViewController.activeViewController presentingPopover] dismissPopoverAnimated: NO];
    }
    KKWarningView *view = [[[NSBundle mainBundle] loadNibNamed:@"KKWarningView" owner:self options:0] objectAtIndex:0];
    
    view.warningTitle.text = [KKLanguage localizedString:@"TimeoutTitle"];
    view.warningMessage.text = [KKLanguage localizedString:@"TimeoutMessage"];
    
    UIView *presentingView = vc.view;
    
    if (vc.presentedViewController){
        presentingView = vc.presentedViewController.view;
    }
    
    [presentingView addSubview:view];
     */
}

- (void)didQuitWaitingMode {
    _isMovedInWaitingMode = NO;
    NSLog(@"Moved from waiting mode");
}

- (void)movedInWaitingMode {
    NSLog(@"Moved in waiting mode");
    
    [UIApplication sharedApplication].keyWindow.userInteractionEnabled = YES;
    
    if (_needToShowLoadingView) {
        _needToShowLoadingView = NO;
        NSInteger resourcesCount = [KKAssetsManager sharedManager].currentAssetsCount;
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        UIViewController *initialView = [storyboard instantiateInitialViewController];
        self.window.rootViewController = initialView;
        
        KKLoadingViewController *loadingViewController = [storyboard instantiateViewControllerWithIdentifier:@"LoadingView"];
        loadingViewController.totalResourcesCount = resourcesCount;
        
        [initialView presentViewController:loadingViewController
                                  animated:YES
                                completion:^{
                                    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                                                    name:UserDidTouchWindowNotification
                                                                                  object:nil];
                                    [[KKTimerManager sharedManager] stopTimerWithIdentifier:kHomeScreenTimerName];
                                    [[KKTimerManager sharedManager] stopTimerWithIdentifier:kIdleTimerName];
                                }];
        return;
    }
    
    if (_hasNoSpace) {
        KKMemoryAlertView *alertView = [[[NSBundle mainBundle] loadNibNamed:@"KKMemoryAlertView"
                                                                      owner:self
                                                                    options:0] objectAtIndex:0];
        alertView.alpha = 0.0;
        UIView *view = [_window.subviews lastObject];
        [view addSubview:alertView];
        [UIView animateWithDuration:0.35
                         animations:^{
                             alertView.alpha = 1.0;
                         }];
        return;
    }
    
    if (_isTimeToUpdateContent) {
        [self showUpdatingWarning];
        [self commitUpdatesWithCompletionBlock:^{
            //Showing alert about updating for 2 secs
            [[KKTimerManager sharedManager] startTimerWithIdentifier:kCommitingUpdatesTimer
                                                             timeout:2];
        }];
    }
}

- (void)loadingInitialized:(NSNotification *)notification {
    NSInteger resourcesCount = [notification.userInfo[@"assetsCount"] integerValue];
    BOOL needToShowHomeScreen = [notification.userInfo[@"needToShowHomeScreen"] boolValue];
    
    if (needToShowHomeScreen && !_isMovedInWaitingMode) {
        _needToShowLoadingView = YES;
        [UIApplication sharedApplication].keyWindow.userInteractionEnabled = NO;
        [self showHomeScreen];
    } else {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        UIViewController *initialView = [storyboard instantiateInitialViewController];
        self.window.rootViewController = initialView;
        
        KKLoadingViewController *loadingViewController = [storyboard instantiateViewControllerWithIdentifier:@"LoadingView"];
        loadingViewController.totalResourcesCount = resourcesCount;
        
        [initialView presentViewController:loadingViewController
                                  animated:YES
                                completion:^{
                                    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                                                    name:UserDidTouchWindowNotification
                                                                                  object:nil];
                                    [[KKTimerManager sharedManager] stopTimerWithIdentifier:kHomeScreenTimerName];
                                    [[KKTimerManager sharedManager] stopTimerWithIdentifier:kIdleTimerName];
                                }];
    }
}

- (void)showMemoryWarning {
    /*@"You have reached the storage limit on your device, please clear some space, then try again"*/
    if (!_hasNoSpace) {
        _hasNoSpace = YES;
        if (_applicationMode == KKInitializingMode) {
            _applicationMode = KKLaunchingMode;
        }
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:UserDidTouchWindowNotification
                                                      object:nil];
        [[KKTimerManager sharedManager] stopTimerWithIdentifier:kHomeScreenTimerName];
        [[KKTimerManager sharedManager] stopTimerWithIdentifier:kIdleTimerName];
        
        if (_applicationMode == KKUpdatingMode && !_isMovedInWaitingMode) {
            [UIApplication sharedApplication].keyWindow.userInteractionEnabled = NO;
            [self showHomeScreen];
        } else {
            KKMemoryAlertView *alertView = [[[NSBundle mainBundle] loadNibNamed:@"KKMemoryAlertView"
                                                                          owner:self
                                                                        options:0] objectAtIndex:0];
            alertView.alpha = 0.0;
            UIView *view = [_window.subviews lastObject];
            [view addSubview:alertView];
            [UIView animateWithDuration:0.35
                             animations:^{
                                 alertView.alpha = 1.0;
                             }];
        }
    }
}

- (void)dismissMemoryWarning {
    if (_hasNoSpace) {
        _hasNoSpace = NO;
        
        UIView *alertViewContainer = [_window.subviews lastObject];
        KKMemoryAlertView *alertView = [alertViewContainer.subviews lastObject];
        [UIView animateWithDuration:0.35
                         animations:^{
                             alertView.alpha = 0.0;
                         }
                         completion:^(BOOL finished) {
                             if (finished) {
                                 [alertView removeFromSuperview];
                             }
                         }];
    }
}

- (void)showUpdatingWarning {
    KKUpdatingWarningView *warningView = [[[NSBundle mainBundle] loadNibNamed:@"KKUpdatingWarningView"
                                                                        owner:self
                                                                      options:0] objectAtIndex:0];
    warningView.updatingContentLabel.font = [UIFont fontWithName:@"MyriadPro-Cond" size:26];
    warningView.alpha = 0.0;
    UIView *view = [_window.subviews lastObject];
    [view addSubview:warningView];
    [UIView animateWithDuration:0.35
                     animations:^{
                         warningView.alpha = 1.0;
                     }];
}

- (void)dismissUpdatingWarningWithCompletionBlock:(void (^)())completionBlock {
    UIView *warningView = [_window.subviews lastObject];
    [UIView animateWithDuration:0.25
                     animations:^{
                         warningView.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                         if (finished) {
                             [warningView removeFromSuperview];
                             if (completionBlock) {
                                 completionBlock();
                             }
                         }
                     }];
}

#pragma mark - Emergency reload

- (void)emergencyReload {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    UIViewController *view = [storyboard instantiateInitialViewController];
    self.window.rootViewController = view;
}



@end
