//
//  KKImageVideoGalleryViewController.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 1/29/16.
//
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

#import "KKGalleryViewController.h"

@interface KKImageVideoGalleryViewController : KKGalleryViewController

@property NSUInteger startGalleryWithIndex;

@end
