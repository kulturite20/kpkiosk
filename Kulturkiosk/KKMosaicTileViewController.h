//
//  KKMosaicTileViewController.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 2/20/17.
//
//

#import <UIKit/UIKit.h>
#import "MosaicItem+CoreDataClass.h"
#import "KKPresentationViewController.h"

@interface KKMosaicTileViewController : KKPresentationViewController

@property (strong, nonatomic) MosaicItem *mosaicItem;
@property (weak, nonatomic) IBOutlet UIImageView *tileImageView;
@property (weak, nonatomic) IBOutlet UIButton *tileButton;
@property (nonatomic, copy) void (^tapBlock)(MosaicItem *mosaicItem, BOOL isRecordTile);
@property (weak, nonatomic) IBOutlet UILabel *topTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

- (void)addMosaicItem:(MosaicItem*)mosaicItem;

@end
