//
//  KKContainerViewController.m
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 13.06.13.
//
//

#import "KKContainerViewController.h"
#import "KKMapViewController.h"
#import "KKTimeLineViewController.h"
#import "KKContentViewController.h"
#import "KKNavigationViewController.h"
#import "KKIndexViewController.h"

@implementation KKContainerViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    //REF
    /*
    if  (self.presentation)
        self.node= [self.presentation node];
    
    [(KKContentViewController*)self.childViewControllers[0] setNode:self.node];
     */
}
    //REF
/*
- (void)setNode:(Nodes *)node
{
    if (node) {
        if (_node != node)
            _node = node;
        
        if (self.childViewControllers.count)
            [(KKContentViewController*)self.childViewControllers[0] setNode:_node];
    }
}
 */

- (void)setPresentation:(Presentation *)presentation
{
    if (presentation) {
        if (self.presentation != presentation)
            _presentation = presentation;
    }
}
@end
