//
//  RBViewController.m
//  dasdas
//
//  Created by Rune Botten on 09.08.12.
//  Copyright (c) 2012 Rune Botten. All rights reserved.
//

#import "KKTreeNavigationController.h"
#import "KKTreeContentRowController.h"

@interface KKTreeNavigationController ()

@property (strong) NSMutableArray *rows;
@property (strong) UINib *collectionNib, *contentRowNib;

@end

@implementation KKTreeNavigationController

@synthesize rows, collectionNib, contentRowNib;


-(void) prepareData
{
  // Add the children of the toplevel project root node
    //REF
    /*
  NSArray *childs = [[Nodes rootNode] childrenForLanguage:[KKLanguage currentLanguage]];
  NSMutableArray *childsSorted = [NSMutableArray arrayWithCapacity:[childs count]];
  for(Nodes *n in childs)
    if([n hasContentForLanguage:[KKLanguage currentLanguage]])
      [childsSorted addObject:n];
  
  [childsSorted sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"position" ascending:YES]]];
  
  [self createRowWithNodes:childsSorted];
     */
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
  
  // Keep the tableViewCells
  self.rows = [[NSMutableArray alloc] initWithCapacity:10];

  // Register some NIBS we use
  [self.tableView registerNib:[UINib nibWithNibName:@"TableRow" bundle:nil] forCellReuseIdentifier:@"TableRowCell"];
  self.collectionNib = [UINib nibWithNibName:@"Collection" bundle:nil];
  self.contentRowNib = [UINib nibWithNibName:@"RecordCollection" bundle:nil];
  
  [self prepareData];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeLanguage:) name:@"languageChange" object:nil];
}

-(void) dealloc
{
  [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) didChangeLanguage:(NSNotification*) notification
{
  self.rows = [NSMutableArray array];
  [self prepareData];
  [self.tableView reloadData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  return self.tableView.bounds.size.height * (CGFloat)0.5;
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
  return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return (NSInteger)[self.rows count];
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TableRowCell"];
  
  // Switch the content
  for(UIView *s in cell.contentView.subviews) {
    [s removeFromSuperview];
  }
  
  UIViewController *vc = [self.rows objectAtIndex:(NSUInteger)indexPath.row];
  [cell.contentView addSubview:vc.view];

  // Make a label if its a content row (ie, bottom row)
  if([vc class] == [KKTreeContentRowController class]) {

    KKTreeContentRowController *controller = (KKTreeContentRowController*)vc;
    CGRect frame = CGRectMake(0, 30, 1024, 50);

    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];

    // Color and font taken from label in start screen
    label.font = [UIFont fontWithName:@"Baskerville" size:40];
    label.textColor = [UIColor colorWithRed:0.651 green:0.592 blue:0.471 alpha:1.000];
      //REF
    /*
    Content *content = [controller.node contentForLanguage:[KKLanguage currentLanguage]];
    label.text = [content.title uppercaseString];
    label.textAlignment = NSTextAlignmentCenter;
     */

    // Move the collectionView down a bit
    CGRect collectionFrame = [controller collectionView].frame;

    collectionFrame.origin.y = 50;
    [controller collectionView].frame = collectionFrame;

    [cell.contentView addSubview:label];
  }

  return cell;
}

/*
 * Adds a row of content items. This happens when the user selects a 'record' node.
 * We add items for text, pictures and video.
 *
 */
//REF
/*
-(NSIndexPath*) createContentRowForNode:(Nodes*) node
{
  NSArray *nibObjects = [self.contentRowNib instantiateWithOwner:self options:nil];
  KKTreeContentRowController *vc = [nibObjects objectAtIndex:0];
  
  vc.delegate = self;
  vc.node = node;
  
  [self.rows insertObject:vc atIndex:[self.rows count]];
  [self addChildViewController:vc];
  
  NSIndexPath *ip = [NSIndexPath indexPathForItem:(NSInteger)[self.rows count] - 1 inSection:0];
  return ip;
}
 */

/*
 * Adds a row of subnodes for the selected 'folder' node.
 *
 */
//REF
/*
-(NSIndexPath*) createRowWithNodes:(NSArray*) nodes
{
  NSArray *nibObjects = [self.collectionNib instantiateWithOwner:self options:nil];
  KKTreeCollectionViewController *vc = [nibObjects objectAtIndex:0];

  vc.delegate = self;
  vc.nodes = nodes;
  
  [self.rows insertObject:vc atIndex:[self.rows count]];
  [self addChildViewController:vc];
  
  NSIndexPath *ip = [NSIndexPath indexPathForItem:(NSInteger) [self.rows count] - 1 inSection:0];
  return ip;
}
 */

-(void) insertRowWithIndexPath:(NSIndexPath*) indexPath
{
  [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
}

/*
 * Removes all rows from index and and to the bottom
 */
-(void) removeRowsFromIndex:(NSUInteger) index
{
  for(NSUInteger i = index; i < [self.rows count]; i++) {
    UIViewController *v = [self.rows objectAtIndex:i];
    [v.view removeFromSuperview];
    [self.rows removeObjectAtIndex:i];
    NSIndexPath *ip = [NSIndexPath indexPathForItem:(NSInteger)[self.rows count] inSection:0];
    [self.tableView deleteRowsAtIndexPaths:@[ip] withRowAnimation:UITableViewRowAnimationAutomatic];
  }
}

//REF
/*
-(void) collectionViewController:(KKTreeCollectionViewController *)collectionViewController
                   didSelectNode:(Nodes *)node atIndex:(NSInteger)index
{
  NSUInteger rowIndex = [self.rows indexOfObject:collectionViewController];
  
  [self.tableView beginUpdates];
  // Remove rows below this one
  while(rowIndex < [self.rows count]-1) {
    [self removeRowsFromIndex:[self.rows count]-1];
  }
  
  NSIndexPath *ip = nil;
  if(node.nodeType == KKTypeFolder)
  {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"parent_id == %@", node];
    
    // Filter out the ones with content for current language
    NSMutableArray *array = [NSMutableArray array];
    for(Nodes *n in [Nodes objectsWithPredicate:predicate])
      if([n hasContentForLanguage:[KKLanguage currentLanguage]])
        [array addObject:n];
    ip = [self createRowWithNodes:array];
  } else if(node.nodeType == KKTypeRecord)
  {
    ip = [self createContentRowForNode:node];
  }
  
  [self insertRowWithIndexPath:ip];
  [self.tableView endUpdates];
  
  if(self.tableView.contentSize.height > self.tableView.frame.size.height)
    [self.tableView scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}
 */

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

@end
