//
//  KKSlideShowPresentationViewController.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 7/11/16.
//
//

#import "KKPresentationViewController.h"

@class KKSlideShowPresentationViewController;

@protocol KKSlideShowPresentationViewControllerDelegate <NSObject>

- (void)slideShowControllerDidDismiss:(KKSlideShowPresentationViewController *)slideShowController;

@end

@interface KKSlideShowPresentationViewController : KKPresentationViewController
<
UICollectionViewDelegate,
UICollectionViewDataSource
>

@property (nonatomic, weak) id <KKSlideShowPresentationViewControllerDelegate> navBarDelegate;

@end
