//
//  KKRecordMovieViewController.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 7/18/14.
//
//

#import <UIKit/UIKit.h>
#import "File.h"

//For testing
#import "TestFile.h"

@interface KKMediaItemViewController : UIViewController

@property (nonatomic, strong) File *mediaItem;
@property (nonatomic, assign) BOOL isInVideoGalleryMode;
@property (nonatomic, assign) BOOL isInFullscreenMode;
@property (nonatomic, assign) BOOL isInSlideShowMode;
@property (nonatomic, assign, readonly) BOOL isPlayingMediaItem;
@property (nonatomic) BOOL shouldPlay;

/** 
 *  Tells if given touch in slider's area.
 *  Used to give 1st priority for slider to get touches instead of scroll view's
 */
- (BOOL)isTouchInSliderArea:(UITouch *)touch;
- (void)play;
- (void)pauseVideoPlayer;

- (void)refreshVideoLayerFrameWith:(CGRect)rect;

// Special flag for full screen mode
@property (nonatomic, assign) BOOL inFixFullScreenMode;

@end
