//
//  KKRecordMovieContainerViewController.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 29.07.14.
//
//

//Controllers
#import "KKMediaItemViewController.h"
#import "KKMediaItemsContainerViewController.h"

// Model
#import "KKDataManager.h"

// 3rd party
#import "DMLazyScrollView.h"

@interface KKMediaItemsContainerViewController ()
<
DMLazyScrollViewDelegate
>
@end

@implementation KKMediaItemsContainerViewController {
    // To reshrink controller
    BOOL _isPresentingDetailedInfo;
    BOOL _hasSomethingToPresent;
    BOOL _needToPresentDetailedInfoView;
    
    __weak IBOutlet DMLazyScrollView *_lazyScrollView;
    __weak IBOutlet UIPageControl    *_pageControl;
    // bottom bar pannel with page control
    __weak IBOutlet UIView *_infoContainerView;
    
    __weak IBOutlet UILabel *_titleLabel;
    __weak IBOutlet UILabel *_descriptionLabel;
    __weak IBOutlet UILabel *_creditsLabel;
    __weak IBOutlet UIButton *_detailedInfoButton;
    __weak IBOutlet UIView   *_noDataView;
    
    __weak IBOutlet UIButton *_rightArrowButton;
    __weak IBOutlet UIButton *_leftArrowButton;
    
    NSMutableArray *_imageVideoMediaItems;
    
    CGRect _initialSelfFrame;
    CGRect _initialInfoFrame;

}

#pragma mark - View's Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    _pageControl.currentPage = 0;
    
    _isPresentingDetailedInfo = NO;
    _hasSomethingToPresent = NO;
    _needToPresentDetailedInfoView = NO;
    
    _initialSelfFrame = self.view.frame;
    _initialInfoFrame = _infoContainerView.frame;
    
    // Settuping datasource.
    NSMutableArray *controllersForMediaItems = [NSMutableArray new];
    NSArray *sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"position"
                                                               ascending:YES]];
    NSArray *sortedFiles = [[_videoBlock files] sortedArrayUsingDescriptors:sortDescriptors];
    
    for (File *file in sortedFiles) {
        KKMediaItemViewController *mediaItemViewController = [KKMediaItemViewController new];
        mediaItemViewController.mediaItem = file;
        
        // For autoplay video if video first resource.
        if ([file.position intValue] == 1 && self.shouldPlayAtStart) {
            mediaItemViewController.shouldPlay = YES;
        }
        
        mediaItemViewController.isInVideoGalleryMode = NO;
        [controllersForMediaItems addObject:mediaItemViewController];
    }
    
    _lazyScrollView.translatesAutoresizingMaskIntoConstraints = NO;
    
    if ([controllersForMediaItems count] > 0) {
        _imageVideoMediaItems = [[NSArray arrayWithArray:controllersForMediaItems] mutableCopy];
        
        if ([_imageVideoMediaItems count] > 1) {
            _leftArrowButton.hidden  = NO;
            _rightArrowButton.hidden = NO;
        }
        
        if (_imageVideoMediaItems.count > 1) {
            _pageControl.hidden = false;
        } else {
            _pageControl.hidden = true;
        }
        
        __weak __typeof (&*self) weakSelf = self;
        _lazyScrollView.dataSource = ^(NSUInteger index) {
            return [weakSelf imageVideoMediaItemAtIndex:index];
        };
    
        _lazyScrollView.numberOfPages = [_imageVideoMediaItems count];
        _lazyScrollView.controlDelegate = self;
        [_lazyScrollView setEnableCircularScroll:YES];
    
        // Setting number of pages for page controller to show pages indication
        _pageControl.numberOfPages = [_imageVideoMediaItems count];
        
        // Refresh data
        [self setupInfoForController:[self imageVideoMediaItemAtIndex:0]];
    } else {
        [_noDataView setHidden:NO];
        
        // return delegate for remove from data array this item
        if ([_delegate respondsToSelector:(@selector(removeFromSourceItem:))]) {
            [_delegate removeFromSourceItem:self];
        }
    }
}

- (void)dealloc {
    [_imageVideoMediaItems removeAllObjects];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

#pragma mark - Callbacks

- (IBAction)didTapContainer:(UITapGestureRecognizer *)sender {
    NSArray *sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"position"
                                                               ascending:YES]];
    NSArray *mediaItemsForGallery = [[_videoBlock files] sortedArrayUsingDescriptors:sortDescriptors];
    [self.delegate showImageVideoGalleryWithStartingIndex:_pageControl.currentPage andData:mediaItemsForGallery];
}

- (IBAction)previousMediaItemDidClick:(UIButton *)sender {
    if ([_pageControl currentPage] == 0) {
        [_lazyScrollView setPage:([_imageVideoMediaItems count] - 1)
                      transition:DMLazyScrollViewTransitionBackward
                        animated:YES];
        return;
    }
    
    [_lazyScrollView setPage:([_pageControl currentPage] - 1)
                  transition:DMLazyScrollViewTransitionBackward
                    animated:YES];
}

- (IBAction)nextMediaItemDidClick:(UIButton *)sender {
    if (([_pageControl currentPage] + 1) == [_imageVideoMediaItems count]) {
        [_lazyScrollView setPage:0
                      transition:DMLazyScrollViewTransitionForward
                        animated:YES];
        return;
    }
    
    [_lazyScrollView setPage:([_pageControl currentPage] + 1)
                  transition:DMLazyScrollViewTransitionForward
                    animated:YES];
}

- (IBAction)infoButtonClicked:(id)sender {
    
    if (!_hasSomethingToPresent) {
        return;
    }
    
    _isPresentingDetailedInfo = !_isPresentingDetailedInfo;

    [self showDescription:_isPresentingDetailedInfo andTitle:YES];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"shouldInvalidateLayout"
                                                       object:self];
}

#pragma mark - UIScrollViewController Delegate

//- (void)lazyScrollViewDidScroll:(DMLazyScrollView *)pagingView at:(CGPoint)visibleOffset {
//    
//}

- (void)lazyScrollView:(DMLazyScrollView *)pagingView currentPageChanged:(NSInteger)currentPageIndex {
    [UIAppDelegate startIdleTimers];
    UIAppDelegate.shouldRegisterTouch = YES;
    _pageControl.currentPage = currentPageIndex;
    

    [self.view setFrame:_initialSelfFrame];
    [_infoContainerView setFrame:_initialInfoFrame];
    
    [self setupInfoForController:[self imageVideoMediaItemAtIndex:currentPageIndex]];
    
    //if (_isPresentingDetailedInfo) {
        // Detailed info for another controller can request more space, so invalidate layout.
        [[NSNotificationCenter defaultCenter] postNotificationName:@"shouldInvalidateLayout"
                                                            object:self];
   // }
}

- (BOOL)lazyScrollView:(DMLazyScrollView *)scrollView shouldReceiveTouch:(UITouch *)touch {
    KKMediaItemViewController *itemController = (KKMediaItemViewController *)scrollView.visibleViewController;
    
    return ![itemController isTouchInSliderArea:touch];
}

#pragma mark - Private

- (KKMediaItemViewController *)imageVideoMediaItemAtIndex:(NSInteger)index {
    return _imageVideoMediaItems[index];
}

- (void)showDescription:(BOOL)showDescription andTitle:(BOOL)showTitle {
    
    [self.view setFrame:_initialSelfFrame];
    [_infoContainerView setFrame:_initialInfoFrame];
    
    CGFloat newHeight = CGRectGetHeight(self.view.frame) + 10.0;
    CGFloat newInfoHeight = CGRectGetHeight(_infoContainerView.frame);
    
    CGFloat bottomInfoContainerY = _infoContainerView.frame.origin.y + CGRectGetHeight(_infoContainerView.frame);
    CGFloat distanceWithPageControlGap = fabs(CGRectGetHeight(self.view.frame) - bottomInfoContainerY);
    
    if (showTitle && showDescription) {
        CGFloat creditsLabelY = [self.view convertPoint:CGPointZero fromView:_creditsLabel].y;
        
        newHeight = creditsLabelY + CGRectGetHeight(_creditsLabel.frame) + distanceWithPageControlGap + 10.0;
        newInfoHeight = CGRectGetHeight(_creditsLabel.frame) + creditsLabelY - CGRectGetMinY(_infoContainerView.frame);
    }
    
    if (!showTitle) {
        newHeight = newHeight - 10.0 - CGRectGetHeight(_infoContainerView.frame);
    }
    
    if ([_pageControl isHidden]) {
        newHeight = (newHeight + 10.0) - distanceWithPageControlGap;
    }
    
//    if (!showTitle && [_pageControl isHidden]) {
//        newHeight += 10.0;
//    }

    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    [CATransaction setAnimationDuration:0.3];
    
    self.view.frame = (CGRect){self.view.frame.origin,
        CGRectGetWidth(self.view.frame),
        newHeight};
    _infoContainerView.frame = (CGRect){_infoContainerView.frame.origin,
        CGRectGetWidth(_infoContainerView.frame),
        newInfoHeight};
    [_lazyScrollView setFrame:(CGRect){_lazyScrollView.frame.origin,
        CGRectGetWidth(_lazyScrollView.frame), 500}];
    
    _pageControl.frame = (CGRect){_pageControl.frame.origin.x, self.view.frame.size.height - 48.0,
        _pageControl.frame.size};
    
    [CATransaction commit];
}

- (void)setupInfoForController:(KKMediaItemViewController *)mediaItemController {
    File *mediaItem = mediaItemController.mediaItem;
    
    // Enable/Disable info button.
    Content *content = [mediaItem contentForLanguage:[KKLanguage currentLanguage]];
    _titleLabel.text = content.title;
    
    BOOL __block hasAnyCredit = NO;
    
    [mediaItem.credits enumerateObjectsUsingBlock:^(Credit *credit, BOOL *stop) {
        hasAnyCredit = credit.name.length > 0;
        *stop = hasAnyCredit;
    }];
    
    _hasSomethingToPresent = content.desc.length > 0 || hasAnyCredit;
    
    _needToPresentDetailedInfoView = _hasSomethingToPresent || ([content.title length] > 0);
    _infoContainerView.hidden = !_needToPresentDetailedInfoView;
    
    _detailedInfoButton.hidden = !_hasSomethingToPresent;
    
    if (!_hasSomethingToPresent) {
        _isPresentingDetailedInfo = NO;

        [self showDescription:_isPresentingDetailedInfo andTitle:_needToPresentDetailedInfoView];
        
        // Because of _isPresentingDetailedInfo set to NO, layout won't be invalidated, post it.
        [[NSNotificationCenter defaultCenter] postNotificationName:@"shouldInvalidateLayout"
                                                            object:self];

        return;
    }
    
    // Calculating offset between description label and credentials label to restore it later.
    CGFloat bottomYPositionOfDescriptionLabel = _descriptionLabel.frame.origin.y + CGRectGetHeight(_descriptionLabel.frame);
    CGFloat labelsOffset = fabs(bottomYPositionOfDescriptionLabel - _creditsLabel.frame.origin.y);
    
    // Calculating label size for description.
    CGFloat maxAllowedWidth = _detailedInfoButton.frame.origin.x - CGRectGetWidth(_detailedInfoButton.frame);
    
    NSString *description = content.desc;
    
    CGSize descriptionSize = [description sizeWithFont:_descriptionLabel.font
                                     constrainedToSize:(CGSize){maxAllowedWidth,
                                                                CGFLOAT_MAX}];
    _descriptionLabel.frame = CGRectIntegral((CGRect){_descriptionLabel.frame.origin, descriptionSize});
    _descriptionLabel.text = description;
    
    // Calculating label size for credits.
    NSMutableString *allCreditsString = [@"" mutableCopy];
    for (Credit *credit in mediaItem.credits) {
        if (credit.name.length > 0) {
            [allCreditsString appendFormat:@"%@: %@\n", [[KKDataManager sharedManager] localizedCreditTypeForTitle:credit.credit_type], credit.name];
        }
    }
    
    CGSize sizeForCredits = [allCreditsString sizeWithFont:_creditsLabel.font
                                         constrainedToSize:(CGSize){maxAllowedWidth,
                                                                    CGFLOAT_MAX}];
    _creditsLabel.text = allCreditsString;
    
    // Place it right after description label.
    CGFloat descriptionLabelHeight = CGRectGetHeight(_descriptionLabel.frame);
    
//    _creditsLabel.frame = CGRectIntegral((CGRect){_creditsLabel.frame.origin.x,
//        _descriptionLabel.frame.origin.y + descriptionLabelHeight + labelsOffset,
//        sizeForCredits});
    _creditsLabel.frame = CGRectIntegral((CGRect){_creditsLabel.frame.origin.x,
        _descriptionLabel.frame.origin.y + descriptionLabelHeight + 10,
        sizeForCredits});
    
    [self showDescription:_isPresentingDetailedInfo andTitle:YES];
}

@end
