//
//  TestRecord.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 30.07.14.
//
//

#import "TestContent.h"

@implementation TestContent

- (id)initWithTitle:(NSString *)title
              pages:(NSArray *)pages {
    if (self = [super init]) {
        _title = title;
        _pages = pages;
    }
    return self;
}

@end
