//
//  KKVideoBlockCell.m
//  Kulturkiosk
//
//  Created by Roman Ivchenko on 14.08.14.
//
//

#import "KKVideoBlockCell.h"
#import "KKMediaItemsContainerViewController.h"

NSString *const kVideoCellIdentifier = @"VideoBlockCell";
CGFloat   const kVideoCellHeight = 600.0;

@implementation KKVideoBlockCell {
    __weak IBOutlet UIView *_videoBlockContainer;
}

#pragma mark - Dynamic

- (void)setVideoContainer:(KKMediaItemsContainerViewController *)videoContainer {
    [self cleanUp];
    UIView *videoControllerView = videoContainer.view;
    
//    [videoControllerView setFrame:CGRectOffset(videoControllerView.frame, 10, 0)];
    [_videoBlockContainer addSubview:videoControllerView];
}

- (void)cleanUp {
    for (UIView *view in _videoBlockContainer.subviews) {
//        [view setFrame:CGRectOffset(view.frame, -10, 0)];
        [view removeFromSuperview];
    }
}

@end
