//
//  TestRecord.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 30.07.14.
//
//

#import <Foundation/Foundation.h>

@interface TestContent : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSArray  *pages;

- (id)initWithTitle:(NSString *)title
              pages:(NSArray *)pages;

@end
