//
//  TestFile.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 30.07.14.
//
//

#import "TestFile.h"

@implementation TestFile

- (id)initWithName:(NSString *)name
              path:(NSURL *)path
    additionalInfo:(NSString *)addInfo {
    if (self = [super init]) {
        _name           = name;
        _path           = path;
        _additionalInfo = addInfo;
    }
    return self;
}

- (id)initWithName:(NSString *)name {
    return [self initWithName:name
                         path:nil
               additionalInfo:nil];
}

- (id)initWithName:(NSString *)name
              path:(NSURL *)path {
    return [self initWithName:name
                         path:path
               additionalInfo:nil];
}

- (NSURL *)urlPath {
    return [NSURL URLWithString:_path];
}

@end
