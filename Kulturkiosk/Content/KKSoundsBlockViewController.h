//
//  KKRecordSoundsViewController.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 7/18/14.
//
//

#import <UIKit/UIKit.h>
#import "AudioBlock.h"

//For testing
#import "TestAudioBlock.h"

@interface KKSoundsBlockViewController : UIViewController

@property (nonatomic) AudioBlock *audioBlock;

//For testing
//@property (nonatomic) TestAudioBlock *audioBlock;

@end
