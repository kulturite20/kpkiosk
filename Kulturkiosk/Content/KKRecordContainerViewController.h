//
//  KKRecordsContainerViewController.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 7/18/14.
//
//

#import <UIKit/UIKit.h>
#import "Record.h"
#import "Content.h"

//For testing
#import "TestContent.h"

@class KKRecordContainerViewController;

@protocol KKRecordContainerDelegate <NSObject>

- (void)recordContainerDidDismiss:(KKRecordContainerViewController *)recordContainer;

@optional
- (void)recordContainerDidChangePage:(KKRecordContainerViewController *)recordContainer;

@end

@interface KKRecordContainerViewController : UIViewController

@property (nonatomic) Record *record;
@property (nonatomic, weak) id <KKRecordContainerDelegate> navBarDelegate;
@property (nonatomic, weak) id <KKRecordContainerDelegate> timeLineDelegate;
@property (nonatomic, weak) id <KKRecordContainerDelegate> imageMapDelegate;

@property (nonatomic, strong) NSArray *controllersForPages;
@property (nonatomic, assign) BOOL isSingle;
@property (nonatomic, assign) BOOL isLaunchedFromHomeScreen;

//For testing
//@property (nonatomic) TestContent *content;

@end
