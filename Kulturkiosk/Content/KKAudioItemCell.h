//
//  KKAudioItemCell.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 31.07.14.
//
//

#import <UIKit/UIKit.h>
#import "Sound.h"

@class KKAudioItemCell;

@protocol KKAudioItemCellDelegate <NSObject>

- (void)audioItemCell:(KKAudioItemCell *)cell didPressPlayButtonAtIndexPath:(NSIndexPath *)indexPath;

@end

@interface KKAudioItemCell : UITableViewCell

@property (nonatomic, weak) id <KKAudioItemCellDelegate> delegate;
@property (nonatomic, strong) Sound *sound;

@property (nonatomic,strong) NSIndexPath *indexPath;

@end
