//
//  TestPage.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 30.07.14.
//
//

#import <Foundation/Foundation.h>
#import "TestVideoBlock.h"
#import "TestAudioBlock.h"
#import "TestTextBlock.h"

@interface TestPage : NSObject


@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSArray  *blocks;

- (id)initWithTitle:(NSString *)title
             blocks:(NSArray *)blocks;

- (TestVideoBlock *)videoBlock;
- (TestAudioBlock *)audioBlock;
- (TestTextBlock *)textBlock;

@end
