//
//  KKRecordViewController.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 7/18/14.
//
//

#import <UIKit/UIKit.h>
#import "Page.h"

//For testing
#import "TestPage.h"
#import "TestAudioBlock.h"

@protocol KKRecordPageViewControllerDelegate;

@interface KKRecordPageViewController : UIViewController

@property (nonatomic) Page *page;
@property (nonatomic, weak) id <KKRecordPageViewControllerDelegate> delegate;

@property (nonatomic, assign) BOOL isShouldHideCloseButton;

@property (nonatomic) BOOL isFirstVideo;

@end

@protocol KKRecordPageViewControllerDelegate <NSObject>

- (void)recordPageControllerShouldCloseWholeRecord:(KKRecordPageViewController *)recordPage;
- (CGFloat)heightControlContainer:(KKRecordPageViewController *)recordPage;

@end
