//
//  Sound.m
//  VKTeleport
//
//  Created by Vadym Osovets on 11/17/13.
//  Copyright (c) 2013 Micro-B. All rights reserved.
//

#import "Sound.h"

//frameworks
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

static NSString *const kSoundIDKey          = @"soundID";
static NSString *const kTitleKey            = @"title";
static NSString *const kGenreKey            = @"type";
static NSString *const kCreatorKey          = @"creator";
static NSString *const kAlbumNameKey        = @"albumName";
static NSString *const kArtistKey           = @"artist";
static NSString *const kArtworkKey          = @"artwork";
static NSString *const kCreationDateKey     = @"creationDate";
static NSString *const kDurationKey         = @"duration";

__attribute__((deprecated))
static NSString *const kRelativeURLKey      = @"relativeURL";
static NSString *const kSoundFilenameKey    = @"soundFilename";

static NSString *const kSoundIDVKKey        = @"id";

typedef enum {
    kSoundTypeInternal,
    kSoundTypeiPodLibrary
} SoundType;

@interface Sound ()
<
NSCoding
>
@end

@implementation Sound {
    SoundType _soundType;
}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        NSString *soundName         = [aDecoder decodeObjectForKey:kSoundFilenameKey];
        NSString *fullPathToSound   = [[NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches/DownloadedMedia"] stringByAppendingPathComponent:soundName];
        
        _soundID        = [aDecoder decodeObjectForKey:kSoundIDKey];
        _title          = [aDecoder decodeObjectForKey:kTitleKey];
        _genre          = [aDecoder decodeObjectForKey:kGenreKey];
        _creator        = [aDecoder decodeObjectForKey:kCreatorKey];
        _albumName      = [aDecoder decodeObjectForKey:kAlbumNameKey];
        _artist         = [aDecoder decodeObjectForKey:kArtistKey];
        _artwork        = [aDecoder decodeObjectForKey:kArtworkKey];
        _creationDate   = [aDecoder decodeObjectForKey:kCreationDateKey];
        _duration       = [aDecoder decodeIntegerForKey:kDurationKey];
        
        _relativeURL    = [NSURL fileURLWithPath:fullPathToSound];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    NSString *soundFilename = [_relativeURL.absoluteString lastPathComponent];
    
    [aCoder encodeObject:_soundID       forKey:kSoundIDKey];
    [aCoder encodeObject:_title         forKey:kTitleKey];
    [aCoder encodeObject:_genre         forKey:kGenreKey];
    [aCoder encodeObject:_creator       forKey:kCreatorKey];
    [aCoder encodeObject:_albumName     forKey:kAlbumNameKey];
    [aCoder encodeObject:_artist        forKey:kArtistKey];
    [aCoder encodeObject:_artwork       forKey:kArtworkKey];
    [aCoder encodeObject:_creationDate  forKey:kCreationDateKey];
    [aCoder encodeInteger:_duration     forKey:kDurationKey];
    [aCoder encodeObject:soundFilename  forKey:kSoundFilenameKey];
}

#pragma mark - Lifecycle

- (instancetype)initWithMediaItem:(MPMediaItem *)item {
    if (self = [super init]) {
        
        _relativeURL    = [item valueForProperty:MPMediaItemPropertyAssetURL];
//        NSDictionary *metaData = [self retrieveMetadataFromURL:_relativeURL];
//        [self fillPropertiesFromMetadata:metaData];
        
        MPMediaItemArtwork *artwork = [item valueForProperty:MPMediaItemPropertyArtwork];
        _soundID        = [item valueForProperty:MPMediaItemPropertyPersistentID];
        _title          = [item valueForProperty:MPMediaItemPropertyTitle];
        _duration       = [[item valueForKey:MPMediaItemPropertyPlaybackDuration] floatValue];
        _genre          = [item valueForProperty:MPMediaItemPropertyGenre];
        _creator        = [item valueForProperty:MPMediaItemPropertyComposer];
        _albumName      = [item valueForProperty:MPMediaItemPropertyAlbumTitle];
        _artist         = [item valueForProperty:MPMediaItemPropertyArtist];
        _artwork        = [artwork imageWithSize:artwork.bounds.size];
        //TODO : FIX DATE.
        _creationDate   = [item valueForProperty:MPMediaItemPropertyReleaseDate];
        _listenCount    = [[item valueForProperty:MPMediaItemPropertyPlayCount] integerValue];
 
        _soundType      = kSoundTypeiPodLibrary;
    }
    return self;
}

- (instancetype)initWithFullPath:(NSURL *)fullPath {
    if (self = [super init]) {
        _relativeURL = fullPath;
        
        NSDictionary *metaData = [self retrieveMetadataFromURL:_relativeURL];
        [self fillPropertiesFromMetadata:metaData];
        
        _soundType = kSoundTypeInternal;
    }
    return self;
}

#pragma mark - Dynamic

- (void)setFile:(File *)file {
    _file = file;
    
    if ([_title isEqualToString:@"Unknown artist"]) {
        Content *content = [_file contentForLanguage:[KKLanguage currentLanguage]];
        if ([[content title] length] > 0) {
            _title = [content title];
        } else {
            _title = _file.name;
        }
    } else {
        Content *content = [_file contentForLanguage:[KKLanguage currentLanguage]];
        if ([[content title] length] > 0) {
            _title = [content title];
        }
    }
}

#pragma mark - Public

- (BOOL)isURLValid {
//    if (_soundType == kSoundTypeInternal) {
//        return [_relativeURL checkResourceIsReachableAndReturnError:nil];
//    } else {
        AVAsset *asset = [AVAsset assetWithURL:_relativeURL];
        return asset.playable;
//    }
}

#pragma mark - NSObject

- (BOOL)isEqual:(Sound *)object {
    return [self.soundID isEqualToNumber:object.soundID];
}

- (NSUInteger)hash {
    return [self.soundID.stringValue hash];
}

#pragma mark - Private

- (NSDictionary *)retrieveMetadataFromURL:(NSURL *)url {
    AVURLAsset *urlAsset = [AVURLAsset assetWithURL:url];

    NSMutableDictionary *metaData = [NSMutableDictionary new];
    for (AVMetadataItem *currMetadata in [urlAsset commonMetadata]) {
        metaData[currMetadata.commonKey] = currMetadata.value;
    }
    
    CMTime audioDuration = urlAsset.duration;
    _duration = (NSInteger)CMTimeGetSeconds(audioDuration);
    
    return metaData;
}

- (void)fillPropertiesFromMetadata:(NSDictionary *)metadata {
    UIImage *artworkImage = [UIImage imageWithData:metadata[kArtworkKey][@"data"]];
    _title          = metadata[kTitleKey]           ? metadata[kTitleKey]           : @"Unknown artist";
    _genre          = metadata[kGenreKey]           ? metadata[kGenreKey]           : @"Unknown genre";
    _creator        = metadata[kCreatorKey]         ? metadata[kCreatorKey]         : @"Unknown creator";
    _albumName      = metadata[kAlbumNameKey]       ? metadata[kAlbumNameKey]       : @"Unknown album";
    _artist         = metadata[kArtistKey]          ? metadata[kArtistKey]          : @"Unknown artist";
    _artwork        = artworkImage                  ? artworkImage                  : [UIImage imageNamed:@"NoArtwork"];
    _creationDate   = metadata[kCreationDateKey]    ? metadata[kCreationDateKey]    : @"Unknown creation date";
}

@end
