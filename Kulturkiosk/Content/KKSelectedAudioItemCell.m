//
//  KKSelectedAudioItemCell.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 04.08.14.
//
//

#import "KKSelectedAudioItemCell.h"
#import "MediaPlayer.h"

// Categories
#import "UIImage+animatedGIF.h"
#import "KKDataManager.h"

@interface NSString (Time)

+ (NSString *)timeRepresentationFromSeconds:(NSInteger)totalSeconds;

@end

@implementation NSString (Time)

+ (NSString *)timeRepresentationFromSeconds:(NSInteger)totalSeconds {
    int32_t minutes       = (int32_t)totalSeconds / 60;
    int32_t seconds       = (int32_t)totalSeconds % 60;
    int32_t hours         = minutes / 60;
    
    NSString *timeRepresentation = [NSString stringWithFormat:@"%02d:%02d:%02d", hours, minutes % 60, seconds];
//
//    if (minutes >= 60) {
//        timeRepresentation  = [NSString stringWithFormat:@"%02d:%02d:%02d", hours, minutes % 60, seconds];
//    }
    
    return timeRepresentation;
}

@end

@interface KKSelectedAudioItemCell ()
<
MediaPlayerDelegate
>

@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UILabel *elapsedTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *remainingTimeLabel;

@end

@implementation KKSelectedAudioItemCell {
    __weak IBOutlet UILabel  *_soundTitleLabel;
    __weak IBOutlet UIButton *_playPauseButton;
    __weak IBOutlet UIImageView *_animatedEQImageView;
    
    __weak IBOutlet UIButton *_detailedInfoButton;
    __weak IBOutlet UILabel *_titleLabel;
    __weak IBOutlet UILabel *_descriptionLabel;
    __weak IBOutlet UILabel *_creditsLabel;
    __weak IBOutlet UIView *_infoContainerView;
    
    MediaPlayer *_mediaPlayer;
    BOOL _hasSomethingToPresent;
}

#pragma mark - Lifecycle

- (void)awakeFromNib {
    UIImage *minImage = [[UIImage imageNamed:@"min_sound"] resizableImageWithCapInsets:(UIEdgeInsets){4, 0, 4, 0}];
    UIImage *maxImage = [[UIImage imageNamed:@"max_sound"] resizableImageWithCapInsets:(UIEdgeInsets){4, 0, 4, 0}];
    
    [_slider setMinimumTrackImage:minImage
                         forState:UIControlStateNormal];
    [_slider setMaximumTrackImage:maxImage
                         forState:UIControlStateNormal];
    [_slider setThumbImage:[UIImage imageNamed:@"sound_block_pointer"]
                  forState:UIControlStateNormal];
    
    UITapGestureRecognizer *tapRec = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                             action:@selector(didTapCellContainer:)];
    self.contentView.userInteractionEnabled = YES;
    [self.contentView addGestureRecognizer:tapRec];
}

#pragma mark - Dynamic

- (void)setSelectedSound:(Sound *)selectedSound {

    _soundTitleLabel.text = selectedSound.title;
    
    [_playPauseButton setSelected:NO];
    
    _mediaPlayer = [MediaPlayer shared];
    [_mediaPlayer addObserverForPlayerEvents:self];
    
    [_slider setValue:(float)_mediaPlayer.currentTime/_mediaPlayer.currentTrack.duration];
    
    _remainingTimeLabel.text = [NSString timeRepresentationFromSeconds:selectedSound.duration];
    
    if (_mediaPlayer.isPlaying) {
        [_playPauseButton setSelected:NO];
        [self showAnimation];
    } else {
        [_playPauseButton setSelected:YES];
        [self dismissAnimation];
    }
}

- (void)setRawFile:(File *)rawFile {
    _rawFile = rawFile;
    
    File *mediaItem = _rawFile;
    // Settuping info depending on file.
    // Enable/Disable info button.
    Content *content = [mediaItem contentForLanguage:[KKLanguage currentLanguage]];
    _titleLabel.text = content.title;
    
    BOOL __block hasAnyCredit = NO;
    
    [mediaItem.credits enumerateObjectsUsingBlock:^(Credit *credit, BOOL *stop) {
        hasAnyCredit = credit.name.length > 0;
        *stop = hasAnyCredit;
    }];
    
    _hasSomethingToPresent = content.desc.length > 0 || hasAnyCredit;
    
    _detailedInfoButton.hidden = !_hasSomethingToPresent;
    
    // Calculating offset between description label and credentials label to restore it later.
    CGFloat bottomYPositionOfDescriptionLabel = _descriptionLabel.frame.origin.y + CGRectGetHeight(_descriptionLabel.frame);
    CGFloat labelsOffset = fabs(bottomYPositionOfDescriptionLabel - _creditsLabel.frame.origin.y);
    
    // Calculating label size for description.
    CGFloat maxAllowedWidth = _detailedInfoButton.frame.origin.x - CGRectGetWidth(_detailedInfoButton.frame);
    
    NSString *description = content.desc;
    
    CGSize descriptionSize = [description sizeWithFont:_descriptionLabel.font
                                     constrainedToSize:(CGSize){maxAllowedWidth, CGFLOAT_MAX}];
    _descriptionLabel.frame = CGRectIntegral((CGRect){_descriptionLabel.frame.origin, descriptionSize});
    _descriptionLabel.text = description;
    
    // Calculating label size for credits.
    NSMutableString *allCreditsString = [@"" mutableCopy];
    for (Credit *credit in mediaItem.credits) {
        if (credit.name.length > 0) {
            [allCreditsString appendFormat:@"%@: %@\n", [[KKDataManager sharedManager] localizedCreditTypeForTitle:credit.credit_type], credit.name];
        }
    }
        
    CGSize sizeForCredits = [allCreditsString sizeWithFont:_creditsLabel.font
                                         constrainedToSize:(CGSize){maxAllowedWidth,
                                             CGFLOAT_MAX}];
    _creditsLabel.text = allCreditsString;
    
    // Place it right after description label.
    CGFloat descriptionLabelHeight = CGRectGetHeight(_descriptionLabel.frame);
    
    _creditsLabel.frame = CGRectIntegral((CGRect){_creditsLabel.frame.origin.x,
                                                    _descriptionLabel.frame.origin.y + descriptionLabelHeight + labelsOffset,
                                                    sizeForCredits});
}

- (void)setShouldPresentInfo:(BOOL)shouldPresentInfo {
    _shouldPresentInfo = shouldPresentInfo;
    
    [self presentInfo:shouldPresentInfo
       notifyDelegate:NO];
}

#pragma marl - Callbacks

- (IBAction)didPressPlayPauseButton:(UIButton *)sender {
    if (sender.isSelected) {
        [_mediaPlayer resume];
        [_delegate selectedAudioItemCellDidPressPlayButton:self];
        [self showAnimation];
        [sender setSelected:NO];
    } else {
        [_mediaPlayer pause];
        [_delegate selectedAudioItemCellDidPressPauseButton:self];
        [self dismissAnimation];
        [sender setSelected:YES];
    }
}

- (IBAction)didBeginSliding:(UISlider *)sender {
    [_mediaPlayer pause];
    [self dismissAnimation];
}

- (IBAction)sliderValueChanged:(UISlider *)sender {
    NSInteger seekTime = sender.value * [[_mediaPlayer currentTrack] duration];
    _elapsedTimeLabel.text = [NSString timeRepresentationFromSeconds:seekTime];
}

- (IBAction)sliderDidEndSliding:(UISlider *)sender {
    NSInteger seekTime = sender.value * [[_mediaPlayer currentTrack] duration];
    if (![_playPauseButton isSelected]) {
        [_mediaPlayer play];
        [self showAnimation];
    }
    [_mediaPlayer seekToTime:seekTime];
}

- (IBAction)didTapCellContainer:(UITapGestureRecognizer *)sender {
    if (!_hasSomethingToPresent) {
        return;
    }
    
    _shouldPresentInfo = !_shouldPresentInfo;
    
    [self presentInfo:self.shouldPresentInfo notifyDelegate:YES];
}

- (IBAction)didPressInfoButton:(UIButton *)sender {
    _shouldPresentInfo = !_shouldPresentInfo;
    
    [self presentInfo:self.shouldPresentInfo notifyDelegate:YES];
}

#pragma marl - MediaPlayerDelegate

- (void)mediaPlayer:(MediaPlayer *)player reportsCurrentTime:(NSTimeInterval)currentTime {
    [_slider setValue:(float)currentTime/player.currentTrack.duration];
    _elapsedTimeLabel.text = [NSString timeRepresentationFromSeconds:currentTime];
}

- (void)dealloc {
    [_mediaPlayer removeObserverForPlayerEvents:self];
}

#pragma mark - Private

- (void)showAnimation {
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"equalizer_icon_animated" withExtension:@"gif"];
    _animatedEQImageView.image = [UIImage animatedImageWithAnimatedGIFURL:url];
}

- (void)dismissAnimation {
    _animatedEQImageView.image = [UIImage imageNamed:@"equalizer_icon"];
}

- (void)presentInfo:(BOOL)present notifyDelegate:(BOOL)notify {
    CGFloat creditsLabelY = [self convertPoint:CGPointZero
                                      fromView:_creditsLabel].y;
    CGFloat bottomInfoContainerY = _infoContainerView.frame.origin.y + CGRectGetHeight(_infoContainerView.frame);
    CGFloat distanceWithPageControlGap = fabs(CGRectGetHeight(self.frame) - bottomInfoContainerY);
    
    // TODO: Const offset.
    CGFloat newHeight = creditsLabelY + CGRectGetHeight(_creditsLabel.frame) + distanceWithPageControlGap + 10;
    
    // TODO: Add animation.
    
    //    [CATransaction begin];
    
    if (present) {
        //        [UIView animateWithDuration:0.25
        //                         animations:^{
//        self.frame = (CGRect){self.frame.origin,
//            CGRectGetWidth(self.frame),
//            newHeight};
        
        // Show info.
        if (notify) {
            [self.delegate selectedAudioItemCellWillPresentAdditionalInfo:self
                                                                newHeight:newHeight];
        }
        //        }];
        
    } else {
        //        [UIView animateWithDuration:1
        //                         animations:^{
//        self.frame = _rawCellFrame;
        
        // Hide info.
        if (notify) {
            [self.delegate selectedAudioItemCellWillHideAdditionalInfo:self
                                                             newHeight:-1];
        }
        //        }];
    }
    
    //    [CATransaction commit];
}

#pragma mark - Public

- (void)syncUIForState:(KKSelectedAudioItemCellState)state {
    if (state == KKSelectedAudioItemCellStateIsPaused) {
        [self dismissAnimation];
        [_playPauseButton setSelected:YES];
    }
}

@end
