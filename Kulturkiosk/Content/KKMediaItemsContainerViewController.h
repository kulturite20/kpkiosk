//
//  KKRecordMovieContainerViewController.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 29.07.14.
//
//

#import <UIKit/UIKit.h>
#import "VideoBlock.h"

//For testing
#import "TestVideoBlock.h"

@protocol KKMediaItemsContainerViewControllerDelegate;

@interface KKMediaItemsContainerViewController : UIViewController

@property (nonatomic, strong) VideoBlock *videoBlock;
@property (nonatomic) BOOL shouldPlayAtStart;

@property (nonatomic, weak) id <KKMediaItemsContainerViewControllerDelegate> delegate;

@end

@protocol KKMediaItemsContainerViewControllerDelegate <NSObject>

- (void)showImageVideoGalleryWithStartingIndex:(NSUInteger)index andData:(NSArray *)data;
- (void)removeFromSourceItem:(KKMediaItemsContainerViewController *)mediaContainer;

@end
