//
//  KKAudioItemCell.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 31.07.14.
//
//

#import "KKAudioItemCell.h"

@implementation KKAudioItemCell {
    __weak IBOutlet UILabel *_audioTitleLabel;
    __weak IBOutlet UIView *_audioCellContainer;
}

- (void)awakeFromNib {
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(activateAudioItem:)];
    _audioCellContainer.gestureRecognizers = @[tapRecognizer];
}

#pragma mark - Dynamic

- (void)setSound:(Sound *)sound {
    _audioTitleLabel.text = sound.title;
}

#pragma mark - Callbacks

- (IBAction)activateAudioItem:(UITapGestureRecognizer *)sender {
    [_delegate audioItemCell:self didPressPlayButtonAtIndexPath:_indexPath];
}

@end
