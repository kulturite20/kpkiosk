//
//  TestPage.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 30.07.14.
//
//

#import "TestPage.h"

@implementation TestPage

- (id)initWithTitle:(NSString *)title
              blocks:(NSArray *)blocks {
    if (self = [super init]) {
        _title = title;
        _blocks = blocks;
    }
    return self;
}

- (TestVideoBlock *)videoBlock {
    for (TestBlock *block in self.blocks) {
        if ([block isKindOfClass:[TestVideoBlock class]]) {
            return (TestVideoBlock *)block;
        }
    }
    return nil;
}

- (TestAudioBlock *)audioBlock {
    for (TestBlock *block in self.blocks) {
        if ([block isKindOfClass:[TestAudioBlock class]]) {
            return (TestAudioBlock *)block;
        }
    }
    return nil;
}

- (TestTextBlock *)textBlock {
    for (TestBlock *block in self.blocks) {
        if ([block isKindOfClass:[TestTextBlock class]]) {
            return (TestTextBlock *)block;
        }
    }
    return nil;
}

@end
