//
//  KKRecordMovieViewController.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 7/18/14.
//
//

#import "KKMediaItemViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "AGWindowView.h"

#import "UIView+Autolayout.h"

NSString *kMediaTracksKey        = @"tracks";
NSString *kMediaStatusKey        = @"status";
NSString *kMediaRateKey          = @"rate";
NSString *kMediaPlayableKey      = @"playable";
NSString *kMediaCurrentItemKey   = @"currentItem";

static void *KKMediaMovieViewControllerRateObservationContext = &KKMediaMovieViewControllerRateObservationContext;
static void *KKMediaMovieViewControllerCurrentItemObservationContext = &KKMediaMovieViewControllerCurrentItemObservationContext;
static void *KKMediaMovieViewControllerPlayerItemStatusObserverContext = &KKMediaMovieViewControllerPlayerItemStatusObserverContext;


static UIImageView *imageView = nil;

@interface KKMediaItemViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageContainer;
@property (strong, nonatomic) IBOutlet UIView *videoContainer;
@property (strong, nonatomic) AVPlayer *avPlayer;
@property (strong, nonatomic) AVPlayerItem *avPlayerItem;
@property (strong, nonatomic) NSArray *videos;
@property (strong, nonatomic) UIWindow *externalWindow;
@property (strong, nonatomic)  id timeObserver;

@property (weak, nonatomic) IBOutlet UIButton *pauseButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UISlider *progressSlider;

@end

@implementation KKMediaItemViewController {
    __weak IBOutlet UIView      *_controlContainer;
    __weak IBOutlet UIImageView *_playOverlayerImage;
    __weak IBOutlet UIView      *_mainVideoContainer;
    __weak IBOutlet UILabel     *_durationTextLabel;
    __weak IBOutlet UILabel     *_currentTimeTextLabel;
    __weak IBOutlet UIView *_nonFullScreenVideoViewPresenter;
    __weak IBOutlet UISlider *_volumeSlider;
    __weak IBOutlet UIButton *_fullScreenButton;
    
    CALayer *_videoLayer;
    
    double restoreAfterScrubbingRate;
    BOOL   seekToZero;
    BOOL   _shouldShowFullScreen;
}

#pragma mark - View's lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];

    if ([_mediaItem.type isEqualToString:@"video"]) {
        
        //set same preview image as the thumbNail image
        CGSize size = _imageContainer.frame.size;
        UIImage *bigThumbnail = [_mediaItem thumbnailWithSize:&size];
        [_imageContainer setImage:bigThumbnail];
        
        [self.imageContainer setHidden:YES];
        
        [_controlContainer.layer setCornerRadius:10.0f];
        
        UIImage *minimumImage = [[UIImage imageNamed:@"min_track_image"] resizableImageWithCapInsets:(UIEdgeInsets){0, 6, 0, 6}];
        UIImage *maximumImage = [[UIImage imageNamed:@"max_track_image"] resizableImageWithCapInsets:(UIEdgeInsets){0, 6, 0, 6}];
        
        [_progressSlider setMinimumTrackImage:minimumImage
                                     forState:UIControlStateNormal];
        [_progressSlider setMaximumTrackImage:maximumImage
                                     forState:UIControlStateNormal];
        [_progressSlider setThumbImage:[UIImage imageNamed:@"pointer"]
                              forState:UIControlStateNormal];
        
        [_volumeSlider setMinimumTrackImage:minimumImage
                                   forState:UIControlStateNormal];
        [_volumeSlider setMaximumTrackImage:maximumImage
                                   forState:UIControlStateNormal];
        [_volumeSlider setThumbImage:[UIImage imageNamed:@"sound_pointer"]
                            forState:UIControlStateNormal];
        [self loadVideo:_mediaItem];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(disableFullScreenMode)
                                                     name:@"homeScreenTimerFired"
                                                   object:nil];
        
        if (!_isInVideoGalleryMode) {
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(videoItemDidPressPlay:)
                                                         name:@"videoItemDidPressPlay"
                                                       object:nil];

            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(restoreAfterFullscreenMode:)
                                                         name:@"restoreAfterBlockVideoFullscreen"
                                                       object:nil];
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(audioItemDidPressPlay:)
                                                         name:@"didPressPlayAudioItem"
                                                       object:nil];
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(recordClosed:)
                                                         name:@"recordClosed"
                                                       object:nil];
        } else {
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(restoreAfterFullscreenMode:)
                                                         name:@"restoreAfterGalleryVideoFullscreen"
                                                       object:nil];
        }
    } else {
        [_playOverlayerImage setHidden:YES];
        [_controlContainer setHidden:YES];
        [_videoContainer setHidden:YES];
        UIImage *image = [UIImage imageWithContentsOfFile:_mediaItem.pathToMainItem];
        _imageContainer.image = image;

    }
    
    if (!_isInVideoGalleryMode) {
        _fullScreenButton.hidden = YES;
    }
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    _imageContainer.contentMode = UIViewContentModeScaleAspectFit;
    _imageContainer.clipsToBounds = YES;
    
    if (self.externalWindow != nil) {
        CGRect videoFrame = self.externalWindow.frame;
        videoFrame.origin.x = videoFrame.origin.y = 0;
        self.externalWindow.frame = videoFrame;
    }
    
    // DEBUG
//    NSLog(@"Video contrainer: %@", NSStringFromCGRect(_videoContainer.frame));
//    NSLog(@"Self.view: %@", NSStringFromCGRect(self.view.frame));
//    NSLog(@"Self.mainVideoContainer: %@", NSStringFromCGRect(_mainVideoContainer.frame));
//    
//    NSLog(@"== END ==");
    
    if (self.inFixFullScreenMode) {
        CGRect windowRect = self.view.window.frame;
        CGFloat windowWidth = windowRect.size.width;
        CGFloat windowHeight = windowRect.size.height;
        
        _videoLayer.frame = CGRectMake(0.0f, 0.0f, windowWidth, windowHeight);
        _videoContainer.frame = _videoLayer.frame;
    
        // DEBUG
//        NSLog(@"Video layer: %@", NSStringFromCGRect(_videoLayer.frame));
//        NSLog(@"Video contrainer: %@", NSStringFromCGRect(_videoContainer.frame));
    
    } else {
        _videoLayer.frame = _videoContainer.frame;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (_isInFullscreenMode) {
        seekToZero = NO;
        [self.avPlayer seekToTime:self.mediaItem.timeToSeek];
        self.avPlayer.volume = self.mediaItem.volumeLevel;
        _volumeSlider.value = self.avPlayer.volume;
        [self play];
    } else if (self.mediaItem.timeToSeek.value > 0.0) {
        
        [self.avPlayer seekToTime:self.mediaItem.timeToSeek];
        
    } else if (CMTimeCompare([self.avPlayer currentTime], kCMTimeZero) == 0) {
            [_videoContainer setHidden:YES];
            [self.imageContainer setHidden:NO];
    }
    
    if (self.shouldPlay && !self.isInVideoGalleryMode && [_mediaItem.type isEqualToString:@"video"]) {
        // For autoplay video if video first resource. Comment it if not need.
        [self play];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (_isInFullscreenMode) {
        _videoLayer.frame = (CGRect){CGPointZero, _videoContainer.frame.size};
    }
    
    if (_isInSlideShowMode) {
        _videoLayer.frame = (CGRect){CGPointZero, _videoContainer.frame.size};
        _controlContainer.hidden = YES;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.avPlayer pause];
    
    //TODO: FINISH HERE autoplay
//    if (_isInVideoGalleryMode && !_isInFullscreenMode) {
//        [self startTimer];
//    }
}

#pragma mark - Public

- (BOOL)isPlayingMediaItem {
    
    //Check if avPlayer is in playing status
    if ((_avPlayer.rate != 0) && (_avPlayer.error == nil)) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)isTouchInSliderArea:(UITouch *)touch {
    CGPoint touchLocalLocation = [touch locationInView:_controlContainer];
    return CGRectContainsPoint(_progressSlider.frame, touchLocalLocation) ||
    CGRectContainsPoint(_volumeSlider.frame, touchLocalLocation);
}

- (void)play {
    [self playButtonClicked:nil];
}

#pragma mark - Video Player

- (void)loadVideo:(File *)file {
    seekToZero = NO;
    
    NSURL *url = [file pathToOriginalItem] ? [NSURL fileURLWithPath:[file pathToOriginalItem]] : nil;
    
    NSLog(@"URL: %@", url);
    
    if (!url) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invalid video"
                                                        message:@"There is no internal link for this video"
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        return;
        
    }
    
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:url
                                            options:nil];
    
    if(!asset.playable) {
        
    // uncomment this to show alert when is not possible to play madia item.
    
    //        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Invalid video"
    //                                                      message:@"The specified video could not be loaded"
    //                                                     delegate:self
    //                                            cancelButtonTitle:@"Ok"
    //                                            otherButtonTitles:nil];
    //        [alert show];
        
        return;
    }
    
    if (self.avPlayerItem) {
        [self.avPlayerItem removeObserver:self
                               forKeyPath:kMediaStatusKey];
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:AVPlayerItemDidPlayToEndTimeNotification
                                                      object:self.avPlayerItem];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    
    self.avPlayerItem = [AVPlayerItem playerItemWithAsset:asset];
    [self.avPlayerItem addObserver:self
                        forKeyPath:kMediaStatusKey
                           options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                           context:KKMediaMovieViewControllerPlayerItemStatusObserverContext];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:self.avPlayerItem];
    if(!self.avPlayer) {
        
        self.avPlayer = [AVPlayer playerWithPlayerItem:self.avPlayerItem];
        [self.avPlayer addObserver:self
                        forKeyPath:kMediaCurrentItemKey
                           options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                           context:KKMediaMovieViewControllerCurrentItemObservationContext];
        
        /* Observe the AVPlayer "rate" property to update the scrubber control. */
        [self.avPlayer addObserver:self
                        forKeyPath:kMediaRateKey
                           options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                           context:KKMediaMovieViewControllerRateObservationContext];
        
        _videoLayer = [AVPlayerLayer playerLayerWithPlayer:self.avPlayer];
        CGRect frame = self.videoContainer.frame;
        frame.origin.x = frame.origin.y = 0;
    
//        NSLog(@"Load video: frame: %@", NSStringFromCGRect(frame));
        
        _videoLayer.frame = frame;
        
        [self.videoContainer.layer addSublayer:_videoLayer];
        
//        if([[UIScreen screens] count] > 1) {
//            UIScreen *externalDisplay = [[UIScreen screens] objectAtIndex:1];
//            CGRect bounds = externalDisplay.bounds;
//            self.externalWindow = [[UIWindow alloc] initWithFrame: bounds];
//            self.externalWindow.screen = externalDisplay;
//            self.externalWindow.hidden = NO;
//            AVPlayerLayer *layer2 = [AVPlayerLayer playerLayerWithPlayer:self.avPlayer];
//            layer2.frame = self.externalWindow.frame;
//            
//            [self.externalWindow.layer addSublayer:layer2];
//        }
        
        [self setupControls];
    }
    
    if (self.avPlayer.currentItem != self.avPlayerItem) {
        [self.avPlayer replaceCurrentItemWithPlayerItem:self.avPlayerItem];
    }
    
    self.progressSlider.value = 0.0;
    [self syncPlayPauseButtons];
    //Set time label
    _durationTextLabel.text = [self formattedTimeWithDuration:asset.duration];
}

- (void)setupControls {
    double interval = .1f;
    CMTime playerDuration = [self playerItemDuration]; // return player duration.
    if (CMTIME_IS_INVALID(playerDuration)) {
        return;
    }
    double duration = CMTimeGetSeconds(playerDuration);
    
    if (isfinite(duration))
    {
        CGFloat width = CGRectGetWidth([self.progressSlider bounds]);
        interval = 0.5f * duration / width;
    }
    
    /* Update the scrubber during normal playback. */
    __weak typeof (self) weakSelf = self;
    self.timeObserver = [self.avPlayer addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(interval, NSEC_PER_SEC)
                                                                    queue:NULL
                                                               usingBlock:^(CMTime time) {
                                                                   [weakSelf syncScrubber];
                                                               }];
}

- (void)syncScrubber {
    CMTime playerDuration = [self playerItemDuration];
    
    if (CMTIME_IS_INVALID(playerDuration)) {
        self.progressSlider.minimumValue = 0.0;
        return;
    }
    
    double duration = CMTimeGetSeconds(playerDuration);
    if (isfinite(duration) && (duration > 0)) {
        
        float minValue = [self.progressSlider minimumValue];
        float maxValue = [self.progressSlider maximumValue];
        double time = CMTimeGetSeconds([self.avPlayer currentTime]);
        [self.progressSlider setValue:(maxValue - minValue) * time / duration + minValue];
        
        // Sync time label
        _currentTimeTextLabel.text = [self formattedTimeWithDuration:[self.avPlayer currentTime]];
    }
}

/* The user is dragging the movie controller thumb to scrub through the movie. */
- (IBAction)beginScrubbing:(id)sender {
    restoreAfterScrubbingRate = [self.avPlayer rate];
    //    [self.avPlayer setRate:0.f];
    
    /* Remove previous timer. */
    [self removePlayerTimeObserver];
}

/** Called when scrub was interrupted by other gesture for example. */
- (IBAction)scrubInterrupted:(id)sender {
    [self endScrubbing:sender];
}

/* The user has released the movie thumb control to stop scrubbing through the movie. */
- (IBAction)endScrubbing:(id)sender {
    
    if (!self.timeObserver)
    {
        CMTime playerDuration = [self playerItemDuration];
        if (CMTIME_IS_INVALID(playerDuration))
        {
            return;
        }
        
        double duration = CMTimeGetSeconds(playerDuration);
        if (isfinite(duration))
        {
            CGFloat width = CGRectGetWidth([self.progressSlider bounds]);
            double tolerance = 0.5f * duration / width;
            
            __weak typeof(self) weakSelf = self;
            
            self.timeObserver = [self.avPlayer addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(tolerance, NSEC_PER_SEC)
                                                                            queue:dispatch_get_main_queue()
                                                                       usingBlock:^(CMTime time) {
                                                                           [weakSelf syncScrubber];
                                                                       }];
        }
    }
    
    if (restoreAfterScrubbingRate) {
        [self.avPlayer setRate:restoreAfterScrubbingRate];
        restoreAfterScrubbingRate = 0.f;
    }
}

- (IBAction)scrub:(id)sender {
    if ([sender isKindOfClass:[UISlider class]]) {
        UISlider* slider = sender;
        
        CMTime playerDuration = [self playerItemDuration];
        if (CMTIME_IS_INVALID(playerDuration)) {
            return;
        }
        
        double duration = CMTimeGetSeconds(playerDuration);
        if (isfinite(duration))
        {
            float minValue = [slider minimumValue];
            float maxValue = [slider maximumValue];
            float value = [slider value];
            
            double time = duration * (value - minValue) / (maxValue - minValue);
            
            [self.avPlayer seekToTime:CMTimeMakeWithSeconds(time, NSEC_PER_SEC)];
        }
    }
}

- (void)removePlayerTimeObserver {
    if (self.timeObserver) {
        [self.avPlayer removeTimeObserver:self.timeObserver];
        self.timeObserver = nil;
    }
}

- (CMTime)playerItemDuration {
    AVPlayerItem *thePlayerItem = [self.avPlayer currentItem];
    
    if (thePlayerItem.status == AVPlayerItemStatusReadyToPlay) {
        
        return([thePlayerItem duration]);
    }
    
    return (kCMTimeInvalid);
}

- (IBAction)playButtonClicked:(id)sender {
    
    if ([_videoContainer isHidden]) {
        [_videoContainer setHidden:NO];
        _imageContainer.alpha = 1.0;
        _videoContainer.alpha = 0.0;
        
        [UIView animateWithDuration:0.2
                         animations:^{
                             _imageContainer.alpha = 0.0;
                             _videoContainer.alpha = 1.0;
                         } completion:^(BOOL finished) {
                             [_imageContainer setHidden:YES];
                             _imageContainer.alpha = 1.0;
                         }];
    }
    
    [self stopTimer];
    if (seekToZero) {
        [self.avPlayer seekToTime:kCMTimeZero];
        seekToZero = NO;
    }
    
    [self.avPlayer play];
    [self syncPlayPauseButtons];
    
    if (!_isInVideoGalleryMode || _isInFullscreenMode) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"videoItemDidPressPlay"
                                                                object:_mediaItem];
    }
}

- (IBAction)pauseButtonClicked:(id)sender {
    [self startTimer];
    [self.avPlayer pause];
    [self syncPlayPauseButtons];
}

- (void)pauseVideoPlayer {
    [self.avPlayer pause];
}

- (void)syncPlayPauseButtons {
    if ([self.avPlayer rate] != 0.0) {
        self.pauseButton.hidden = NO;
        self.playButton.hidden = YES;
        
        [self hidePlayOverlayImage];
    } else {
        self.pauseButton.hidden = YES;
        self.playButton.hidden = NO;
        
        [self showPlayOverlayImage];
    }
}

- (void)stopTimer {
    UIAppDelegate.shouldRegisterTouch = NO;
    [UIAppDelegate stopIdleTimers];
}


- (void)startTimer {
    [UIAppDelegate startIdleTimers];
    UIAppDelegate.shouldRegisterTouch = YES;
}

- (void)initScrubberTimer {
    double interval = .1f;
    
    CMTime playerDuration = [self playerItemDuration];
    if (CMTIME_IS_INVALID(playerDuration))
    {
        return;
    }
    double duration = CMTimeGetSeconds(playerDuration);
    if (isfinite(duration))
    {
        CGFloat width = CGRectGetWidth([self.progressSlider bounds]);
        interval = 0.5f * duration / width;
    }
    
    /* Update the scrubber during normal playback. */
    __weak typeof(self) weakSelf = self;
    
    self.timeObserver = [self.avPlayer addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(interval, NSEC_PER_SEC)
                                                                    queue:NULL
                                                               usingBlock:^(CMTime time) {
                                                                   [weakSelf syncScrubber];
                                                               }];
}

- (void)observeValueForKeyPath:(NSString *)path
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    
    /* AVPlayerItem "status" property value observer. */
    if (context == KKMediaMovieViewControllerPlayerItemStatusObserverContext) {
        [self syncPlayPauseButtons];
        
        AVPlayerStatus status = [[change objectForKey:NSKeyValueChangeNewKey] integerValue];
        switch (status)
        {
                /* Indicates that the status of the player is not yet known because
                 it has not tried to load new media resources for playback */
            case AVPlayerStatusUnknown:
            {
                [self removePlayerTimeObserver];
                [self syncScrubber];
                self.progressSlider.enabled=NO;
            }
                break;
                
            case AVPlayerStatusReadyToPlay:
            {
                /* Once the AVPlayerItem becomes ready to play, i.e.
                 [playerItem status] == AVPlayerItemStatusReadyToPlay,
                 its duration can be fetched from the item. */
                
                /* Show the movie slider control since the movie is now ready to play. */
                self.progressSlider.enabled = YES;
                [self initScrubberTimer];
            }
                break;
                
            case AVPlayerStatusFailed:
            {
                //     AVPlayerItem *thePlayerItem = (AVPlayerItem *)object;
                //      [self assetFailedToPrepareForPlayback:thePlayerItem.error];
            }
                break;
        }
    }
    /* AVPlayer "rate" property value observer. */
    else if (context == KKMediaMovieViewControllerRateObservationContext)
    {
        [self syncPlayPauseButtons];
    }
    /* AVPlayer "currentItem" property observer.
     Called when the AVPlayer replaceCurrentItemWithPlayerItem:
     replacement will/did occur. */
    else if (context == KKMediaMovieViewControllerCurrentItemObservationContext)
    {
        AVPlayerItem *newPlayerItem = [change objectForKey:NSKeyValueChangeNewKey];
        
        /* New player item null? */
        if (newPlayerItem == (id)[NSNull null])
        {
            //   [self disablePlayerButtons];
            //   [self disableScrubber];
            
        }
        else /* Replacement of player currentItem has occurred */
        {
            /* Set the AVPlayer for which the player layer displays visual output. */
            //   [playerLayerView.playerLayer setPlayer:self.player];
            
            /* Specifies that the player should preserve the video’s aspect ratio and
             fit the video within the layer’s bounds. */
            //   [playerLayerView setVideoFillMode:AVLayerVideoGravityResizeAspect];
            [self syncPlayPauseButtons];
        }
    } else {
        [super observeValueForKeyPath:path
                             ofObject:object
                               change:change
                              context:context];
    }
    
    return;
}

/* Called when the player item has played to its end time. */
- (void) playerItemDidReachEnd:(NSNotification*) aNotification {
    /* Hide the 'Pause' button, show the 'Play' button in the slider control */
    [self syncPlayPauseButtons];
    [self startTimer];
    
    /* After the movie has played to its end time, seek back to time zero
     to play it again */
    seekToZero = YES;
    
    // Notify
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MediaPlayerMediaItemDidReachEnd" object:nil];
    
}

/*
 - (IBAction)dismissController:(id)sender {
 [self.avPlayer pause];
 self.externalWindow.hidden=YES;
 [self dismissViewControllerAnimated:NO
 completion:^{
 [[NSNotificationCenter defaultCenter] removeObserver:self
 name:AVPlayerItemDidPlayToEndTimeNotification
 object:nil];
 [self.avPlayer removeObserver:self
 forKeyPath:kMediaCurrentItemKey];
 [self.avPlayer removeObserver:self
 forKeyPath:kMediaRateKey];
 [self.avPlayerItem removeObserver:self
 forKeyPath:kMediaStatusKey];
 }];
 }*/

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [self.avPlayer removeObserver:self
                       forKeyPath:kMediaCurrentItemKey];
    [self.avPlayer removeObserver:self
                       forKeyPath:kMediaRateKey];
    [self.avPlayerItem removeObserver:self
                           forKeyPath:kMediaStatusKey];
}

#pragma mark - Dynamic

- (void)setMediaItem:(File *)mediaItem {
    _mediaItem = mediaItem;
    if (self.avPlayer) {
        [self loadVideo:_mediaItem];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(restoreAfterFullscreenMode:)
                                                     name:@"restoreAfterGalleryVideoFullscreen"
                                                   object:nil];
        [self play];
    }
}

#pragma mark - Observers

- (void)restoreAfterFullscreenMode:(NSNotification *)notification {
    File *videoFile = notification.userInfo[@"videoFile"];
    if ([videoFile.name isEqualToString:self.mediaItem.name]) {
        [self.avPlayer seekToTime:videoFile.timeToSeek];
        self.avPlayer.volume = videoFile.volumeLevel;
        _volumeSlider.value = self.avPlayer.volume;
        [self play];
    }
}

- (void)videoItemDidPressPlay:(NSNotification *)notification {
    File *file = (File *)notification.object;
    if (![file.identifier isEqualToNumber:file.identifier]) {
        [self.avPlayer pause];
        [self syncPlayPauseButtons];
    }
}

- (void)audioItemDidPressPlay:(NSNotification *)notification {
    [self.avPlayer pause];
    [self syncPlayPauseButtons];
}

- (void)recordClosed:(NSNotification *)notification {
    if ([notification.userInfo[@"invokedFrom"] isEqualToString:@"recordContainer"]) {
        [self.avPlayer pause];
        return;
    }
    [self startTimer];
    [self.avPlayer pause];
    [self syncPlayPauseButtons];
}

#pragma mark - Callbacks

- (IBAction)fullScreenTapped:(UIButton *)sender {
    if (!_isInFullscreenMode && _isInVideoGalleryMode) {
        self.mediaItem.timeToSeek = self.avPlayer.currentTime;
        self.mediaItem.volumeLevel = self.avPlayer.volume;
        NSString *notificationShowIdentifier = _isInVideoGalleryMode ? @"showGalleryVideoFullscreen" : @"showBlockVideoFullscreen";
        
        [[NSNotificationCenter defaultCenter] postNotificationName:notificationShowIdentifier
                                                            object:nil
                                                          userInfo:@{@"videoFile" : self.mediaItem}];
    } else if (_isInFullscreenMode) {
        self.mediaItem.timeToSeek = self.avPlayer.currentTime;
        self.mediaItem.volumeLevel = self.avPlayer.volume;
        NSString *notificationHideIdentifier = _isInVideoGalleryMode ? @"restoreAfterGalleryVideoFullscreen" : @"restoreAfterBlockVideoFullscreen";
        [[NSNotificationCenter defaultCenter] postNotificationName:notificationHideIdentifier
                                                            object:nil
                                                          userInfo:@{@"videoFile" : self.mediaItem}];
        [self dismissViewControllerAnimated:YES
                                 completion:NULL];
    }
    
    
//    _shouldShowFullScreen = sender.selected;
//    
//    if (_shouldShowFullScreen) {
//        _fullScreenView = [[AGWindowView alloc] initAndAddToKeyWindow];
//        [_fullScreenView addSubviewAndFillBounds:_mainVideoContainer];
//        
//        _videoLayer.frame = (CGRect){CGPointZero, _videoContainer.frame.size};
//        _playOverlayerImage.center = _videoContainer.center;
//    } else {
//        [_fullScreenView removeFromSuperview];
//        _mainVideoContainer.frame = (CGRect){CGPointZero, _nonFullScreenVideoViewPresenter.bounds.size};
//        [_nonFullScreenVideoViewPresenter addSubview:_mainVideoContainer];
//        
//        _videoLayer.frame = (CGRect){CGPointZero, _videoContainer.frame.size};
//        _playOverlayerImage.center = _videoContainer.center;
//    }
}

- (IBAction)volumeChanged:(id)sender {
    self.avPlayer.volume = _volumeSlider.value;
}

- (IBAction)muteTapped:(UIButton *)sender {
    self.avPlayer.volume = 0;
    _volumeSlider.value = 0;
}

- (IBAction)playImageDidTap:(UITapGestureRecognizer *)sender {
    [self playButtonClicked:_playButton];
}

#pragma mark - Private

- (void)showPlayOverlayImage {
    [UIView animateWithDuration:0.5
                     animations:^{
                         _playOverlayerImage.alpha = 1.0f;
                         _controlContainer.alpha = 0.0f;
                     }];
}

- (void)hidePlayOverlayImage {
    [UIView animateWithDuration:0.5
                     animations:^{
                         _playOverlayerImage.alpha = 0.0f;
                         _controlContainer.alpha = 1.0f;
                     }];
}

- (NSString *)formattedTimeWithDuration:(CMTime)duration {
    
    int32_t time        = (int32_t)CMTimeGetSeconds(duration);
    int32_t minutes     = time / 60;
    int32_t seconds     = time % 60;
    int32_t hours       = minutes / 60;
    
    NSString *formattedTime  = [NSString stringWithFormat:@"%02d:%02d:%02d",  hours,  minutes % 60, seconds];
    
    return formattedTime;
}

- (void)disableFullScreenMode {
    if (_isInFullscreenMode) {
        self.mediaItem.timeToSeek = self.avPlayer.currentTime;
        NSString *notificationHideIdentifier = _isInVideoGalleryMode ? @"restoreAfterGalleryVideoFullscreen" : @"restoreAfterBlockVideoFullscreen";
        [self dismissViewControllerAnimated:YES
                                 completion:^{
                                     [[NSNotificationCenter defaultCenter] postNotificationName:notificationHideIdentifier
                                                                                         object:nil
                                                                                       userInfo:@{@"videoFile" : self.mediaItem}];
                                 }];
    }
}

- (void)refreshVideoLayerFrameWith:(CGRect)rect {
    
    // DEBUG
//    NSLog(@"Old frame: %@", NSStringFromCGRect(self.videoContainer.frame));
//    NSLog(@"Old frame: %@", NSStringFromCGRect(self.view.frame));
    
    [_videoContainer removeAllConstraints];
    
    [_videoContainer centeringWithParentView:self.view];
    
    CGRect windowRect = self.view.window.frame;
    CGFloat windowWidth = windowRect.size.width;
    CGFloat windowHeight = windowRect.size.height;
    
    _videoLayer.frame = CGRectMake(0.0f, 0.0f, windowWidth, windowHeight);
    
    // DEBUG
//    NSLog(@"Video contrainer: %@", NSStringFromCGRect(_videoContainer.frame));
//    NSLog(@"Self.view: %@", NSStringFromCGRect(self.view.frame));
//    NSLog(@"Self.mainVideoContainer: %@", NSStringFromCGRect(_mainVideoContainer.frame));
//    
//    NSLog(@"===end===");
}

@end
