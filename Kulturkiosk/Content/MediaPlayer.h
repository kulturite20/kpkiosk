//
//  MediaPlayer.h
//  VKTeleport
//
//  Created by Vadym Osovets on 11/17/13.
//  Copyright (c) 2013 Micro-B. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Sound, MediaPlayer;

extern NSString *const kMediaPlayerDomain;

extern NSInteger const kFileCorruptedCode;
extern NSInteger const kNoSoundsToPlayCode;
extern NSInteger const kPlayerFailedCode;

@protocol MediaPlayerDelegate <NSObject>

@optional
- (void)mediaPlayerDidPause:(MediaPlayer *)player;
- (void)mediaPlayerDidStartPlaying:(MediaPlayer *)player;
- (void)mediaPlayer:(MediaPlayer *)player didFinishPlayingSound:(Sound *)sound;
- (void)mediaPlayer:(MediaPlayer *)player didFailWithError:(NSError *)error;
- (void)mediaPlayer:(MediaPlayer *)player reportsCurrentTime:(NSTimeInterval)currentTime;

@end

@interface MediaPlayer : NSObject

//current's player volume
@property (nonatomic) CGFloat volume;
//sound to play.
@property (nonatomic) NSArray *sounds;

//singleton
+ (instancetype)shared;

//status
- (BOOL)isPlaying;

//player controls
- (void)play;
- (void)resume;
- (void)pause;
- (void)nextTrack;
- (void)previousTrack;

//seeking
- (NSTimeInterval)currentTime;
- (void)seekToTime:(NSTimeInterval)time;
- (void)seekForwardForTime:(NSTimeInterval)time;
- (void)seekBackwardForTime:(NSTimeInterval)time;

/** Implicitly adds sound to sounds queue and starts playing it */
- (void)playSound:(Sound *)sound;
- (void)removeSoundFromQueue:(Sound *)sound;
/** Plays single sound, removes all other sounds from queue */
- (void)playSingleSound:(Sound *)sound;

//current info
- (Sound *)currentTrack;

//Observers. maintains strong reference
- (void)addObserverForPlayerEvents:(id <MediaPlayerDelegate>)object;
- (void)removeObserverForPlayerEvents:(id <MediaPlayerDelegate>)object;

@end
