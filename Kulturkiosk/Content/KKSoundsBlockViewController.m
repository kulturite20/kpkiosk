//
//  KKRecordSoundsViewController.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 7/18/14.
//
//

// Controllers
#import "KKSoundsBlockViewController.h"

// Audio model
#import "MediaPlayer.h"
#import "Sound.h"
#import "TestFile.h"

// Cells
#import "KKAudioItemCell.h"
#import "KKSelectedAudioItemCell.h"

static CGFloat const kPlayingCellDefaultHeight = 173.0;
static CGFloat const kNonPlayingCellDefaultHeight = 80.0;

@interface KKSoundsBlockViewController ()
<
UITableViewDataSource,
UITableViewDelegate,
KKAudioItemCellDelegate,
KKSelectedAudioItemCellDelegate,
MediaPlayerDelegate
>
@end

@implementation KKSoundsBlockViewController {
    __weak IBOutlet UITableView *_soundsTableView;
    __weak IBOutlet UIView *_noDataAlertScreen;
    MediaPlayer *_mediaPlayer;
    KKSelectedAudioItemCell *_activeCell;
    
    // Stores raw file info's
    NSMutableArray *_files;
    // Stores sound from files.
    NSMutableArray *_sounds;
    // Array of dict to store height for cell and is cell should present info (because of reusability).
    NSMutableArray *_cellsMeta;
    
    CGRect _initialRect;
}

#pragma mark - View's lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _soundsTableView.delegate   = self;
    _soundsTableView.dataSource = self;
    
    _sounds = [NSMutableArray new];
    _files = [NSMutableArray new];
    _cellsMeta = [NSMutableArray new];
    
    _mediaPlayer = [MediaPlayer shared];
    
    NSArray *sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"position" ascending:YES]];
    NSArray *sortedFiles = [[_audioBlock files] sortedArrayUsingDescriptors:sortDescriptors];

    for (File *soundFile in sortedFiles) {
        if ([soundFile pathToOriginalItem]) {
            Sound *sound = [[Sound alloc] initWithFullPath:[NSURL fileURLWithPath:[soundFile pathToOriginalItem]]];
            sound.file = soundFile;
            [_sounds addObject:sound];
            [_files addObject:soundFile];
            
            NSMutableDictionary *cellMeta = [@{
                                               @"height" : @(kNonPlayingCellDefaultHeight),
                                               @"shouldShowInfo" : @NO
                                               } mutableCopy];
            [_cellsMeta addObject:cellMeta];
        }
    }
    if ([_sounds count] > 0) {
        _initialRect = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height * [_sounds count]);
        [self setUpControllerFrameWithRect:_initialRect];
    } else {
        [_noDataAlertScreen setHidden:NO];
        [self setUpControllerFrameWithRect:_noDataAlertScreen.frame];
    }
    
    // Observers
    [_mediaPlayer addObserverForPlayerEvents:self];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(soundViewControllerDidPressPlayAudioItem:)
                                                 name:@"didPressPlayAudioItem"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(videoItemDidPressPlay:)
                                                 name:@"videoItemDidPressPlay"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(pageChanged:)
                                                 name:@"pageChanged"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recordClosed:)
                                                 name:@"recordClosed"
                                               object:nil];
    
    UINib *audioCellNib = [UINib nibWithNibName:@"KKAudioItemCell"
                                         bundle:nil];
    UINib *selectedAudioCellNib = [UINib nibWithNibName:@"KKSelectedAudioItemCell"
                                                 bundle:nil];
    [_soundsTableView registerNib:audioCellNib
           forCellReuseIdentifier:@"AudioCell"];
    [_soundsTableView registerNib:selectedAudioCellNib
           forCellReuseIdentifier:@"SelectedAudioCell"];
}

#pragma mark - UITableView Delegate/DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_sounds count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Sound *sound = _sounds[indexPath.row];
    if (sound.isPlaying) {
        KKSelectedAudioItemCell *selectedAudioCell = [tableView dequeueReusableCellWithIdentifier:@"SelectedAudioCell"];
        
        selectedAudioCell.selectedSound     = _sounds[indexPath.row];
        selectedAudioCell.rawFile           = _files[indexPath.row];
        selectedAudioCell.delegate          = self;
        selectedAudioCell.indexPath         = indexPath;
        selectedAudioCell.shouldPresentInfo = [_cellsMeta[indexPath.row][@"shouldShowInfo"] boolValue];
        
        _activeCell = selectedAudioCell;
        
        return selectedAudioCell;
    } else {
        KKAudioItemCell *audioCell = [tableView dequeueReusableCellWithIdentifier:@"AudioCell"];
        
        audioCell.sound     = _sounds[indexPath.row];
        audioCell.delegate  = self;
        audioCell.indexPath = indexPath;
        
        return audioCell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [_cellsMeta[indexPath.row][@"height"] floatValue];
}

#pragma mark - KKAudioItemCellDelegate

- (void)audioItemCell:(KKAudioItemCell *)cell didPressPlayButtonAtIndexPath:(NSIndexPath *)indexPath {
    [self disactivateAudioItems];
    [self stopTimer];
    
    Sound *sound = _sounds[indexPath.row];
    
    [_mediaPlayer playSingleSound:sound];
    
    if (!sound.isPlaying) {
        sound.isPlaying = YES;
        _cellsMeta[indexPath.row][@"height"] = @(kPlayingCellDefaultHeight);
    } else {
        sound.isPlaying = NO;
        _cellsMeta[indexPath.row][@"height"] = @(kNonPlayingCellDefaultHeight);
    }
    
    [_soundsTableView reloadRowsAtIndexPaths:@[indexPath]
                            withRowAnimation:UITableViewRowAnimationAutomatic];
    
    CGRect newRect = (CGRect){_soundsTableView.frame.origin,
                                CGRectGetWidth(_soundsTableView.frame),
                                [[_cellsMeta valueForKeyPath:@"@sum.height"] floatValue]};
    self.view.frame = newRect;
    if (_audioBlock.identifier) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"didPressPlayAudioItem"
                                                            object:nil
                                                          userInfo:@{@"audioBlockID" : _audioBlock.identifier}];
    }
}

#pragma mark - KKSelectedAudioItemCellDelegate

- (void)selectedAudioItemCellWillPresentAdditionalInfo:(KKSelectedAudioItemCell *)cell newHeight:(CGFloat)newHeight {
    NSIndexPath *cellIdx = [_soundsTableView indexPathForCell:cell];
    
    if (cellIdx) {
        _cellsMeta[cellIdx.row][@"height"] = @(newHeight);
        _cellsMeta[cellIdx.row][@"shouldShowInfo"] = @YES;
        
        [_soundsTableView reloadRowsAtIndexPaths:@[cellIdx]
                                withRowAnimation:UITableViewRowAnimationAutomatic];
        
        CGRect newRect = (CGRect){_soundsTableView.frame.origin,
                                    CGRectGetWidth(_soundsTableView.frame),
                                    [[_cellsMeta valueForKeyPath:@"@sum.height"] floatValue]};
        self.view.frame = newRect;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"shouldInvalidateLayout"
                                                            object:self
                                                          userInfo:nil];
    }
}

- (void)selectedAudioItemCellWillHideAdditionalInfo:(KKSelectedAudioItemCell *)cell newHeight:(CGFloat)newHeight {
    NSIndexPath *cellIdx = [_soundsTableView indexPathForCell:cell];
    
    if (cellIdx) {
        _cellsMeta[cellIdx.row][@"height"] = [_sounds[cellIdx.row] isPlaying] ? @(kPlayingCellDefaultHeight) :
                                                                                @(kNonPlayingCellDefaultHeight);
        _cellsMeta[cellIdx.row][@"shouldShowInfo"] = @NO;
        
        [_soundsTableView reloadRowsAtIndexPaths:@[cellIdx]
                                withRowAnimation:UITableViewRowAnimationAutomatic];
        
        CGRect newRect = (CGRect){_soundsTableView.frame.origin,
                                    CGRectGetWidth(_soundsTableView.frame),
                                    [[_cellsMeta valueForKeyPath:@"@sum.height"] floatValue]};
        self.view.frame = newRect;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"shouldInvalidateLayout"
                                                            object:self
                                                          userInfo:nil];
    }
}

- (void)selectedAudioItemCell:(KKSelectedAudioItemCell *)cell didScrubWithValue:(CGFloat)value {
    NSInteger seekTime = value * cell.selectedSound.duration;
    [_mediaPlayer seekToTime:seekTime];
}

- (void)selectedAudioItemCell:(KKSelectedAudioItemCell *)cell willDismissAtIndexPath:(NSIndexPath *)indexPath {
    Sound *sound = _sounds[indexPath.row];
    if (!sound.isPlaying) {
        sound.isPlaying = YES;
    } else {
        sound.isPlaying = NO;
    }
    [_mediaPlayer pause];
    [_soundsTableView reloadRowsAtIndexPaths:@[indexPath]
                            withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)selectedAudioItemCellDidPressPauseButton:(KKSelectedAudioItemCell *)cell {
    [self startTimer];
}

- (void)selectedAudioItemCellDidPressPlayButton:(KKSelectedAudioItemCell *)cell {
    [self stopTimer];
}

#pragma mark - Private

- (void)disactivateAudioItems {
    for (Sound *sound in _sounds) {
        sound.isPlaying = NO;
    }
    
    [_cellsMeta setValue:@NO
              forKeyPath:@"shouldShowInfo"];
    [_cellsMeta setValue:@(kNonPlayingCellDefaultHeight)
                  forKey:@"height"];
    
    [self setUpControllerFrameWithRect:_noDataAlertScreen.isHidden ? _initialRect : _noDataAlertScreen.frame];
    [_soundsTableView reloadData];
}

- (void)setUpControllerFrameWithRect:(CGRect)rect {
    self.view.frame        = rect;
    _soundsTableView.frame = rect;
}

- (void)dismissActiveCells {
    BOOL isActive = NO;
    for (Sound *sound in _sounds) {
        if (sound.isPlaying) {
            isActive = YES;
        }
    }
    if (isActive) {
        [_mediaPlayer pause];
        [self disactivateAudioItems];
        if (_audioBlock.identifier) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"didDismissAudioItem"
                                                                object:nil
                                                              userInfo:@{@"audioBlockID" : _audioBlock.identifier}];
        }
    }
}

- (void)stopTimer {
    UIAppDelegate.shouldRegisterTouch = NO;
    [UIAppDelegate stopIdleTimers];
}


- (void)startTimer {
    [UIAppDelegate startIdleTimers];
    UIAppDelegate.shouldRegisterTouch = YES;
}

- (void)dealloc {
    [_mediaPlayer pause];
    [_mediaPlayer removeObserverForPlayerEvents:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Observers

- (void)recordClosed:(NSNotification *)notification {
    if ([notification.userInfo[@"invokedFrom"] isEqualToString:@"recordContainer"]) {
        [_mediaPlayer pause];
        return;
    }
    [_mediaPlayer pause];
    [_activeCell syncUIForState:KKSelectedAudioItemCellStateIsPaused];
//    if (_audioBlock.identifier) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"didDismissAudioItem"
//                                                            object:nil
//                                                          userInfo:@{@"audioBlockID" : _audioBlock.identifier}];
//    }
}


- (void)pageChanged:(NSNotification *)notification {
    [self dismissActiveCells];
}

- (void)soundViewControllerDidPressPlayAudioItem:(NSNotification *)notification {
    NSNumber *audioBlockID = notification.userInfo[@"audioBlockID"];
    
    if (!_audioBlock.identifier) {
        return;
    }

    if ([audioBlockID isEqualToNumber:_audioBlock.identifier]) {
        return;
    } else {
        [self disactivateAudioItems];
        if (_audioBlock.identifier) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"didDismissAudioItem"
                                                                object:nil
                                                              userInfo:@{@"audioBlockID" : _audioBlock.identifier}];
        }
    }
}

- (void)videoItemDidPressPlay:(NSNotification *)notification {
    [_mediaPlayer pause];
    [self dismissActiveCells];
}

@end
