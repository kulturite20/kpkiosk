//
//  TestBlock.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 30.07.14.
//
//

#import "TestBlock.h"

@implementation TestBlock

- (id)initWithFiles:(NSArray *)files {
    if (self = [super init]) {
        _files = files;
    }
    return self;
}

@end
