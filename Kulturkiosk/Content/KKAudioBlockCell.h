//
//  KKAudioBlockCell.h
//  Kulturkiosk
//
//  Created by Roman Ivchenko on 14.08.14.
//
//

#import <UIKit/UIKit.h>

// Model
#import "KKSoundsBlockViewController.h"

extern NSString *const kAudioCellIdentifier;
extern CGFloat   const kAudioCellHeight;

@interface KKAudioBlockCell : UITableViewCell

@property (nonatomic, strong) KKSoundsBlockViewController *soundsContainer;

@end
