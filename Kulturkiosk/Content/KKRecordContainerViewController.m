//
//  KKRecordsContainerViewController.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 7/18/14.
//
//

//Controllers
#import "KKRecordContainerViewController.h"
#import "KKRecordPageViewController.h"
#import "KKMediaItemViewController.h"

//3rd party
#import "DMLazyScrollView.h"

//For testing
#import "TestFile.h"

@interface KKRecordContainerViewController ()
<
DMLazyScrollViewDelegate,
KKRecordPageViewControllerDelegate
>
@end

@implementation KKRecordContainerViewController {
    __weak IBOutlet DMLazyScrollView         *_lazyScrollView;
    __weak IBOutlet UIPageControl            *_pageControl;
    __weak IBOutlet UIActivityIndicatorView  *_activityIndicator;
    __weak IBOutlet UILabel *_pageLabel;
    __weak IBOutlet UIView *_pageControlContainer;
    
    __weak IBOutlet UIButton *_previousArrowButton;
    __weak IBOutlet UIButton *_nextArrowButton;
    __weak IBOutlet UIButton *_dismissButton;
    
    NSArray *_pages;
}

#pragma mark - View's Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //For testing
    // _pages = [_content pages];
    [_activityIndicator startAnimating];
    
    Content *content = [_record contentForLanguage:[KKLanguage currentLanguage]];
    
    _pages = [[content pages] allObjects];
    _pageControl.currentPage = 0;
    
    _pageLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %@", [KKLanguage localizedString:@"Page"], @(_pageControl.currentPage + 1), [KKLanguage localizedString:@"of"], @([_pages count])];
    
    if ([_pages count] == 0) {
        
        _dismissButton.hidden = NO;
        [_activityIndicator stopAnimating];
        _nextArrowButton.hidden     = YES;
        _previousArrowButton.hidden = YES;
        
        _pageLabel.hidden = YES;
        
    } else if ([_pages count] == 1) {
        _pageControlContainer.hidden = YES;

    } else {
        CGRect rect = CGRectMake(CGRectGetMinX(_lazyScrollView.frame), CGRectGetMinY(_lazyScrollView.frame), CGRectGetWidth(_lazyScrollView.frame), CGRectGetHeight(_lazyScrollView.frame) - CGRectGetHeight(_pageControlContainer.frame));
        _lazyScrollView.frame = rect;
        _pageControlContainer.hidden = NO;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(childControllersDidLoad:)
                                                 name:@"childControllersDidLoad"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showVideoInFullscreen:)
                                                 name:@"showBlockVideoFullscreen"
                                               object:nil];
    
    // Setting up datasource.
    NSMutableArray *controllersForPages = [NSMutableArray new];
    NSArray *sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"position"
                                                               ascending:YES]];
    NSArray *sortedPages = [_pages sortedArrayUsingDescriptors:sortDescriptors];
    
    [sortedPages enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        Page *page = (Page *)obj;
        
        
        KKRecordPageViewController *recordPageController = [KKRecordPageViewController new];
        recordPageController.page = page;
        
        if (idx == 0) {
            // set autoplay flag if it is first resource
            for (VideoBlock *videoBlock in [page videoBlocks]) {
                for (File *file in [videoBlock files]) {
                    if ([file.type isEqualToString:@"video"]) {
                        
                        // For autoplay video if video first resource.
                        if ([file.position integerValue] == 1) {
                            recordPageController.isFirstVideo = YES;
                        }
                    }
                }
            }
        }
        
        recordPageController.delegate = self;
        
        // Set property for hide Close Button if the record is single
        if (self.isSingle) {
            recordPageController.isShouldHideCloseButton = YES;
        }
        //For testing
        //        recordPageController.testAudioBlocks = @[audioBlock1, audioBlock2, audioBlock3, audioBlock4];
        [controllersForPages addObject:recordPageController];
        
    }];
    
    _controllersForPages = [NSArray arrayWithArray:controllersForPages];

    // Setting up lazy scroll view.
    __weak __typeof (&*self) weakSelf = self;
    _lazyScrollView.dataSource = ^(NSUInteger index) {
        return [weakSelf pageAtIndex:index];
    };
    
    _lazyScrollView.numberOfPages = [_pages count];
    _lazyScrollView.controlDelegate = self;
    
    // Setting up number of pages for page controller to show pages indication
    _pageControl.numberOfPages = [_pages count];
    _previousArrowButton.hidden = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    //[self.view layoutSubviews];
    
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    //[self.view layoutSubviews];
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UIScrollViewController Delegate

- (void)lazyScrollView:(DMLazyScrollView *)pagingView currentPageChanged:(NSInteger)currentPageIndex {
    _pageControl.currentPage = currentPageIndex;
    _pageLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %@", [KKLanguage localizedString:@"Page"], @(_pageControl.currentPage + 1), [KKLanguage localizedString:@"of"], @([_pages count])];
    [_activityIndicator stopAnimating];
}

#pragma mark - Observers

- (void)childControllersDidLoad:(NSNotification *)notification {
    [_activityIndicator stopAnimating];
}

- (void)showVideoInFullscreen:(NSNotification *)notification {
    File *videoFile = notification.userInfo[@"videoFile"];
    KKMediaItemViewController *videoController = [KKMediaItemViewController new];
    videoController.mediaItem = videoFile;
    videoController.isInFullscreenMode = YES;
    videoController.isInVideoGalleryMode = NO;
    videoController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:videoController
                       animated:YES
                     completion:NULL];
}


#pragma mark - Private

- (KKRecordPageViewController *)pageAtIndex:(NSInteger)index {
    return _controllersForPages[index];
}

#pragma mark - Callbacks

- (IBAction)previousPageClicked:(UIButton *)sender {
    [UIAppDelegate startIdleTimers];
    UIAppDelegate.shouldRegisterTouch = YES;
    
    if ([_pageControl currentPage] == 0) {
        return;
    }
    if ([_pageControl currentPage] == 1) {
        _previousArrowButton.hidden = YES;
    }
    _nextArrowButton.hidden = NO;
    [_activityIndicator startAnimating];
    [_lazyScrollView setPage:([_pageControl  currentPage] - 1)
                  transition:DMLazyScrollViewTransitionBackward
                    animated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pageChanged"
                                                        object:nil];
}

- (IBAction)nextPageClicked:(UIButton *)sender {
    [UIAppDelegate startIdleTimers];
    UIAppDelegate.shouldRegisterTouch = YES;
    
    if (([_pageControl currentPage] + 1) == [_pages count]) {
        return;
    }
    if (([_pageControl currentPage] + 1) == ([_pages count] - 1)) {
        _nextArrowButton.hidden = YES;
    }
    _previousArrowButton.hidden = NO;
    [_activityIndicator startAnimating];
    [_lazyScrollView setPage:([_pageControl  currentPage] + 1)
                  transition:DMLazyScrollViewTransitionForward
                    animated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pageChanged"
                                                        object:nil];
}

- (IBAction)dismissButtonClicked:(UIButton *)sender {
    [self recordPageControllerShouldCloseWholeRecord:nil];
}

#pragma mark - KKRecordPageViewControllerDelegate

- (void)recordPageControllerShouldCloseWholeRecord:(KKRecordPageViewController *)recordPage {
    UIAppDelegate.needToShowDefaultRecord = NO;
    if (_isSingle) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"recordClosed"
                                                            object:nil
                                                          userInfo:@{@"invokedFrom" : @"recordContainer"}];
        [UIAppDelegate startIdleTimers];
        UIAppDelegate.shouldRegisterTouch = YES;
        [_navBarDelegate recordContainerDidDismiss:self];
    } else {
        [self dismissViewControllerAnimated:YES
                                 completion:^{
                                     [_timeLineDelegate recordContainerDidDismiss:self];
                                     [[NSNotificationCenter defaultCenter] postNotificationName:@"recordClosed"
                                                                                         object:self
                                                                                       userInfo:@{@"invokedFrom" : @"recordContainer"}];
                                     [UIAppDelegate startIdleTimers];
                                     UIAppDelegate.shouldRegisterTouch = YES;
                                 }];
    }
}

- (CGFloat)heightControlContainer:(KKRecordPageViewController *)recordPage {
    if ([_pages count] > 1) {
        return CGRectGetHeight(_pageControlContainer.frame);
    } else {
        return 0.0f;
    }
}

@end
