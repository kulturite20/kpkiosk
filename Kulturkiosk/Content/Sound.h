//
//  Sound.h
//  VKTeleport
//
//  Created by Vadym Osovets on 11/17/13.
//  Copyright (c) 2013 Micro-B. All rights reserved.
//

// Frameworks
#import <MediaPlayer/MediaPlayer.h>
#import <Foundation/Foundation.h>

// Model
#import "File.h"

typedef void (^SoundProgressBlock)(CGFloat value);

@interface Sound : NSObject

@property (nonatomic, readonly) NSNumber  *soundID;
@property (nonatomic, readonly) NSString  *title;
@property (nonatomic, readonly) NSString  *creator;
@property (nonatomic, readonly) NSString  *genre;
@property (nonatomic, readonly) NSString  *albumName;
@property (nonatomic, readonly) NSString  *artist;
@property (nonatomic, readonly) UIImage   *artwork;
@property (nonatomic, readonly) NSString  *creationDate;
@property (nonatomic, readonly) NSInteger duration;
@property (nonatomic, readonly) NSInteger listenCount;

@property (nonatomic, assign) BOOL isPlaying;
@property (nonatomic, copy) SoundProgressBlock progressBlock;
@property (nonatomic) File *file;

@property (nonatomic, readonly) NSURL     *relativeURL;

- (instancetype)initWithMediaItem:(MPMediaItem *)item;
- (instancetype)initWithFullPath:(NSURL *)fullPath;

- (BOOL)isURLValid;

@end
