//
//  MediaPlayer.m
//  EhanceCards
//
//  Created by Vadim Osovets on 11/17/13.
//  Copyright (c) 2013 Micro-B. All rights reserved.
//

#import "MediaPlayer.h"

#import "Sound.h"

//frameworks
#import <AVFoundation/AVFoundation.h>
#import <libkern/OSAtomic.h>

typedef enum {
    kMediaEventTypeStartPlaying,
    KMediaEventTypePause,
    KMediaEventTypeFail,
    KMediaEventTypeSoundEnded,
    kMediaEventTypeTrackTimeChanged
} MediaEventType;

NSString *const kMediaPlayerDomain  = @"MediaPlayer Error Domain";
NSInteger const kFileCorruptedCode  = 1001;
NSInteger const kNoSoundsToPlayCode = 1002;
NSInteger const kPlayerFailedCode   = 1003;

static NSString *const kErrorKey        = @"Error";
static NSString *const kTimeValueKey    = @"TimeValue";

@implementation MediaPlayer {
    NSMutableArray  *_eventObservers;
    NSMutableArray  *_sounds;
    
    NSInteger       _currentSoundIndex;
    AVPlayer        *_player;
    id              _currentTimeObserver;
}

+ (instancetype)shared {
    static MediaPlayer *sharedPlayer = nil;
    static void * volatile pointerToShared = NULL;
    
    MediaPlayer *candidate = [MediaPlayer new];
    OSAtomicCompareAndSwapPtrBarrier(NULL, (__bridge void *)candidate, &pointerToShared);
    sharedPlayer = (__bridge MediaPlayer *)(pointerToShared);
    
    return sharedPlayer;
}

#pragma mark - Lifecycle

- (instancetype)init {
    if (self = [super init]) {
        //if user wont set sounds but will play sound.
        _sounds             = [NSMutableArray new];
        _eventObservers     = [NSMutableArray new];
        
        //set audio session
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback
                                               error:nil];
        [[AVAudioSession sharedInstance] setActive:YES
                                             error:nil];
        [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
        //add observers
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playerItemDidReachEnd:)
                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                   object:nil];
        //player will be created for every song
    }
    return self;
}

- (void)dealloc {
    //unsubscribe
    [_player removeObserver:self
                 forKeyPath:@"status"];
    [_player removeObserver:self
                 forKeyPath:@"rate"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
}

#pragma mark - Dynamic properties

- (void)setSounds:(NSArray *)soundsArray {
    _currentSoundIndex = 0;
    _sounds = [soundsArray mutableCopy];
    [self pause];
}

- (void)setVolume:(CGFloat)volume {
    _player.volume = volume;
}

- (CGFloat)volume {
    return _player.volume;
}

#pragma mark - Status

- (BOOL)isPlaying {
    return _player.rate > 0 && _player.currentItem;
}

#pragma mark - Controls

- (void)play {
    //1. check if we can play sounds
    if ([self haveSomethingToPlay]) {
        //safe access
        if ([self isIndexCorrect]) {
            Sound *currentSound = _sounds[_currentSoundIndex];
            
            //2.check for mp3 file
            if ([currentSound isURLValid]) {
                [self recreatePlayerWithSound:currentSound];
                [_player play];

                //4.set current info for media center
//                MPMediaItemArtwork *artwork = [[MPMediaItemArtwork alloc] initWithImage:currentSound.artwork];
                NSDictionary *nowPlayingInfo = @{MPNowPlayingInfoPropertyElapsedPlaybackTime    : @0,
                                                 MPNowPlayingInfoPropertyPlaybackRate           : @(_player.rate),
                                                 MPNowPlayingInfoPropertyPlaybackQueueIndex     : @(_currentSoundIndex),
                                                 MPNowPlayingInfoPropertyPlaybackQueueCount     : @(_sounds.count),
                                                 MPMediaItemPropertyTitle                       : currentSound.title,
                                                 MPMediaItemPropertyArtist                      : currentSound.artist,
                                                 MPMediaItemPropertyPlaybackDuration            : @(currentSound.duration)
                                                 };
                // NowPlayingCenter bug - if i provide it with the same dict, it wouldn't change anything, and would cause 'passed time' bug
                [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = nowPlayingInfo;
                MediaPlayer *__weak weakSelf = self;
                _currentTimeObserver = [_player addPeriodicTimeObserverForInterval:CMTimeMake(1, 1)
                                                                             queue:dispatch_get_main_queue()
                                                                        usingBlock:^(CMTime time) {
                                                                            NSValue *timeForDict = [NSValue valueWithCMTime:time];
                                                                            [weakSelf notifyObserversWithEvent:kMediaEventTypeTrackTimeChanged
                                                                                                additionalInfo:@{
                                                                                                                 kTimeValueKey : timeForDict
                                                                                                                 }];
                                                                        }];
            } else {
                //2. no file found - remove this sound from queue and process further
                NSError *soundCorruptedError = [NSError errorWithDomain:kMediaPlayerDomain
                                                                   code:kFileCorruptedCode
                                                               userInfo:nil];
                [self notifyObserversWithEvent:KMediaEventTypeFail
                                additionalInfo:@{kErrorKey : soundCorruptedError}];
                
                [self removeSoundFromQueue:currentSound];
            }
        }
    } else {
        //1. nothing to play. we should shutdown our player in case it's playing some sound
        [self shutdownPlayer];
        NSError *noSoundError = [NSError errorWithDomain:kMediaPlayerDomain
                                                    code:kNoSoundsToPlayCode
                                                userInfo:nil];
        [self notifyObserversWithEvent:KMediaEventTypeFail
                        additionalInfo:@{kErrorKey : noSoundError}];
    }
}

- (void)resume {
    if (_player.currentItem) {
        [_player play];
    }
}

- (void)pause {

    if (_player.currentItem) {
        [_player pause];
    }
}

- (void)nextTrack {
    [self incrementIndex];
    [self play];
}

- (void)previousTrack {
    [self decrementIndex];
    [self play];
}

#pragma mark - Concrete Play/Remove

- (void)playSound:(Sound *)sound {
    
    if ([_sounds containsObject:sound]) {
        _currentSoundIndex = [_sounds indexOfObject:sound];
    } else {
        //starts playing this sound, implicitly adding it to our queue
        _currentSoundIndex = _sounds.count;
        [_sounds addObject:sound];
    }
    
    [self play];
}

- (void)playSingleSound:(Sound *)sound {
    [self setSounds:@[sound]];
    [self play];
}

- (void)removeSoundFromQueue:(Sound *)sound {
    NSInteger indexOfSound = [_sounds indexOfObject:sound];
    if (indexOfSound != NSNotFound) {
        Sound *currentSound = [self currentTrack];
        
        //decrement index
        if (indexOfSound <= _currentSoundIndex) {
            [self decrementIndex];
        }

        [_sounds removeObject:sound];
        if ([currentSound isEqual:sound] && [self isPlaying]) {
            [self nextTrack];
        }
    }
}

#pragma mark - Seeking

- (NSTimeInterval)currentTime {
    return CMTimeGetSeconds(_player.currentTime);
}

- (void)seekToTime:(NSTimeInterval)time {
    [_player seekToTime:CMTimeMake(time, 1)];
}

- (void)seekForwardForTime:(NSTimeInterval)time {
   [self seekToTime:CMTimeGetSeconds([_player currentTime]) + time];
}

- (void)seekBackwardForTime:(NSTimeInterval)time {
    [self seekToTime:CMTimeGetSeconds([_player currentTime]) - time];
}

#pragma mark - Current info

- (Sound *)currentTrack {
    if ([self isIndexCorrect]) {
        return _sounds[_currentSoundIndex];
    } else {
        return nil;
    }
}

#pragma mark - Observers

- (void)addObserverForPlayerEvents:(id <MediaPlayerDelegate>)object {
    [_eventObservers addObject:object];
}

- (void)removeObserverForPlayerEvents:(id <MediaPlayerDelegate>)object {
    [_eventObservers removeObject:object];
}

#pragma mark - Notifications

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    [self seekToTime:0];
    [self pause];
    [self notifyObserversWithEvent:KMediaEventTypeSoundEnded
                    additionalInfo:nil];
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    NSNumber *newValue = change[@"new"];
    
    if ([keyPath isEqualToString:@"status"]) {
        //fail
        if ([newValue integerValue] == AVPlayerStatusFailed) {
            [self recreatePlayerWithSound:nil];
            NSError *playerFailedError = [NSError errorWithDomain:kMediaPlayerDomain
                                                             code:kPlayerFailedCode
                                                         userInfo:nil];
            [self notifyObserversWithEvent:KMediaEventTypeFail
                            additionalInfo:@{kErrorKey : playerFailedError}];

        }
    } else if ([keyPath isEqualToString:@"rate"]) {
        //play/pause
        if ([newValue floatValue] > 0) {
            [self notifyObserversWithEvent:kMediaEventTypeStartPlaying
                            additionalInfo:nil];
        } else {
            [self notifyObserversWithEvent:KMediaEventTypePause
                            additionalInfo:nil];
        }
    }
}

#pragma mark - Private

- (void)shutdownPlayer {
    [self recreatePlayerWithSound:nil];
}

- (void)recreatePlayerWithSound:(Sound *)sound {
    /** Cleanup */
    [_player pause];
    [_player removeTimeObserver:_currentTimeObserver];
    [self subscribeForPlayerEvents:NO];
    _player = nil;
    
    if (sound) {
        AVPlayerItem *currentItem = [AVPlayerItem playerItemWithURL:sound.relativeURL];
        
        //unsubscribe from rate/status KV's because we are going to deallocate player instance
        _player = [AVPlayer playerWithPlayerItem:currentItem];
        [self subscribeForPlayerEvents:YES];
    }
}

//safe index increment
- (void)incrementIndex {
    if (++_currentSoundIndex >= _sounds.count) {
        _currentSoundIndex = 0;
    }
}

- (void)decrementIndex {
    if (--_currentSoundIndex < 0) {
        _currentSoundIndex = [self haveSomethingToPlay] ? _sounds.count - 1 : 0;
    }
}

//correctness check
- (BOOL)isIndexCorrect {
    //used for safe-array access
    return _currentSoundIndex >= 0 && _currentSoundIndex < _sounds.count;
}

- (BOOL)haveSomethingToPlay {
    return _sounds.count > 0;
}

//KVO subscribing/unsubscribing
- (void)subscribeForPlayerEvents:(BOOL)subscribe {
    if (subscribe) {
        [_player addObserver:self
                  forKeyPath:@"status"
                     options:NSKeyValueObservingOptionNew
                     context:NULL];
        
        [_player addObserver:self
                  forKeyPath:@"rate"
                     options:NSKeyValueObservingOptionNew
                     context:NULL];
    } else {
        [_player removeObserver:self forKeyPath:@"status"];
        [_player removeObserver:self forKeyPath:@"rate"];
    }
}

//observers
- (void)notifyObserversWithEvent:(MediaEventType)eventType additionalInfo:(NSDictionary *)info {
    
    switch (eventType) {
        case KMediaEventTypePause:
            for (id <MediaPlayerDelegate> observer in _eventObservers) {
                if ([observer respondsToSelector:@selector(mediaPlayerDidPause:)]) {
                    [observer mediaPlayerDidPause:self];
                }
            }
            break;
        case KMediaEventTypeFail:
            for (id <MediaPlayerDelegate> observer in _eventObservers) {
                if ([observer respondsToSelector:@selector(mediaPlayer:didFailWithError:)]) {
                    [observer mediaPlayer:self didFailWithError:info[kErrorKey]];
                }
            }
            break;
        case kMediaEventTypeStartPlaying:
            for (id <MediaPlayerDelegate> observer in _eventObservers) {
                if ([observer respondsToSelector:@selector(mediaPlayerDidStartPlaying:)]) {
                    [observer mediaPlayerDidStartPlaying:self];
                }
            }
            break;
        case KMediaEventTypeSoundEnded:
            for (id <MediaPlayerDelegate> observer in _eventObservers) {
                if ([observer respondsToSelector:@selector(mediaPlayer:didFinishPlayingSound:)]) {
                    [observer mediaPlayer:self didFinishPlayingSound:[self currentTrack]];
                }
            }
        case kMediaEventTypeTrackTimeChanged:
            for (id <MediaPlayerDelegate> observer in _eventObservers) {
                if ([observer respondsToSelector:@selector(mediaPlayer:reportsCurrentTime:)]) {
                    CMTime currentTime = [info[kTimeValueKey] CMTimeValue];
                    [observer mediaPlayer:self reportsCurrentTime:CMTimeGetSeconds(currentTime)];
                }
            }
            break;
        default:
            break;
    }
    
}

@end
