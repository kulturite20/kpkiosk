//
//  KKTextBlockCell.m
//  Kulturkiosk
//
//  Created by Roman Ivchenko on 14.08.14.
//
//

#import "KKTextBlockCell.h"
#import "KKTextBlockViewController.h"

NSString *const kTextCellIdentifier = @"TextBlockCell";
CGFloat   const kTextCellHeight = 169.0;

@implementation KKTextBlockCell {
    __weak IBOutlet UIView *_textBlockContainer;
}

#pragma mark - Dynamic

- (void)setTextContainer:(KKTextBlockViewController *)textContainer {
    [self cleanUp];
    [_textBlockContainer addSubview:textContainer.view];
}

- (void)cleanUp {
    for (UIView *view in _textBlockContainer.subviews) {
        [view removeFromSuperview];
    }
}

@end
