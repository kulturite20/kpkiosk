//
//  KKRecordTextViewController.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 7/18/14.
//
//

#import <UIKit/UIKit.h>
#import "TextBlock.h"

//For testing
#import "TestTextBlock.h"

@interface KKTextBlockViewController : UIViewController

@property (nonatomic, strong) TextBlock *textBlock;

//For testing
//@property (nonatomic) TestTextBlock *testTextBlock;

@end
