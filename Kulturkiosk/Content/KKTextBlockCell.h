//
//  KKTextBlockCell.h
//  Kulturkiosk
//
//  Created by Roman Ivchenko on 14.08.14.
//
//

#import <UIKit/UIKit.h>

// Model
#import "KKTextBlockViewController.h"

extern NSString *const kTextCellIdentifier;
extern CGFloat   const kTextCellHeight;

@interface KKTextBlockCell : UITableViewCell

@property (nonatomic, strong) KKTextBlockViewController *textContainer;

@end
