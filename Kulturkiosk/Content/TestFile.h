//
//  TestFile.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 30.07.14.
//
//

#import <Foundation/Foundation.h>

@interface TestFile : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSURL    *path;
@property (nonatomic, strong) NSString *additionalInfo;
@property (nonatomic, assign) BOOL isActive;
@property (nonatomic, strong) NSString *type;

- (id)initWithName:(NSString *)name;
- (id)initWithName:(NSString *)name
              path:(NSURL *)path;
- (id)initWithName:(NSString *)name
              path:(NSURL *)path
    additionalInfo:(NSString *)addInfo;

@end
