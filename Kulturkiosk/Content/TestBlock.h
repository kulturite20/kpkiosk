//
//  TestBlock.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 30.07.14.
//
//

#import <Foundation/Foundation.h>

@interface TestBlock : NSObject

@property (nonatomic, strong) NSArray *files;

- (id)initWithFiles:(NSArray *)files;

@end
