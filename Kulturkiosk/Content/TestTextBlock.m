//
//  TestTextBlock.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 30.07.14.
//
//

#import "TestTextBlock.h"

@implementation TestTextBlock

- (id)initWithText:(NSString *)text {
    if (self = [super init]) {
        _text = text;
    }
    return self;
}

@end
