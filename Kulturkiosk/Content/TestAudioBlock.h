//
//  TestAudioBlock.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 30.07.14.
//
//

#import <Foundation/Foundation.h>
#import "TestBlock.h"

@interface TestAudioBlock : TestBlock

@property (nonatomic, strong) NSNumber *identifier;

@end
