//
//  KKVideoBlockCell.h
//  Kulturkiosk
//
//  Created by Roman Ivchenko on 14.08.14.
//
//

#import <UIKit/UIKit.h>

// Controllers
#import "KKMediaItemsContainerViewController.h"

extern NSString *const kVideoCellIdentifier;
extern CGFloat   const kVideoCellHeight;

@interface KKVideoBlockCell : UITableViewCell

@property (nonatomic,strong) KKMediaItemsContainerViewController *videoContainer;

@end
