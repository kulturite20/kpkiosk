//
//  KKAudioBlockCell.m
//  Kulturkiosk
//
//  Created by Roman Ivchenko on 14.08.14.
//
//

#import "KKAudioBlockCell.h"
#import "KKSoundsBlockViewController.h"

NSString *const kAudioCellIdentifier = @"AudioBlockCell";
CGFloat   const kAudioCellHeight = 100.0;

@implementation KKAudioBlockCell {
    __weak IBOutlet UIView *_internalAudioContainer;
}

#pragma mark - Dynamic

- (void)setSoundsContainer:(KKSoundsBlockViewController *)soundsContainer {
    [self cleanUp];
    [_internalAudioContainer addSubview:soundsContainer.view];
}

#pragma mark - Private

- (void)cleanUp {
    for (UIView *view in _internalAudioContainer.subviews) {
        [view removeFromSuperview];
    }
}

@end
