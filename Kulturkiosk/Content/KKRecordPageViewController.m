//
//  KKRecordViewController.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 7/18/14.
//
//

// Controllers
#import "KKRecordPageViewController.h"
#import "KKMediaItemsContainerViewController.h"
#import "KKTextBlockViewController.h"
#import "KKSoundsBlockViewController.h"
#import "KKVideoGalleryViewController.h"
#import "KKImageGalleryViewController.h"
#import "KKImageVideoGalleryViewController.h"

// Cells
#import "KKVideoBlockCell.h"
#import "KKTextBlockCell.h"
#import "KKAudioBlockCell.h"

//For testing
#import "TestAudioBlock.h"

static CGFloat const kCellInsetValue              = 20.0;

@interface KKRecordPageViewController ()
<
UIScrollViewDelegate,
KKMediaItemsContainerViewControllerDelegate
>

@end

@implementation KKRecordPageViewController {
    __weak IBOutlet UILabel      *_contentTitle;
    __weak IBOutlet UILabel      *_pageTitle;
    __weak IBOutlet UIScrollView *_scrollViewContainer;
    __weak IBOutlet UIButton     *_mapButton;
    __weak IBOutlet UIView       *_viewForImageGalleryButton;
    __weak IBOutlet UIView       *_viewForVideoGalleryButton;

    __weak IBOutlet UIView       *_pageTitleView;
    __weak IBOutlet UIView       *_blocksContainer;
    __weak IBOutlet UITableView  *_blocksTableView;
    
    __weak IBOutlet UIView   *_headerView;
    
    __weak IBOutlet UIButton *_detachedCloseRecordButton;
    __weak IBOutlet UIButton *_staticCloseRecordButton;
    
    __weak IBOutlet UILabel *_noDataAvailableLabel;

    __weak IBOutlet UILabel *_imageGalleryLabel;
    __weak IBOutlet UILabel *_videoGalleryLabel;
    
    NSMutableArray *_blockItems;
    NSMutableArray *_imageResources;
    NSMutableArray *_videoResources;
    NSMutableArray *_imageVideoResources;
    
    CGFloat         _actualBlocksContainerHeight;
    CGRect          _actualScrollFrame;
    BOOL _layoutWasInvalidated;
}

#pragma mark - View's lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _contentTitle.text = [[_page content] title];
    _scrollViewContainer.delaysContentTouches = YES;
    _scrollViewContainer.canCancelContentTouches = NO;
    
    _imageGalleryLabel.text = [KKLanguage localizedString:@"Photos"];
    _videoGalleryLabel.text = [KKLanguage localizedString:@"Videos"];
    
    // Setting up map marker
    for (Presentation *presentaton in [Presentation allObjects]) {
        if ([presentaton.type isEqualToString:@"map_image"]) {
            if ([[[_page content] record] recordDescriptionWithPresentation:presentaton]) {
                [_mapButton setHidden:NO];
            }
        }
    }
    
    // Getting videos and photos for video/photo gallery
    _imageVideoResources = [NSMutableArray new];
    for (VideoBlock *videoBlock in [_page videoBlocks]) {
        for (File *file in [videoBlock files]) {
            [_imageVideoResources addObject:file];
        }
    }
    
    // Getting videos for video gallery
    _videoResources = [NSMutableArray new];
    // Getting images for image gallery
    _imageResources = [NSMutableArray new];
    
    for (VideoBlock *videoBlock in [_page videoBlocks]) {
        for (File *file in [videoBlock files]) {
            if ([file.type isEqualToString:@"video"]) {
                [_videoResources addObject:file];
            } else if ([file.type isEqualToString:@"image"]) {
                [_imageResources addObject:file];
            }
        }
    }
    
    if ([_videoResources count] == 0) {
        _viewForVideoGalleryButton.hidden = YES;
        _viewForImageGalleryButton.frame = CGRectOffset(_viewForImageGalleryButton.frame, _viewForVideoGalleryButton.frame.size.width, 0);
    }
    
    if ([_imageResources count] == 0) {
        _viewForImageGalleryButton.hidden = YES;
    }
    
    if (_page.title && [_page.title length] > 0) {
        _pageTitleView.hidden = NO;
        _pageTitle.text = _page.title;
        _blocksContainer.frame = (CGRect){_blocksContainer.frame.origin.x,
            _blocksContainer.frame.origin.y,
            _blocksContainer.frame.size.width,
            _blocksContainer.frame.size.height};
        [_blocksContainer layoutSubviews];

    } else {
        _pageTitleView.hidden = YES;
        _blocksContainer.frame = (CGRect){_pageTitleView.frame.origin.x,
            _pageTitleView.frame.origin.y,
            _blocksContainer.frame.size.width,
            _blocksContainer.frame.size.height};

    }
    
    [self setUpBlockItems];

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(childTextContainersDidLoad:) name:@"childTextContainersDidLoad"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(childTableViewDidActivateAudioItem:) name:@"didPressPlayAudioItem"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(childTableViewDidDisactivateAudioItem:) name:@"didDismissAudioItem"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(layoutShouldBeInvalidatedNotification:)
                                                 name:@"shouldInvalidateLayout"
                                               object:nil];


    UINib *videoBlockCellNib = [UINib nibWithNibName:@"KKVideoBlockCell"
                                              bundle:nil];
    UINib *textBlockCellNib = [UINib nibWithNibName:@"KKTextBlockCell"
                                              bundle:nil];
    UINib *audioBlockCellNib = [UINib nibWithNibName:@"KKAudioBlockCell"
                                         bundle:nil];

    [_blocksTableView registerNib:videoBlockCellNib
               forCellReuseIdentifier:@"VideoBlockCell"];
    [_blocksTableView registerNib:textBlockCellNib
              forCellReuseIdentifier:@"TextBlockCell"];
    [_blocksTableView registerNib:audioBlockCellNib
               forCellReuseIdentifier:@"AudioBlockCell"];
    
    BOOL hasTextContainers = NO;
    for (id obj in _blockItems) {
        if ([obj isKindOfClass:[KKTextBlockViewController class]]) {
            hasTextContainers = YES;
        }
    }
    if (!hasTextContainers) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"childControllersDidLoad"
                                                            object:nil];
    }
    
    // Configure visibility of close button in case if Record have only 1 item.
    Device *device=[Device objectWithPredicate: [NSPredicate predicateWithFormat: @"identifier == %@" argumentArray:@[[[NSUserDefaults standardUserDefaults] objectForKey: @"device_id"]]]];
    if (!device.idleScreenActiveValue && device.presentations.count == 1 && self.isShouldHideCloseButton) {
        _detachedCloseRecordButton.hidden = YES;
        _staticCloseRecordButton.hidden = YES;
    } else {
        _detachedCloseRecordButton.hidden = NO;
        _staticCloseRecordButton.hidden = NO;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    CGRect scrollFrame = _scrollViewContainer.frame;
    
    if ([self.delegate respondsToSelector:@selector(heightControlContainer:)]) {
        
        // TODO: with unknown reason when we back to this screen after
        // we open new gallery heigh of bottom toolbar is not taking in mind
        // that is why currenly we need to apply this workaround:
        
        // if scroll view frame is whole screen
        // and bottom toolbar should be showing we need to decrease height of
        // scroll view appropriatelly
        
        UIWindow *window = [[[UIApplication sharedApplication] windows] firstObject];
        
        if (window.frame.size.height == scrollFrame.size.height) {
            scrollFrame.size.height -= [self.delegate heightControlContainer:self];
            _scrollViewContainer.frame = scrollFrame;
        }
    }

    if (!_layoutWasInvalidated && [_blockItems count] != 0) {

        _actualBlocksContainerHeight = 0;
        [_blocksTableView reloadData];
        [self setUpActualContainerHeight];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];

}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UIDeviceOrientation

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    return orientation;
}

- (BOOL) shouldAutorotate {
    return  YES;
}

#pragma mark - UITableViewDelegate/Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_blockItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id blockContainer = _blockItems[indexPath.row];
    if ([blockContainer isKindOfClass:[KKMediaItemsContainerViewController class]]) {
        KKVideoBlockCell *videoBlockCell = [tableView dequeueReusableCellWithIdentifier:kVideoCellIdentifier];
        KKMediaItemsContainerViewController *mediaContainer = _blockItems[indexPath.row];
        videoBlockCell.videoContainer = _blockItems[indexPath.row];
        
        
        return videoBlockCell;
    } else if ([blockContainer isKindOfClass:[KKTextBlockViewController class]]) {
        KKTextBlockCell *textBlockCell = [tableView dequeueReusableCellWithIdentifier:kTextCellIdentifier];
        textBlockCell.textContainer = _blockItems[indexPath.row];
        
        return textBlockCell;
    } else if ([blockContainer isKindOfClass:[KKSoundsBlockViewController class]]) {
        KKAudioBlockCell *audioBlockCell = [tableView dequeueReusableCellWithIdentifier:kAudioCellIdentifier];
        audioBlockCell.soundsContainer = _blockItems[indexPath.row];
        
        return audioBlockCell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    id blockContainer = _blockItems[indexPath.row];
    if ([blockContainer isKindOfClass:[KKMediaItemsContainerViewController class]]) {
        KKMediaItemsContainerViewController *mediaContainer = _blockItems[indexPath.row];
//        _actualBlocksContainerHeight += mediaContainer.view.frame.size.height + kCellInsetValue;
        return mediaContainer.view.frame.size.height + kCellInsetValue;
    } else if ([blockContainer isKindOfClass:[KKTextBlockViewController class]]) {
        KKTextBlockViewController *textContainer = _blockItems[indexPath.row];
//        _actualBlocksContainerHeight += textContainer.view.frame.size.height + kCellInsetValue;
        return textContainer.view.frame.size.height + kCellInsetValue;
    } else if ([blockContainer isKindOfClass:[KKSoundsBlockViewController class]]) {
        KKSoundsBlockViewController *soundContainer = _blockItems[indexPath.row];
//        _actualBlocksContainerHeight += soundContainer.view.frame.size.height + kCellInsetValue;
        return soundContainer.view.frame.size.height + kCellInsetValue;
    }
    return 0;
}


#pragma mark - KKMediaItemsContainerViewControllerDelegate

- (void)showImageVideoGalleryWithStartingIndex:(NSUInteger)index andData:(NSMutableArray *)data {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"recordClosed"
                                                        object:nil
                                                      userInfo:@{@"invokedFrom" : @"recordPage"}];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    KKImageVideoGalleryViewController *imageVideoGalleryView = [storyboard instantiateViewControllerWithIdentifier:@"imageVideoGalleryView"];
    
    imageVideoGalleryView.resources = data;
    
    imageVideoGalleryView.startGalleryWithIndex = index;
    
    [self presentViewController:imageVideoGalleryView
                       animated:YES
                     completion:NULL];
    
}

- (void)removeFromSourceItem:(KKMediaItemsContainerViewController *)mediaContainer {

    [_blockItems removeObject:mediaContainer];
    
    _layoutWasInvalidated = NO;
    _actualBlocksContainerHeight = 0;
    [_blocksTableView reloadData];
    [self setUpActualContainerHeight];
}


#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    BOOL shouldShowCloseButton = scrollView.contentOffset.y >= CGRectGetHeight(_headerView.frame);
    
    [self presentCloseButton:shouldShowCloseButton];
}


#pragma mark - Callbacks

- (IBAction)mapButtonClicked:(UIButton *)sender {
    //TODO: Implement me
}

- (IBAction)videoGalleryButtonClicked:(UIButton *)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"recordClosed"
                                                        object:nil
                                                      userInfo:@{@"invokedFrom" : @"recordPage"}];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    KKVideoGalleryViewController *videoGalleryView = [storyboard instantiateViewControllerWithIdentifier:@"videoGalleryView"];
    
    videoGalleryView.resources = [_videoResources sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"identifier" ascending:YES]]];
    
    [self presentViewController:videoGalleryView
                       animated:YES
                     completion:NULL];
}

- (IBAction)imageGalleryButtonClicked:(UIButton *)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"recordClosed"
                                                        object:nil
                                                      userInfo:@{@"invokedFrom" : @"recordPage"}];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    KKImageGalleryViewController *imageGalleryView = [storyboard instantiateViewControllerWithIdentifier:@"imageGalleryView"];
    NSArray *sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"identifier"
                                                               ascending:YES]];

    imageGalleryView.resources = [_imageResources sortedArrayUsingDescriptors:sortDescriptors];
    
    [self presentViewController:imageGalleryView
                       animated:YES
                     completion:NULL];
}

- (IBAction)closeRecordClicked:(id)sender {
    [self.delegate recordPageControllerShouldCloseWholeRecord:self];
}

#pragma mark - Observers

- (void)childTextContainersDidLoad:(NSNotification *)notification {
    _actualBlocksContainerHeight = 0.0;
    [_blocksTableView reloadData];
    [self setUpActualContainerHeight];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"childControllersDidLoad"
                                                        object:nil];
}

- (void)childTableViewDidActivateAudioItem:(NSNotification *)notification {
    NSNumber *audioBlockID = notification.userInfo[@"audioBlockID"];
    
    [_blockItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[KKSoundsBlockViewController class]]) {
            KKSoundsBlockViewController *audioContainer = (KKSoundsBlockViewController *)obj;
            if ([audioContainer.audioBlock.identifier isEqualToNumber:audioBlockID]) {
                _actualBlocksContainerHeight = 0.0;
                [_blocksTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:idx
                                                                              inSection:0]]
                                        withRowAnimation:UITableViewRowAnimationAutomatic];
                [self setUpActualContainerHeight];
            }
        }
    }];
}

- (void)childTableViewDidDisactivateAudioItem:(NSNotification *)notification {
    NSNumber *audioBlockID = notification.userInfo[@"audioBlockID"];

    [_blockItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[KKSoundsBlockViewController class]]) {
            KKSoundsBlockViewController *audioContainer = (KKSoundsBlockViewController *)obj;
            if ([audioContainer.audioBlock.identifier isEqualToNumber:audioBlockID]) {
                _actualBlocksContainerHeight = 0.0;
                [_blocksTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:idx
                                                                              inSection:0]]
                                        withRowAnimation:UITableViewRowAnimationAutomatic];
                [self setUpActualContainerHeight];
            }
        }
    }];
}

- (void)layoutShouldBeInvalidatedNotification:(NSNotification *)notification {
    id sender = [notification object];
    
    NSInteger itemIndex = [_blockItems indexOfObject:sender];
    
    if (itemIndex != NSNotFound) {
        
        _actualBlocksContainerHeight = 0.0;
        [CATransaction begin];
        [CATransaction setDisableActions:YES];
        [CATransaction setAnimationDuration:0.3];

        // Add any animation option, so it won't be really animated.
        [_blocksTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:itemIndex inSection:0]]
                                withRowAnimation:UITableViewRowAnimationNone];
        [self setUpActualContainerHeight];
        
        [CATransaction commit];
    }
}

#pragma mark - Private

- (void)setUpActualContainerHeight {
    
    _layoutWasInvalidated = YES;
    _actualBlocksContainerHeight = 0.0;
    
    for (UIViewController *blockController in _blockItems) {
        _actualBlocksContainerHeight += blockController.view.frame.size.height + kCellInsetValue;
    }
    
    _blocksContainer.frame = CGRectMake(_blocksContainer.frame.origin.x, _blocksContainer.frame.origin.y, _blocksContainer.frame.size.width, _actualBlocksContainerHeight);
    _blocksTableView.frame = CGRectMake(_blocksTableView.frame.origin.x, _blocksTableView.frame.origin.y, _blocksTableView.frame.size.width, _actualBlocksContainerHeight);
    
    if (![_headerView isHidden]) {
        _actualBlocksContainerHeight += CGRectGetHeight(_headerView.frame) + kCellInsetValue;
    }
    
    if (![_pageTitleView isHidden]) {
        _actualBlocksContainerHeight += CGRectGetHeight(_pageTitleView.frame) + kCellInsetValue;
    }
    
    _actualBlocksContainerHeight += kCellInsetValue;
    _scrollViewContainer.contentSize = CGSizeMake(_scrollViewContainer.frame.size.width, _actualBlocksContainerHeight);
}

- (void)setUpBlockItems {
    _blockItems = [NSMutableArray new];
    NSArray *sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"position"
                                                               ascending:YES]];
    
    NSArray *blocksData = [[[_page blocks] allObjects] sortedArrayUsingDescriptors:sortDescriptors];
    
    if ([blocksData count] == 0) {
        _blocksTableView.hidden = YES;
        _noDataAvailableLabel.hidden = NO;
        _blocksContainer.frame = (CGRect){_blocksContainer.frame.origin.x,
                                          _blocksContainer.frame.origin.y,
                                          _blocksContainer.frame.size.width,
                                            550.0};
    }
    for (Block *block in blocksData) {
        if ([block isKindOfClass:[VideoBlock class]]) {
            KKMediaItemsContainerViewController *videoContainer = [KKMediaItemsContainerViewController new];
            
            // For autoplay video if video first resource.
            if ([block.position intValue] == 1 && self.isFirstVideo) {
                videoContainer.shouldPlayAtStart = YES;
            }
            
            videoContainer.videoBlock = (VideoBlock *)block;
            videoContainer.delegate = self;
            [_blockItems addObject:videoContainer];
        }
        if ([block isKindOfClass:[AudioBlock class]]) {
            KKSoundsBlockViewController *audioContainer = [KKSoundsBlockViewController new];
            audioContainer.audioBlock = (AudioBlock *)block;
            [_blockItems addObject:audioContainer];
        }
        if ([block isKindOfClass:[TextBlock class]]) {
            KKTextBlockViewController *textContainer = [KKTextBlockViewController new];
            textContainer.textBlock = (TextBlock *)block;
            [_blockItems addObject:textContainer];
        }
    }
}

- (void)presentCloseButton:(BOOL)present {
    // If button is already presented/hidden - ignore.
    BOOL isAlreadyPresented = _detachedCloseRecordButton.alpha == 1;
    if (present == isAlreadyPresented) {
        return;
    }
    
    dispatch_block_t animationBlock = ^{
        _detachedCloseRecordButton.alpha = 1;
    };
    
    if (!present) {
        animationBlock = ^{
            _detachedCloseRecordButton.alpha = 0;
        };
    }
    
    // TODO: Const.
    [UIView animateWithDuration:0.4
                     animations:animationBlock];
}

@end
