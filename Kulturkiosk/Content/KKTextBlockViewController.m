//
//  KKRecordTextViewController.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 7/18/14.
//
//

#import "KKTextBlockViewController.h"

@interface KKTextBlockViewController ()
<
UIWebViewDelegate
>
@end

@implementation KKTextBlockViewController {
    __weak IBOutlet UIWebView *_webView;
    __weak IBOutlet UIView    *_noDataAlertScreen;
}

#pragma mark - View's lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    _webView.delegate = self;
    [_webView setOpaque:NO];
    if ([_textBlock.text length] > 0) {
        [_webView loadHTMLString:[NSString stringWithFormat:@"<html><body style=\"background-color:clear; font-size: 17; font-family: HelveticaNeue; color: #ffffff\">%@</body></html>", _textBlock.text] baseURL:nil];
    } else {
        [_noDataAlertScreen setHidden:NO];
    }
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    CGFloat webViewHeight = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight;"] floatValue];
    [self setupActualHeight:webViewHeight];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"childTextContainersDidLoad"
                                                        object:nil];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    NSLog(@"Error while loading UIWebView: %@", [error localizedDescription]);
}

#pragma mark - Private

- (void)setupActualHeight:(CGFloat)height {
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, height);
}

#pragma mark - dealloc

- (void)dealloc {
    [_webView setDelegate:nil];
    [_webView stopLoading];
}

@end
