//
//  TestTextBlock.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 30.07.14.
//
//

#import <Foundation/Foundation.h>
#import "TestBlock.h"

@interface TestTextBlock : TestBlock

@property (nonatomic) NSString *text;

- (id)initWithText:(NSString *)text;

@end
