//
//  KKSelectedAudioItemCell.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 04.08.14.
//
//

#import <UIKit/UIKit.h>
#import "Sound.h"

@class KKSelectedAudioItemCell;

typedef enum {
    KKSelectedAudioItemCellStateIsPlaying,
    KKSelectedAudioItemCellStateIsPaused
} KKSelectedAudioItemCellState;

@protocol KKSelectedAudioItemCellDelegate <NSObject>

// TODO: More better names
- (void)selectedAudioItemCellWillPresentAdditionalInfo:(KKSelectedAudioItemCell *)cell newHeight:(CGFloat)newHeight;
// TODO: Don't pass height, it's not used.
- (void)selectedAudioItemCellWillHideAdditionalInfo:(KKSelectedAudioItemCell *)cell newHeight:(CGFloat)newHeight;

- (void)selectedAudioItemCell:(KKSelectedAudioItemCell *)cell willDismissAtIndexPath:(NSIndexPath *)indexPath;
- (void)selectedAudioItemCellDidPressPauseButton:(KKSelectedAudioItemCell *)cell;
- (void)selectedAudioItemCellDidPressPlayButton:(KKSelectedAudioItemCell *)cell;
- (void)selectedAudioItemCell:(KKSelectedAudioItemCell *)cell didScrubWithValue:(CGFloat)value;

@end

@interface KKSelectedAudioItemCell : UITableViewCell

@property (nonatomic, weak) id <KKSelectedAudioItemCellDelegate> delegate;

@property (nonatomic, strong) Sound *selectedSound;
@property (nonatomic) File *rawFile;
@property (nonatomic) BOOL shouldPresentInfo;

@property (nonatomic, strong) NSIndexPath *indexPath;

- (void)syncUIForState:(KKSelectedAudioItemCellState)state;

@end
