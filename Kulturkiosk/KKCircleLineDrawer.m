//
//  KKCircleLineDrawer.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 3/24/17.
//
//

#import "KKCircleLineDrawer.h"
#import "UIColor+HexColor.h"
#import "MapTitle+CoreDataClass.h"
#import "MapText+CoreDataClass.h"
#import "MapDescription+CoreDataClass.h"
#import "NSMutableString + FormatHTMLTags.h"
#import "UILabel+Size.h"
#import "UIView+Simplified.h"
#import "UIAlertView+Blocks.h"

#import "KKMapPlacer.h"

@interface KKCircleLineDrawer ()

@property (nonatomic) CGSize maxSizeLabel;
@property (nonatomic) CGSize sizeLabel;
@property (nonatomic) BOOL isDrawed;
@property (nonatomic) UIView *textContainer;

@property (nonatomic, assign) CGFloat annotationOffset;
@property (nonatomic, assign) CGFloat scaleFactor;
@property (nonatomic, assign) CGFloat multiplier;

@property (nonatomic) MapImage *mapImage;
@property (nonatomic) MapTitle *mapTitle;
@property (nonatomic) MapDescription *mapDescription;

// chain of responsibility to place text around circe in a correspondense with placement,
// whith come from server-side and borders of main view
@property (nonatomic) NSArray<KKMapPlacer *> *placers;
@property (nonatomic) NSUInteger procedeedPlacersCounter;

@end


//MARK: Constants
//const CGFloat kDiamentrCircle = 24;
const CGFloat kDiamentrCircle = 24;
const CGFloat kbuttonCenterDeviation = 5.5;

const CGFloat kDistanceBetweenViews = 20.0;
const CGFloat kOpacity = 0.5;
const CGFloat kMaxLabelWidth = 400.0f;


@implementation KKCircleLineDrawer

#pragma mark - View lifecicle

- (instancetype)initWithMapView:(KKMapView *)mapView{
    self = [super init];
    if (self) {
        self.mapView = mapView;
        self.mapViewFrame = mapView.frame;
    }
    return self;
}

- (void)reloadCircleLineDrawer {
    
    [self mapTextConfigurationSetup];
    [self configureLabels];
    [self configurePositionOfLabels];
    [self configureTextContainer];
    
    [self setupLabelsPosition];
    [self addReccordButtonsOnDots];
    [self addCircleLineOnMapView];
    
}

#pragma mark - Initializer

- (void)creatCircleWith:(RecordDescription *)recordDescription annotationOffset:(CGFloat)annotationOffset scaleFactor:(CGFloat)scaleFactor {
    
    self.isDrawed = NO;
    
    self.recordDescription = recordDescription;
    self.mapImage = recordDescription.map_image;
    
    // TODO: Vadim fix me
    self.color = [UIColor redColor];//[UIColor colorWithHexColor:self.recordDescription.color];
    self.annotationOffset = annotationOffset;
    self.scaleFactor = scaleFactor;
    self.multiplier = annotationOffset * scaleFactor;
    
    //TODO: only for testing single dot
//    [self configureSingleCircle];
//    [self reloadCircleLineDrawer];
//    return;
    
    if ([self.mapImage.anchorOn boolValue]) {
        [self configureCircleWithAnchor];
    } else {
        [self configureSingleCircle];
    }
    
    [self reloadCircleLineDrawer];
    
}

- (void)updateSizeOfViews {
    
    CGAffineTransform transform = CGAffineTransformMakeScale(1.0 / self.mapView.zoomScale, 1.0 / self.mapView.zoomScale);

    self.firstView.transform = transform;
    self.secondView.transform = transform;
    
    self.recordButton.transform = transform;
    self.recordButton.center = self.firstView.center;
    
    self.secondRecordButton.transform = transform;
    self.secondRecordButton.center = self.secondView.center;
}


- (void)addCircleLineOnMapView {
    
    [self.mapView.containerView addSubview:self.textContainer];

    if(!self.isDrawed) {
        
        [self.mapView.containerView addSubview:self.firstView];
        [self.mapView.containerView addSubview:self.secondView];
        [self.mapView.containerView.layer addSublayer:self.line];
        [self.mapView.containerView addSubview:self.recordButton];
        [self.mapView.containerView addSubview:self.secondRecordButton];
        
        self.isDrawed = YES;
    }
    
}

#pragma mark - Configuration

- (void)configureSingleCircle {

    
    //startPointCircleButton configure
    self.firstView = [[UIButton alloc] initWithFrame:CGRectMake(([self.recordDescription.x intValue] * self.multiplier - kDiamentrCircle / 2) + kbuttonCenterDeviation,
                                                                ([self.recordDescription.y intValue] * self.multiplier - kDiamentrCircle / 2) + kbuttonCenterDeviation,
                                                                kDiamentrCircle,
                                                                kDiamentrCircle)];
    
   // self.firstView.transform = CGAffineTransformMakeScale(1.0 / self.mapView.zoomScale, 1.0 / self.mapView.zoomScale);
    
    
    self.firstView.layer.cornerRadius = kDiamentrCircle/2;
    self.firstView.tag = [self.recordDescription.record_id integerValue];
    self.firstView.backgroundColor = self.color;
    self.firstView.layer.opacity = kOpacity;
   // [self.firstView addTarget:self action:@selector(recordButtonClicked:) forControlEvents:UIControlEventTouchUpInside];

}

- (void)configureCircleWithAnchor {
    
    // For handling correct size of dots
    CGFloat zoomFactor = 1.0 / self.mapView.zoomScale;
    
    CGAffineTransform transform = CGAffineTransformMakeScale(zoomFactor, zoomFactor);
    
    self.firstView = [[UIView alloc] initWithFrame:CGRectMake(([self.recordDescription.x intValue] * self.multiplier - kDiamentrCircle / 2) + kbuttonCenterDeviation,
                                                              ([self.recordDescription.y intValue] * self.multiplier - kDiamentrCircle / 2) + kbuttonCenterDeviation,
                                                              kDiamentrCircle,
                                                              kDiamentrCircle)];
    self.firstView.layer.cornerRadius = kDiamentrCircle/2;
    self.firstView.tag = [self.recordDescription.record_id integerValue];
    self.firstView.backgroundColor = self.color;
    self.firstView.layer.opacity = kOpacity;
    self.firstView.transform = transform;
    
    //endPointCircleView configure
    self.secondView = [[UIView alloc] initWithFrame:CGRectMake(([self.mapImage.anchor_x intValue] * self.multiplier - kDiamentrCircle / 2) + kbuttonCenterDeviation,
                                                               ([self.mapImage.anchor_y intValue] * self.multiplier - kDiamentrCircle / 2) + kbuttonCenterDeviation,
                                                               kDiamentrCircle,
                                                               kDiamentrCircle)];
    
    self.secondView.layer.cornerRadius = kDiamentrCircle/2;
    self.secondView.layer.opacity = kOpacity;
    self.secondView.backgroundColor = self.color;
    self.secondView.transform = transform;
    
    
    CGFloat radius = (kDiamentrCircle * zoomFactor) / 2;
//    CGFloat radius = kDiamentrCircle/2;
    
    // Calculate x, y point in button circle view
    
    // Getting first cathetus
    CGFloat cathetusX = self.secondView.center.x - self.firstView.center.x;
    
    // Getting first cathetus
    CGFloat cathetusY = self.secondView.center.y - self.firstView.center.y;
    
    // Getting hypotenuse with Pythagorean theorem
    CGFloat hypotenuse = sqrtf(cathetusX * cathetusX + cathetusY * cathetusY);
    
    // Getting cosinus from first circle center
    CGFloat firstPointCos = cathetusX/hypotenuse;
    
    // Need to find triangle 2 cathetus in first cirle
    
    // Getting firstCircleCathetusX
    CGFloat firstCircleCathetusX = firstPointCos * radius;
    
    // Getting firstCircleCathetusY with Pythagorean theorem
    CGFloat firstCircleCathetusY = sqrtf(radius * radius - firstCircleCathetusX * firstCircleCathetusX);
    
    // Calculate x, y point in second circle view
    
    
    // Getting cosinus from second circle center
    CGFloat secondPointCos = cathetusY/hypotenuse;
    
    // Need to find triangle 2 cathetus in second cirle
    
    // Getting secondCircleCathetusY
    CGFloat secondCircleCathetusY = secondPointCos * radius;
    
    // Getting firstCircleCathetusY with Pythagorean theorem
    CGFloat secondCircleCathetusX = sqrtf(radius * radius - secondCircleCathetusY * secondCircleCathetusY);

    
    CGFloat firstPointX;
    CGFloat firstPointY;
    
    CGFloat secondPointX;
    CGFloat secondPointY;
    
    if (self.secondView.frame.origin.x < self.firstView.frame.origin.x) {
        secondPointX = self.secondView.center.x + secondCircleCathetusX;
    } else {
        secondPointX = self.secondView.center.x - secondCircleCathetusX;
    }
    
    secondPointY = self.secondView.center.y - secondCircleCathetusY;
    
    /*
    if (self.secondView.frame.origin.y < self.firstView.frame.origin.y) {
        secondPointY = self.secondView.center.y - secondCircleCathetusY;
    } else {
        secondPointY = self.secondView.center.y - secondCircleCathetusY;
    } */
    
    
    /*
    if (self.secondView.frame.origin.x < self.firstView.frame.origin.x) {
        firstPointX = self.firstView.center.x - firstCircleCathetusX;
    } else {
        firstPointX = self.firstView.center.x + firstCircleCathetusX;
    }*/
    
    firstPointX = self.firstView.center.x + firstCircleCathetusX;
    
    
    if (self.secondView.frame.origin.y < self.firstView.frame.origin.y) {
        firstPointY = self.firstView.center.y - firstCircleCathetusY;
    } else {
        firstPointY = self.firstView.center.y + firstCircleCathetusY;
    }
    
    //Creating firstCirclePoint and secondCirclePoint
    CGPoint secondCirclePoint = CGPointMake(secondPointX, secondPointY);
    
    CGPoint firstCirclePoint = CGPointMake(firstPointX, firstPointY);
    
    self.line = [self createLineFromPoint:&firstCirclePoint toPoint:&secondCirclePoint];

}

- (void)mapTextConfigurationSetup {
    
    for (MapText *mapText in self.recordDescription.map_text) {
        
        NSLog(@"REC: %@", self.recordDescription.record_id);
        NSLog(@"Map text: %@", mapText);
        
        if ([mapText.language isEqualToString: [KKLanguage contentForLanguage:[KKLanguage currentLanguage]]]) {
            self.mapTitle = mapText.mapTitle;
            self.mapDescription = mapText.mapDescription;
            break;
        }
    }
    
}

- (void)configureLabels {
    
    self.firstLabel = [UILabel new];
    self.firstLabel.font = [self.mapTitle fontWithSize];
    self.firstLabel.textColor = [UIColor colorWithHexColor: self.mapTitle.color];
    self.firstLabel.textAlignment = [self.mapTitle textAlignment];
    self.firstLabel.text = [NSMutableString formatLineBreak:self.mapTitle.content];
    
    self.secondLabel = [UILabel new];
    self.secondLabel.font = [self.mapDescription fontWithSize];
    self.secondLabel.textColor = [UIColor colorWithHexColor:self.mapDescription.color];
    self.secondLabel.textAlignment = [self.mapDescription textAlignment];
    self.secondLabel.text = [NSMutableString formatLineBreak:self.mapDescription.content];

}

- (void)configurePositionOfLabels {
    
    NSAttributedString *titleString = self.firstLabel.attributedText;
    NSAttributedString *descriptionString = self.secondLabel.attributedText;
    
    // Add text according to text position
    if ([self.mapTitle.position integerValue] > [self.mapDescription.position integerValue]) {
        self.firstLabel.attributedText = descriptionString;
        self.secondLabel.attributedText = titleString;
    }
    
}



- (void)configureTextContainer {
    //TODO: refactor hardcode
    CGSize maximumLabelSize = CGSizeMake(400, 150);
    CGFloat distanceBetweenText = 10.0;
    
    [self.firstLabel resizeForExpectedSize:maximumLabelSize andPosition:self.firstLabel.frame.origin];
    
    [self.secondLabel resizeForExpectedSize:maximumLabelSize andPosition:CGPointMake(self.secondLabel.frame.origin.x,
                                                                                     [self.firstLabel height] + distanceBetweenText)];
    
    
    CGFloat containerWidth;
    if ([self.firstLabel width] > [self.secondLabel width]) {
        containerWidth = [self.firstLabel width];
    } else {
        containerWidth = [self.secondLabel width];
    }
    
    CGFloat containerHeight = self.firstLabel.height + distanceBetweenText + self.secondLabel.height;
    
    self.textContainer = [[UIView alloc] initWithFrame:(CGRect){.origin = self.textContainer.frame.origin,
                                                                .size = CGSizeMake(containerWidth, containerHeight)}];
    
    [self.textContainer addSubview:self.firstLabel];
    [self.textContainer addSubview:self.secondLabel];

    // TODO: VADIM remove me
//    self.textContainer.layer.borderWidth = 1;
//    self.textContainer.layer.borderColor = [[UIColor greenColor] CGColor];
    
                          //    CGFloat zoomFactor = 1.0 / self.mapView.zoomScale;
//
//    
//    // Max size of label
//    self.maxSizeLabel = CGSizeMake(400 * zoomFactor, 150 * zoomFactor);
//    
//    //Need set fixed width
//    if ([self.mapImage.text_placement isEqualToString:@"right"] || [self.mapImage.text_placement isEqualToString:@"left"]) {
//        
//        if (self.firstView.frame.origin.x < kMaxLabelWidth) {
//            
//            self.maxSizeLabel = CGSizeMake(self.firstView.frame.origin.x - kDistanceFromTextToCirle, 150);
//            
//        } else if(self.firstView.frame.origin.x > self.mapViewFrame.size.width - kMaxLabelWidth) {
//            
//            self.maxSizeLabel = CGSizeMake(self.mapViewFrame.size.width - self.firstView.frame.origin.x - kDistanceFromTextToCirle, 150);
//            
//        }
//        
//    }
    
}

- (void)setupLabelsPosition {
    
    // Get best size to fit given size
    //self.sizeLabel = [self.firstLabel sizeThatFits:self.maxSizeLabel];
    
//    
//    CGRect firstPointFrame = self.firstView.frame;
//    
//    self.firstLabel.frame = (CGRect){.origin = firstPointFrame.origin, .size = self.sizeLabel};
//    self.secondLabel.frame = (CGRect){.origin = firstPointFrame.origin, .size = self.sizeLabel};
//    
//    self.firstLabel.transform = CGAffineTransformMakeScale(zoomFactor, zoomFactor);
//    self.secondLabel.transform = CGAffineTransformMakeScale(zoomFactor, zoomFactor);

    // calculate zoom factor
    CGFloat zoomFactor = 1.0 / self.mapView.zoomScale;
    
    // apply transform to text container
    self.textContainer.transform = CGAffineTransformMakeScale(zoomFactor, zoomFactor);
    
    // nillify counter
    self.procedeedPlacersCounter = 0;
    
    // initialize placer chain
    NSMutableArray *chain = [NSMutableArray arrayWithCapacity:7];
    
    TextPlacement textPlacement = [self.mapImage textPlacement];
    
    NSMutableArray *allPlacers = [NSMutableArray arrayWithCapacity:7];
    

    [allPlacers addObject:[[KKMoveLeftMapPlacer alloc] initWithBackground:self.mapView circlePointView:self.firstView withZoomFactor:zoomFactor]];
    [allPlacers addObject:[[KKMoveRightMapPlacer alloc] initWithBackground:self.mapView circlePointView:self.firstView withZoomFactor:zoomFactor]];
    [allPlacers addObject:[[KKTopMapPlacer alloc] initWithBackground:self.mapView circlePointView:self.firstView withZoomFactor:zoomFactor]];
    [allPlacers addObject:[[KKBottomMapPlacer alloc] initWithBackground:self.mapView circlePointView:self.firstView withZoomFactor:zoomFactor]];
    [allPlacers addObject:[[KKLeftMapPlacer alloc] initWithBackground:self.mapView circlePointView:self.firstView withZoomFactor:zoomFactor]];
    [allPlacers addObject:[[KKRightMapPlacer alloc] initWithBackground:self.mapView circlePointView:self.firstView withZoomFactor:zoomFactor]];
//    [allPlacers addObject:[[KKCenterPlacer alloc] initWithBackground:self.mapView circlePointView:self.firstView withZoomFactor:zoomFactor]];
    
    NSLog(@"TExt: %@", self.mapDescription.content);
    
    if ([self.mapDescription.content isEqualToString:@"STILISERTE FOLDER"] ){
        NSLog(@"TExt: %@", self.mapDescription.content);
    }
    
    switch (textPlacement) {
        case TextPlacementLeft: {
            [chain addObject:[[KKLeftMapPlacer alloc] initWithBackground:self.mapView circlePointView:self.firstView withZoomFactor:zoomFactor]];
            break;
        }
            
        case TextPlacementTop: {
            [chain addObject:[[KKTopMapPlacer alloc] initWithBackground:self.mapView circlePointView:self.firstView withZoomFactor:zoomFactor]];
            break;
        }
            
        case TextPlacementBottom: {
            [chain addObject:[[KKBottomMapPlacer alloc] initWithBackground:self.mapView circlePointView:self.firstView withZoomFactor:zoomFactor]];
            break;
        }
        case TextPlacementRight: {
            [chain addObject:[[KKRightMapPlacer alloc] initWithBackground:self.mapView circlePointView:self.firstView withZoomFactor:zoomFactor]];
            break;
        }
            
        case TextPlacementCenter: {
            [chain addObject:[[KKCenterPlacer alloc] initWithBackground:self.mapView circlePointView:self.firstView withZoomFactor:zoomFactor]];
            break;
        }
            
        default: {
            [chain addObject:[[KKCenterPlacer alloc] initWithBackground:self.mapView circlePointView:self.firstView withZoomFactor:zoomFactor]];
            break;
        }
    }

    for (KKMapPlacer *placer in allPlacers) {
        if (![chain containsObject:placer]) {
            [chain addObject:placer];
        }
    }
    
    // apply placer chain
    [chain enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        KKMapPlacer *placer = (KKMapPlacer *)obj;
        
        if (idx == 0) {
            [placer applyActionToView:self.textContainer];
        } else {
            
            CGRect rect = CGRectMake(self.mapViewFrame.origin.x * zoomFactor, self.mapViewFrame.origin.y * zoomFactor, self.mapViewFrame.size.width * zoomFactor, self.mapViewFrame.size.height * zoomFactor);
            
            if (!CGRectContainsRect(rect, self.textContainer.frame)) {
                [placer applyActionToView:self.textContainer];
            }
        }
    }];
    
    /*self.textContainer.transform = CGAffineTransformMakeScale(zoomFactor, zoomFactor);
    
    
    TextPlacement textPlacement = TextPlacementTop;
    
    switch (textPlacement) {
        case TextPlacementLeft: {
            [self positionWithLeftPlacement];
            break;
        }
            
        case TextPlacementTop: {
            [self positionWithTopPlacement];
            break;
        }
            
        case TextPlacementBottom: {
            [self positionWithBottomPlacement];
            break;
        }
        case TextPlacementRight: {
            [self positionWithRightPlacement];
            break;
        }
            
        case TextPlacementCenter: {
            [self positionWithCenterPlacement];
            break;
        }
            
        default: {
           // [self positionWithRightPlacement];
            break;
        }
    }
     */
}

#pragma mark - Positions

- (void)positionWithCenterPlacement {
    
    
    NSLog(@"Center - %@", NSStringFromCGPoint(self.firstView.center));
    NSLog(@"Origin - %@", NSStringFromCGPoint(self.firstView.frame.origin));
    
//    
//    self.firstLabel.center = CGPointMake(self.firstView.center.x,
//                                         self.firstView.center.y);
//    
//    
//    self.secondLabel.center = CGPointMake(self.firstView.center.x,
//                                          self.firstLabel.center.y + self.firstLabel.frame.size.height / 2 + kDistanceBetweenViews);
    
}

- (void)positionWithRightPlacement {
    
    CGFloat zoomFactor = 1.0 / self.mapView.zoomScale;
    
    NSLog(@"Center - %@", NSStringFromCGPoint(self.firstView.center));
    NSLog(@"Origin - %@", NSStringFromCGPoint(self.firstView.frame.origin));
    
    CGFloat placeForFitWidth = self.mapViewFrame.size.width * zoomFactor - self.firstView.frame.origin.x;
    
    BOOL isFitOnRightSide = [self.textContainer width] < placeForFitWidth - kDistanceBetweenViews * zoomFactor * 2;
    BOOL isFitOnTopSide = [self.textContainer height] < self.firstView.frame.origin.y - kDistanceBetweenViews * zoomFactor * 2;
    
    if (isFitOnRightSide) {
        
        CGFloat x = [self.firstView rightTopPoint].x + kDistanceBetweenViews;
        CGFloat y = [self.firstView rightTopPoint].y;
        
        CGPoint point = CGPointMake(x, y);
        
        self.textContainer.frame = (CGRect){.origin = point,
                                            .size   = self.textContainer.frame.size};
    } else if (isFitOnTopSide) {
        
        [self positionWithTopPlacement];

        
    } else {
    
        [self positionWithBottomPlacement];
        
    }
    
    

}

- (void)positionWithLeftPlacement {
    
    NSLog(@"Center - %@", NSStringFromCGPoint(self.firstView.center));
    NSLog(@"Origin - %@", NSStringFromCGPoint(self.firstView.frame.origin));
    
    CGFloat placeForFitWidth = self.firstView.frame.origin.x;
    
    BOOL isFitOnLeftSide = [self.textContainer width] < placeForFitWidth + kDistanceBetweenViews * 2;
    BOOL isFitOnTopSide = [self.textContainer height] < self.firstView.center.y - [self.firstView halfHeight] - kDistanceBetweenViews * 2;
    
    if (isFitOnLeftSide) {
        
        CGFloat x = [self.firstView leftTopPoint].x - kDistanceBetweenViews - [self.textContainer width];
        CGFloat y = [self.firstView leftTopPoint].y;
        
        CGPoint point = CGPointMake(x, y);
        
        self.textContainer.frame = (CGRect){.origin = point,
                                            .size   = self.textContainer.frame.size};
        
    } else if (isFitOnTopSide) {
        
        [self positionWithTopPlacement];
        
        
    } else {
        
        [self positionWithBottomPlacement];
        
    }
    
}

- (void)positionWithBottomPlacement {
    
    
    BOOL isFitOnBottomSide = [self.textContainer height];
    BOOL isFitOnScreenFromRight = [self.textContainer width] <= self.mapViewFrame.size.width - self.firstView.frame.origin.x;
    BOOL isFitOnScreenFromLeft = self.mapViewFrame.size.width - self.firstView.frame.origin.x;
    
    if (isFitOnBottomSide) {
        //Bottom placement
        
        CGFloat moveValueX;
        
        if (isFitOnScreenFromRight && isFitOnScreenFromLeft) {
            moveValueX = 0.0;
        } else if (isFitOnScreenFromRight) {
            moveValueX = self.firstView.center.x + [self.textContainer halfWidth] + kDistanceBetweenViews;
        } else {
            moveValueX = -(self.firstView.center.x - [self.textContainer width] - kDistanceBetweenViews);
        }
        
        CGFloat x = self.firstView.center.x - [self.textContainer halfWidth];
        CGFloat y = self.firstView.center.y + kDistanceBetweenViews;
        
        CGPoint point = CGPointMake(x, y);
        
        self.textContainer.frame = (CGRect){.origin = point,
            .size   = self.textContainer.frame.size};
        
    } else if (isFitOnScreenFromRight) {
        // Move down and right
        [self positionWithRightPlacement];
        
    } else {
        // Move down and left
        [self positionWithLeftPlacement];
        
    }

    

//    if (self.secondLabel.frame.size.width > self.firstView.frame.origin.x) {
//        
//        //Move right
//        
//        CGFloat movePoint = fabsf(self.secondLabel.frame.size.width / 2 - self.firstView.center.x);
//        
//        self.firstLabel.center = CGPointMake(self.firstView.center.x + movePoint + kDistanceFromTextToCirle,
//                                             self.firstView.center.y + kDistanceFromTextToCirle * 2);
//        
//        
//        self.secondLabel.center = CGPointMake(self.firstView.center.x + movePoint + kDistanceFromTextToCirle,
//                                              self.firstLabel.center.y + self.firstLabel.frame.size.height / 2 + kDistanceFromTextToCirle);
//        
//    } else if (self.firstView.frame.origin.x > self.mapViewFrame.size.width - kMaxLabelWidth) {
//        
//        // Move left
//        
//        CGFloat movePoint = self.secondLabel.frame.size.width / 2 - fabs(self.mapViewFrame.size.width - self.firstView.center.x);
//        
//        self.firstLabel.center = CGPointMake(self.firstView.center.x - movePoint - kDistanceFromTextToCirle,
//                                             self.firstView.center.y + kDistanceFromTextToCirle * 2);
//        
//        self.secondLabel.center = CGPointMake(self.firstView.center.x - movePoint - kDistanceFromTextToCirle,
//                                              self.firstLabel.center.y + self.firstLabel.frame.size.height / 2 + kDistanceFromTextToCirle);
//        
//    } else if (self.firstView.center.y + kDistanceFromTextToCirle * 2 + self.firstLabel.frame.size.height / 2 > self.mapViewFrame.size.height) {
//        
//        // Move up and left or right
//        
//        CGFloat movePointX = 0;
//        
//        if (self.firstView.frame.origin.x <= self.mapViewFrame.origin.x) {
//            
//            movePointX = self.sizeLabel.width + kDistanceFromTextToCirle;
//            
//        } else if (self.firstView.frame.origin.x > self.mapViewFrame.origin.x) {
//            
//            movePointX = - (self.sizeLabel.width + kDistanceFromTextToCirle) / 2;
//            
//        }
//        
//        self.secondLabel.center = CGPointMake(self.firstView.center.x - movePointX,
//                                              self.firstLabel.center.y - self.secondLabel.frame.size.height / 2 - kDistanceFromTextToCirle);
//        
//        self.firstLabel.center = CGPointMake(self.firstView.center.x - movePointX,
//                                             self.firstLabel.center.y);
//        
//    } else {
//        
//        // Default case
//        
//        self.firstLabel.center = CGPointMake(self.firstView.center.x,
//                                             self.firstView.center.y + kDistanceFromTextToCirle * 2);
//        
//        
//        self.secondLabel.center = CGPointMake(self.firstView.center.x,
//                                              self.firstLabel.center.y + self.firstLabel.frame.size.height / 2 + kDistanceFromTextToCirle);
//        
//    }

}

- (void)positionWithTopPlacement {
    
//    // get current zoom factor
    CGFloat zoomFactor = 1.0 / self.mapView.zoomScale;
//    
//    UIView *circleView = self.firstView;
//    UIView *textContainer = self.textContainer;
//    
//    
//    // 1. left come out of bounds
//    //CGPoint point = CGPointMake(-100, self.mapViewFrame.size.height / 2);
//    
////    if (point.x < kDistanceBetweenViews * zoomFactor) {
////        point.x = kDistanceBetweenViews * zoomFactor;
////    }
//    
//    // 2. right come out of bounds
////    CGPoint point = CGPointMake((self.mapViewFrame.size.width * zoomFactor - [textContainer halfWidth]), (self.mapViewFrame.size.height / 2) * zoomFactor);
////    
////    if (point.x > self.mapViewFrame.size.width * zoomFactor - [textContainer width] - kDistanceBetweenViews * zoomFactor) {
////        point.x = self.mapViewFrame.size.width * zoomFactor - [textContainer width] - kDistanceBetweenViews * zoomFactor;
////    }
////
////    self.textContainer.frame = (CGRect){.origin = point,
////        .size   = self.textContainer.frame.size};
//    
//    // 3. top come out of bounds
////    CGPoint point = CGPointMake(self.mapViewFrame.size.width * zoomFactor - [textContainer width] - kDistanceBetweenViews * zoomFactor, -50);
////    if (point.y < [textContainer height]) {
////        point.y = kDistanceBetweenViews * zoomFactor;
////    }
//
//    
////        self.textContainer.frame = (CGRect){.origin = point,
////            .size   = self.textContainer.frame.size};
//    
//    // 4. bottom come out of bounds
//    CGPoint point = CGPointMake(20, self.mapViewFrame.size.height * zoomFactor + [textContainer halfHeight]);
//    
//    if (point.y > self.mapViewFrame.size.height * zoomFactor) {
//        point.y = self.mapViewFrame.size.height * zoomFactor - [textContainer height] - kDistanceBetweenViews * zoomFactor;
//        
//    }
//    
//    // 5. corner come out of bounds
////        CGPoint point = CGPointMake(- 100 * zoomFactor, (self.mapViewFrame.size.height - 10) * zoomFactor);
////    
////        self.textContainer.frame = (CGRect){.origin = point,
////            .size   = self.textContainer.frame.size};
//    
//    textContainer.frame = (CGRect){.origin = point,
//        .size   = self.textContainer.frame.size};
//    
   // return;
    
    /*BOOL isFitOnTopSide = [self.textContainer height] < self.firstView.center.y - kDistanceBetweenViews * zoomFactor;
    
    if (isFitOnTopSide) {
        //Top placement
        
        CGFloat moveValueX = 0.0;
        
        if (isFitOnScreenFromRight && isFitOnScreenFromLeft) {
            moveValueX = 0.0;
        } else if (isFitOnScreenFromRight) {
            moveValueX = [self.textContainer halfWidth] - self.firstView.center.x;
        } else {
            moveValueX = -([self.textContainer halfWidth] - (self.mapViewFrame.size.width * zoomFactor - self.firstView.center.x));
        }

        
        CGFloat x = self.firstView.center.x - [self.textContainer halfWidth] + moveValueX;
        CGFloat y = self.firstView.center.y - [self.firstView halfHeight] - kDistanceBetweenViews - [self.textContainer height];
        
        CGPoint point = CGPointMake(x, y);
        
        self.textContainer.frame = (CGRect){.origin = point,
                                            .size   = self.textContainer.frame.size};
        
    } else if (isFitOnScreenFromRight) {
        // Move down and right
        [self positionWithRightPlacement];
        
    } else {
        // Move down and left
        [self positionWithLeftPlacement];
        
    }
     */

    
//    CGFloat zoomFactor = 1.0 / self.mapView.zoomScale;
//    
//    // if not size no fit on the screen, move text to left or right
//    if (self.secondLabel.frame.size.width / 2 > self.firstView.center.x) {
//        
//        //Move right
//        
//        CGFloat movePoint = fabsf(self.secondLabel.frame.size.width / 2 + self.firstView.center.x + kDistanceFromTextToCirle * zoomFactor + kDiamentrCircle / 2);
//        
//        self.secondLabel.center = CGPointMake(movePoint,
//                                              self.firstLabel.center.y - self.firstLabel.frame.size.height / 2 - kDistanceFromTextToCirle);
//        
//        self.firstLabel.center = CGPointMake(movePoint,
//                                             self.firstView.center.y - self.secondLabel.frame.size.height / 2 - kDistanceFromTextToCirle * movePoint);
//        
//    } else if (self.firstView.frame.origin.x > self.mapViewFrame.size.width - kMaxLabelWidth) {
//        
//        //Move left
//        
//        CGFloat movePoint = self.secondLabel.frame.size.width / 2 - fabs(self.mapViewFrame.size.width - self.firstView.center.x);
//        
//        self.secondLabel.center = CGPointMake(self.firstView.center.x - movePoint - kDistanceFromTextToCirle,
//                                              self.firstLabel.center.y - self.firstLabel.frame.size.height / 2 - kDistanceFromTextToCirle);
//        
//        self.firstLabel.center = CGPointMake(self.firstView.center.x - movePoint - kDistanceFromTextToCirle,
//                                             self.firstView.center.y - self.secondLabel.frame.size.height / 2 - kDistanceFromTextToCirle * 2);
//    } else if (self.secondLabel.frame.size.height + self.firstLabel.frame.size.height + kDistanceFromTextToCirle > self.firstView.frame.origin.y) {
//        
//        // Move down and left or right
//        
//        CGFloat movePointX = 0;
//        
//        if (self.firstView.frame.origin.x <= self.mapViewFrame.origin.x) {
//            
//            movePointX = self.sizeLabel.width + kDistanceFromTextToCirle;
//            
//        } else if (self.firstView.frame.origin.x > self.mapViewFrame.origin.x) {
//            
//            movePointX = - (self.sizeLabel.width + kDistanceFromTextToCirle) / 2;
//            
//        }
//        
//        self.secondLabel.center = CGPointMake(self.firstView.center.x + movePointX,
//                                              self.firstLabel.center.y + self.secondLabel.frame.size.height / 2 + kDistanceFromTextToCirle);
//        
//        self.firstLabel.center = CGPointMake(self.firstView.center.x + movePointX,
//                                             self.firstLabel.center.y);
//        
//    } else {
//        
//        // Default case
//        
//        self.secondLabel.center = CGPointMake(self.firstView.center.x,
//                                              self.firstLabel.center.y - self.firstLabel.frame.size.height / 2 - kDistanceFromTextToCirle);
//        
//        self.firstLabel.center = CGPointMake(self.firstView.center.x,
//                                             self.firstView.center.y - self.secondLabel.frame.size.height / 2 - kDistanceFromTextToCirle * 2);
//        
//    }


}

- (void)addReccordButtonsOnDots {
    
    // Stretch record button on text
//    self.recordButton = [[UIButton alloc] initWithFrame:CGRectMake(self.firstLabel.frame.origin.x,
//                                                                        self.firstLabel.frame.origin.y,
//                                                                        self.secondLabel.frame.size.width,
//                                                                        self.secondLabel.frame.size.height + self.firstLabel.frame.size.height + kDistanceFromTextToCirle)];
    
    // TODO: Only to show tappable area borders
//    self.secondRecordButton.layer.borderColor = [UIColor greenColor].CGColor;
//    self.secondRecordButton.tag = [self.recordDescription.record_id  integerValue];
//    self.recordButton.layer.borderColor = [UIColor greenColor].CGColor;
//    self.recordButton.tag = [self.recordDescription.record_id  integerValue];
    
    CGFloat sizeMultiplier = 35.0;
    
    
    // Configure first record button
    self.recordButton = [[UIButton alloc] initWithFrame: CGRectMake(0,
                                                                    0,
                                                                    self.firstView.frame.size.width + sizeMultiplier,
                                                                    self.firstView.frame.size.height + sizeMultiplier)];
    
    self.recordButton.backgroundColor = [UIColor clearColor];
    [self.recordButton addTarget:self action:@selector(recordButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    // Configure second record button
    self.secondRecordButton = [[UIButton alloc] initWithFrame: CGRectMake(0,
                                                                          0,
                                                                          self.secondView.frame.size.width + sizeMultiplier,
                                                                          self.secondView.frame.size.height + sizeMultiplier)];
    self.secondRecordButton.backgroundColor = [UIColor clearColor];
    [self.secondRecordButton addTarget:self action:@selector(recordButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    // For handle zooming
    self.recordButton.transform = CGAffineTransformMakeScale(1.0 / self.mapView.zoomScale, 1.0 / self.mapView.zoomScale);
    self.secondRecordButton.transform = CGAffineTransformMakeScale(1.0 / self.mapView.zoomScale, 1.0 / self.mapView.zoomScale);
    
    // Setup posiiton
    self.recordButton.center = self.firstView.center;
    self.secondRecordButton.center = self.secondView.center;
    
}


#pragma mark - Actions

- (void)recordButtonClicked {
    
    // TODO: only to debug empty text
    
    if (!self.mapDescription && !self.mapTitle) {
        
        [UIAlertView showWithTitle:NSLocalizedString(@"Warning",@"")
                           message:@"There is empty map description and map title."
                 cancelButtonTitle:NSLocalizedString(@"Ok",@"")
                 otherButtonTitles:nil
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          }];
    }
    
    if (self.tapBlock != NULL) {
        self.tapBlock([self.recordDescription.record_id  integerValue]);
    }
    
}



- (CAShapeLayer *)createLineFromPoint:(CGPoint *)firstPoint toPoint:(CGPoint *)secondPoint {
    
    CAShapeLayer *line = [CAShapeLayer layer];
    UIBezierPath *linePath = [UIBezierPath bezierPath];
    
    [linePath moveToPoint: *secondPoint];
    [linePath addLineToPoint: *firstPoint];
    
    line.path = linePath.CGPath;
    line.lineWidth = 2.0;
    line.fillColor = nil;
    line.opacity = kOpacity;
    line.strokeColor = self.color.CGColor;
    
    return line;
    
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
//    //TODO: only for testing single dot
//    if (self.mapImage.anchorOn) {
//        if ([keyPath isEqualToString:@"contentSize"]) {
//            [self updateSizeOfViews];
//        }
//    }
//    return;
    
    
    if ([keyPath isEqualToString:@"contentSize"]) {
        [self updateSizeOfViews];
    }
    


}


@end

