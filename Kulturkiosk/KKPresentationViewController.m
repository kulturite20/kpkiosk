//
//  KKPresentationViewController.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 8/16/13.
//
//

#import "KKPresentationViewController.h"

@interface KKPresentationViewController ()

@end

@implementation KKPresentationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dismissController:(id)sender {
    
    [self doesNotRecognizeSelector:_cmd];
    
}

/*
- (void)highlightNode:(Nodes *)node
{
  [NSException raise:NSInternalInconsistencyException
              format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
}*/

@end
