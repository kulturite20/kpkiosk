//
//  KKCircleLineDrawer.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 3/24/17.
//
//

#import <Foundation/Foundation.h>
#import "KKMapView.h"

@interface KKCircleLineDrawer : NSObject

@property (nonatomic, strong) RecordDescription *recordDescription;
@property (nonatomic, strong) UIColor *color;
@property (nonatomic) CGRect mapViewFrame;
@property (nonatomic, strong) UIView *firstView;
@property (nonatomic, strong) UIView *secondView;
@property (nonatomic, strong) UILabel *firstLabel;
@property (nonatomic, strong) UILabel *secondLabel;
@property (nonatomic, strong) CAShapeLayer *line;
@property (nonatomic, strong) UIButton *recordButton;
@property (nonatomic, strong) UIButton *secondRecordButton;
@property (nonatomic) KKMapView *mapView;

@property (nonatomic, copy) void (^tapBlock)(NSInteger recordID);

- (instancetype)initWithMapView:(KKMapView *)mapView;

- (void)creatCircleWith:(RecordDescription *)recordDescription annotationOffset:(CGFloat)annotationOffset scaleFactor:(CGFloat)scaleFactor;

- (void)mapTextConfigurationSetup;
- (void)configureLabels;
    
- (void)setupText;
- (void)setupLabelsPosition;
- (void)addReccordButtonsOnDots;

- (void)reloadCircleLineDrawer;

@end
