//
//  KKImageContainerViewController.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 1/29/16.
//
//

#import "KKImageContainerViewController.h"
#import "File.h"
#import "KKMediaItemViewController.h"
#import "ImageScrollView.h"
#import "UIImage+ScaleFactor.h"

#import "UIView+Autolayout.h"

@interface KKImageContainerViewController ()
<
UIGestureRecognizerDelegate,
UIScrollViewDelegate
>
@end

@implementation KKImageContainerViewController {
    
    __weak IBOutlet UIView *_videoContainer;
    
    UIView *_videoContainerView;
    
    NSUInteger _pageIndex;
    File *_mediaItem;
    
    KKMediaItemViewController *_videoPlayerController;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    self.parentRect = CGRectZero;
    
    return self;
}

- (void)mediaItem:(File *)mediaItem {
    _mediaItem = mediaItem;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor blackColor];
    self.view.userInteractionEnabled = YES;
    
    if ([_mediaItem.type isEqualToString:@"image"]) {
        
        _videoContainer = nil;
        
        NSString *original = [_mediaItem pathToOriginalItem];
        UIImage *imageToShow = [UIImage imageWithContentsOfFile:original];
        
        ImageScrollView *imageContainer = [[ImageScrollView alloc] init];
        
        imageContainer.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        imageContainer.isHaveTopBottomBars = YES;
        imageContainer.showsVerticalScrollIndicator = NO;
        imageContainer.showsHorizontalScrollIndicator = NO;
        imageContainer.bouncesZoom = YES;
        imageContainer.decelerationRate = UIScrollViewDecelerationRateFast;
        
        imageContainer.didPinchWillStart = self.willhideScrollImageBars;
        
        if (imageToShow.size.height < 1046) {
            CGFloat scaleFactor = 1046 / imageToShow.size.height;
            CGSize imageSize = CGSizeMake(floorf(imageToShow.size.width*scaleFactor), floorf(imageToShow.size.height*scaleFactor));
            imageContainer.image = [UIImage imageWithImage:imageToShow scaledToSize:imageSize];
        } else {
            imageContainer.image = imageToShow;
        }
        
        self.view = imageContainer;
        
    } else {
        
        _videoPlayerController = [KKMediaItemViewController new];
        
        [_videoPlayerController willMoveToParentViewController:self];
        [self addChildViewController:_videoPlayerController];
        
        _videoPlayerController.mediaItem = _mediaItem;
        
        _videoPlayerController.isInVideoGalleryMode = YES;
        
        [_videoPlayerController didMoveToParentViewController:self];
        [_videoContainer addSubview:_videoPlayerController.view];
        
        CGRect videoFrame =  _videoContainer.frame;
        videoFrame.origin.x = videoFrame.origin.y = 0;
        
        _videoPlayerController.view.frame = (CGRect){0.0,0.0,_videoContainer.frame.size.width,_videoContainer.frame.size.height};
        
        // only in case of full mode, from ImageVideo Gallery
        if (self.parentRect.size.width != 0) {
            
            [_videoContainer removeAllConstraints];
            
            [_videoContainer centeringWithParentView:self.view];
            
            [_videoPlayerController refreshVideoLayerFrameWith:_videoContainer.frame];
            _videoPlayerController.inFixFullScreenMode = self.imageVideoGalleryMode;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    if (_videoPlayerController) {
        [_videoPlayerController pauseVideoPlayer];
    }
}

@end
