//
//  KKVideoPresentationViewController.h
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 05.11.13.
//
//

#import "KKIndexViewController.h"
#import "Record.h"

@interface KKVideoPresentationViewController : KKPresentationViewController <UICollectionViewDelegate, UICollectionViewDataSource>

- (void)loadVideo: (Resource*)video;
- (IBAction)dismissController:(id)sender;


@end
