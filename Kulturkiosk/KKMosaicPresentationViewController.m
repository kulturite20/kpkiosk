//
//  KKMosaicPresentationViewController.m
//  Kulturkiosk
//
//  Created by Vadim Osovets Zinchenko on 2/10/17.
//
//

#import "KKMosaicPresentationViewController.h"

#import "KKPresentationViewController.h"
#import "KKRecordContainerViewController.h"
#import "MosaicPresentation+CoreDataClass.h"
#import "MosaicTemplate+CoreDataClass.h"
#import "MosaicElementSize+CoreDataClass.h"
#import "KKMapViewController.h"
#import "KKImagePresentationViewController.h"
#import "KKMosaicTileViewController.h"
#import "MosaicItemText+CoreDataClass.h"
#import "MosaicItemTextDescription+CoreDataClass.h"
#import "KKImageMapViewController.h"
#import "KKNavigationViewController.h"

#import <QuartzCore/QuartzCore.h>

@interface TileModel : NSObject

//// original points comes from server
//@property (nonatomic) MosaicElementSize *size;

@property (nonatomic) BOOL isDrawn;
//@property (nonatomic) CGRect devicePoint;

@end

@interface MosaicViewModel : NSObject

//// original points comes from server
@property (nonatomic) MosaicElementSize *size;
@property (nonatomic) CGRect frame;

// view ???

@end

const CGFloat kTrasitionDuration = 0.3;

@implementation MosaicViewModel

@end


@interface KKMosaicPresentationViewController ()

@property (strong, nonatomic) MosaicTemplate *mosaicTemplate;
@property (strong, nonatomic) NSArray<MosaicElementSize *> *mosaicElementSizeArray;

// aux array to draw tiles
@property (strong, nonatomic) NSMutableArray<TileModel *> *tileModels;

// aux array to keep all views model
@property (strong, nonatomic) NSMutableArray<MosaicViewModel *> *mosaicViewModels;

// size of one tile
@property (nonatomic) CGSize tileSize;

// array of tile views
@property (nonatomic) NSMutableArray<KKMosaicTileViewController *> *tileViews;


@end

@implementation TileModel

@end

@implementation KKMosaicPresentationViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(viewDidLoad)
                                                 name:@"languageChange"
                                               object:nil];

    self.mosaicTemplate = self.mosaicPresentation.presentation_template;
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"position"
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    self.mosaicItems = [[[self.mosaicPresentation.mosaic_items allObjects] mutableCopy] sortedArrayUsingDescriptors:sortDescriptors];
    self.mosaicElementSizeArray = [[[[self.mosaicTemplate elementSizes] allObjects] mutableCopy] sortedArrayUsingDescriptors:sortDescriptors];
    
    self.mosaicViewModels = [NSMutableArray arrayWithCapacity:self.mosaicElementSizeArray.count];
    
    // setting background image
    for (Resource *resource in [self.mosaicPresentation resources]) {
        self.backgroundImageView.image = [UIImage imageWithContentsOfFile:[resource pathToOriginalItem]];
    }
    
    
    for (MosaicElementSize *size in self.mosaicElementSizeArray) {
        MosaicViewModel *viewModel = [MosaicViewModel new];
        viewModel.size = size;
        [self.mosaicViewModels addObject:viewModel];
    }
    
    if (self.mapViewController == nil) {
        [self drawMosaic];
    }
    
   // self.view.backgroundColor = [UIColor redColor];
    
}

- (void)drawMosaic {

    // initialize view for tiles
    // initialize tile model array, keep in mind capacity we need
    NSInteger mosaicHeight = self.mosaicTemplate.height.integerValue;
    NSInteger mosaicWidth = self.mosaicTemplate.width.integerValue;
    
    self.tileModels = [NSMutableArray arrayWithCapacity:mosaicWidth * mosaicHeight];
    
    for (int i = 0; i < mosaicWidth * mosaicHeight; i++) {
        TileModel *model = [TileModel new];
        [self.tileModels addObject:model];
    }
    
    // calculate tile size
    CGFloat marginSides = 15.0;
    CGFloat maraginTop = 80.0;
    CGFloat bottomContainerHeight = 110.0;
    
    UIView *mosaicBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(marginSides,
                                                                            maraginTop,
                                                                            self.view.frame.size.width - marginSides * 2,
                                                                            self.view.frame.size.height - marginSides * 3 - bottomContainerHeight)];
    
    // get view w,h
    CGSize size = mosaicBackgroundView.frame.size;
    
    // we assume that tile is a square
    self.tileSize = CGSizeMake(floor(size.width / mosaicWidth), floor(size.height / mosaicHeight));
    
    // calculate frames, save in tile models
    
    __block float x = 0;
    __block float y = 0;
    
    __block int viewCurrentIndex = 0;
    
    [self.tileModels enumerateObjectsUsingBlock:^(TileModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        // ignore if we already show something there
        if (obj.isDrawn) {
            
            // NSLog(@"\n\n=======\n %@ \n=========\n\n", @(idx));
            // increment x
            // move to next tile
            
            // calculate next x/y
            // calculate next element size
            x += 1;
            
            float z = (idx + 1) % mosaicWidth;
            
            if (idx != 0 && z == 0) {
                x = 0.0;
                y += 1 ;
            }
            
            
        } else {
            // try to draw view and fill appropriate tiles
            
            if (viewCurrentIndex == self.mosaicViewModels.count) {
                NSLog(@"Wrong happened.");
                return;
            }
            
            MosaicElementSize *size = self.mosaicViewModels[viewCurrentIndex].size;
            CGRect modelFrame = CGRectMake(x * _tileSize.width, y * _tileSize.height, size.x.floatValue * _tileSize.width, size.y.floatValue * _tileSize.height);
            
            self.mosaicViewModels[viewCurrentIndex].frame = modelFrame;
            
            // increment view current index
            viewCurrentIndex++;
            NSLog(@"\nViewCurrentIndex: %@", @(viewCurrentIndex));
            
            // mark all tiles which we need as already drawn
            obj.isDrawn = YES;
            NSLog(@"1Tile with index filled: %@", @(idx));
            
            // drawn also on next tiles to right
            if (size.x.integerValue > 1) {
                
                int tileLength = size.x.intValue - 1;
                
                for (NSUInteger i = idx + 1; i < self.tileModels.count; i++) {
                    self.tileModels[i].isDrawn = YES;
                    NSLog(@"x Tile with index filled: %@", @(i));
                    
                    tileLength -= 1;
                    
                    if (tileLength == 0) {
                        break;
                    }
                }
            }
            
            // drawn also on next tiles to bottom
            if (size.y.integerValue > 1) {
                int tileLength = size.y.intValue;
                
                for (NSUInteger i = idx; i < self.tileModels.count; i += mosaicWidth ) {
                    
                    // to handle cases with 2x2 and more elements by horizontal and vertical
                    for (NSUInteger j = 0; j < size.x.unsignedIntegerValue; j++) {
                        self.tileModels[i + j].isDrawn = YES;
                    }
                    
                    NSLog(@"y Tile with index filled: %@", @(i));
                    
                    tileLength -= 1;
                    
                    if (tileLength == 0) {
                        break;
                    }
                }
            }
            
            
            // calculate next x/y
            // calculate next element size
            x += 1;
            
            float z = (idx + 1) % mosaicWidth;
            
            if (idx != 0 && z == 0) {
                x = 0.0;
                y += 1 ;
            }
        }
        
    }];
    
    // initialize views array
    self.tileViews = [NSMutableArray arrayWithCapacity:self.mosaicElementSizeArray.count];
    
    for (int i = 0; i < self.mosaicElementSizeArray.count; i++) {
        // add appropriate initialization of view
        [self.tileViews addObject:[KKMosaicTileViewController new]];
    }
    
    // draw tiles according to tile models
    
    [self.tileViews enumerateObjectsUsingBlock:^(KKMosaicTileViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        obj.view.frame = self.mosaicViewModels[idx].frame;
        
        NSLog(@"Idx: %@ %@<>%@ RECT: %@", @(idx), self.mosaicViewModels[idx].size.x, self.mosaicViewModels[idx].size.y, NSStringFromCGRect(obj.view.frame));
        
        // setting mosaicItem and mosaic image into tile from server.
        if (idx < self.mosaicItems.count) {
            NSLog(@"%@", self.mosaicItems[idx]);
            
            [obj addMosaicItem: self.mosaicItems[idx]];
            
            [obj.tileImageView setImageWithURL:[NSURL URLWithString:self.mosaicItems[idx].imageUrl]];
            
            [obj setTapBlock:^(MosaicItem *mosaicItem, BOOL isPresentationTile) {
                
                // Presentation tile
                if (isPresentationTile) {
                    
                    NSLog(@"\n\nPresent Image Map\n\n");
                    
                    // to save from double-tap by user
                    if (self.mapViewController == nil) {
                        
                        self.mapViewController = [KKMapViewController new];
                        
                        self.mapViewController = [MainStoryboard instantiateViewControllerWithIdentifier:@"MapViewController"];
                        
                        for (Presentation *presentation in [Presentation findAll]) {
                            if ([presentation.identifier isEqualToNumber: mosaicItem.presentationID]) {
                                self.mapViewController.presentation = presentation;
                                break;
                            }
                        }
                        
                        CGRect frame = self.view.frame;
                        
                        self.mapViewController.view.frame = CGRectMake(frame.origin.x,
                                                                       frame.size.height,
                                                                       frame.size.width,
                                                                       frame.size.height - kNavigationBarDefaultHeight);
                        
                        [self addChildViewController:self.mapViewController];
                        [self.view addSubview:self.mapViewController.view];
                        
                        
                        [UIView animateWithDuration: kTrasitionDuration animations:^{
                            
                            CGRect frame = self.view.frame;
                            
                            self.mapViewController.view.frame = CGRectMake(frame.origin.x,
                                                                           frame.origin.y,
                                                                           frame.size.width,
                                                                           frame.size.height - kNavigationBarDefaultHeight);
                            
                        } completion: nil];
                        
                    }
                    
                    
                } else {
                    
                    // Record tile
                    NSLog(@"\n\nPresent record\n\n");
                    
                    //Presenting record
                    NSLog(@"\n\n===============\n Tile tapped! \n===============\n\n");
                    KKRecordContainerViewController *recordViewController = [KKRecordContainerViewController new];
                    // get appropriate record
                    for (Record *record in [Record findAll]) {
                        if ([record.identifier  isEqualToNumber:mosaicItem.record_id]) {
                            recordViewController.record = record;
                            break;
                        }
                    }
                    
                    // show record
                    [self presentViewController:recordViewController
                                       animated:YES
                                     completion:nil];
                }
                    
                    
                
                
            }];
        }
        
        obj.view.layer.borderColor = [UIColor clearColor].CGColor;
        
        // NSLog(@"\n\n==========\n %@ \n==========\n\n", obj.mosaicItem);
        
        [mosaicBackgroundView addSubview:obj.view];
    }];
    
    [self.view addSubview:mosaicBackgroundView];
    
}
    


@end
