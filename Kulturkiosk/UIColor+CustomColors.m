//
//  UIColor+CustomColors.m
//  Kulturkiosk
//
//  Created by Rune Botten on 18.12.12.
//
//

#import "UIColor+CustomColors.h"

@implementation UIColor(CustomColors)

// Yellow-ish border color for selections
+(UIColor*) selectedColor
{
  return [UIColor colorWithRed:223/255.0f green:187/255.0f blue:49/255.0f alpha:1];
}

@end
