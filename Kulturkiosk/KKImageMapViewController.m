//
//  KKImageMapViewController.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 2/28/17.
//
//

#import "KKImageMapViewController.h"
#import "KKNavigationViewController.h"
#import "KKRecordContainerViewController.h"
#import "KKNavigationViewController.h"

@interface KKImageMapViewController ()

@property CGFloat widthBackgroundImage;
@property CGFloat heightBackgroundImage;

@property (nonatomic) NSMutableArray *recordButtonsArray;

@end

@implementation KKImageMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Changing language
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setupRecordLines)
                                                 name:@"languageChange"
                                               object:nil];
    
    //Setting background image
    for (Resource *resource in [self.presentation resources]) {
        NSLog(@"%@", self.presentation.identifier);
        
        UIImage *image = [UIImage imageWithContentsOfFile:[resource pathToOriginalItem]];
        
        self.widthBackgroundImage = image.size.width * image.scale;
        self.heightBackgroundImage = image.size.height * image.scale;
        
        self.backgroundImageView.image = [UIImage imageWithContentsOfFile:[resource pathToOriginalItem]];
        
    }
    
}

- (void) viewDidAppear:(BOOL)animated {

}

- (void) viewWillDisappear:(BOOL)animated {
   // [self viewWillDisappear:animated];

}

- (void)recordButtonClicked:(UIButton *)sender{
    
    UIButton *recordButton = sender;
    
    NSLog(@"Button tag @%", recordButton.tag);
    
    for (Record *record in [self.presentation records]) {
        if ([record.identifier integerValue] == recordButton.tag) {
            KKRecordContainerViewController *recordViewController = [KKRecordContainerViewController new];
            
            recordViewController.record = record;
            
            //[self addChildViewController:recordViewController];
            
            [self presentViewController:recordViewController animated:YES completion:nil];
        }
    }
    
}


- (void)setupRecordLines {
    
    CGFloat diamentrCircle = 15;
    CGFloat distanceFromTextToCirle = 20;
    
    CGSize multiplier = CGSizeMake(self.backgroundImageView.frame.size.width / self.widthBackgroundImage,
                                   self.backgroundImageView.frame.size.height / self.heightBackgroundImage);
    
    
    for (RecordDescription *recordDescription in [self.presentation records_decriptions]) {
        MapImage *mapImage = recordDescription.map_image;
        
        if (mapImage != nil) {
            
            //TODO: Add case when circle button in second half of screen.
            
            //startPointCircleButton configure
            UIButton *startPointCircleButton = [[UIButton alloc] initWithFrame:CGRectMake([recordDescription.x floatValue] * multiplier.width,
                                                                                          [recordDescription.y floatValue] * multiplier.height,
                                                                                          diamentrCircle,
                                                                                          diamentrCircle)];
            startPointCircleButton.layer.cornerRadius = diamentrCircle/2;
            startPointCircleButton.tag = [recordDescription.record_id integerValue];
            startPointCircleButton.backgroundColor = [UIColor redColor];
            startPointCircleButton.layer.opacity = 0.5;
            [startPointCircleButton addTarget:self action:@selector(recordButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            
            //endPointCircleView configure
            UIView *endPointCircleView = [[UIView alloc] initWithFrame:CGRectMake([mapImage.anchor_x floatValue] * multiplier.width,
                                                                                  [mapImage.anchor_y floatValue] * multiplier.height,
                                                                                  diamentrCircle,
                                                                                  diamentrCircle)];
            endPointCircleView.layer.cornerRadius = diamentrCircle/2;
            endPointCircleView.layer.opacity = 0.5;
            endPointCircleView.backgroundColor = [UIColor redColor];
            
            //Draw view between two views
            CAShapeLayer *line = [CAShapeLayer layer];
            UIBezierPath *linePath=[UIBezierPath bezierPath];
            
            [linePath moveToPoint: CGPointMake(endPointCircleView.center.x - diamentrCircle / 2,
                                               endPointCircleView.center.y)];
            
            [linePath addLineToPoint:CGPointMake(startPointCircleButton.center.x + diamentrCircle / 2,
                                                 startPointCircleButton.center.y)];
            
            line.path = linePath.CGPath;
            line.fillColor = nil;
            line.opacity = 1.0;
            line.strokeColor = [UIColor redColor].CGColor;
            [self.backgroundImageView.layer addSublayer:line];
            
            
            //Getting record and creating labels near
            for (Record *record in [Record findAll]) {
                if ([record.identifier isEqualToNumber:recordDescription.record_id]) {
                    
                    NSMutableString *titleString;
                    NSMutableString *descriptionString;
                    
                    for (Content *content in record.contents) {
                        if ([content.language isEqualToString:[KKLanguage contentForLanguage:[KKLanguage currentLanguage]]]) {
                            
                            titleString = [content.title mutableCopy];
                            descriptionString = [content.desc mutableCopy];
                            
                            break;
                        }
                    }
                    
                    
                    //Configure title label
                    UILabel *titleLabel = [UILabel new];
                    
                    titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
                    titleLabel.numberOfLines = 0;
                    titleLabel.text = titleString;
                    
                    //Configure description label
                    UILabel *descriptionLabel = [UILabel new];
                    
                    descriptionLabel.lineBreakMode = NSLineBreakByWordWrapping;
                    descriptionLabel.numberOfLines = 0;
                    descriptionLabel.text = descriptionString;
                    
                    // Max size of label
                    CGSize maxSize = CGSizeMake(400, 150);
                    
                    //Need set fixed width
                    if (startPointCircleButton.frame.origin.x < 400) {
                        
                        maxSize = CGSizeMake(startPointCircleButton.frame.origin.x - distanceFromTextToCirle * 2, 300);
                        
                    }
                    
                    
                    // Get best size to fit given size
                    CGSize sizeTitleLabel = [titleLabel sizeThatFits:maxSize];
                    CGSize sizeDescriptionLabel = [titleLabel sizeThatFits:maxSize];
                    
                    
                    //configure position of text
                    if (startPointCircleButton.frame.origin.x <= self.backgroundImageView.frame.size.width){
                        
                        //Configure title position
                        
                        titleLabel.textAlignment = NSTextAlignmentRight;
                        
                        // Put label on circle
                        titleLabel.frame = CGRectMake(startPointCircleButton.frame.origin.x,
                                                      startPointCircleButton.frame.origin.y,
                                                      sizeTitleLabel.width,
                                                      sizeTitleLabel.height);
                        
                        // Move left from circle
                        titleLabel.center = CGPointMake(startPointCircleButton.frame.origin.x - titleLabel.frame.size.width/2 - distanceFromTextToCirle,
                                                        startPointCircleButton.frame.origin.y + titleLabel.frame.size.height/2);
                        
                        //Configure description position
                        
                        descriptionLabel.textAlignment = NSTextAlignmentRight;
                        
                        // Put label on circle
                        titleLabel.frame = CGRectMake(startPointCircleButton.frame.origin.x,
                                                      startPointCircleButton.frame.origin.y,
                                                      sizeTitleLabel.width,
                                                      sizeTitleLabel.height);
                        
                        // Move left from circle
                        descriptionLabel.center = CGPointMake(startPointCircleButton.frame.origin.x - descriptionLabel.frame.size.width/2 - distanceFromTextToCirle,
                                                              CGRectGetMaxY(titleLabel.frame) + distanceFromTextToCirle);
                        
                        
                    }
                    
                    
                    titleLabel.backgroundColor = [UIColor greenColor];
                    descriptionLabel.backgroundColor = [UIColor yellowColor];
                    
                    [self.view addSubview:titleLabel];
                    [self.view addSubview:descriptionLabel];
                    
                    
                    break;
                }
            }
            
            
            [self.recordButtonsArray addObject: startPointCircleButton];
            [self.backgroundImageView addSubview:startPointCircleButton];
            [self.backgroundImageView addSubview:endPointCircleView];
            
        }

    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
