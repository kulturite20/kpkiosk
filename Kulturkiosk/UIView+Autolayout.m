//
//  UIView+Autolayout.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 10/20/16.
//
//

#import "UIView+Autolayout.h"

@implementation UIView (Autolayout)

- (void)centeringWithParentView:(UIView *)view {
    // use autolayout to center content properly
    UIView *containerView = view;
    UIView *newSubview = self;
    
    newSubview.frame = CGRectZero;
    
    newSubview.translatesAutoresizingMaskIntoConstraints = NO;
    //[containerView addSubview:newSubview];
    
    [containerView addConstraint:[NSLayoutConstraint constraintWithItem:newSubview
                                                              attribute:NSLayoutAttributeTop
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:containerView
                                                              attribute:NSLayoutAttributeTop
                                                             multiplier:1.0
                                                               constant:0.0]];
    
    [containerView addConstraint:[NSLayoutConstraint constraintWithItem:newSubview
                                                              attribute:NSLayoutAttributeLeading
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:containerView
                                                              attribute:NSLayoutAttributeLeading
                                                             multiplier:1.0
                                                               constant:0.0]];
    
    [containerView addConstraint:[NSLayoutConstraint constraintWithItem:newSubview
                                                              attribute:NSLayoutAttributeBottom
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:containerView
                                                              attribute:NSLayoutAttributeBottom
                                                             multiplier:1.0
                                                               constant:0.0]];
    
    [containerView addConstraint:[NSLayoutConstraint constraintWithItem:newSubview
                                                              attribute:NSLayoutAttributeTrailing
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:containerView
                                                              attribute:NSLayoutAttributeTrailing
                                                             multiplier:1.0
                                                               constant:0.0]];
    
    // if not doing this in `viewDidLoad` (e.g. you're doing this in
    // `viewDidAppear` or later), you can force `layoutIfNeeded` if you want
    // to look at `frame` values. Generally you don't need to do this unless
    // manually inspecting `frame` values or when changing constraints in a
    // `animations` block of `animateWithDuration`.
    
    [containerView layoutIfNeeded];
}

- (void)addToParentAndCentered:(UIView *)view {
    // use autolayout to center content properly
    UIView *containerView = view;
    UIView *newSubview = self;
    
    newSubview.frame = CGRectZero;
    
    newSubview.translatesAutoresizingMaskIntoConstraints = NO;
    [containerView addSubview:newSubview];
    
    [containerView addConstraint:[NSLayoutConstraint constraintWithItem:newSubview
                                                              attribute:NSLayoutAttributeTop
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:containerView
                                                              attribute:NSLayoutAttributeTop
                                                             multiplier:1.0
                                                               constant:0.0]];
    
    [containerView addConstraint:[NSLayoutConstraint constraintWithItem:newSubview
                                                              attribute:NSLayoutAttributeLeading
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:containerView
                                                              attribute:NSLayoutAttributeLeading
                                                             multiplier:1.0
                                                               constant:0.0]];
    
    [containerView addConstraint:[NSLayoutConstraint constraintWithItem:newSubview
                                                              attribute:NSLayoutAttributeBottom
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:containerView
                                                              attribute:NSLayoutAttributeBottom
                                                             multiplier:1.0
                                                               constant:0.0]];
    
    [containerView addConstraint:[NSLayoutConstraint constraintWithItem:newSubview
                                                              attribute:NSLayoutAttributeTrailing
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:containerView
                                                              attribute:NSLayoutAttributeTrailing
                                                             multiplier:1.0
                                                               constant:0.0]];
    
    // if not doing this in `viewDidLoad` (e.g. you're doing this in
    // `viewDidAppear` or later), you can force `layoutIfNeeded` if you want
    // to look at `frame` values. Generally you don't need to do this unless
    // manually inspecting `frame` values or when changing constraints in a
    // `animations` block of `animateWithDuration`.
    
    [containerView layoutIfNeeded];
}

- (void)removeAllConstraints {
    UIView *superview = self.superview;
    while (superview != nil) {
        for (NSLayoutConstraint *c in superview.constraints) {
            if (c.firstItem == self || c.secondItem == self) {
                [superview removeConstraint:c];
            }
        }
        superview = superview.superview;
    }
    
    [self removeConstraints:self.constraints];
    self.translatesAutoresizingMaskIntoConstraints = YES;
}

@end
