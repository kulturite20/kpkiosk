//
//  UIImage+ScaleFactor.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 2/18/16.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (ScaleFactor)

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

@end
