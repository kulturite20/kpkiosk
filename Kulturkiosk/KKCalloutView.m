//
//  KKCalloutView.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 6/14/13.
//
//

#import "KKCalloutView.h"
#import <QuartzCore/QuartzCore.h>

#define ARROW_BASE 30.0
#define ARROW_HEIGHT 40.0

@interface KKCalloutView () {
  UIImageView *_borderImageView;
  UIImageView *_arrowView;
  CGFloat _arrowOffset;
  UIPopoverArrowDirection _arrowDirection;
}

@end

@implementation KKCalloutView

- (id)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self) {
    
    self.backgroundColor = [UIColor clearColor];
    _borderImageView.backgroundColor = [UIColor blackColor];
    _arrowView.backgroundColor = [UIColor clearColor];
    
    self.layer.shadowRadius = 15;
    self.layer.shadowOpacity = 0.7;
    self.layer.shadowColor = [UIColor clearColor].CGColor;
    self.layer.shadowOffset = CGSizeMake(0, 4);
    
    _borderImageView.layer.shadowRadius = 15;
    _borderImageView.layer.shadowOpacity = 0.7;
    _borderImageView.layer.shadowColor = [UIColor blackColor].CGColor;
    _borderImageView.layer.shadowOffset = CGSizeMake(0, 4);
    
    _arrowView.layer.shadowRadius = 0;
    _arrowView.layer.shadowOpacity = 0;
    
    [self addSubview:_borderImageView];
    [self addSubview:_arrowView];
   
  }
  
  return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

- (void)layoutSubviews{
  
  
  
  CGFloat _height = self.frame.size.height;
  CGFloat _width = self.frame.size.width;
  CGFloat _left = 0.0;
  CGFloat _top = 0.0;
  CGFloat _coordinate = 0.0;
    //CGAffineTransform _rotation = CGAffineTransformIdentity;
  
  
  switch (self.arrowDirection) {
    case UIPopoverArrowDirectionUp:
      NSLog(@"Arrow up");
        _top += ARROW_HEIGHT;
      _height -= ARROW_HEIGHT;
//      _coordinate = ((self.frame.size.width / 2) + self.arrowOffset) - (ARROW_BASE/2);
//      _arrowView.frame = CGRectMake(_coordinate, 0, ARROW_BASE, ARROW_HEIGHT);
      break;
      
      
    case UIPopoverArrowDirectionDown:
      NSLog(@"Arrow down");
      _top  -= ARROW_HEIGHT;
      _height -= ARROW_HEIGHT;
//      _coordinate = ((self.frame.size.width / 2) + self.arrowOffset) - (ARROW_BASE/2);
//      _arrowView.frame = CGRectMake(_coordinate, _height, ARROW_HEIGHT, ARROW_BASE);
//      _rotation = CGAffineTransformMakeRotation( M_PI );
      break;
      
    case UIPopoverArrowDirectionLeft:
      NSLog(@"Arrow left");
      _left += ARROW_HEIGHT;
      _width += ARROW_HEIGHT;
    _coordinate = ((self.frame.size.height / 2) + self.arrowOffset) - (ARROW_BASE/2);
//      _arrowView.frame = CGRectMake(0, _coordinate, ARROW_BASE, ARROW_HEIGHT);
//      _rotation = CGAffineTransformMakeRotation( -M_PI_2 );
      break;
      
    case UIPopoverArrowDirectionRight:
      NSLog(@"Arrow right");
      _left -= ARROW_HEIGHT;
      _width -= ARROW_HEIGHT;
      _coordinate = ((self.frame.size.height / 2) + self.arrowOffset)- (ARROW_HEIGHT/2);
//      _arrowView.frame = CGRectMake(_width, _coordinate, ARROW_BASE, ARROW_HEIGHT);
//      _rotation = CGAffineTransformMakeRotation( M_PI_2 );
      
      break;
    case UIPopoverArrowDirectionAny:
    case UIPopoverArrowDirectionUnknown:
    default:
      break;
  }
  
  _borderImageView.frame =  CGRectMake(_left , _top, _width, _height);
  _arrowView.frame = CGRectZero;
  
    //[_arrowView setTransform:_rotation];
}

+ (UIEdgeInsets)contentViewInsets
{
  return UIEdgeInsetsMake(5, 5, 5, 5);
}

- (CGFloat) arrowOffset {
  return _arrowOffset;
}

- (void) setArrowOffset:(CGFloat)arrowOffset {
  _arrowOffset = arrowOffset;
}

- (UIPopoverArrowDirection)arrowDirection {
  return _arrowDirection;
}

- (void)setArrowDirection:(UIPopoverArrowDirection)arrowDirection {
  _arrowDirection = arrowDirection;
}

+(CGFloat)arrowHeight {
  return ARROW_HEIGHT;
}

+(CGFloat)arrowBase{
  return ARROW_BASE;
}

+(BOOL)wantsDefaultContentAppearance
{
  return NO;
}

@end
