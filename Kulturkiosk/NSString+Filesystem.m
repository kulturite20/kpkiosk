//
//  NSString+Filesystem.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 9/22/16.
//
//

#import "NSString+Filesystem.h"

@implementation NSString (Filesystem)

- (NSArray *)listOfFiles {
    //-----> LIST ALL FILES <-----//
    NSLog(@"LISTING ALL FILES FOUND");
    
    int count;
    
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:self error:NULL];
    for (count = 0; count < (int)[directoryContent count]; count++)
    {
        NSLog(@"File %d: %@", (count + 1), [directoryContent objectAtIndex:count]);
    }
    return directoryContent;
}

@end
