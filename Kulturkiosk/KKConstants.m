//
//  KKConstants.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 10.12.12.
//
//

#import "KKConstants.h"

@implementation KKConstants

NSString* const UserDidTouchWindowNotification      = @"UserDidTouchWindow";
NSString* const TimerDidEndNotification             = @"TimerDidEnd";
NSString* const ApplicationShouldHideLanguageMenu   = @"ApplicationShouldHideLanguageMenu";

NSString* const ApplicationFont = @"Roboto-Medium";

+ (void)initialize {
    
}

+ (NSURL *)mainStoreURL {
    return [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Kulturkiosk.sqlite"];
}

+ (NSURL *)tempStoreURL {
    return [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Kulturkiosk_temp.sqlite"];
}

// used during moving temp store instead of main, to keep main safety and to be able to restore it in case of failure
+ (NSURL *)mainSlaveStoreURL {
    return [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Kulturkiosk_slave.sqlite"];
}


#pragma mark - Private 

+ (NSURL *)applicationDocumentsDirectory {
//    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    return [NSURL fileURLWithPath:RKApplicationDataDirectory()];
}

@end
