//
//  KKNavigationViewController.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 6/11/13.
//
//

#import "KKNavigationViewController.h"
#import "KKContainerViewController.h"
#import "KKHelpViewController.h"
#import "KKMapViewController.h"
#import "KKIndexViewController.h"
#import "KKContainerViewController.h"
#import "KKTimeLineViewController.h"
#import "KKPresentationViewController.h"
#import "KKPopoverBackgroundView.h"
#import "KKVideoGalleryViewController.h"
#import "KKImageGalleryViewController.h"
#import "KKTextContentViewController.h"
#import "KKHomeScreenViewController.h"
#import "KKGalleryListViewController.h"
#import "KKVideoPresentationViewController.h"
#import "KKImagePresentationViewController.h"
#import "KKImageVideoGalleryViewController.h"

#import "KKRecordContainerViewController.h"
#import "KKRecordPageViewController.h"

#import "KKSlideShowPresentationViewController.h"

#import "KKMosaicPresentationViewController.h"
#import "MosaicPresentation+CoreDataClass.h"
#import "MosaicTemplate+CoreDataClass.h"
#import "KKImageMapViewController.h"
#import "MapImage+CoreDataClass.h"
#import "MapText+CoreDataClass.h"
#import "MapTitle+CoreDataClass.h"
#import "MapDescription+CoreDataClass.h"

//Fake datamodel
#import "TestContent.h"
#import "TestBlock.h"
#import "TestFile.h"
#import "TestPage.h"
#import "TestAudioBlock.h"
#import "TestTextBlock.h"
#import "TestVideoBlock.h"

const CGFloat kNavigationBarDefaultHeight = 60.0f;

@interface KKNavigationViewController ()
<
KKRecordContainerDelegate,
KKSlideShowPresentationViewControllerDelegate
>

@property (nonatomic, strong) NSMutableDictionary *viewControllers;
@property (nonatomic, strong) UIPopoverController *presentedPopoverController;
@property (assign, nonatomic) BOOL restmode;
@property (assign, nonatomic) BOOL isDisplayingNodeContent;
@property (weak, nonatomic) IBOutlet UIButton *homeButton;
@property (assign, nonatomic) KKHelpType helpType;
@property (weak, nonatomic) IBOutlet UIView *navigationBar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightNavigationBarConstraint;
@end

@implementation KKNavigationViewController {
    NSInteger _previousSelectedTabBarItem;
    NSInteger _currentSelectedTabBarItem;
    __weak IBOutlet UILabel *_testingInfoLabel;
    
    BOOL _singlePresentationWasLaunched;
    NSNumber *_defaultPresentationNumber;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    _testingInfoLabel.hidden = YES;
    self.shouldEnterRestmode = YES;
    _defaultPresentationNumber = [[Device allObjects][0] defaultPresentation];
    self.navigationBar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar-gradient.png"]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didTapCallout:) name:@"didTapCallout" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeLanguage) name:@"languageChange" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(highlightContentInPresentation:) name:@"highlightContentInPresentation" object:nil];
    
    // For testing
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(fileLoaded:)
//                                                 name:@"loadedUpdatingFile"
//                                               object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(updatingTimerFired)
//                                                 name:@"updatingTimerFired"
//                                               object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(updateIsNotNeeded)
//                                                 name:@"updateIsNotNeeded"
//                                               object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(isReadyToChangeContent)
//                                                 name:@"loadingUpdatesFinished"
//                                               object:nil];
    
    Device *device = [Device findFirst];
    
    MosaicPresentation *mosaicPresentation = [MosaicPresentation findFirst];
    self.mosaicPresentation = mosaicPresentation;
    
    self.presentations = [device.presentations allObjects];
    self.restmode = device.idleScreenActiveValue;
    self.homeButton.hidden = self.restmode;
    
    NSMutableArray<MapText *> *mapTextArray = [[MapText findAll] mutableCopy];
    
    for (MapText *mapText in mapTextArray) {
        NSLog(@"\n\n ========== \n\nMapText - %@", mapText);
        NSLog(@"MapTitle - %@", mapText.mapTitle);
        NSLog(@"MapDescription - %@", mapText.mapDescription);
    }

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(resetViewState)
                                                 name:TimerDidEndNotification
                                               object:nil];
    
    self.viewControllers = [NSMutableDictionary dictionaryWithCapacity:[self.presentations count]];
    
    [self.buttonContainer setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    int buttonCount = 0;
    NSMutableDictionary *buttons = [NSMutableDictionary dictionary];
    NSArray *sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"position"
                                                               ascending:YES]];
    self.presentations = [self.presentations sortedArrayUsingDescriptors:sortDescriptors];
    
    if (self.mosaicPresentation != nil) {
        self.buttonContainer.hidden = true;
    }
    
    
    for (Presentation *presentation in self.presentations) {
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectZero];
        
        if ([presentation.type isEqualToString:@"map_image"]) {
            [button setImage:[UIImage imageNamed:@"map.png"]
                    forState:UIControlStateNormal];
            [button setImage:[UIImage imageNamed:@"map-active.png"]
                    forState:UIControlStateHighlighted];
            [button setImage:[UIImage imageNamed:@"map-active.png"]
                    forState:UIControlStateSelected];
        } else if ([presentation.type isEqualToString:@"slideshow"]) {
            [button setImage:[UIImage imageNamed:@"index.png"]
                    forState:UIControlStateNormal];
            [button setImage:[UIImage imageNamed:@"index-active.png"]
                    forState:UIControlStateHighlighted];
            [button setImage:[UIImage imageNamed:@"index-active.png"]
                    forState:UIControlStateSelected];
            
        } else {
            [button setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", presentation.type]] forState:UIControlStateNormal];
            [button setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@-active.png", presentation.type]] forState:UIControlStateHighlighted];
            [button setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@-active.png", presentation.type]] forState:UIControlStateSelected];
        }
        
        if ([presentation.type isEqualToString:@"map_image"]) {
            [button addTarget:self
                       action:NSSelectorFromString([NSString stringWithFormat:@"showMapView:"])
             forControlEvents:UIControlEventTouchUpInside];
        } else {
            
            if ([[presentation className] isKindOfClass:[NSNull class]] || self.mosaicPresentation != nil) {
                [button addTarget:self
                           action:NSSelectorFromString(@"showMosaicView:")
                 forControlEvents:UIControlEventTouchUpInside];
            } else {
                [button addTarget:self
                           action:NSSelectorFromString([NSString stringWithFormat:@"show%@View:",[presentation className]])
                 forControlEvents:UIControlEventTouchUpInside];
            }
            
        }
        
        [button setTranslatesAutoresizingMaskIntoConstraints:NO];
        [button setTag:presentation.identifierValue];
        [buttons setObject:button forKey:[NSString stringWithFormat:@"button%d", buttonCount]];
        [self.buttonContainer addSubview:button];
        
        buttonCount++;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.presentations && !self.activeViewController) {
        if (!_singlePresentationWasLaunched) {
            // Tap the first button in the presentations-container
            if (_defaultPresentationNumber) {
                for (UIButton *button in [self.buttonContainer subviews]) {
                    
//                    if (self.mosaicPresentation != nil) {
//                        
//                        [self showMosaicView:button];
//                        
//                        return;
//                    }
                    
                    if (button.tag == [_defaultPresentationNumber integerValue]) {
                        [button sendActionsForControlEvents:UIControlEventTouchUpInside];
                        _currentSelectedTabBarItem = [[self.buttonContainer subviews] indexOfObject:button];
                        return;
                    }
                }
            }
            
            //Check for slide show mode to prevent bugs.
            KKAppDelegate *appDelegate = (KKAppDelegate *)[[UIApplication sharedApplication] delegate];
            if ([appDelegate isInSlideShowMode]) {
                for (Presentation *presentation in self.presentations) {
                    if ([presentation.type isEqualToString:@"slideshow"]) {
                        UIButton *slideShowButton = [[self.buttonContainer subviews] objectAtIndex:[self.presentations indexOfObject:presentation]];
                        //_defaultPresentationNumber = [NSNumber numberWithInt:slideShowButton.tag];
                        [slideShowButton sendActionsForControlEvents:UIControlEventTouchUpInside];
                        _currentSelectedTabBarItem = [[self.buttonContainer subviews] indexOfObject:slideShowButton];
                        return;
                    }
                }
            }
            
            _currentSelectedTabBarItem = 0;
        } else {
            _currentSelectedTabBarItem = 1;
        }
        
        UIButton *button = [[self.buttonContainer subviews] objectAtIndex:_currentSelectedTabBarItem];
        [button sendActionsForControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    NSMutableDictionary *buttons = [NSMutableDictionary dictionary];
    int buttonCount = 0;
    
    for (UIView *view in self.buttonContainer.subviews) {
        [buttons setObject:view forKey:[NSString stringWithFormat:@"item%d", buttonCount]];
        buttonCount++;
    }
    
    NSMutableString *constraintString = [NSMutableString string];
    
    NSUInteger edgeSpacing = (540 - ((buttonCount - 1) * 30) - (buttonCount * 40)) / 2;
    
    // TODO: tmp hack
    
    if (edgeSpacing > 10000) {
        edgeSpacing = 1;
    }
    
    for (int i = 0; i < self.buttonContainer.subviews.count; i++) {
        
        if (i == 0) {
            [constraintString appendFormat:@"|-%d-[%@(40)]", edgeSpacing, [buttons allKeysForObject:self.buttonContainer.subviews[i]][0]];
        } else {
            [constraintString appendFormat:@"-60-[%@(40)]", [buttons allKeysForObject:self.buttonContainer.subviews[i]][0]];
        }
        NSLayoutConstraint *verticalConstraint = [NSLayoutConstraint constraintWithItem:self.buttonContainer.subviews[i]
                                                                              attribute:NSLayoutAttributeTop
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:self.buttonContainer
                                                                              attribute:NSLayoutAttributeTop
                                                                             multiplier:1
                                                                               constant:0];
        [self.buttonContainer addConstraint:verticalConstraint];
    }
    if(self.buttonContainer.subviews.count) {
        [self.buttonContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:constraintString options:NSLayoutFormatAlignAllCenterY metrics:nil views:buttons]];
        [self.buttonContainer setNeedsLayout];
    }
}

- (void)highlightContentInPresentation:(id)notification
{
    //REF
    /*
     float delay = 0.2;
     if(![[self view] window])
     return;
     NSNumber *nodeId = [notification valueForKeyPath:@"userInfo.nodeId"];
     Nodes *node = [Nodes objectWithPredicate:[NSPredicate predicateWithFormat:@"item_id == %@",nodeId]];
     NSNumber *presentationId = [notification valueForKeyPath:@"userInfo.presentationId"];
     Presentation *presentation = [self presentationWithIdentifier:[presentationId intValue]];
     UIButton *button = nil;
     for (UIButton *_button in self.buttonContainer.subviews) {
     if (_button.tag == [presentationId integerValue]) {
     button = _button;
     break;
     }
     }
     
     if ([self.activeViewController isKindOfClass:[KKMapViewController class]]) {
     KKMapViewController *mapController = (KKMapViewController*) self.activeViewController;
     [mapController viewWillDisappear:NO];
     }
     
     [self performSelector:@selector(showPresentationWithSender:) withObject:button];
     [self.activeViewController performSelector:@selector(highlightNode:) withObject:node afterDelay:delay];
     
     if ([presentation.type isEqualToString:@"timeline"]) {
     KKTimeLineViewController *timelineController = (KKTimeLineViewController*) [self viewcontrollerForPresentationType:presentation.type];
     [timelineController restoreScreen];
     delay = 0.7;
     }
     */
}

- (void)resetViewState {
    
    if ([self.activeViewController isKindOfClass:[KKMosaicPresentationViewController class]]) {
        if (((KKMosaicPresentationViewController *) self.activeViewController).mapViewController == nil && self.activeViewController.presentedViewController == nil) {
            return;
        } else if (((KKMosaicPresentationViewController *) self.activeViewController).mapViewController != nil) {
            if (!self.activeViewController.presentedViewController) {
                [((KKMosaicPresentationViewController *) self.activeViewController).mapViewController willMoveToParentViewController:nil];
                [((KKMosaicPresentationViewController *) self.activeViewController).mapViewController.view removeFromSuperview];
                [((KKMosaicPresentationViewController *) self.activeViewController).mapViewController removeFromParentViewController];
                
                ((KKMosaicPresentationViewController *) self.activeViewController).mapViewController = nil;
                
                
                return;
                
            }
        }
    }
    
    [self.presentedPopoverController dismissPopoverAnimated:NO];
    self.presentedPopoverController = nil;

    
    if ([self.childViewControllers count] && [self.childViewControllers[0] isKindOfClass:[KKHelpViewController class]]) {
        [[self.childViewControllers[0] view] removeFromSuperview];
        [self.childViewControllers[0] removeFromParentViewController];
    }
    
    if ([self.activeViewController isKindOfClass:[KKMapViewController class]]) {
        KKMapViewController *mapController = (KKMapViewController*) self.activeViewController;
        [mapController.presentingPopover dismissPopoverAnimated:NO];
        mapController.presentingPopover = nil;
        if ([self.activeViewController.presentedViewController isKindOfClass:[KKRecordContainerViewController class]]) {
            KKRecordContainerViewController *recordContainer = (KKRecordContainerViewController *)[self.activeViewController presentedViewController];
            
            void (^completionBlock)() = ^{
                [self.activeViewController dismissViewControllerAnimated:YES
                                                              completion:^{
                                                                  [self performViewStateResetCompletion];
                                                              }];
            };
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"homeScreenTimerFired"
                                                                object:nil];
            for (KKRecordPageViewController *pageController in recordContainer.controllersForPages) {
                if ([pageController.presentedViewController isKindOfClass:[KKVideoGalleryViewController class]]) {
                    KKVideoGalleryViewController *videoGalleryController = (KKVideoGalleryViewController*)pageController.presentedViewController;
                    //REFACTOR
//                    if (videoGalleryController.moviePlayer.fullscreen) {
//                        videoGalleryController.moviePlayer.fullscreen = NO;
//                    }
                    [videoGalleryController goBackWithCompletionBlock:^{
                        [self.activeViewController dismissViewControllerAnimated:YES
                                                                      completion:completionBlock];
                    }];
                    return;
                }
                if ([pageController.presentedViewController isKindOfClass:[KKImageGalleryViewController class]]) {
                    KKImageGalleryViewController *imageGalleryController = (KKImageGalleryViewController *)pageController.presentedViewController;
                    [imageGalleryController dismissViewControllerAnimated:YES
                                                               completion:completionBlock];
                    return;
                }
                if ([pageController.presentedViewController isKindOfClass:[KKImageVideoGalleryViewController class]]) {
                    KKImageVideoGalleryViewController *imageVideoGalleryController = (KKImageVideoGalleryViewController *)pageController.presentedViewController;
                    [imageVideoGalleryController dismissViewControllerAnimated:YES
                                                               completion:completionBlock];
                    return;
                }
            }
            
            completionBlock();
            return;
        }
    }
    
    if ([self.activeViewController.presentedViewController isKindOfClass:[KKImageGalleryViewController class]]
        || [self.activeViewController.presentedViewController isKindOfClass:[KKTextContentViewController class]]
        || [self.activeViewController.presentedViewController  isKindOfClass:[KKVideoGalleryViewController class]]) {
        if ([self.activeViewController.presentedViewController isKindOfClass:[KKVideoGalleryViewController class]]) {
//            KKVideoGalleryViewController *videoGalleryController = (KKVideoGalleryViewController*)self.activeViewController.presentedViewController;
            //REFACTOR
//            if (videoGalleryController.moviePlayer.fullscreen) {
//                videoGalleryController.moviePlayer.fullscreen = NO;
//            }
        }
    }
    
    if ([self.activeViewController.presentedViewController isKindOfClass:[KKRecordContainerViewController class]] || [self.activeViewController isKindOfClass:[KKRecordContainerViewController class]]) {
        
        KKRecordContainerViewController *recordContainer = nil;
        if ([self.activeViewController presentedViewController]) {
            recordContainer = (KKRecordContainerViewController *)[self.activeViewController presentedViewController];
        } else {
            recordContainer = (KKRecordContainerViewController *)self.activeViewController;
        }
        
        void (^completionBlock)() = ^{
            if (![self.activeViewController presentingViewController] && recordContainer.isSingle) {
                [self performViewStateResetCompletion];
            } else {
                [self.activeViewController dismissViewControllerAnimated:YES
                                                              completion:^{
                                                                  [self performViewStateResetCompletion];
                                                              }];
            }
        };
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"homeScreenTimerFired"
                                                            object:nil];
        for (KKRecordPageViewController *pageController in recordContainer.controllersForPages) {
            if ([pageController.presentedViewController isKindOfClass:[KKVideoGalleryViewController class]]) {
                KKVideoGalleryViewController *videoGalleryController = (KKVideoGalleryViewController*)pageController.presentedViewController;
                //REFACTOR
//                if (videoGalleryController.moviePlayer.fullscreen) {
//                    videoGalleryController.moviePlayer.fullscreen = NO;
//                }
                [videoGalleryController goBackWithCompletionBlock:completionBlock];
                return;
            }
            if ([pageController.presentedViewController isKindOfClass:[KKImageGalleryViewController class]]) {
                KKImageGalleryViewController *imageGalleryController = (KKImageGalleryViewController *)pageController.presentedViewController;
                [imageGalleryController dismissViewControllerAnimated:YES
                                                           completion:completionBlock];
                return;
            }
            if ([pageController.presentedViewController isKindOfClass:[KKImageVideoGalleryViewController class]]) {
                //KKImageVideoGalleryViewController *imageVideoGalleryController = (KKImageVideoGalleryViewController *)pageController.presentedViewController;
                
                // Dismiss immediately
                [pageController dismissViewControllerAnimated:NO completion:nil];
                [self.activeViewController dismissViewControllerAnimated: NO completion:nil];
                
                // Dismiss viewController one after another
//                [imageVideoGalleryController dismissViewControllerAnimated:YES
//                                                           completion:completionBlock];
                return;
            }
        }
        
        completionBlock();
        return;
    }
    
    if ([self.activeViewController isKindOfClass:[KKGalleryListViewController class]]) {
        if (self.activeViewController.presentedViewController) {
            KKPresentationViewController *presentationVC = (KKPresentationViewController *)self.activeViewController.presentedViewController;
            [presentationVC dismissController:nil];
        }
    }
    
    if ([[self presentedViewController] isKindOfClass:[KKHomeScreenViewController class]]) {
        KKHomeScreenViewController *homeScreen = (KKHomeScreenViewController *)[self presentedViewController];
        [[homeScreen presentedViewController] dismissViewControllerAnimated:YES
                                                                 completion:NULL];
        return;
    }
    

    [self performViewStateResetCompletion];
    
}

- (void)performViewStateResetCompletion {

    for (UIView *subview in self.activeView.subviews) {
        [subview removeFromSuperview];
    };
    self.activeViewController = nil;
    self.viewControllers = [NSMutableDictionary dictionaryWithCapacity:[self.presentations count]];
    if (self.shouldEnterRestmode && self.restmode) {
        if (self.view.window != nil) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard"
                                                                 bundle:nil];
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier: @"HomeScreen"];
            [self presentViewController:viewController animated:YES completion:^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"waitingModeEnabled"
                                                                    object:nil];
                NSLog(@"Look ma, no hands");
            }];
        }
    } else if (_defaultPresentationNumber) {
        for (UIButton *button in self.buttonContainer.subviews) {
            if (button.tag == [_defaultPresentationNumber integerValue]) {
                [button sendActionsForControlEvents:UIControlEventTouchUpInside];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"waitingModeEnabled"
                                                                    object:nil];
                NSLog(@"Look ma, no hands");
                break;
            }
        }
    } else {
        UIButton *button = [self.buttonContainer.subviews objectAtIndex:0];
        [button sendActionsForControlEvents:UIControlEventTouchUpInside];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"waitingModeEnabled"
                                                            object:nil];
    }
}


#pragma mark Segue actions

- (void)setUpBottomBar {
    for (UIButton *button in self.buttonContainer.subviews) {
        if (button.selected) {
            _previousSelectedTabBarItem = [self.buttonContainer.subviews indexOfObject:button];
        }
        [button setSelected:NO];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"DisplayHelpView"]) {
        KKHelpViewController *vc = segue.destinationViewController;
        [vc setHelpType:self.helpType];
    }
    if ([segue.identifier isEqualToString:@"DisplayLanguageMenu"]) {
        self.presentedPopoverController = [(UIStoryboardPopoverSegue*)segue popoverController];
        self.presentedPopoverController.popoverBackgroundViewClass = [KKPopoverBackgroundView class];
        
        CGFloat popoverHeight = ([[KKLanguage availableLanguages] count] > 3 ? 200 : 100);
        self.presentedPopoverController.popoverContentSize = CGSizeMake(630, popoverHeight);
    }
}

- (Presentation *) presentationWithIdentifier:(int)index
{
    for (Presentation *presentation in self.presentations) {
        if (presentation.identifierValue == index) {
            return presentation;
        }
    }
    return nil;
}

- (UIViewController *) viewcontrollerForPresentationType:(NSString*)presentationType {
    NSString *type = [presentationType lowercaseString];
    
    UIViewController *viewController;
    
    if ([self.viewControllers objectForKey:type] && ![type isEqualToString:@"slideshow"]){
        viewController = [self.viewControllers objectForKey:type];
    } else {
        if ([type isEqualToString:@"single"])
        {
            viewController = [KKRecordContainerViewController new];
        }
        else if ([type isEqualToString:@"map_image"])
        {
            viewController = [MainStoryboard instantiateViewControllerWithIdentifier:@"MapViewController"];
        }
        else if ([type isEqualToString:@"timeline"])
        {
            viewController = [MainStoryboard instantiateViewControllerWithIdentifier:@"TimelineViewController"];
        }
        else if ([type isEqualToString:@"index"])
        {
            viewController = [MainStoryboard instantiateViewControllerWithIdentifier:@"IndexViewController"];
        }
        else if ([type isEqualToString:@"image_gallery"] || [type isEqualToString:@"video_gallery"])
        {
            viewController = [MainStoryboard instantiateViewControllerWithIdentifier:@"GalleryListViewController"];
        }
        else if ([type isEqualToString:@"slideshow"])
        {
            viewController = [MainStoryboard instantiateViewControllerWithIdentifier:@"SlideShowViewController"];
        }
        else if ([self.mosaicPresentation.type isEqualToString:@"mosaic"]){
            viewController = [MainStoryboard instantiateViewControllerWithIdentifier:@"MosaicViewController"];
            [self.viewControllers setValue:viewController forKey:@"mosaic"];
            return viewController;
        }
        
        [self.viewControllers setValue:viewController forKey:type];
    }
    
    return viewController;
}

#pragma mark - Public

- (void)showSinglePresentationFromHomeScreen:(Presentation *)presentation {
    [self setUpBottomBar];
    
    KKRecordContainerViewController *recordContainer = [KKRecordContainerViewController new];
    recordContainer.record = [presentation singleRecord];
    recordContainer.navBarDelegate = self;
    recordContainer.isSingle = YES;
    recordContainer.isLaunchedFromHomeScreen = YES;
    if (_heightNavigationBarConstraint.constant == kNavigationBarDefaultHeight) {
        _heightNavigationBarConstraint.constant = 0;
    }

    
    if (self.activeViewController == recordContainer)
        return;
    
    for (UIView *subview in self.activeView.subviews) {
        [subview removeFromSuperview];
    }
    
    self.activeViewController = recordContainer;
    self.activeView.frame = self.activeViewController.view.frame;

    [self.activeView addSubview:self.activeViewController.view];
}

#pragma mark - Private

- (void)showPresentationWithSender:(id)sender {
    UIButton *button = (UIButton *)sender;
    Presentation *presentation = [self presentationWithIdentifier:(int)button.tag];
    _currentPresentation = presentation;
    if (![presentation isDefault]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"waitingModeDisabled"
                                                            object:nil];
    }
    [self setUpBottomBar];
    [button setSelected:YES];
    
    
    self.navigationBar.hidden = NO;
    
    UIViewController *viewController = nil;
    
    viewController = [self viewcontrollerForPresentationType:presentation.type];
    
//    if ([viewController respondsToSelector:@selector(setPresentation:)]) {
//        [viewController performSelector:@selector(setPresentation:)
//                             withObject:presentation];
//    }
    
//    if ([viewController respondsToSelector:@selector(setMosaicPresentation:)]) {
//        [viewController performSelector:@selector(setMosaicPresentation:)
//                             withObject:self.mosaicPresentation];
//        self.activeViewController = viewController;
//        [self.activeView addSubview:self.activeViewController.view];
//        
//    } else if ([viewController respondsToSelector:@selector(setPresentation:)]) {
//        [viewController performSelector:@selector(setPresentation:)
//                             withObject:presentation];
//    }

    // Mosaic presentation case
    if ([viewController isKindOfClass:[KKMosaicPresentationViewController class]]) {
        
        NSLog(@"Current type of presetation - mosaic!");
        
        // special configuration for mosaic presentation
        self.helpButton.hidden = true;
        
        ((KKMosaicPresentationViewController *) viewController).mosaicPresentation = self.mosaicPresentation;
        
       // ((KKMosaicPresentationViewController *) viewController).navigationKKController = self;
        
        self.activeViewController = viewController;
        
        [self.activeView addSubview:viewController.view];
        
        [self addChildViewController:viewController];
        //[self presentViewController:self.activeViewController animated:YES completion:nil];
        //[self.activeView addSubview:self.activeViewController.view];
        
        return;
    }
    
    
    if ([viewController respondsToSelector:@selector(setPresentation:)]) {
        [viewController performSelector:@selector(setPresentation:)
                             withObject:presentation];
    }
    
    if ([presentation.type isEqualToString:@"single"]) {
        KKRecordContainerViewController *recordContainer = (KKRecordContainerViewController *)viewController;
        recordContainer.record = [presentation singleRecord];
        recordContainer.navBarDelegate = self;
        recordContainer.isSingle = YES;
        
        //HACK: It's needs because hidding navigation bar with setting height constraint 0 is not working.
        self.navigationBar.hidden = YES;
        
        if (_heightNavigationBarConstraint.constant == kNavigationBarDefaultHeight) {
            _heightNavigationBarConstraint.constant = 0;
        }
    } else if ([presentation.type isEqualToString:@"slideshow"]) {
        KKSlideShowPresentationViewController *recordContainer = (KKSlideShowPresentationViewController *)viewController;
        recordContainer.navBarDelegate = self;
        recordContainer.presentation = presentation;
        
        //HACK: It's needs because hidding navigation bar with setting height constraint 0 is not working.
        self.navigationBar.hidden = YES;
        
        
        if (_heightNavigationBarConstraint.constant == kNavigationBarDefaultHeight) {
            _heightNavigationBarConstraint.constant = 0;
        }
    } else {
        if (_heightNavigationBarConstraint.constant == 0) {
            _heightNavigationBarConstraint.constant = kNavigationBarDefaultHeight;
        }
    }
    
    if (self.activeViewController == viewController && ![self.activeViewController isKindOfClass:[KKSlideShowPresentationViewController class]]) {
        return;
    } else if ([self.activeViewController isKindOfClass:[KKSlideShowPresentationViewController class]]) {
        self.activeViewController = nil;
    }
    
    for (UIView *subview in self.activeView.subviews) {
        [subview removeFromSuperview];
    }
    
    [self.view layoutIfNeeded];
    
    self.activeViewController = viewController;
    self.activeViewController.view.frame = self.activeView.bounds;
    if ([presentation.type isEqualToString:@"single"]) {
        self.activeView.frame = self.activeViewController.view.frame;
    } else if ([presentation.type isEqualToString:@"slideshow"]) {
        self.activeView.frame = self.activeViewController.view.frame;
    } else {
        self.activeViewController.view.frame = self.activeView.frame;
    }
    [self.activeView addSubview:self.activeViewController.view];
}

/** Returns YES if presentation mode is default */
/*
 - (BOOL)displayPresentation:(Presentation *)presentation {
 UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
 UIViewController *viewControllerToPresent = nil;
 switch (filter) {
 case KKArticleFilter: {
 KKTextContentViewController *textContentView = [storyboard instantiateViewControllerWithIdentifier:@"textView"];
 textContentView.node = node;
 viewControllerToPresent = textContentView;
 }
 break;
 case KKImagesFilter: {
 NSArray *imageResources = [node resourcesWithType:KKResourceTypeImage forLanguage:[KKLanguage currentLanguage]];
 if ([imageResources count] == 0) {
 [self showDefaultPresentationWithNode:node];
 return YES;
 }
 KKImageGalleryViewController *imageGalleryView = [storyboard instantiateViewControllerWithIdentifier:@"imageGalleryView"];
 NSArray *sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"page_id" ascending:YES],[NSSortDescriptor sortDescriptorWithKey:@"position" ascending:YES]];
 imageGalleryView.resources = [imageResources sortedArrayUsingDescriptors:sortDescriptors];
 viewControllerToPresent = imageGalleryView;
 }
 break;
 case KKVideoFilter: {
 NSArray *videoResources = [node resourcesWithType:KKResourceTypeVideo forLanguage:[KKLanguage currentLanguage]];
 if ([videoResources count] == 0) {
 [self showDefaultPresentationWithNode:node];
 return YES;
 }
 KKVideoGalleryViewController *videoGalleryView = [storyboard instantiateViewControllerWithIdentifier:@"videoGalleryView"];
 videoGalleryView.resources = [videoResources sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"identifier" ascending:YES]]];
 viewControllerToPresent = videoGalleryView;
 }
 break;
 case KKNoFilter: {
 [self showDefaultPresentationWithNode:node];
 return YES;
 }
 break;
 
 default:
 break;
 }
 
 [self presentViewController:viewControllerToPresent
 animated:YES
 completion:NULL];
 return NO;
 }
 */

- (void)showTimelineView:(id)sender {
    self.helpType = KKHelpTypeSwipe;
    [self showPresentationWithSender:sender];
}

- (void)showMapView:(id)sender {
    self.helpType = KKHelpTypeZoom;
    [self showPresentationWithSender:sender];
}

- (void)showSlideshowView:(id)sender {
    self.helpType = KKHelpTypeZoom;
    [self showPresentationWithSender:sender];
}

- (void)showSingleView:(id)sender {
    self.helpType = KKHelpTypeTap;
    [self showPresentationWithSender:sender];
}

- (void)showIndexView:(id)sender
{
    self.helpType = KKHelpTypeSwipe;
    [self showPresentationWithSender:sender];
}

- (void)showVideoGalleryView:(id)sender
{
    self.helpType = KKHelpTypeSwipe;
    [self showPresentationWithSender:sender];
}

- (void)showImageGalleryView:(id)sender {
    self.helpType = KKHelpTypeSwipe;
    [self showPresentationWithSender:sender];
}

- (void)showMosaicView:(id)sender {
    self.helpType = KKHelpTypeTap;
    [self showPresentationWithSender:sender];
}

- (IBAction)enterRestMode:(id)sender {
    self.shouldEnterRestmode = YES;
    [self resetViewState];
}

- (IBAction)goBack:(id)sender {
    
}

- (IBAction)goForward:(id)sender {
    
}

- (void)didTapCallout:(id)notification {
    //    Record *record = [notification valueForKeyPath:@"userInfo.record"];
    //
    //    KKRecordContainerViewController *recordContainer = [KKRecordContainerViewController new];
    //    recordContainer.record = record;
    //
    //    [self presentViewController:recordContainer
    //                       animated:YES
    //                     completion:NULL];
}

- (void)didChangeLanguage {
    [self.presentedPopoverController dismissPopoverAnimated:YES];
}

//REF
/*
 - (void)showDefaultPresentationWithNode:(Nodes *)node {
 KKContainerViewController *vc = (KKContainerViewController *)[self viewcontrollerForPresentationType:@"single"];
 vc.presentation = nil;
 self.activeViewController = vc;
 self.activeViewController.view.frame=self.activeView.frame;
 [self.activeView addSubview:self.activeViewController.view];
 }
 */

- (IBAction)showHelpForCurrentPresentation:(id)sender
{
    
}

#pragma mark - RecordContainerDelegate

- (void)recordContainerDidDismiss:(KKRecordContainerViewController *)recordContainer {
    NSInteger index = _previousSelectedTabBarItem;
    
    if (self.mosaicPresentation != nil) {
        _heightNavigationBarConstraint.constant = kNavigationBarDefaultHeight;
    }
    
    if (_currentPresentation.isDefault && [_currentPresentation.type isEqualToString:@"single"]) {
        if (_currentSelectedTabBarItem == 0) {
            index = _currentSelectedTabBarItem + 1;
        } else if (_currentSelectedTabBarItem == ([[self.buttonContainer subviews] count] - 1)) {
            index = _currentSelectedTabBarItem - 1;
        } else if (_currentSelectedTabBarItem == _previousSelectedTabBarItem) {
            index = 0;
        }
    }
    UIButton *button = [[self.buttonContainer subviews] objectAtIndex:index];
    [button sendActionsForControlEvents:UIControlEventTouchUpInside];
    [self.viewControllers removeObjectForKey:@"single"];
    
    if (recordContainer.isLaunchedFromHomeScreen) {
        [self performViewStateResetCompletion];
    }
}

#pragma mark - SlideShowControllerDelegate

- (void)slideShowControllerDidDismiss:(KKSlideShowPresentationViewController *)slideShowController {
    if(!self.navigationBar.hidden) {
        _heightNavigationBarConstraint.constant = kNavigationBarDefaultHeight;
    }
}

#pragma mark - For testing

- (void)fileLoaded:(NSNotification *)notification {
    _testingInfoLabel.text  = [NSString stringWithFormat:@"%@", notification.userInfo[@"fileName"]];
}

- (void)updatingTimerFired {
    _testingInfoLabel.text = @"Updating timer fired";
}

- (void)updateIsNotNeeded {
    _testingInfoLabel.text = @"JSONs are equal, no update needed";
}

- (void)isReadyToChangeContent {
    _testingInfoLabel.text = @"Assets loaded. Ready to change content";
}

@end
