//
//  UIView+Autolayout.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 10/20/16.
//
//

#import <UIKit/UIKit.h>

@interface UIView (Autolayout)

- (void)addToParentAndCentered:(UIView *)view;
- (void)centeringWithParentView:(UIView *)view;
- (void)removeAllConstraints;

@end
