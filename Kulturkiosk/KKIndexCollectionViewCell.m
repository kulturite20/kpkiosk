//
//  KKIndexCollectionViewCell.m
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 08.08.13.
//
//

#import "KKIndexCollectionViewCell.h"

@interface KKIndexCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *subTitle;
@property (weak, nonatomic) IBOutlet UILabel *desc;


@end


@implementation KKIndexCollectionViewCell

- (id)initWithCoder:(NSCoder *)aDecoder
{
  self = [super initWithCoder:aDecoder];
  if (self)
  {

  }
  return self;
}

- (void)awakeFromNib
{
  self.title.font = [UIFont fontWithName:@"MyriadPro-Bold" size:21];
  self.subTitle.font = [UIFont fontWithName:@"MyriadPro-Bold" size:16];
  self.desc.font =[UIFont fontWithName:@"MyriadPro-Bold" size:15];
  
}


- (void)populateWithRecord:(Record*)record
              presentation:(Presentation *)presentation
{
  self.record = record;
  
  
//  NSArray *records = [Record objectsWithPredicate:[NSPredicate predicateWithFormat:@"node == %@ AND presentation != nil",record.node]];
  self.mapButton.hidden=YES;
  self.timelineButton.hidden=YES;
  
//  for (Record *presentationRecord in records) {
    
    if ([presentation.type isEqualToString:@"map"]) {
      self.mapButton.hidden = NO;
//      self.mapButton.tag = presentationRecord.node.item_idValue;
    }else if ([presentation.type isEqualToString:@"timeline"]){
      self.timelineButton.hidden = NO;
//      self.timelineButton.tag = self.record.node.item_idValue;
    }
//  }
  Content *content= [record contentForLanguage:[KKLanguage currentLanguage]];
  self.image.image= [[record image] thumbnailImage];
  self.title.text = content.title;
  //self.subTitle.text = content.subtitle;
  self.desc.text = content.desc;
}


@end
