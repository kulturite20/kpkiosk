//
//  KKTimerManager.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 4/17/14.
//
//

#import <Foundation/Foundation.h>
#import "NSTimer+Identification.h"

@protocol KKTimerManagerDelegate <NSObject>

@required
- (void)timerTimeoutFinished:(NSTimer *)timer;
@optional
- (void)timerDidStart:(NSTimer *)timer;

@end

@interface KKTimerManager : NSObject

@property (nonatomic, weak) id <KKTimerManagerDelegate> delegate;

+ (instancetype)sharedManager;

- (void)startTimerWithIdentifier:(NSString *)name
                         timeout:(NSInteger)seconds;
- (void)stopTimerWithIdentifier:(NSString *)name;
- (void)restartTimerWithIdentifier:(NSString *)name;

- (NSTimer *)timerWithIdentifier:(NSString *)name;
- (NSArray *)activeTimers;

@end
