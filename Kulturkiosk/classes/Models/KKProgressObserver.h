//
//  KKProgressManager.h
//  Kulturkiosk
//
//  Created by Roman Ivchenko on 5/16/14.
//
//

#import <Foundation/Foundation.h>

@protocol KKProgressObserverDelegate <NSObject>

- (void)idleProgressWithPercents:(CGFloat)percents
                        stopFlag:(BOOL *)stopFlag;

- (void)progressWithPercents:(CGFloat)percents;

@end

@interface KKProgressObserver : NSObject

@property (nonatomic, weak) id <KKProgressObserverDelegate> delegate;

@property (nonatomic) CGFloat percentsStepInterval;
@property (nonatomic) CGFloat invocationTime;

+ (instancetype)sharedObserver;

- (void)increaseProgressWithValueInPercents:(CGFloat)percents;
- (void)observeProgress:(CGFloat)progress;
- (void)startIdleProgress;

@end
