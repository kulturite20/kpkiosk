// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AudioBlock.h instead.

#import <CoreData/CoreData.h>
#import "Block.h"

extern const struct AudioBlockAttributes {
} AudioBlockAttributes;

extern const struct AudioBlockRelationships {
	__unsafe_unretained NSString *files;
} AudioBlockRelationships;

extern const struct AudioBlockFetchedProperties {
} AudioBlockFetchedProperties;

@class File;


@interface AudioBlockID : NSManagedObjectID {}
@end

@interface _AudioBlock : Block {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (AudioBlockID*)objectID;





@property (nonatomic, strong) NSSet *files;

- (NSMutableSet*)filesSet;





@end

@interface _AudioBlock (CoreDataGeneratedAccessors)

- (void)addFiles:(NSSet*)value_;
- (void)removeFiles:(NSSet*)value_;
- (void)addFilesObject:(File*)value_;
- (void)removeFilesObject:(File*)value_;

@end

@interface _AudioBlock (CoreDataGeneratedPrimitiveAccessors)



- (NSMutableSet*)primitiveFiles;
- (void)setPrimitiveFiles:(NSMutableSet*)value;


@end
