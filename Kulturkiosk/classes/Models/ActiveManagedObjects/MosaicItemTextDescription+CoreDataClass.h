//
//  MosaicItemTextDescription+CoreDataClass.h
//  
//
//  Created by Vadim Osovets on 2/24/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface MosaicItemTextDescription : NSManagedObject

- (UIFont *)fontWithSize;
- (NSInteger)textAlignment;

@end

NS_ASSUME_NONNULL_END

#import "MosaicItemTextDescription+CoreDataProperties.h"
