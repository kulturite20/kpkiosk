#import "Resource.h"


@interface Resource ()

// Private interface goes here.

@end


@implementation Resource

// Custom logic goes here.
- (NSString *)pathToOriginalItem {
    for (Link *link in self.links) {
        if ([link.type isEqualToString:@"original"]) {
            return link.localUrl;
        }
    }
    return nil;
}

- (NSString *)pathToMainItem {
    for (Link *link in self.links) {
        if ([link.type isEqualToString:@"main"]) {
            return link.localUrl;
        }
    }
    return nil;
}

- (NSString *)pathToThumbnailItem {
    for (Link *link in self.links) {
        if ([link.type isEqualToString:@"thumb"]) {
            return link.localUrl;
        }
    }
    return nil;
}

@end
