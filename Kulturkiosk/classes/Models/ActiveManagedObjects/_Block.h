// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Block.h instead.

#import <CoreData/CoreData.h>


extern const struct BlockAttributes {
	__unsafe_unretained NSString *identifier;
	__unsafe_unretained NSString *position;
	__unsafe_unretained NSString *type;
} BlockAttributes;

extern const struct BlockRelationships {
	__unsafe_unretained NSString *page;
} BlockRelationships;

extern const struct BlockFetchedProperties {
} BlockFetchedProperties;

@class Page;





@interface BlockID : NSManagedObjectID {}
@end

@interface _Block : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (BlockID*)objectID;





@property (nonatomic, strong) NSNumber* identifier;



@property int32_t identifierValue;
- (int32_t)identifierValue;
- (void)setIdentifierValue:(int32_t)value_;

//- (BOOL)validateIdentifier:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* position;



@property int32_t positionValue;
- (int32_t)positionValue;
- (void)setPositionValue:(int32_t)value_;

//- (BOOL)validatePosition:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* type;



//- (BOOL)validateType:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Page *page;

//- (BOOL)validatePage:(id*)value_ error:(NSError**)error_;





@end

@interface _Block (CoreDataGeneratedAccessors)

@end

@interface _Block (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSNumber*)value;

- (int32_t)primitiveIdentifierValue;
- (void)setPrimitiveIdentifierValue:(int32_t)value_;




- (NSNumber*)primitivePosition;
- (void)setPrimitivePosition:(NSNumber*)value;

- (int32_t)primitivePositionValue;
- (void)setPrimitivePositionValue:(int32_t)value_;




- (NSString*)primitiveType;
- (void)setPrimitiveType:(NSString*)value;





- (Page*)primitivePage;
- (void)setPrimitivePage:(Page*)value;


@end
