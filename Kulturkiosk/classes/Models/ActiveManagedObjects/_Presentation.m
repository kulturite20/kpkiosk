// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Presentation.m instead.

#import "_Presentation.h"

const struct PresentationAttributes PresentationAttributes = {
	.identifier = @"identifier",
	.name = @"name",
	.position = @"position",
	.type = @"type",
};

const struct PresentationRelationships PresentationRelationships = {
	.contents = @"contents",
	.device = @"device",
	.records_decriptions = @"records_decriptions",
	.resources = @"resources",
};

const struct PresentationFetchedProperties PresentationFetchedProperties = {
};

@implementation PresentationID
@end

@implementation _Presentation

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Presentation" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Presentation";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Presentation" inManagedObjectContext:moc_];
}

- (PresentationID*)objectID {
	return (PresentationID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"identifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"identifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"positionValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"position"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic identifier;



- (int16_t)identifierValue {
	NSNumber *result = [self identifier];
	return [result shortValue];
}

- (void)setIdentifierValue:(int16_t)value_ {
	[self setIdentifier:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveIdentifierValue {
	NSNumber *result = [self primitiveIdentifier];
	return [result shortValue];
}

- (void)setPrimitiveIdentifierValue:(int16_t)value_ {
	[self setPrimitiveIdentifier:[NSNumber numberWithShort:value_]];
}





@dynamic name;






@dynamic position;



- (int32_t)positionValue {
	NSNumber *result = [self position];
	return [result intValue];
}

- (void)setPositionValue:(int32_t)value_ {
	[self setPosition:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitivePositionValue {
	NSNumber *result = [self primitivePosition];
	return [result intValue];
}

- (void)setPrimitivePositionValue:(int32_t)value_ {
	[self setPrimitivePosition:[NSNumber numberWithInt:value_]];
}





@dynamic type;






@dynamic contents;

	
- (NSMutableSet*)contentsSet {
	[self willAccessValueForKey:@"contents"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"contents"];
  
	[self didAccessValueForKey:@"contents"];
	return result;
}
	

@dynamic device;

	

@dynamic records_decriptions;

	
- (NSMutableSet*)records_decriptionsSet {
	[self willAccessValueForKey:@"records_decriptions"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"records_decriptions"];
  
	[self didAccessValueForKey:@"records_decriptions"];
	return result;
}
	

@dynamic resources;

	
- (NSMutableSet*)resourcesSet {
	[self willAccessValueForKey:@"resources"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"resources"];
  
	[self didAccessValueForKey:@"resources"];
	return result;
}
	






@end
