// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AudioBlock.m instead.

#import "_AudioBlock.h"

const struct AudioBlockAttributes AudioBlockAttributes = {
};

const struct AudioBlockRelationships AudioBlockRelationships = {
	.files = @"files",
};

const struct AudioBlockFetchedProperties AudioBlockFetchedProperties = {
};

@implementation AudioBlockID
@end

@implementation _AudioBlock

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"AudioBlock" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"AudioBlock";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"AudioBlock" inManagedObjectContext:moc_];
}

- (AudioBlockID*)objectID {
	return (AudioBlockID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic files;

	
- (NSMutableSet*)filesSet {
	[self willAccessValueForKey:@"files"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"files"];
  
	[self didAccessValueForKey:@"files"];
	return result;
}
	






@end
