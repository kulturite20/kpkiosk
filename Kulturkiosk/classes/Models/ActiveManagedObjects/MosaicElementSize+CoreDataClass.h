//
//  MosaicElementSize+CoreDataClass.h
//  
//
//  Created by Vadim Osovets on 2/24/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface MosaicElementSize : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "MosaicElementSize+CoreDataProperties.h"
