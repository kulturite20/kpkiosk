// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to RecordImage.m instead.

#import "_RecordImage.h"

const struct RecordImageAttributes RecordImageAttributes = {
	.identifier = @"identifier",
	.name = @"name",
};

const struct RecordImageRelationships RecordImageRelationships = {
	.credits = @"credits",
	.links = @"links",
	.record = @"record",
};

const struct RecordImageFetchedProperties RecordImageFetchedProperties = {
};

@implementation RecordImageID
@end

@implementation _RecordImage

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"RecordImage" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"RecordImage";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"RecordImage" inManagedObjectContext:moc_];
}

- (RecordImageID*)objectID {
	return (RecordImageID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"identifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"identifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic identifier;



- (int32_t)identifierValue {
	NSNumber *result = [self identifier];
	return [result intValue];
}

- (void)setIdentifierValue:(int32_t)value_ {
	[self setIdentifier:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveIdentifierValue {
	NSNumber *result = [self primitiveIdentifier];
	return [result intValue];
}

- (void)setPrimitiveIdentifierValue:(int32_t)value_ {
	[self setPrimitiveIdentifier:[NSNumber numberWithInt:value_]];
}





@dynamic name;






@dynamic credits;

	
- (NSMutableSet*)creditsSet {
	[self willAccessValueForKey:@"credits"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"credits"];
  
	[self didAccessValueForKey:@"credits"];
	return result;
}
	

@dynamic links;

	
- (NSMutableSet*)linksSet {
	[self willAccessValueForKey:@"links"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"links"];
  
	[self didAccessValueForKey:@"links"];
	return result;
}
	

@dynamic record;

	
- (NSMutableSet*)recordSet {
	[self willAccessValueForKey:@"record"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"record"];
  
	[self didAccessValueForKey:@"record"];
	return result;
}
	






@end
