//
//  _MosaicPresentation.h
//  Kulturkiosk
//
//  Created by Dmitry on 2/14/17.
//
//

#import <CoreData/CoreData.h>

extern const struct MosaicPresentationAttributes {
    __unsafe_unretained NSString *identifier;
    __unsafe_unretained NSString *type;
    __unsafe_unretained NSString *updated_at;
} MosaicPresentationAttributes;

extern const struct MosaicPresentationRelationships {
    __unsafe_unretained NSString *mosaic_items;
    __unsafe_unretained NSString *contents;
    __unsafe_unretained NSString *presentation_template;
    __unsafe_unretained NSString *resources;
    
} MosaicPresentationRelationships;

extern const struct MosaicPresentationFetchedProperties {
} MosaicPresentationFetchedProperties;

@class Content;
@class MosaicItem;
@class Resource;
@class MosaicPresentation;
@class MosaicTemplate;

@interface MosaicPresentationID : NSManagedObjectID {}
@end

@interface _MosaicPresentation : NSManagedObject

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (MosaicPresentationID*)objectID;

@property (nonatomic, strong) NSNumber* identifier;

@property int16_t identifierValue;
- (int16_t)identifierValue;
- (void)setIdentifierValue:(int16_t)value_;

@property (nonatomic, strong) NSString* type;

@property (nonatomic, strong) NSString* updated_at;

@property (nonatomic, strong) NSSet *mosaic_items;

- (NSMutableSet*)mosaic_itemsSet;

@property (nonatomic, strong) NSSet *contents;

- (NSMutableSet*)contentsSet;

@property (nonatomic, strong) NSSet *presentation_template;

- (NSMutableSet*)mosaicPresentationTemplateSet;

@property (nonatomic, strong) NSSet *resources;

- (NSMutableSet*)mosaicResourcesSet;

@end


@interface _MosaicPresentation (CoreDataGeneratedAccessors)

- (void)addContents:(NSSet*)value_;
- (void)removeContents:(NSSet*)value_;
- (void)addContentsObject:(Content*)value_;
- (void)removeContentsObject:(Content*)value_;

- (void)addmosaic_items:(NSSet*)value_;
- (void)removemosaic_items:(NSSet*)value_;
- (void)addmosaic_itemsObject:(MosaicItem*)value_;
- (void)removemosaic_itemsObject:(MosaicItem*)value_;

- (void)addPresentationTemplate:(NSSet*)value_;
- (void)removePresentationTemplate:(NSSet*)value_;
- (void)addPresentationTemplateObject:(MosaicTemplate*)value_;
- (void)removePresentationTemplateObject:(MosaicTemplate*)value_;

- (void)addResources:(NSSet*)value_;
- (void)removeResources:(NSSet*)value_;
- (void)addResourcesObject:(Resource*)value_;
- (void)removeResourcesObject:(Resource*)value_;

@end

@interface _MosaicPresentation (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSNumber*)value;

- (int16_t)primitiveIdentifierValue;
- (void)setPrimitiveIdentifierValue:(int16_t)value_;


- (NSString*)primitiveType;
- (void)setPrimitiveType:(NSString*)value;

- (NSString*)primitiveUpdatedAt;
- (void)setPrimitiveUpadtedAt:(NSString*)value;

- (NSMutableSet*)primitivemosaic_items;
- (void)setPrimitivemosaic_items:(NSMutableSet*)value;

- (NSMutableSet*)primitiveContents;
- (void)setPrimitiveContents:(NSMutableSet*)value;

- (NSMutableSet*)primitivePresentationTemplate;
- (void)setPrimitivePresentationTemplate:(NSMutableSet*)value;

- (NSMutableSet*)primitiveResources;
- (void)setPrimitiveResources:(NSMutableSet*)value;

@end

