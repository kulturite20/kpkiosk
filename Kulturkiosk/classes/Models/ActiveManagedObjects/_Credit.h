// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Credit.h instead.

#import <CoreData/CoreData.h>


extern const struct CreditAttributes {
	__unsafe_unretained NSString *credit_type;
	__unsafe_unretained NSString *identifier;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *parent_id;
} CreditAttributes;

extern const struct CreditRelationships {
	__unsafe_unretained NSString *file;
	__unsafe_unretained NSString *record_image;
} CreditRelationships;

extern const struct CreditFetchedProperties {
} CreditFetchedProperties;

@class File;
@class RecordImage;






@interface CreditID : NSManagedObjectID {}
@end

@interface _Credit : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (CreditID*)objectID;





@property (nonatomic, strong) NSString* credit_type;



//- (BOOL)validateCredit_type:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* identifier;



@property int32_t identifierValue;
- (int32_t)identifierValue;
- (void)setIdentifierValue:(int32_t)value_;

//- (BOOL)validateIdentifier:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* parent_id;



//- (BOOL)validateParent_id:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) File *file;

//- (BOOL)validateFile:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) RecordImage *record_image;

//- (BOOL)validateRecord_image:(id*)value_ error:(NSError**)error_;





@end

@interface _Credit (CoreDataGeneratedAccessors)

@end

@interface _Credit (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveCredit_type;
- (void)setPrimitiveCredit_type:(NSString*)value;




- (NSNumber*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSNumber*)value;

- (int32_t)primitiveIdentifierValue;
- (void)setPrimitiveIdentifierValue:(int32_t)value_;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




- (NSString*)primitiveParent_id;
- (void)setPrimitiveParent_id:(NSString*)value;





- (File*)primitiveFile;
- (void)setPrimitiveFile:(File*)value;



- (RecordImage*)primitiveRecord_image;
- (void)setPrimitiveRecord_image:(RecordImage*)value;


@end
