#import "Presentation.h"


@interface Presentation ()
// Private interface goes here.
@end

@implementation Presentation

- (NSString *)className {
    return [[self.type capitalizedString] stringByReplacingOccurrencesOfString: @"_" withString: @""];
}

- (NSArray *)records {
    NSMutableArray *records = [NSMutableArray new];
    for (RecordDescription *recordDescription in self.records_decriptions) {
        for (Record *record in [Record allObjects]) {
            if (record.identifierValue == [recordDescription record_idValue]) {
                [records addObject:record];
            }
        }
    }
    return [records copy];
}

- (Record *)singleRecord {
    if ([self.type isEqualToString:@"single"]) {
        
        Record *record = self.records.firstObject;
        record.isSingle = YES;
        
        return record;
    } else {
        return nil;
    }
}

- (UIImage *)timelineBackground {
    for (Resource *resource in self.resources) {
        if ([resource.type isEqualToString:@"timeline_bg"]) {
            return [UIImage imageWithContentsOfFile:[resource pathToOriginalItem]];
        }
    }
    return nil;
}

- (UIImage *)icon {
    UIImage *presentationIcon = nil;
    for (Resource *resource in self.resources) {
        if ([resource.type isEqualToString:@"icon"]) {
            presentationIcon = [UIImage imageWithContentsOfFile:[resource pathToThumbnailItem]];
            break;
        }
    }
    return presentationIcon ? presentationIcon : [UIImage imageNamed:[NSString stringWithFormat:@"%@-idle", self.type]];
}

- (UIImage *)mapBackground {
    for (Resource *resource in self.resources) {
        if ([resource.type isEqualToString:@"map"]) {
            return [UIImage imageWithContentsOfFile:[resource pathToOriginalItem]];
        }
    }
    //Try to get main image if no original image present
//    for (Resource *resource in self.resources) {
//        if ([resource.type isEqualToString:@"map"]) {
//            return [UIImage imageWithContentsOfFile:[resource pathToMainItem]];
//        }
//    }
    return nil;
}

- (UIImage *)timelineInterLayer {
    for (Resource *resource in self.resources) {
        if ([resource.type isEqualToString:@"timeline_interlayer"]) {
            return [UIImage imageWithContentsOfFile:[resource pathToOriginalItem]];
        }
    }
    return nil;
}

- (BOOL)isDefault {
    return [[[Device allObjects][0] defaultPresentation] integerValue] == [self.identifier integerValue] ? YES : NO;
}

- (Record *)defaultRecord {
    Device *device = [Device allObjects][0];
    if ([device defaultPoint]) {
        for (Record *record in self.records) {
            if (record.identifierValue == device.defaultPointValue) {
                return record;
            }
        }
    }
    return nil;
}

@end
