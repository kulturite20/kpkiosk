//
//  MosaicItemTextDescription+CoreDataClass.m
//  
//
//  Created by Vadim Osovets on 2/24/17.
//
//

#import "MosaicItemTextDescription+CoreDataClass.h"

@implementation MosaicItemTextDescription

- (UIFont *)fontWithSize {
    
    NSLog(@"Current description font is - %@", self.font);
    
    //Default size
    int fontSize = 17;
    
    //Change defailt size in case we have one from server
    if ([self.font_size isEqualToString:@"small"]) {
        fontSize = 13;
    } else if ([self.font_size isEqualToString:@"medium"]) {
        fontSize = 17;
    } else if ([self.font_size isEqualToString:@"large"]) {
        fontSize = 27;
    }
    
    NSLog(@"Current description font size is - %d", fontSize);
    
    if (self.font == nil) {
        return [UIFont fontWithName:ApplicationFont size:fontSize];
    }
    
    return [UIFont fontWithName:self.font size:fontSize];
}

- (NSInteger)textAlignment {
    if ([self.alignment  isEqual: @"left"]) {
        return NSTextAlignmentLeft;
    } else if ([self.alignment  isEqual: @"right"]) {
        return NSTextAlignmentRight;
    } else if ([self.alignment  isEqual: @"center"]) {
        return NSTextAlignmentCenter;
    }
    return NSTextAlignmentLeft;
}

@end
