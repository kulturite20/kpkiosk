//
//  MosaicItemText+CoreDataClass.h
//  
//
//  Created by Vadim Osovets on 2/24/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MosaicItemTextDescription, MosaicItemTextTitle;

NS_ASSUME_NONNULL_BEGIN

@interface MosaicItemText : NSManagedObject

- (NSString *)contentForLanguage:(KKLanguageType)lang;

@end

NS_ASSUME_NONNULL_END

#import "MosaicItemText+CoreDataProperties.h"
