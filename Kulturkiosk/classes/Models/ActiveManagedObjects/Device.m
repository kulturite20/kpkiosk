#import "Device.h"


@interface Device ()

// Private interface goes here.

@end


@implementation Device

// Custom logic goes here.
- (UIImage *)idleScreenBackground {
    for (Resource *resource in self.resources) {
        if ([resource.type isEqualToString:@"idle_screen_background"]) {
            return [UIImage imageWithContentsOfFile:[resource pathToMainItem]];
        }
    }
    return nil;
}

- (UIImage *)idleScreenLogo {
    for (Resource *resource in self.resources) {
        if ([resource.type isEqualToString:@"idle_screen_logo"]) {
            return [UIImage imageWithContentsOfFile:[resource pathToOriginalItem]];
        }
    }
    return nil;
}

@end
