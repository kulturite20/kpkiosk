#import "_Presentation.h"
#import "Record.h"

typedef enum KKHelpType {
    KKHelpTypeSwipe = 0,
    KKHelpTypeSwipeLeft,
    KKHelpTypeSwipeRight,
    KKHelpTypePinchZoom,
    KKHelpTypeZoom,
    KKHelpTypeTap
} KKHelpType;

@interface Presentation : _Presentation {}
// Custom logic goes here.

- (UIImage *)icon;

- (NSArray *)records;
- (UIImage *)timelineBackground;
- (UIImage *)timelineInterLayer;
- (UIImage *)mapBackground;

- (NSString *)className;
- (Record *)singleRecord;

- (BOOL)isDefault;
- (Record *)defaultRecord;

@end
