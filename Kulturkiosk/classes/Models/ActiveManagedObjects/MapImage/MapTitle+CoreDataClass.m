//
//  MapTitle+CoreDataClass.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 3/17/17.
//
//

#import "MapTitle+CoreDataClass.h"

@implementation MapTitle

- (UIFont *)fontWithSize {
    
    NSLog(@"DEBUGTEXT MapTitle FONT-NAME: %@", self.font);
    NSLog(@"DEBUGTEXT MapTitle FONT-SIZE: %@", self.size);
        NSLog(@"DEBUGTEXT MapTitle CONTENT: %@", self.content);
    
    //Default size
    int fontSize = 17;
    
    //Change defailt size in case we have one from server
    if ([self.size isEqualToString:@"small"]) {
        fontSize = 13;
    } else if ([self.size isEqualToString:@"medium"]) {
        fontSize = 15;
    } else if ([self.size isEqualToString:@"large"]) {
        fontSize = 17;
    }
    
    NSLog(@"Current description font size is - %d", fontSize);
    
    if (self.font == nil) {
        return [UIFont fontWithName:ApplicationFont size:fontSize];
    }
    
    return [UIFont fontWithName:self.font size:fontSize];

}

- (NSInteger)textAlignment {
    if ([self.alignment  isEqual: @"left"]) {
        return NSTextAlignmentLeft;
    } else if ([self.alignment  isEqual: @"right"]) {
        return NSTextAlignmentRight;
    } else if ([self.alignment  isEqual: @"center"]) {
        return NSTextAlignmentCenter;
    }
    return NSTextAlignmentLeft;
}

@end
