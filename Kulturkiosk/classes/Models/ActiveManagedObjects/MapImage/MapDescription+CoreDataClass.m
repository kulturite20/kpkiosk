//
//  MapDescription+CoreDataClass.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 3/17/17.
//
//

#import "MapDescription+CoreDataClass.h"

@implementation MapDescription

- (UIFont *)fontWithSize {
    
    NSLog(@"DEBUGTEXT MapDescription FONT-NAME: %@", self.font);
    NSLog(@"DEBUGTEXT MapDescription FONT-SIZE: %@", self.font_size);
    NSLog(@"DEBUGTEXT MapDescription CONTENT: %@", self.content);
    
    //Default size
    int fontSize = 17;
    
    //Change defailt size in case we have one from server
    if ([self.font_size isEqualToString:@"small"]) {
        fontSize = 17;
    } else if ([self.font_size isEqualToString:@"medium"]) {
        fontSize = 20;
    } else if ([self.font_size isEqualToString:@"large"]) {
        fontSize = 23;
    }
    
    NSLog(@"Current description font size is - %d", fontSize);
    
    if (self.font == nil) {
        return [UIFont fontWithName:ApplicationFont size:fontSize];
    }
    
    return [UIFont fontWithName:self.font size:fontSize];
}

- (NSInteger)textAlignment {
    if ([self.alignment  isEqual: @"left"]) {
        return NSTextAlignmentLeft;
    } else if ([self.alignment  isEqual: @"right"]) {
        return NSTextAlignmentRight;
    } else if ([self.alignment  isEqual: @"center"]) {
        return NSTextAlignmentCenter;
    }
    
    return NSTextAlignmentLeft;
}

@end
