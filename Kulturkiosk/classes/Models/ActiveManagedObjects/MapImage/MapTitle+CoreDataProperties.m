//
//  MapTitle+CoreDataProperties.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 3/17/17.
//
//

#import "MapTitle+CoreDataProperties.h"

@implementation MapTitle (CoreDataProperties)

+ (NSFetchRequest<MapTitle *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MapTitle"];
}

@dynamic alignment;
@dynamic color;
@dynamic content;
@dynamic font;
@dynamic position;
@dynamic size;

@end
