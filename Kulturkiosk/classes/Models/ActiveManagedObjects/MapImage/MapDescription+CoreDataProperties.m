//
//  MapDescription+CoreDataProperties.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 3/17/17.
//
//

#import "MapDescription+CoreDataProperties.h"

@implementation MapDescription (CoreDataProperties)

+ (NSFetchRequest<MapDescription *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MapDescription"];
}

@dynamic alignment;
@dynamic color;
@dynamic content;
@dynamic font;
@dynamic font_size;
@dynamic position;

@end
