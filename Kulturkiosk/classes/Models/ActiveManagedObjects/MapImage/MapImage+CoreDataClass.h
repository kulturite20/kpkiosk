//
//  MapImage+CoreDataClass.h
//  
//
//  Created by Vadim Osovets on 3/3/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum {
    TextPlacementRight,
    TextPlacementTop,
    TextPlacementBottom,
    TextPlacementLeft, 
    TextPlacementCenter
} TextPlacement;

@interface MapImage : NSManagedObject

- (TextPlacement)textPlacement;

@end

NS_ASSUME_NONNULL_END

#import "MapImage+CoreDataProperties.h"
