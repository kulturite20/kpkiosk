//
//  MapText+CoreDataProperties.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 3/20/17.
//
//

#import "MapText+CoreDataProperties.h"

@implementation MapText (CoreDataProperties)

+ (NSFetchRequest<MapText *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MapText"];
}

@dynamic language;
@dynamic mapDescription;
@dynamic mapTitle;

@end
