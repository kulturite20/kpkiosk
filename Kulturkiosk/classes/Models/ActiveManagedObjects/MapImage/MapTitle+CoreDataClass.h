//
//  MapTitle+CoreDataClass.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 3/17/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface MapTitle : NSManagedObject

- (UIFont *)fontWithSize;
- (NSInteger)textAlignment;

@end

NS_ASSUME_NONNULL_END

#import "MapTitle+CoreDataProperties.h"
