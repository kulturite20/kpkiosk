//
//  MapImage+CoreDataProperties.h
//  
//
//  Created by Vadim Osovets on 3/3/17.
//
//

#import "MapImage+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MapImage (CoreDataProperties)

+ (NSFetchRequest<MapImage *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *anchorOn;
@property (nullable, nonatomic, copy) NSNumber *anchor_y;
@property (nullable, nonatomic, copy) NSNumber *anchor_x;
@property (nullable, nonatomic, copy) NSNumber *map_x;
@property (nullable, nonatomic, copy) NSNumber *map_y;
@property (nullable, nonatomic, copy) NSString *text_placement;

@end

NS_ASSUME_NONNULL_END
