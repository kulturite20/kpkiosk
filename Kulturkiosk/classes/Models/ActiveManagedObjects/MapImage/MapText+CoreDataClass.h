//
//  MapText+CoreDataClass.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 3/20/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MapDescription, MapTitle;

NS_ASSUME_NONNULL_BEGIN

@interface MapText : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "MapText+CoreDataProperties.h"
