//
//  MapText+CoreDataProperties.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 3/20/17.
//
//

#import "MapText+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MapText (CoreDataProperties)

+ (NSFetchRequest<MapText *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *language;
@property (nullable, nonatomic, retain) MapDescription *mapDescription;
@property (nullable, nonatomic, retain) MapTitle *mapTitle;

@end

NS_ASSUME_NONNULL_END
