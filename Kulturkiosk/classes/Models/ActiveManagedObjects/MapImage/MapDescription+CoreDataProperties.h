//
//  MapDescription+CoreDataProperties.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 3/17/17.
//
//

#import "MapDescription+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MapDescription (CoreDataProperties)

+ (NSFetchRequest<MapDescription *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *alignment;
@property (nullable, nonatomic, copy) NSString *color;
@property (nullable, nonatomic, copy) NSString *content;
@property (nullable, nonatomic, copy) NSString *font;
@property (nullable, nonatomic, copy) NSString *font_size;
@property (nullable, nonatomic, copy) NSNumber *position;

@end

NS_ASSUME_NONNULL_END
