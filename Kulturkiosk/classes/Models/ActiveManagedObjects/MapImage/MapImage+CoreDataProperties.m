//
//  MapImage+CoreDataProperties.m
//  
//
//  Created by Vadim Osovets on 3/3/17.
//
//

#import "MapImage+CoreDataProperties.h"

@implementation MapImage (CoreDataProperties)

+ (NSFetchRequest<MapImage *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MapImage"];
}

@dynamic anchorOn;
@dynamic anchor_y;
@dynamic anchor_x;
@dynamic map_x;
@dynamic map_y;
@dynamic text_placement;

@end
