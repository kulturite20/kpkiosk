//
//  MapImage+CoreDataClass.m
//  
//
//  Created by Vadim Osovets on 3/3/17.
//
//

#import "MapImage+CoreDataClass.h"


@implementation MapImage

- (TextPlacement)textPlacement{
    
    if ([self.text_placement isEqualToString:@"right"]) {
        return TextPlacementRight;
        
    } else if ([self.text_placement isEqualToString:@"bottom"]) {
        return TextPlacementBottom;
        
    } else if ([self.text_placement isEqualToString:@"top"]) {
        return TextPlacementTop;

    } else if ([self.text_placement isEqualToString:@"left"]) {
        return TextPlacementLeft;
        
    }
    
    return TextPlacementCenter;

}

@end
