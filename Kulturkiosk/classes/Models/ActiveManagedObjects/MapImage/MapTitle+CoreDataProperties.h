//
//  MapTitle+CoreDataProperties.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 3/17/17.
//
//

#import "MapTitle+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MapTitle (CoreDataProperties)

+ (NSFetchRequest<MapTitle *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *alignment;
@property (nullable, nonatomic, copy) NSString *color;
@property (nullable, nonatomic, copy) NSString *content;
@property (nullable, nonatomic, copy) NSString *font;
@property (nullable, nonatomic, copy) NSNumber *position;
@property (nullable, nonatomic, copy) NSString *size;

@end

NS_ASSUME_NONNULL_END
