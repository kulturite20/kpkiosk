// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Link.m instead.

#import "_Link.h"

const struct LinkAttributes LinkAttributes = {
	.localUrl = @"localUrl",
	.type = @"type",
	.u_id = @"u_id",
	.url = @"url",
	.url2 = @"url2",
};

const struct LinkRelationships LinkRelationships = {
	.file = @"file",
	.record_image = @"record_image",
	.resource = @"resource",
};

const struct LinkFetchedProperties LinkFetchedProperties = {
};

@implementation LinkID
@end

@implementation _Link

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Link" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Link";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Link" inManagedObjectContext:moc_];
}

- (LinkID*)objectID {
	return (LinkID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic localUrl;






@dynamic type;






@dynamic u_id;






@dynamic url;






@dynamic url2;






@dynamic file;

	

@dynamic record_image;

	

@dynamic resource;

	






@end
