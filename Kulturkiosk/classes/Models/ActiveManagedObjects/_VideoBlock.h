// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to VideoBlock.h instead.

#import <CoreData/CoreData.h>
#import "Block.h"

extern const struct VideoBlockAttributes {
} VideoBlockAttributes;

extern const struct VideoBlockRelationships {
	__unsafe_unretained NSString *files;
} VideoBlockRelationships;

extern const struct VideoBlockFetchedProperties {
} VideoBlockFetchedProperties;

@class File;


@interface VideoBlockID : NSManagedObjectID {}
@end

@interface _VideoBlock : Block {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (VideoBlockID*)objectID;





@property (nonatomic, strong) NSSet *files;

- (NSMutableSet*)filesSet;





@end

@interface _VideoBlock (CoreDataGeneratedAccessors)

- (void)addFiles:(NSSet*)value_;
- (void)removeFiles:(NSSet*)value_;
- (void)addFilesObject:(File*)value_;
- (void)removeFilesObject:(File*)value_;

@end

@interface _VideoBlock (CoreDataGeneratedPrimitiveAccessors)



- (NSMutableSet*)primitiveFiles;
- (void)setPrimitiveFiles:(NSMutableSet*)value;


@end
