#import "File.h"
#import <AVFoundation/AVFoundation.h>

@interface File ()

// Private interface goes here.

@end


@implementation File

@synthesize volumeLevel = _volumeLevel;
@synthesize timeToSeek = _timeToSeek;

// Custom logic goes here.

- (NSString *)pathToOriginalItem {
    for (Link *link in self.links) {
        if ([link.type isEqualToString:@"original"]) {
            return link.localUrl;
        }
    }
    return nil;
}

- (NSString *)pathToMainItem {
    for (Link *link in self.links) {
        if ([link.type isEqualToString:@"main"]) {
            return link.localUrl;
        }
    }
    return nil;
}

- (NSString *)pathToThumbnailItem {
    for (Link *link in self.links) {
        if ([link.type isEqualToString:@"thumb"]) {
            return link.localUrl;
        }
    }
    return nil;
}

- (UIImage *)thumbnailWithSize:(CGSize *)size {
    UIImage *theImage = nil;
    
    if([self.type isEqualToString:@"video"])
    {
        // Its a video. Take a screen grab from the middle point in the video.
        
        NSURL *url = [NSURL fileURLWithPath:[self pathToOriginalItem]];
        
        NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:AVAssetReferenceRestrictionForbidNone], AVURLAssetReferenceRestrictionsKey, nil];
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:url
                                                    options:options];
        
        if(!asset.playable) {
            return nil;
        }
        
        AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        generator.requestedTimeToleranceBefore = kCMTimeZero;
        generator.requestedTimeToleranceAfter  = kCMTimeZero;
        
        if(size != NULL)
            generator.maximumSize = *size;
        NSError *err = NULL;
        
        CMTime time = CMTimeMakeWithSeconds(CMTimeGetSeconds([asset duration])/2, 60);
        //NSLog(@"Generating thumbnail for %@", url);
        CGImageRef imgRef = [generator copyCGImageAtTime:time actualTime:NULL error:&err];
        if(imgRef == NULL) {
            NSLog(@"Error generating thumbnail: %@", [err localizedFailureReason]);
            
            // second attempt to generate
            
            NSError *err2 = NULL;
            
            CMTime time = CMTimeMakeWithSeconds(CMTimeGetSeconds([asset duration])/1.5, 60);
            imgRef = [generator copyCGImageAtTime:time actualTime:NULL error:&err2];
            
            if (imgRef == NULL) {
                NSLog(@"Error generating thumbnail: %@", [err2 localizedFailureReason]);
                return nil;
            } else {
                
                theImage = [[UIImage alloc] initWithCGImage:imgRef];
                CGImageRelease(imgRef);
                
                return theImage;
            }
        }
        
        theImage = [[UIImage alloc] initWithCGImage:imgRef];
        CGImageRelease(imgRef);
    }
    
    return theImage;
}

- (Content *)contentForLanguage:(KKLanguageType)languageType {
    Content *__block resultingContent = nil;
 
    NSString *lang = [KKLanguage descriptionForLanguageType:languageType];
    
    [self.contents enumerateObjectsUsingBlock:^(Content *content, BOOL *stop) {
        if ([content.language isEqualToString:lang]) {
            resultingContent = content;
        }
        
        *stop = resultingContent != nil;
    }];
        
    return resultingContent;
}

@end
