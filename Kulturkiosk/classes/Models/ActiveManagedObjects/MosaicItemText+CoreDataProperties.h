//
//  MosaicItemText+CoreDataProperties.h
//  
//
//  Created by Vadim Osovets on 2/24/17.
//
//

#import "MosaicItemText+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MosaicItemText (CoreDataProperties)

+ (NSFetchRequest<MosaicItemText *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *language;
@property (nullable, nonatomic, retain) MosaicItemTextDescription *desriptionText;
@property (nullable, nonatomic, retain) MosaicItemTextTitle *title;

@end

NS_ASSUME_NONNULL_END
