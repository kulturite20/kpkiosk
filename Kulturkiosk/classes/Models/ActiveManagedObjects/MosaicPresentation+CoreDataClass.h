//
//  MosaicPresentation+CoreDataClass.h
//  
//
//  Created by Vadim Osovets on 2/24/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Content, MosaicItem, MosaicTemplate, Resource;

NS_ASSUME_NONNULL_BEGIN

@interface MosaicPresentation : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "MosaicPresentation+CoreDataProperties.h"
