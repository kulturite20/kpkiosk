#import "Page.h"


@interface Page ()

// Private interface goes here.

@end


@implementation Page

- (NSArray *)videoBlocks {
    NSMutableArray *videoBlocks = [NSMutableArray new];
    for (Block *block in self.blocks) {
        if ([block.type isEqualToString:@"image_video"]) {
            [videoBlocks addObject:block];
        }
    }
    return [videoBlocks copy];
}

- (NSArray *)audioBlocks {
    NSMutableArray *audioBlocks = [NSMutableArray new];
    for (Block *block in self.blocks) {
        if ([block.type isEqualToString:@"audio"]) {
            [audioBlocks addObject:block];
        }
    }
    return audioBlocks;
}

- (NSArray *)textBlocks {
    NSMutableArray *textBlocks = [NSMutableArray new];
    for (Block *block in self.blocks) {
        if ([block.type isEqualToString:@"text"]) {
            [textBlocks addObject:block];
        }
    }
    return textBlocks;
}

@end
