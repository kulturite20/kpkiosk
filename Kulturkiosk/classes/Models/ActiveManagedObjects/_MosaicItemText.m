//
//  MosaicItemText.m
//  Kulturkiosk
//
//  Created by Dmitry on 2/14/17.
//
//

#import "_MosaicItemText.h"

const struct MosaicItemTextAttributes MosaicItemTextAttributes = {
};

const struct MosaicItemTextRelationships MosaicItemTextRelationships = {
    .description = @"desriptionText",
    .title = @"title"
};

const struct MosaicItemTextFetchedProperties MosaicItemTextFetchedProperties = {
};

@implementation MosaicItemTextID
@end

@implementation _MosaicItemText

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
    NSParameterAssert(moc_);
    return [NSEntityDescription insertNewObjectForEntityForName:@"MosaicItemText" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
    return @"MosaicItemText";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
    NSParameterAssert(moc_);
    return [NSEntityDescription entityForName:@"MosaicItemText" inManagedObjectContext:moc_];
}

- (MosaicItemTextID*)objectID {
    return (MosaicItemTextID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
    NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
    
    return keyPaths;
}


@dynamic description;


@dynamic title;


@end
