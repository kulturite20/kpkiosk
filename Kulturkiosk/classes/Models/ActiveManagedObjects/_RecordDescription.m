// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to RecordDescription.m instead.

#import "_RecordDescription.h"

const struct RecordDescriptionAttributes RecordDescriptionAttributes = {
	.code = @"code",
	.color = @"color",
	.position = @"position",
	.record_id = @"record_id",
	.u_id = @"u_id",
	.x = @"x",
	.y = @"y",
};

const struct RecordDescriptionRelationships RecordDescriptionRelationships = {
	.presentation = @"presentation",
    .map_image = @"map_image",
    .map_text = @"map_text",
};

const struct RecordDescriptionFetchedProperties RecordDescriptionFetchedProperties = {
};

@implementation RecordDescriptionID
@end

@implementation _RecordDescription

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"RecordDescription" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"RecordDescription";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"RecordDescription" inManagedObjectContext:moc_];
}

- (RecordDescriptionID*)objectID {
	return (RecordDescriptionID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"positionValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"position"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"record_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"record_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"u_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"u_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"xValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"x"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"yValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"y"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic code;






@dynamic color;



@dynamic placement;



@dynamic position;



- (int32_t)positionValue {
	NSNumber *result = [self position];
	return [result intValue];
}

- (void)setPositionValue:(int32_t)value_ {
	[self setPosition:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitivePositionValue {
	NSNumber *result = [self primitivePosition];
	return [result intValue];
}

- (void)setPrimitivePositionValue:(int32_t)value_ {
	[self setPrimitivePosition:[NSNumber numberWithInt:value_]];
}





@dynamic record_id;



- (int16_t)record_idValue {
	NSNumber *result = [self record_id];
	return [result shortValue];
}

- (void)setRecord_idValue:(int16_t)value_ {
	[self setRecord_id:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveRecord_idValue {
	NSNumber *result = [self primitiveRecord_id];
	return [result shortValue];
}

- (void)setPrimitiveRecord_idValue:(int16_t)value_ {
	[self setPrimitiveRecord_id:[NSNumber numberWithShort:value_]];
}





@dynamic u_id;



- (int32_t)u_idValue {
	NSNumber *result = [self u_id];
	return [result intValue];
}

- (void)setU_idValue:(int32_t)value_ {
	[self setU_id:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveU_idValue {
	NSNumber *result = [self primitiveU_id];
	return [result intValue];
}

- (void)setPrimitiveU_idValue:(int32_t)value_ {
	[self setPrimitiveU_id:[NSNumber numberWithInt:value_]];
}





@dynamic x;



- (int32_t)xValue {
	NSNumber *result = [self x];
	return [result intValue];
}

- (void)setXValue:(int32_t)value_ {
	[self setX:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveXValue {
	NSNumber *result = [self primitiveX];
	return [result intValue];
}

- (void)setPrimitiveXValue:(int32_t)value_ {
	[self setPrimitiveX:[NSNumber numberWithInt:value_]];
}





@dynamic y;



- (int32_t)yValue {
	NSNumber *result = [self y];
	return [result intValue];
}

- (void)setYValue:(int32_t)value_ {
	[self setY:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveYValue {
	NSNumber *result = [self primitiveY];
	return [result intValue];
}

- (void)setPrimitiveYValue:(int32_t)value_ {
	[self setPrimitiveY:[NSNumber numberWithInt:value_]];
}





@dynamic presentation;

	
- (NSMutableSet*)presentationSet {
	[self willAccessValueForKey:@"presentation"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"presentation"];
  
	[self didAccessValueForKey:@"presentation"];
	return result;
}

@dynamic map_image;

@dynamic map_text;


//- (NSMutableSet*)map_imageSet {
//    [self willAccessValueForKey:@"map_image"];
//    
//    NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"map_image"];
//    
//    [self didAccessValueForKey:@"map_image"];
//    return result;
//}









@end
