// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TextBlock.m instead.

#import "_TextBlock.h"

const struct TextBlockAttributes TextBlockAttributes = {
	.text = @"text",
};

const struct TextBlockRelationships TextBlockRelationships = {
};

const struct TextBlockFetchedProperties TextBlockFetchedProperties = {
};

@implementation TextBlockID
@end

@implementation _TextBlock

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TextBlock" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TextBlock";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TextBlock" inManagedObjectContext:moc_];
}

- (TextBlockID*)objectID {
	return (TextBlockID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic text;











@end
