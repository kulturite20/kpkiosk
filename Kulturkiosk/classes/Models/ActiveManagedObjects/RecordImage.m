#import "RecordImage.h"


@interface RecordImage ()

// Private interface goes here.

@end


@implementation RecordImage

// Custom logic goes here.
- (UIImage *)thumbnailImage {
    UIImage *thumbImage = nil;
    for (Link *link in self.links) {
        if ([link.type isEqualToString:@"thumb"]) {
            thumbImage = [UIImage imageWithContentsOfFile:link.localUrl];
        }
    }
    return thumbImage;
}

@end
