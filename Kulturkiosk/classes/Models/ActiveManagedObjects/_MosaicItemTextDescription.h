//
//  MosaicItemTextDescription.h
//  Kulturkiosk
//
//  Created by Dmitry on 2/14/17.
//
//

#import <CoreData/CoreData.h>

extern const struct MosaicItemTextDescriptionAttributes {
    __unsafe_unretained NSString *font_size;
    __unsafe_unretained NSString *position;
    __unsafe_unretained NSString *placement;
    __unsafe_unretained NSString *font;
    __unsafe_unretained NSString *color;
    __unsafe_unretained NSString *alignment;
} MosaicItemTextDescriptionAttributes;

extern const struct MosaicItemTextDescriptionRelationships {
} MosaicItemTextDescriptionRelationships;

extern const struct MosaicItemTextDescriptionFetchedProperties {
} MosaicItemTextTitleFetchedProperties;


@interface MosaicItemTextDescriptionID : NSManagedObjectID {}
@end

@interface _MosaicItemTextDescription : NSManagedObject

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (MosaicItemTextDescriptionID*)objectID;


@property (nonatomic, strong) NSString* font_size;

@property (nonatomic, strong) NSString* placement;

@property (nonatomic, strong) NSString* font;

@property (nonatomic, strong) NSString* color;

@property (nonatomic, strong) NSString* alignment;

@property (nonatomic, strong) NSNumber* position;

@property int32_t positionValue;
- (int32_t)positionValue;
- (void)setPositionValue:(int16_t)value_;

@end

@interface _MosaicItemTextDescription (CoreDataGeneratedAccessors)
@end

@interface _MosaicItemTextDescription (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveFontSize;
- (void)setPimitiveFontSize:(NSString*)value;

- (NSString*)primitivePlacement;
- (void)setPrimitivePlacement:(NSString*)value;

- (NSString*)primitiveFont;
- (void)setPrimitiveFont:(NSString*)value;

- (NSString*)primitiveColor;
- (void)setPrimitiveColor:(NSString*)value;

- (NSString*)primitiveAlignment;
- (void)setPrimitiveAlignment:(NSString*)value;

- (NSString*)primitiveItemType;
- (void)setPrimitiveItemType:(NSString*)value;

- (NSNumber*)primitivePosition;
- (void)setPrimitivePosition:(NSNumber*)value;

- (int32_t)primitivePositionValue;
- (void)setPrimitivePositionValue:(int32_t)value_;

@end
