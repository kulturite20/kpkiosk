// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to VideoBlock.m instead.

#import "_VideoBlock.h"

const struct VideoBlockAttributes VideoBlockAttributes = {
};

const struct VideoBlockRelationships VideoBlockRelationships = {
	.files = @"files",
};

const struct VideoBlockFetchedProperties VideoBlockFetchedProperties = {
};

@implementation VideoBlockID
@end

@implementation _VideoBlock

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"VideoBlock" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"VideoBlock";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"VideoBlock" inManagedObjectContext:moc_];
}

- (VideoBlockID*)objectID {
	return (VideoBlockID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic files;

	
- (NSMutableSet*)filesSet {
	[self willAccessValueForKey:@"files"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"files"];
  
	[self didAccessValueForKey:@"files"];
	return result;
}
	






@end
