// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Resource.h instead.

#import <CoreData/CoreData.h>


extern const struct ResourceAttributes {
	__unsafe_unretained NSString *identifier;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *type;
	__unsafe_unretained NSString *u_id;
} ResourceAttributes;

extern const struct ResourceRelationships {
	__unsafe_unretained NSString *device;
	__unsafe_unretained NSString *links;
	__unsafe_unretained NSString *presentation;
} ResourceRelationships;

extern const struct ResourceFetchedProperties {
} ResourceFetchedProperties;

@class Device;
@class Link;
@class Presentation;






@interface ResourceID : NSManagedObjectID {}
@end

@interface _Resource : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ResourceID*)objectID;





@property (nonatomic, strong) NSNumber* identifier;



@property int32_t identifierValue;
- (int32_t)identifierValue;
- (void)setIdentifierValue:(int32_t)value_;

//- (BOOL)validateIdentifier:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* type;



//- (BOOL)validateType:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* u_id;



//- (BOOL)validateU_id:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Device *device;

//- (BOOL)validateDevice:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSSet *links;

- (NSMutableSet*)linksSet;




@property (nonatomic, strong) Presentation *presentation;

//- (BOOL)validatePresentation:(id*)value_ error:(NSError**)error_;





@end

@interface _Resource (CoreDataGeneratedAccessors)

- (void)addLinks:(NSSet*)value_;
- (void)removeLinks:(NSSet*)value_;
- (void)addLinksObject:(Link*)value_;
- (void)removeLinksObject:(Link*)value_;

@end

@interface _Resource (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSNumber*)value;

- (int32_t)primitiveIdentifierValue;
- (void)setPrimitiveIdentifierValue:(int32_t)value_;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




- (NSString*)primitiveType;
- (void)setPrimitiveType:(NSString*)value;




- (NSString*)primitiveU_id;
- (void)setPrimitiveU_id:(NSString*)value;





- (Device*)primitiveDevice;
- (void)setPrimitiveDevice:(Device*)value;



- (NSMutableSet*)primitiveLinks;
- (void)setPrimitiveLinks:(NSMutableSet*)value;



- (Presentation*)primitivePresentation;
- (void)setPrimitivePresentation:(Presentation*)value;


@end
