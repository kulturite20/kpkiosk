// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TextBlock.h instead.

#import <CoreData/CoreData.h>
#import "Block.h"

extern const struct TextBlockAttributes {
	__unsafe_unretained NSString *text;
} TextBlockAttributes;

extern const struct TextBlockRelationships {
} TextBlockRelationships;

extern const struct TextBlockFetchedProperties {
} TextBlockFetchedProperties;




@interface TextBlockID : NSManagedObjectID {}
@end

@interface _TextBlock : Block {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (TextBlockID*)objectID;





@property (nonatomic, strong) NSString* text;



//- (BOOL)validateText:(id*)value_ error:(NSError**)error_;






@end

@interface _TextBlock (CoreDataGeneratedAccessors)

@end

@interface _TextBlock (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveText;
- (void)setPrimitiveText:(NSString*)value;




@end
