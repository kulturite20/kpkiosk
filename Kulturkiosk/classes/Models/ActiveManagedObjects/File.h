#import "_File.h"
#import "Content.h"
#import "KKLanguage.h"
#import <CoreMedia/CoreMedia.h>

@interface File : _File {}

// if file is a videofile
@property (nonatomic, assign) CMTime timeToSeek;
@property (nonatomic, assign) float volumeLevel;

- (NSString *)pathToOriginalItem;
- (NSString *)pathToMainItem;
- (NSString *)pathToThumbnailItem;

- (UIImage *)thumbnailWithSize:(CGSize *)size;

- (Content *)contentForLanguage:(KKLanguageType)language;

@end
