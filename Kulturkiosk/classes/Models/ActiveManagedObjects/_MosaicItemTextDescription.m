//
//  MosaicItemTextDescription.m
//  Kulturkiosk
//
//  Created by Dmitry on 2/14/17.
//
//

#import "_MosaicItemTextDescription.h"

const struct MosaicItemTextDescriptionAttributes MosaicItemTextDescriptionAttributes = {
    .font_size = @"font_size",
    .position = @"position",
    .placement = @"placement",
    .font = @"font",
    .color = @"color",
    .alignment = @"alignment",
};

const struct MosaicItemTextDescriptionRelationships MosaicItemTextDescriptionRelationships = {
};

const struct MosaicItemTextDescriptionFetchedProperties MosaicItemTextDescriptionFetchedProperties = {
};

@implementation MosaicItemTextDescriptionID
@end

@implementation _MosaicItemTextDescription 

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
    NSParameterAssert(moc_);
    return [NSEntityDescription insertNewObjectForEntityForName:@"MosaicItemTextDescription" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
    return @"MosaicItemTextDescription";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
    NSParameterAssert(moc_);
    return [NSEntityDescription entityForName:@"MosaicItemTextDescription" inManagedObjectContext:moc_];
}

- (MosaicItemTextDescriptionID*)objectID {
    return (MosaicItemTextDescriptionID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
    NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
    
    if ([key isEqualToString:@"positionValue"]) {
        NSSet *affectingKey = [NSSet setWithObject:@"position"];
        keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
        return keyPaths;
    }
    return keyPaths;
}

@dynamic font_size;

@dynamic placement;

@dynamic alignment;

@dynamic font;

@dynamic color;

@dynamic position;

- (int16_t)positionValue{
    NSNumber *result = [self position];
    return [result intValue];
}

- (void)setPositionValue:(int16_t)value_ {
    [self setPosition:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitivePositionValue {
    NSNumber *result = [self primitivePosition];
    return [result intValue];
}

- (void)setPrimitivePositionValue:(int32_t)value_ {
    [self setPrimitivePosition:[NSNumber numberWithInt:value_]];
}


@end

