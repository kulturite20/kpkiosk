// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to File.m instead.

#import "_File.h"

const struct FileAttributes FileAttributes = {
	.identifier = @"identifier",
	.name = @"name",
	.position = @"position",
	.type = @"type",
	.u_id = @"u_id",
};

const struct FileRelationships FileRelationships = {
	.audio_block = @"audio_block",
	.contents = @"contents",
	.credits = @"credits",
	.links = @"links",
	.video_block = @"video_block",
};

const struct FileFetchedProperties FileFetchedProperties = {
};

@implementation FileID
@end

@implementation _File

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"File" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"File";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"File" inManagedObjectContext:moc_];
}

- (FileID*)objectID {
	return (FileID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"identifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"identifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"positionValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"position"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic identifier;



- (int32_t)identifierValue {
	NSNumber *result = [self identifier];
	return [result intValue];
}

- (void)setIdentifierValue:(int32_t)value_ {
	[self setIdentifier:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveIdentifierValue {
	NSNumber *result = [self primitiveIdentifier];
	return [result intValue];
}

- (void)setPrimitiveIdentifierValue:(int32_t)value_ {
	[self setPrimitiveIdentifier:[NSNumber numberWithInt:value_]];
}





@dynamic name;






@dynamic position;



- (int32_t)positionValue {
	NSNumber *result = [self position];
	return [result intValue];
}

- (void)setPositionValue:(int32_t)value_ {
	[self setPosition:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitivePositionValue {
	NSNumber *result = [self primitivePosition];
	return [result intValue];
}

- (void)setPrimitivePositionValue:(int32_t)value_ {
	[self setPrimitivePosition:[NSNumber numberWithInt:value_]];
}





@dynamic type;






@dynamic u_id;






@dynamic audio_block;

	

@dynamic contents;

	
- (NSMutableSet*)contentsSet {
	[self willAccessValueForKey:@"contents"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"contents"];
  
	[self didAccessValueForKey:@"contents"];
	return result;
}
	

@dynamic credits;

	
- (NSMutableSet*)creditsSet {
	[self willAccessValueForKey:@"credits"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"credits"];
  
	[self didAccessValueForKey:@"credits"];
	return result;
}
	

@dynamic links;

	
- (NSMutableSet*)linksSet {
	[self willAccessValueForKey:@"links"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"links"];
  
	[self didAccessValueForKey:@"links"];
	return result;
}
	

@dynamic video_block;

	






@end
