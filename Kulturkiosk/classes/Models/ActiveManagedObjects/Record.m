#import "Record.h"


@interface Record ()

// Private interface goes here.

@end


@implementation Record

@synthesize position;
@synthesize isSingle;

- (Content *)contentForLanguage:(KKLanguageType)lang {
    NSString *language = nil;
    switch (lang) {
        case KKLanguageTypeEnglish: {
            language = @"en";
        }
            break;
        case KKLanguageTypeFrench: {
            language = @"fr";
        }
            break;
        case KKLanguageTypeGerman: {
            language = @"de";
        }
            break;
        case KKLanguageTypeSwedish: {
            language = @"sv";
        }
            break;
        case KKLanguageTypeSpanish: {
            language = @"es";
        }
            break;
        case KKLanguageTypeNorwegian: {
            language = @"no";
        }
            break;
            
        default:
            break;
    }
    
    for (Content *content in self.contents) {
        if ([content.language isEqualToString:language]) {
            return content;
        }
    }
    return nil;
}

- (RecordDescription *)recordDescriptionWithPresentation:(Presentation *)presentation {
    for (RecordDescription *recordDescription in presentation.records_decriptions) {
        if ([recordDescription.record_id isEqualToNumber:self.identifier]) {
            return recordDescription;
        }
    }
    return nil;
    
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"record_id == %d", self.identifierValue];
//    NSArray *recordDescription = [RecordDescription objectsWithPredicate:predicate];
//    if ([recordDescription count] == 0) {
//        return nil;
//    }
//    return [recordDescription objectAtIndex:0];
}

@end
