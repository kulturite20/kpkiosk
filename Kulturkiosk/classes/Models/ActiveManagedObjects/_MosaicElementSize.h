//
//  MosaicElementSize.h
//  Kulturkiosk
//
//  Created by Dmitry on 2/13/17.
//
//

#import <CoreData/CoreData.h>

extern const struct MosaicElementSizeAttributes {
    __unsafe_unretained NSString *identifier;
    __unsafe_unretained NSString *x;
    __unsafe_unretained NSString *y;
} MosaicElementSizeAttributes;

extern const struct MosaicElementSizeRelationships {
} MosaicElementSizeRelationships;

extern const struct MosaicElementSizeFetchedProperties {
} MosaicElementSizeFetchedProperties;

@interface MosaicElementSizeID : NSManagedObjectID {}
@end


@interface _MosaicElementSize : NSManagedObject
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (MosaicElementSizeID*)objectID;

@property (nonatomic, strong) NSNumber* identifier;

@property int16_t identifierValue;
- (int16_t)identifierValue;
- (void)setIdentifierValue:(int16_t)value_;

@property (nonatomic, strong) NSNumber* x;

@property int32_t xValue;
- (int32_t)xValue;
- (void)setXValue:(int32_t)value_;

@property (nonatomic, strong) NSNumber* y;

@property int32_t yValue;
- (int32_t)yValue;
- (void)setYValue:(int32_t)value_;



@end

@interface _MosaicElementSize (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveX;
- (void)setPrimitiveX:(NSNumber*)value;

- (int32_t)primitiveXValue;
- (void)setPrimitiveXValue:(int32_t)value_;

- (NSNumber*)primitiveY;
- (void)setPrimitiveY:(NSNumber*)value;

- (int32_t)primitiveYValue;
- (void)setPrimitiveYValue:(int32_t)value_;

@end
