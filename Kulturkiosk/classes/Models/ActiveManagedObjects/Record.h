#import "_Record.h"
#import "Content.h"
#import "RecordDescription.h"
#import "KKLanguage.h"

@interface Record : _Record {}
// Custom logic goes here.

@property (nonatomic, assign) NSInteger position;
@property (nonatomic, assign) BOOL isSingle;

- (Content *)contentForLanguage:(KKLanguageType)language;
- (RecordDescription *)recordDescriptionWithPresentation:(Presentation *)presentation;

@end
