#import "_Device.h"

@interface Device : _Device {}
// Custom logic goes here.

- (UIImage *)idleScreenBackground;
- (UIImage *)idleScreenLogo;

@end