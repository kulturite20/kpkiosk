//
//  MosaicItemTextTitle+CoreDataProperties.m
//  
//
//  Created by Vadim Osovets on 2/24/17.
//
//

#import "MosaicItemTextTitle+CoreDataProperties.h"

@implementation MosaicItemTextTitle (CoreDataProperties)

+ (NSFetchRequest<MosaicItemTextTitle *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MosaicItemTextTitle"];
}

@dynamic alignment;
@dynamic color;
@dynamic content;
@dynamic font;
@dynamic placement;
@dynamic position;
@dynamic size;

@end
