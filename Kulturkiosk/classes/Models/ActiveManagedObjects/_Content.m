// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Content.m instead.

#import "_Content.h"

const struct ContentAttributes ContentAttributes = {
	.desc = @"desc",
	.language = @"language",
	.parent_id = @"parent_id",
	.title = @"title",
};

const struct ContentRelationships ContentRelationships = {
	.file = @"file",
	.pages = @"pages",
	.presentation = @"presentation",
	.record = @"record",
    .mosaicPresentation = @"mosaicPresentation"
};

const struct ContentFetchedProperties ContentFetchedProperties = {
};

@implementation ContentID
@end

@implementation _Content

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Content" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Content";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Content" inManagedObjectContext:moc_];
}

- (ContentID*)objectID {
	return (ContentID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic desc;






@dynamic language;






@dynamic parent_id;






@dynamic title;






@dynamic file;

	
- (NSMutableSet*)fileSet {
	[self willAccessValueForKey:@"file"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"file"];
  
	[self didAccessValueForKey:@"file"];
	return result;
}
	

@dynamic pages;

	
- (NSMutableSet*)pagesSet {
	[self willAccessValueForKey:@"pages"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"pages"];
  
	[self didAccessValueForKey:@"pages"];
	return result;
}
	

@dynamic presentation;

	
- (NSMutableSet*)presentationSet {
	[self willAccessValueForKey:@"presentation"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"presentation"];
  
	[self didAccessValueForKey:@"presentation"];
	return result;
}

@dynamic mosaicPresentation;


- (NSMutableSet*)mosaicPresentationSet {
    [self willAccessValueForKey:@"mosaicPresentation"];
    
    NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"mosaicPresentation"];
    
    [self didAccessValueForKey:@"mosaicPresentation"];
    return result;
}

@dynamic record;

	






@end
