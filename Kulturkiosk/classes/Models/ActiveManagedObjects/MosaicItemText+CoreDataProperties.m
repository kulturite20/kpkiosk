//
//  MosaicItemText+CoreDataProperties.m
//  
//
//  Created by Vadim Osovets on 2/24/17.
//
//

#import "MosaicItemText+CoreDataProperties.h"

@implementation MosaicItemText (CoreDataProperties)

+ (NSFetchRequest<MosaicItemText *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MosaicItemText"];
}

@dynamic language;
@dynamic desriptionText;
@dynamic title;

@end
