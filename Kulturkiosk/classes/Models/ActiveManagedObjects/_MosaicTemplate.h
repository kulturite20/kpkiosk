//
//  MosaicTemplate.h
//  Kulturkiosk
//
//  Created by Dmitry on 2/13/17.
//
//

#import <CoreData/CoreData.h>

extern const struct MosaicTemplateAttributes {
    __unsafe_unretained NSString *width;
    __unsafe_unretained NSString *height;
    __unsafe_unretained NSString *name;
} MosaicTemplateAttributes;

extern const struct MosaicTemplateRelationships {
    __unsafe_unretained NSString *elementSize;
} MosaicTemplateRelationships;

extern const struct MosaicTemplateFetchedProperties {
} MosaicTemplateFetchedProperties;

@class MosaicElementSize;

@interface MosaicTemplateID : NSManagedObjectID {}
@end


@interface _MosaicTemplate : NSManagedObject

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (MosaicTemplateID*)objectID;


@property (nonatomic, strong) NSNumber* width;

@property int32_t widthValue;
- (int32_t)widthValue;
- (void)setWidthValue:(int32_t)value_;

@property (nonatomic, strong) NSNumber* height;


@property int32_t heightValue;
- (int32_t)heightValue;
- (void)setHeightValue:(int32_t)value_;


@property (nonatomic, strong) NSString* name;

@property (nonatomic, strong) NSSet *mosaicElementSize;

- (NSMutableSet*)mosaicElementSizeSet;

@end



@interface _MosaicTemplate (CoreDataGeneratedAccessors)

- (void)addElementSize:(NSSet*)value_;
- (void)removeElementSize:(NSSet*)value_;
- (void)addElementSizeObject:(MosaicElementSize*)value_;
- (void)removeElementSizeObject:(MosaicElementSize*)value_;

@end

@interface _MosaicTemplate (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitiveWidth;
- (void)setPrimitiveWidth:(NSNumber*)value;

- (int32_t)primitiveWidthValue;
- (void)setPrimitiveWidthValue:(int32_t)value_;

- (NSNumber*)primitiveHeight;
- (void)setPrimitiveHeight:(NSNumber*)value;

- (int32_t)primitiveHeightValue;
- (void)setPrimitiveHeightValue:(int32_t)value_;

- (NSMutableSet*)primitiveElementSize;
- (void)setPrimitiveElementSize:(NSMutableSet*)value;

@end

