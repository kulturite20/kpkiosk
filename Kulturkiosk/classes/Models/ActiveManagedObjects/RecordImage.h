#import "_RecordImage.h"

@interface RecordImage : _RecordImage {}
// Custom logic goes here.
- (UIImage *)thumbnailImage;
@end
