//
//  MosaicItemTextDescription+CoreDataProperties.h
//  
//
//  Created by Vadim Osovets on 2/24/17.
//
//

#import "MosaicItemTextDescription+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MosaicItemTextDescription (CoreDataProperties)

+ (NSFetchRequest<MosaicItemTextDescription *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *alignment;
@property (nullable, nonatomic, copy) NSString *color;
@property (nullable, nonatomic, copy) NSString *content;
@property (nullable, nonatomic, copy) NSString *font;
@property (nullable, nonatomic, copy) NSString *font_size;
@property (nullable, nonatomic, copy) NSString *placement;
@property (nullable, nonatomic, copy) NSNumber *position;

@end

NS_ASSUME_NONNULL_END
