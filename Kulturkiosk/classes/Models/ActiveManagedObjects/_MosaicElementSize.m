//
//  MosaicElementSize.m
//  Kulturkiosk
//
//  Created by Dmitry on 2/13/17.
//
//

#import "_MosaicElementSize.h"

const struct MosaicElementSizeAttributes MosaicElementSizeAttributes = {
    .identifier = @"identifier",
    .x = @"x",
    .y = @"y"
};

const struct MosaicElementSizeRelationships MosaicElementSizeRelationships = {
};

const struct MosaicElementSizeFetchedProperties MosaicElementSizeFetchedProperties = {
};

@implementation MosaicElementSizeID
@end

@implementation _MosaicElementSize

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
    NSParameterAssert(moc_);
    return [NSEntityDescription insertNewObjectForEntityForName:@"MosaicElementSize" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
    return @"MosaicElementSize";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
    NSParameterAssert(moc_);
    return [NSEntityDescription entityForName:@"MosaicElementSize" inManagedObjectContext:moc_];
}

- (RecordID*)objectID {
    return (RecordID*)[super objectID];
}


+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
    NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
    
    if ([key isEqualToString:@"identifierValue"]) {
        NSSet *affectingKey = [NSSet setWithObject:@"identifier"];
        keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
        return keyPaths;
    }
    
    return keyPaths;
}

@dynamic identifier;



- (int16_t)identifierValue {
    NSNumber *result = [self identifier];
    return [result shortValue];
}

- (void)setIdentifierValue:(int16_t)value_ {
    [self setIdentifier:[NSNumber numberWithShort:value_]];
}



@dynamic x;

- (int32_t)xValue{
    NSNumber *result = [self x];
    return [result intValue];
}

- (void)setXValue:(int32_t)value_ {
    [self setX:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveXValue {
    NSNumber *result = [self primitiveX];
    return [result intValue];
}

- (void)setPrimitiveXValue:(int32_t)value_ {
    [self setPrimitiveX:[NSNumber numberWithInt:value_]];
}

@dynamic y;

- (int32_t)yValue{
    NSNumber *result = [self y];
    return [result intValue];
}

- (void)setYValue:(int32_t)value_ {
    [self setY:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveYValue {
    NSNumber *result = [self primitiveY];
    return [result intValue];
}

- (void)setPrimitiveYValue:(int32_t)value_ {
    [self setPrimitiveY:[NSNumber numberWithInt:value_]];
}



@end



