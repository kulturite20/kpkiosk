//
//  MosaicItemText.h
//  Kulturkiosk
//
//  Created by Dmitry on 2/14/17.
//
//

#import <CoreData/CoreData.h>

extern const struct MosaicItemTextAttributes {
} MosaicItemTextAttributes;

extern const struct MosaicItemTextRelationships {
    __unsafe_unretained NSString *title;
    __unsafe_unretained NSString *description;
} MosaicItemTextRelationships;

extern const struct MosaicItemTextFetchedProperties {
} MosaicItemTextFetchedProperties;

@class MosaicItemTextTitle;
@class MosaicItemTextDescription;

@interface MosaicItemTextID : NSManagedObjectID {}
@end

@interface _MosaicItemText : NSManagedObject

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (MosaicItemTextID*)objectID;

@property (nonatomic, strong) MosaicItemTextTitle *title;

@property (nonatomic, strong) MosaicItemTextDescription *description;

@end

@interface _MosaicItemText (CoreDataGeneratedAccessors)

@end

@interface _MosaicItemText (CoreDataGeneratedPrimitiveAccessors)

- (MosaicItemTextTitle*)primitiveItemTitle;
- (void)setPrimitiveItemTitle:(MosaicItemTextTitle*)value;

- (MosaicItemTextDescription*)primitiveItemDecription;
- (void)setPrimitiveItemDecription:(MosaicItemTextDescription*)value;

@end

