//
//  MosaicItemTextTitle+CoreDataProperties.h
//  
//
//  Created by Vadim Osovets on 2/24/17.
//
//

#import "MosaicItemTextTitle+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MosaicItemTextTitle (CoreDataProperties)

+ (NSFetchRequest<MosaicItemTextTitle *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *alignment;
@property (nullable, nonatomic, copy) NSString *color;
@property (nullable, nonatomic, copy) NSString *content;
@property (nullable, nonatomic, copy) NSString *font;
@property (nullable, nonatomic, copy) NSString *placement;
@property (nullable, nonatomic, copy) NSNumber *position;
@property (nullable, nonatomic, copy) NSString *size;

@end

NS_ASSUME_NONNULL_END
