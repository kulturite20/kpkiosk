// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Credit.m instead.

#import "_Credit.h"

const struct CreditAttributes CreditAttributes = {
	.credit_type = @"credit_type",
	.identifier = @"identifier",
	.name = @"name",
	.parent_id = @"parent_id",
};

const struct CreditRelationships CreditRelationships = {
	.file = @"file",
	.record_image = @"record_image",
};

const struct CreditFetchedProperties CreditFetchedProperties = {
};

@implementation CreditID
@end

@implementation _Credit

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Credit" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Credit";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Credit" inManagedObjectContext:moc_];
}

- (CreditID*)objectID {
	return (CreditID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"identifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"identifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic credit_type;






@dynamic identifier;



- (int32_t)identifierValue {
	NSNumber *result = [self identifier];
	return [result intValue];
}

- (void)setIdentifierValue:(int32_t)value_ {
	[self setIdentifier:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveIdentifierValue {
	NSNumber *result = [self primitiveIdentifier];
	return [result intValue];
}

- (void)setPrimitiveIdentifierValue:(int32_t)value_ {
	[self setPrimitiveIdentifier:[NSNumber numberWithInt:value_]];
}





@dynamic name;






@dynamic parent_id;






@dynamic file;

	

@dynamic record_image;

	






@end
