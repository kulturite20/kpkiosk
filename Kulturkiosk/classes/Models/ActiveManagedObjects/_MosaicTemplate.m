//
//  MosaicTemplate.m
//  Kulturkiosk
//
//  Created by Dmitry on 2/13/17.
//
//

#import "_MosaicTemplate.h"

const struct MosaicTemplateAttributes MosaicTemplateAttributes = {
    .width = @"width",
    .height = @"height",
    .name = @"name"
};

const struct MosaicTemplateRelationships MosaicTemplateRelationships = {
    .elementSize = @"elementSize"
};

const struct MosaicTemplateFetchedProperties MosaicTemplateFetchedProperties = {
};

@implementation MosaicTemplateID
@end



@implementation _MosaicTemplate

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
    NSParameterAssert(moc_);
    return [NSEntityDescription insertNewObjectForEntityForName:@"MosaicTemplate" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
    return @"MosaicTemplate";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
    NSParameterAssert(moc_);
    return [NSEntityDescription entityForName:@"MosaicTemplate" inManagedObjectContext:moc_];
}

- (MosaicTemplateID*)objectID {
    return (MosaicTemplateID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
    NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
    
    return keyPaths;
}


@dynamic width;

- (int32_t)widthValue{
    NSNumber *result = [self width];
    return [result intValue];
}

- (void)setWidthValue:(int32_t)value_ {
    [self setWidth:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveWidthValue {
    NSNumber *result = [self primitiveWidth];
    return [result intValue];
}

- (void)setPrimitiveWidthValue:(int32_t)value_ {
    [self setPrimitiveWidth:[NSNumber numberWithInt:value_]];
}

@dynamic height;

- (int32_t)heightValue{
    NSNumber *result = [self height];
    return [result intValue];
}

- (void)setHeightValue:(int32_t)value_ {
    [self setHeight:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveHeightValue {
    NSNumber *result = [self primitiveHeight];
    return [result intValue];
}

- (void)setPrimitiveHeightValue:(int32_t)value_ {
    [self setPrimitiveHeight:[NSNumber numberWithInt:value_]];
}

@dynamic name;

@dynamic mosaicElementSize;

- (NSMutableSet*)mosaicElementSizeSet {
    [self willAccessValueForKey:@"mosaicElementSizeSet"];
    
    NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"mosaicElementSizeSet"];
    
    [self didAccessValueForKey:@"mosaicElementSizeSet"];
    return result;
}




@end
