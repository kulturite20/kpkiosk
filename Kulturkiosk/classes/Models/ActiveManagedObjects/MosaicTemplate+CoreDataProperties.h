//
//  MosaicTemplate+CoreDataProperties.h
//  
//
//  Created by Vadim Osovets on 2/24/17.
//
//

#import "MosaicTemplate+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MosaicTemplate (CoreDataProperties)

+ (NSFetchRequest<MosaicTemplate *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *height;
@property (nullable, nonatomic, copy) NSNumber *identifier;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSNumber *width;
@property (nullable, nonatomic, retain) NSSet<MosaicElementSize *> *elementSizes;

@end

@interface MosaicTemplate (CoreDataGeneratedAccessors)

- (void)addElementSizesObject:(MosaicElementSize *)value;
- (void)removeElementSizesObject:(MosaicElementSize *)value;
- (void)addElementSizes:(NSSet<MosaicElementSize *> *)values;
- (void)removeElementSizes:(NSSet<MosaicElementSize *> *)values;

@end

NS_ASSUME_NONNULL_END
