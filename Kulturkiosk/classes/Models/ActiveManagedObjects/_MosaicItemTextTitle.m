//
//  _MosaicItemTextTitle.m
//  Kulturkiosk
//
//  Created by Dmitry on 2/14/17.
//
//

#import "_MosaicItemTextTitle.h"

const struct MosaicItemTextTitleAttributes MosaicItemTextTitleAttributes = {
    .size = @"size",
    .position = @"position",
    .placement = @"placement",
    .font = @"font",
    .color = @"color",
    .alignment = @"alignment",
};

const struct MosaicItemTextTitleRelationships MosaicItemTextTitleRelationships = {
};

const struct MosaicItemTextTitleFetchedProperties MosaicItemTextTitleFetchedProperties = {
};

@implementation MosaicItemTextTitleID
@end

@implementation _MosaicItemTextTitle

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
    NSParameterAssert(moc_);
    return [NSEntityDescription insertNewObjectForEntityForName:@"MosaicItemTextTitle" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
    return @"MosaicItemTextTitle";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
    NSParameterAssert(moc_);
    return [NSEntityDescription entityForName:@"MosaicItemTextTitle" inManagedObjectContext:moc_];
}

- (MosaicItemTextTitleID*)objectID {
    return (MosaicItemTextTitleID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
    NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
    if ([key isEqualToString:@"positionValue"]) {
        NSSet *affectingKey = [NSSet setWithObject:@"position"];
        keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
        return keyPaths;
    }
    
    return keyPaths;
}

@dynamic size;

@dynamic placement;

@dynamic alignment;

@dynamic font;

@dynamic color;

@dynamic position;

- (int32_t)positionValue{
    NSNumber *result = [self position];
    return [result intValue];
}

- (void)setPositionValue:(int32_t)value_ {
    [self setPosition:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitivePositionValue {
    NSNumber *result = [self primitivePosition];
    return [result intValue];
}

- (void)setPrimitivePositionValue:(int32_t)value_ {
    [self setPrimitivePosition:[NSNumber numberWithInt:value_]];
}


@end
