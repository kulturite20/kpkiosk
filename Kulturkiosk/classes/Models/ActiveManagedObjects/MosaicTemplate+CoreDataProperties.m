//
//  MosaicTemplate+CoreDataProperties.m
//  
//
//  Created by Vadim Osovets on 2/24/17.
//
//

#import "MosaicTemplate+CoreDataProperties.h"

@implementation MosaicTemplate (CoreDataProperties)

+ (NSFetchRequest<MosaicTemplate *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MosaicTemplate"];
}

@dynamic height;
@dynamic identifier;
@dynamic name;
@dynamic width;
@dynamic elementSizes;

@end
