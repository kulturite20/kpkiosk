// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Record.m instead.

#import "_Record.h"

const struct RecordAttributes RecordAttributes = {
	.identifier = @"identifier",
	.title = @"title",
	.type = @"type",
};

const struct RecordRelationships RecordRelationships = {
	.contents = @"contents",
	.image = @"image",
};

const struct RecordFetchedProperties RecordFetchedProperties = {
};

@implementation RecordID
@end

@implementation _Record

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Record" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Record";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Record" inManagedObjectContext:moc_];
}

- (RecordID*)objectID {
	return (RecordID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"identifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"identifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic identifier;



- (int16_t)identifierValue {
	NSNumber *result = [self identifier];
	return [result shortValue];
}

- (void)setIdentifierValue:(int16_t)value_ {
	[self setIdentifier:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveIdentifierValue {
	NSNumber *result = [self primitiveIdentifier];
	return [result shortValue];
}

- (void)setPrimitiveIdentifierValue:(int16_t)value_ {
	[self setPrimitiveIdentifier:[NSNumber numberWithShort:value_]];
}





@dynamic title;






@dynamic type;






@dynamic contents;

	
- (NSMutableSet*)contentsSet {
	[self willAccessValueForKey:@"contents"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"contents"];
  
	[self didAccessValueForKey:@"contents"];
	return result;
}
	

@dynamic image;

	






@end
