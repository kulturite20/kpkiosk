#import "_Page.h"
#import "VideoBlock.h"
#import "AudioBlock.h"
#import "TextBlock.h"

@interface Page : _Page {}
// Custom logic goes here.

- (NSArray *)videoBlocks;
- (NSArray *)audioBlocks;
- (NSArray *)textBlocks;

@end
