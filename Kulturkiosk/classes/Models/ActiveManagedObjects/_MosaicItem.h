//
//  _MosaicItem.h
//  Kulturkiosk
//
//  Created by Dmitry on 2/14/17.
//
//

#import <CoreData/CoreData.h>

extern const struct MosaicItemAttributes {
    __unsafe_unretained NSString *identifier;
    __unsafe_unretained NSString *u_id;
    __unsafe_unretained NSString *type;
    __unsafe_unretained NSString *presentationID;
    __unsafe_unretained NSString *position;
    __unsafe_unretained NSString *imageUrl;
} MosaicItemAttributes;

extern const struct MosaicItemRelationships {
    __unsafe_unretained NSString *mosaicPresentation;
    __unsafe_unretained NSString *text;

} MosaicItemRelationships;

extern const struct MosaicItemFetchedProperties {
} MosaicItemFetchedProperties;

@class MosaicPresentation;
@class MosaicItemText;

@interface MosaicItemID : NSManagedObjectID {}
@end

@interface _MosaicItem : NSManagedObject

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (MosaicItemID*)objectID;

@property (nonatomic, strong) NSNumber* identifier;

@property int16_t identifierValue;
- (int16_t)identifierValue;
- (void)setIdentifierValue:(int16_t)value_;

@property (nonatomic, strong) NSString* type;

@property (nonatomic, strong) NSNumber* u_id;

@property int32_t u_idValue;
- (int32_t)u_idValue;
- (void)setU_idValue:(int32_t)value_;

@property (nonatomic, strong) NSNumber* presentationID;

@property int16_t presentationIDValue;
- (int16_t)presentationIDValue;
- (void)setPresentationIDValue:(int16_t)value_;

@property (nonatomic, strong) NSNumber* position;

@property int16_t positionValue;
- (int16_t)positionValue;
- (void)setPositionValue:(int16_t)value_;

@property (nonatomic, strong) NSString* imageUrl;

@property (nonatomic, strong) NSSet *mosaicPresentation;

- (NSMutableSet*)mosaicPresentationSet;

@property (nonatomic, strong) NSSet *text;

- (NSMutableSet*)textSet;

@end

@interface _MosaicItem (CoreDataGeneratedAccessors)

- (void)addPresentation:(NSSet*)value_;
- (void)removePresentation:(NSSet*)value_;
- (void)addPresentationObject:(MosaicPresentation*)value_;
- (void)removePresentationObject:(MosaicPresentation*)value_;

- (void)addItemText:(NSSet*)value_;
- (void)removeItemText:(NSSet*)value_;
- (void)addItemTextObject:(MosaicItemText*)value_;
- (void)removeItemTextObject:(MosaicItemText*)value_;

@end

@interface _MosaicItem (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSNumber*)value;

- (int16_t)primitiveIdentifierValue;
- (void)setPrimitiveIdentifierValue:(int16_t)value_;


- (NSString*)primitiveType;
- (void)setPrimitiveType:(NSString*)value;


- (NSNumber*)primitiveU_id;
- (void)setPrimitiveU_id:(NSNumber*)value;

- (int32_t)primitiveU_idValue;
- (void)setPrimitiveU_idValue:(int32_t)value_;

- (NSNumber*)primitivePresentationID;
- (void)setPrimitivePresentationID:(NSNumber*)value;

- (int16_t)primitivePresentationIDValue;
- (void)setPrimitivePresentationIDValue:(int16_t)value_;

- (NSNumber*)primitivePosition;
- (void)setPrimitivePosition:(NSNumber*)value;

- (int16_t)primitivePositionValue;
- (void)setPrimitivePositionValue:(int16_t)value_;

- (NSString*)primitiveImageUrl;
- (void)setPrimitiveImageUrl:(NSString*)value;

- (NSMutableSet*)primitivePresentation;
- (void)setPrimitivePresentation:(NSMutableSet*)value;

- (NSMutableSet*)primitiveItemText;
- (void)setPrimitiveItemText:(NSMutableSet*)value;


@end
