// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to RecordImage.h instead.

#import <CoreData/CoreData.h>


extern const struct RecordImageAttributes {
	__unsafe_unretained NSString *identifier;
	__unsafe_unretained NSString *name;
} RecordImageAttributes;

extern const struct RecordImageRelationships {
	__unsafe_unretained NSString *credits;
	__unsafe_unretained NSString *links;
	__unsafe_unretained NSString *record;
} RecordImageRelationships;

extern const struct RecordImageFetchedProperties {
} RecordImageFetchedProperties;

@class Credit;
@class Link;
@class Record;




@interface RecordImageID : NSManagedObjectID {}
@end

@interface _RecordImage : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (RecordImageID*)objectID;





@property (nonatomic, strong) NSNumber* identifier;



@property int32_t identifierValue;
- (int32_t)identifierValue;
- (void)setIdentifierValue:(int32_t)value_;

//- (BOOL)validateIdentifier:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *credits;

- (NSMutableSet*)creditsSet;




@property (nonatomic, strong) NSSet *links;

- (NSMutableSet*)linksSet;




@property (nonatomic, strong) NSSet *record;

- (NSMutableSet*)recordSet;





@end

@interface _RecordImage (CoreDataGeneratedAccessors)

- (void)addCredits:(NSSet*)value_;
- (void)removeCredits:(NSSet*)value_;
- (void)addCreditsObject:(Credit*)value_;
- (void)removeCreditsObject:(Credit*)value_;

- (void)addLinks:(NSSet*)value_;
- (void)removeLinks:(NSSet*)value_;
- (void)addLinksObject:(Link*)value_;
- (void)removeLinksObject:(Link*)value_;

- (void)addRecord:(NSSet*)value_;
- (void)removeRecord:(NSSet*)value_;
- (void)addRecordObject:(Record*)value_;
- (void)removeRecordObject:(Record*)value_;

@end

@interface _RecordImage (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSNumber*)value;

- (int32_t)primitiveIdentifierValue;
- (void)setPrimitiveIdentifierValue:(int32_t)value_;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;





- (NSMutableSet*)primitiveCredits;
- (void)setPrimitiveCredits:(NSMutableSet*)value;



- (NSMutableSet*)primitiveLinks;
- (void)setPrimitiveLinks:(NSMutableSet*)value;



- (NSMutableSet*)primitiveRecord;
- (void)setPrimitiveRecord:(NSMutableSet*)value;


@end
