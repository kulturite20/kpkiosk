// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Content.h instead.

#import <CoreData/CoreData.h>
#import "MosaicPresentation+CoreDataClass.h"

extern const struct ContentAttributes {
	__unsafe_unretained NSString *desc;
	__unsafe_unretained NSString *language;
	__unsafe_unretained NSString *parent_id;
	__unsafe_unretained NSString *title;
} ContentAttributes;

extern const struct ContentRelationships {
	__unsafe_unretained NSString *file;
	__unsafe_unretained NSString *pages;
	__unsafe_unretained NSString *presentation;
	__unsafe_unretained NSString *record;
    __unsafe_unretained NSString *mosaicPresentation;
} ContentRelationships;

extern const struct ContentFetchedProperties {
} ContentFetchedProperties;

@class File;
@class Page;
@class Presentation;
@class Record;
@class MosaicPresentation;


@interface ContentID : NSManagedObjectID {}
@end

@interface _Content : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ContentID*)objectID;





@property (nonatomic, strong) NSString* desc;



//- (BOOL)validateDesc:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* language;



//- (BOOL)validateLanguage:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* parent_id;



//- (BOOL)validateParent_id:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* title;



//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *file;

- (NSMutableSet*)fileSet;




@property (nonatomic, strong) NSSet *pages;

- (NSMutableSet*)pagesSet;




@property (nonatomic, strong) NSSet *presentation;

- (NSMutableSet*)presentationSet;



@property (nonatomic, strong) NSSet *mosaicPresentation;

- (NSMutableSet*)mosaicPresentationSet;




@property (nonatomic, strong) Record *record;

//- (BOOL)validateRecord:(id*)value_ error:(NSError**)error_;





@end

@interface _Content (CoreDataGeneratedAccessors)

- (void)addFile:(NSSet*)value_;
- (void)removeFile:(NSSet*)value_;
- (void)addFileObject:(File*)value_;
- (void)removeFileObject:(File*)value_;

- (void)addPages:(NSSet*)value_;
- (void)removePages:(NSSet*)value_;
- (void)addPagesObject:(Page*)value_;
- (void)removePagesObject:(Page*)value_;

- (void)addPresentation:(NSSet*)value_;
- (void)removePresentation:(NSSet*)value_;
- (void)addPresentationObject:(Presentation*)value_;
- (void)removePresentationObject:(Presentation*)value_;

- (void)addMosaicPresentation:(NSSet*)value_;
- (void)removeMosaicPresentation:(NSSet*)value_;
- (void)addMosaicPresentationObject:(MosaicPresentation*)value_;
- (void)removeMosaicPresentationObject:(MosaicPresentation*)value_;

@end

@interface _Content (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveDesc;
- (void)setPrimitiveDesc:(NSString*)value;




- (NSString*)primitiveLanguage;
- (void)setPrimitiveLanguage:(NSString*)value;




- (NSString*)primitiveParent_id;
- (void)setPrimitiveParent_id:(NSString*)value;




- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;





- (NSMutableSet*)primitiveFile;
- (void)setPrimitiveFile:(NSMutableSet*)value;



- (NSMutableSet*)primitivePages;
- (void)setPrimitivePages:(NSMutableSet*)value;



- (NSMutableSet*)primitiveMosaicPresentation;
- (void)setPrimitiveMosaicPresentation:(NSMutableSet*)value;


- (NSMutableSet*)primitivePresentation;
- (void)setPrimitivePresentation:(NSMutableSet*)value;



- (Record*)primitiveRecord;
- (void)setPrimitiveRecord:(Record*)value;


@end
