//
//  MosaicElementSize+CoreDataProperties.h
//  
//
//  Created by Vadim Osovets on 2/24/17.
//
//

#import "MosaicElementSize+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MosaicElementSize (CoreDataProperties)

+ (NSFetchRequest<MosaicElementSize *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *position;
@property (nullable, nonatomic, copy) NSNumber *x;
@property (nullable, nonatomic, copy) NSNumber *y;

@end

NS_ASSUME_NONNULL_END
