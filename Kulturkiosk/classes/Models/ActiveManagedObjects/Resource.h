#import "_Resource.h"

@interface Resource : _Resource {}

// Custom logic goes here.
- (NSString *)pathToOriginalItem;
- (NSString *)pathToMainItem;
- (NSString *)pathToThumbnailItem;

@end
