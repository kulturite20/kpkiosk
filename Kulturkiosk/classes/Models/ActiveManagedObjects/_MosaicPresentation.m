//
//  _MosaicPresentation.m
//  Kulturkiosk
//
//  Created by Dmitry on 2/14/17.
//
//

#import "_MosaicPresentation.h"

const struct MosaicPresentationAttributes MosaicPresentationAttributes = {
    .identifier = @"identifier",
    .type = @"type",
    .updated_at = @"updated_at"
};

const struct MosaicPresentationRelationships MosaicPresentationRelationships = {
    .mosaic_items = @"mosaic_items",
    .contents = @"contents",
    .presentation_template = @"presentation_template",
    .resources = @"resources"
};

const struct MosaicPresentationFetchedProperties MosaicPresentationFetchedProperties = {
};


@implementation _MosaicPresentation

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
    NSParameterAssert(moc_);
    return [NSEntityDescription insertNewObjectForEntityForName:@"MosaicPresentation" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
    return @"MosaicPresentation";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
    NSParameterAssert(moc_);
    return [NSEntityDescription entityForName:@"MosaicPresentation" inManagedObjectContext:moc_];
}

- (MosaicPresentationID*)objectID {
    return (MosaicPresentationID*)[super objectID];
}


+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
    NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
    
    if ([key isEqualToString:@"identifierValue"]) {
        NSSet *affectingKey = [NSSet setWithObject:@"identifier"];
        keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
        return keyPaths;
    }
    
    
    return keyPaths;
}

@dynamic identifier;



- (int16_t)identifierValue {
    NSNumber *result = [self identifier];
    return [result shortValue];
}

- (void)setIdentifierValue:(int16_t)value_ {
    [self setIdentifier:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveIdentifierValue {
    NSNumber *result = [self primitiveIdentifier];
    return [result shortValue];
}

- (void)setPrimitiveIdentifierValue:(int16_t)value_ {
    [self setPrimitiveIdentifier:[NSNumber numberWithShort:value_]];
}

@dynamic type;

@dynamic updated_at;

@dynamic mosaic_items;

- (NSMutableSet*)mosaic_itemsSet {
    [self willAccessValueForKey:@"mosaic_items"];
    
    NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"mosaic_items"];
    
    [self didAccessValueForKey:@"mosaic_items"];
    return result;
}

@dynamic contents;

- (NSMutableSet*)mosaicContentSet {
    [self willAccessValueForKey:@"Content"];
    
    NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"Content"];
    
    [self didAccessValueForKey:@"Content"];
    return result;
}


@dynamic presentation_template;

- (NSMutableSet*)mosaicPresentationTemplateSet {
    [self willAccessValueForKey:@"PresentationTemplate"];
    
    NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"PresentationTemplate"];
    
    [self didAccessValueForKey:@"PresentationTemplate"];
    return result;
}

@dynamic resources;

- (NSMutableSet*)mosaicResourcesSet {
    [self willAccessValueForKey:@"Resources"];
    
    NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"Resources"];
    
    [self didAccessValueForKey:@"Resources"];
    return result;
}

@end
