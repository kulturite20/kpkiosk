//
//  MosaicItemTextTitle+CoreDataClass.h
//  
//
//  Created by Vadim Osovets on 2/24/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface MosaicItemTextTitle : NSManagedObject

- (UIFont *)fontWithSize;
- (NSInteger)textAlignment;

@end

NS_ASSUME_NONNULL_END

#import "MosaicItemTextTitle+CoreDataProperties.h"
