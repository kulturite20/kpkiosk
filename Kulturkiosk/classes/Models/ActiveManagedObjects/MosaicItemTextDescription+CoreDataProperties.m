//
//  MosaicItemTextDescription+CoreDataProperties.m
//  
//
//  Created by Vadim Osovets on 2/24/17.
//
//

#import "MosaicItemTextDescription+CoreDataProperties.h"

@implementation MosaicItemTextDescription (CoreDataProperties)

+ (NSFetchRequest<MosaicItemTextDescription *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MosaicItemTextDescription"];
}

@dynamic alignment;
@dynamic color;
@dynamic content;
@dynamic font;
@dynamic font_size;
@dynamic placement;
@dynamic position;

@end
