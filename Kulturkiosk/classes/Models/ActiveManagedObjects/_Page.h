// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Page.h instead.

#import <CoreData/CoreData.h>


extern const struct PageAttributes {
	__unsafe_unretained NSString *identifier;
	__unsafe_unretained NSString *position;
	__unsafe_unretained NSString *title;
} PageAttributes;

extern const struct PageRelationships {
	__unsafe_unretained NSString *blocks;
	__unsafe_unretained NSString *content;
} PageRelationships;

extern const struct PageFetchedProperties {
} PageFetchedProperties;

@class Block;
@class Content;





@interface PageID : NSManagedObjectID {}
@end

@interface _Page : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (PageID*)objectID;





@property (nonatomic, strong) NSNumber* identifier;



@property int16_t identifierValue;
- (int16_t)identifierValue;
- (void)setIdentifierValue:(int16_t)value_;

//- (BOOL)validateIdentifier:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* position;



@property int32_t positionValue;
- (int32_t)positionValue;
- (void)setPositionValue:(int32_t)value_;

//- (BOOL)validatePosition:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* title;



//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *blocks;

- (NSMutableSet*)blocksSet;




@property (nonatomic, strong) Content *content;

//- (BOOL)validateContent:(id*)value_ error:(NSError**)error_;





@end

@interface _Page (CoreDataGeneratedAccessors)

- (void)addBlocks:(NSSet*)value_;
- (void)removeBlocks:(NSSet*)value_;
- (void)addBlocksObject:(Block*)value_;
- (void)removeBlocksObject:(Block*)value_;

@end

@interface _Page (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSNumber*)value;

- (int16_t)primitiveIdentifierValue;
- (void)setPrimitiveIdentifierValue:(int16_t)value_;




- (NSNumber*)primitivePosition;
- (void)setPrimitivePosition:(NSNumber*)value;

- (int32_t)primitivePositionValue;
- (void)setPrimitivePositionValue:(int32_t)value_;




- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;





- (NSMutableSet*)primitiveBlocks;
- (void)setPrimitiveBlocks:(NSMutableSet*)value;



- (Content*)primitiveContent;
- (void)setPrimitiveContent:(Content*)value;


@end
