//
//  MosaicTemplate+CoreDataClass.h
//  
//
//  Created by Vadim Osovets on 2/24/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MosaicElementSize;

NS_ASSUME_NONNULL_BEGIN

@interface MosaicTemplate : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "MosaicTemplate+CoreDataProperties.h"
