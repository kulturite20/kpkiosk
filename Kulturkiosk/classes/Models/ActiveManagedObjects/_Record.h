// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Record.h instead.

#import <CoreData/CoreData.h>


extern const struct RecordAttributes {
	__unsafe_unretained NSString *identifier;
	__unsafe_unretained NSString *title;
	__unsafe_unretained NSString *type;
} RecordAttributes;

extern const struct RecordRelationships {
	__unsafe_unretained NSString *contents;
	__unsafe_unretained NSString *image;
} RecordRelationships;

extern const struct RecordFetchedProperties {
} RecordFetchedProperties;

@class Content;
@class RecordImage;





@interface RecordID : NSManagedObjectID {}
@end

@interface _Record : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (RecordID*)objectID;





@property (nonatomic, strong) NSNumber* identifier;



@property int16_t identifierValue;
- (int16_t)identifierValue;
- (void)setIdentifierValue:(int16_t)value_;

//- (BOOL)validateIdentifier:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* title;



//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* type;



//- (BOOL)validateType:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *contents;

- (NSMutableSet*)contentsSet;




@property (nonatomic, strong) RecordImage *image;

//- (BOOL)validateImage:(id*)value_ error:(NSError**)error_;





@end

@interface _Record (CoreDataGeneratedAccessors)

- (void)addContents:(NSSet*)value_;
- (void)removeContents:(NSSet*)value_;
- (void)addContentsObject:(Content*)value_;
- (void)removeContentsObject:(Content*)value_;

@end

@interface _Record (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSNumber*)value;

- (int16_t)primitiveIdentifierValue;
- (void)setPrimitiveIdentifierValue:(int16_t)value_;




- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;




- (NSString*)primitiveType;
- (void)setPrimitiveType:(NSString*)value;





- (NSMutableSet*)primitiveContents;
- (void)setPrimitiveContents:(NSMutableSet*)value;



- (RecordImage*)primitiveImage;
- (void)setPrimitiveImage:(RecordImage*)value;


@end
