//
//  MosaicItemTextTitle+CoreDataClass.m
//  
//
//  Created by Vadim Osovets on 2/24/17.
//
//

#import "MosaicItemTextTitle+CoreDataClass.h"

@implementation MosaicItemTextTitle

- (UIFont *)fontWithSize {
    
    NSLog(@"Current title font is - %@", self.font);
    
    //Default size
    int fontSize = 17;
    
    NSLog(@"Current title size is - %@", self.size);
    
    //Change defailt size in case we have one from server
    if ([self.size isEqualToString:@"small"]) {
        fontSize = 13;
    } else if ([self.size isEqualToString:@"medium"]) {
        fontSize = 17;
    } else if ([self.size isEqualToString:@"large"]) {
        fontSize = 27;
    }
    
    NSLog(@"Current title font size is - %d", fontSize);
    
    if (self.font == nil) {
        return [UIFont fontWithName:ApplicationFont size:fontSize];
    }
    
    return [UIFont fontWithName:self.font size:fontSize];
}

- (NSInteger)textAlignment {
    if ([self.alignment  isEqual: @"left"]) {
        return NSTextAlignmentLeft;
    } else if ([self.alignment  isEqual: @"right"]) {
        return NSTextAlignmentRight;
    } else if ([self.alignment  isEqual: @"center"]) {
        return NSTextAlignmentCenter;
    }
    return NSTextAlignmentLeft;
}

@end
