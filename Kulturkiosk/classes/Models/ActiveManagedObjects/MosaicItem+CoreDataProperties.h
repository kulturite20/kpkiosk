//
//  MosaicItem+CoreDataProperties.h
//  
//
//  Created by Vadim Osovets on 2/24/17.
//
//

#import "MosaicItem+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MosaicItem (CoreDataProperties)

+ (NSFetchRequest<MosaicItem *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *identifier;
@property (nullable, nonatomic, copy) NSString *imageUrl;
@property (nullable, nonatomic, copy) NSNumber *position;
@property (nullable, nonatomic, copy) NSNumber *presentationID;
@property (nullable, nonatomic, copy) NSNumber *record_id;
@property (nullable, nonatomic, copy) NSString *type;
@property (nullable, nonatomic, copy) NSNumber *u_id;
@property (nullable, nonatomic, retain) MosaicPresentation *presentation;
@property (nullable, nonatomic, retain) NSSet<MosaicItemText *> *text;

@end

@interface MosaicItem (CoreDataGeneratedAccessors)

- (void)addTextObject:(MosaicItemText *)value;
- (void)removeTextObject:(MosaicItemText *)value;
- (void)addText:(NSSet<MosaicItemText *> *)values;
- (void)removeText:(NSSet<MosaicItemText *> *)values;

@end

NS_ASSUME_NONNULL_END
