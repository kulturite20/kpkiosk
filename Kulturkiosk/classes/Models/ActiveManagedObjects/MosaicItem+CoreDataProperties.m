//
//  MosaicItem+CoreDataProperties.m
//  
//
//  Created by Vadim Osovets on 2/24/17.
//
//

#import "MosaicItem+CoreDataProperties.h"

@implementation MosaicItem (CoreDataProperties)

+ (NSFetchRequest<MosaicItem *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MosaicItem"];
}

@dynamic identifier;
@dynamic imageUrl;
@dynamic position;
@dynamic presentationID;
@dynamic record_id;
@dynamic type;
@dynamic u_id;
@dynamic presentation;
@dynamic text;

@end
