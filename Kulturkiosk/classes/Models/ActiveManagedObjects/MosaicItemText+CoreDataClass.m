//
//  MosaicItemText+CoreDataClass.m
//  
//
//  Created by Vadim Osovets on 2/24/17.
//
//

#import "MosaicItemText+CoreDataClass.h"
@implementation MosaicItemText

- (NSString *)contentForLanguage:(KKLanguageType)lang {
    NSString *language = nil;
    switch (lang) {
        case KKLanguageTypeEnglish: {
            language = @"en";
        }
            break;
        case KKLanguageTypeFrench: {
            language = @"fr";
        }
            break;
        case KKLanguageTypeGerman: {
            language = @"de";
        }
            break;
        case KKLanguageTypeSwedish: {
            language = @"sv";
        }
            break;
        case KKLanguageTypeSpanish: {
            language = @"es";
        }
            break;
        case KKLanguageTypeNorwegian: {
            language = @"no";
        }
            break;
            
        default:
            break;
    }
    
    return language;
}

@end
