//
//  MosaicItem+CoreDataClass.h
//  
//
//  Created by Vadim Osovets on 2/24/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MosaicItemText, MosaicPresentation;

NS_ASSUME_NONNULL_BEGIN

@interface MosaicItem : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "MosaicItem+CoreDataProperties.h"
