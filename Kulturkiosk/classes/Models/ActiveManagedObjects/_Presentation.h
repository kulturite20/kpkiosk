// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Presentation.h instead.

#import <CoreData/CoreData.h>


extern const struct PresentationAttributes {
	__unsafe_unretained NSString *identifier;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *position;
	__unsafe_unretained NSString *type;
} PresentationAttributes;

extern const struct PresentationRelationships {
	__unsafe_unretained NSString *contents;
	__unsafe_unretained NSString *device;
	__unsafe_unretained NSString *records_decriptions;
	__unsafe_unretained NSString *resources;
} PresentationRelationships;

extern const struct PresentationFetchedProperties {
} PresentationFetchedProperties;

@class Content;
@class Device;
@class RecordDescription;
@class Resource;






@interface PresentationID : NSManagedObjectID {}
@end

@interface _Presentation : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (PresentationID*)objectID;





@property (nonatomic, strong) NSNumber* identifier;



@property int16_t identifierValue;
- (int16_t)identifierValue;
- (void)setIdentifierValue:(int16_t)value_;

//- (BOOL)validateIdentifier:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* position;



@property int32_t positionValue;
- (int32_t)positionValue;
- (void)setPositionValue:(int32_t)value_;

//- (BOOL)validatePosition:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* type;



//- (BOOL)validateType:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *contents;

- (NSMutableSet*)contentsSet;




@property (nonatomic, strong) Device *device;

//- (BOOL)validateDevice:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSSet *records_decriptions;

- (NSMutableSet*)records_decriptionsSet;




@property (nonatomic, strong) NSSet *resources;

- (NSMutableSet*)resourcesSet;





@end

@interface _Presentation (CoreDataGeneratedAccessors)

- (void)addContents:(NSSet*)value_;
- (void)removeContents:(NSSet*)value_;
- (void)addContentsObject:(Content*)value_;
- (void)removeContentsObject:(Content*)value_;

- (void)addRecords_decriptions:(NSSet*)value_;
- (void)removeRecords_decriptions:(NSSet*)value_;
- (void)addRecords_decriptionsObject:(RecordDescription*)value_;
- (void)removeRecords_decriptionsObject:(RecordDescription*)value_;

- (void)addResources:(NSSet*)value_;
- (void)removeResources:(NSSet*)value_;
- (void)addResourcesObject:(Resource*)value_;
- (void)removeResourcesObject:(Resource*)value_;

@end

@interface _Presentation (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSNumber*)value;

- (int16_t)primitiveIdentifierValue;
- (void)setPrimitiveIdentifierValue:(int16_t)value_;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




- (NSNumber*)primitivePosition;
- (void)setPrimitivePosition:(NSNumber*)value;

- (int32_t)primitivePositionValue;
- (void)setPrimitivePositionValue:(int32_t)value_;




- (NSString*)primitiveType;
- (void)setPrimitiveType:(NSString*)value;





- (NSMutableSet*)primitiveContents;
- (void)setPrimitiveContents:(NSMutableSet*)value;



- (Device*)primitiveDevice;
- (void)setPrimitiveDevice:(Device*)value;



- (NSMutableSet*)primitiveRecords_decriptions;
- (void)setPrimitiveRecords_decriptions:(NSMutableSet*)value;



- (NSMutableSet*)primitiveResources;
- (void)setPrimitiveResources:(NSMutableSet*)value;


@end
