//
//  MosaicPresentation+CoreDataProperties.m
//  
//
//  Created by Vadim Osovets on 2/24/17.
//
//

#import "MosaicPresentation+CoreDataProperties.h"

@implementation MosaicPresentation (CoreDataProperties)

+ (NSFetchRequest<MosaicPresentation *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MosaicPresentation"];
}

@dynamic identifier;
@dynamic type;
@dynamic updated_at;
@dynamic contents;
@dynamic mosaic_items;
@dynamic presentation_template;
@dynamic resources;

@end
