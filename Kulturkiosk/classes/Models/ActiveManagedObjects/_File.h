// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to File.h instead.

#import <CoreData/CoreData.h>


extern const struct FileAttributes {
	__unsafe_unretained NSString *identifier;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *position;
	__unsafe_unretained NSString *type;
	__unsafe_unretained NSString *u_id;
} FileAttributes;

extern const struct FileRelationships {
	__unsafe_unretained NSString *audio_block;
	__unsafe_unretained NSString *contents;
	__unsafe_unretained NSString *credits;
	__unsafe_unretained NSString *links;
	__unsafe_unretained NSString *video_block;
} FileRelationships;

extern const struct FileFetchedProperties {
} FileFetchedProperties;

@class AudioBlock;
@class Content;
@class Credit;
@class Link;
@class VideoBlock;







@interface FileID : NSManagedObjectID {}
@end

@interface _File : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (FileID*)objectID;





@property (nonatomic, strong) NSNumber* identifier;



@property int32_t identifierValue;
- (int32_t)identifierValue;
- (void)setIdentifierValue:(int32_t)value_;

//- (BOOL)validateIdentifier:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* position;



@property int32_t positionValue;
- (int32_t)positionValue;
- (void)setPositionValue:(int32_t)value_;

//- (BOOL)validatePosition:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* type;



//- (BOOL)validateType:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* u_id;



//- (BOOL)validateU_id:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) AudioBlock *audio_block;

//- (BOOL)validateAudio_block:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSSet *contents;

- (NSMutableSet*)contentsSet;




@property (nonatomic, strong) NSSet *credits;

- (NSMutableSet*)creditsSet;




@property (nonatomic, strong) NSSet *links;

- (NSMutableSet*)linksSet;




@property (nonatomic, strong) VideoBlock *video_block;

//- (BOOL)validateVideo_block:(id*)value_ error:(NSError**)error_;





@end

@interface _File (CoreDataGeneratedAccessors)

- (void)addContents:(NSSet*)value_;
- (void)removeContents:(NSSet*)value_;
- (void)addContentsObject:(Content*)value_;
- (void)removeContentsObject:(Content*)value_;

- (void)addCredits:(NSSet*)value_;
- (void)removeCredits:(NSSet*)value_;
- (void)addCreditsObject:(Credit*)value_;
- (void)removeCreditsObject:(Credit*)value_;

- (void)addLinks:(NSSet*)value_;
- (void)removeLinks:(NSSet*)value_;
- (void)addLinksObject:(Link*)value_;
- (void)removeLinksObject:(Link*)value_;

@end

@interface _File (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSNumber*)value;

- (int32_t)primitiveIdentifierValue;
- (void)setPrimitiveIdentifierValue:(int32_t)value_;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




- (NSNumber*)primitivePosition;
- (void)setPrimitivePosition:(NSNumber*)value;

- (int32_t)primitivePositionValue;
- (void)setPrimitivePositionValue:(int32_t)value_;




- (NSString*)primitiveType;
- (void)setPrimitiveType:(NSString*)value;




- (NSString*)primitiveU_id;
- (void)setPrimitiveU_id:(NSString*)value;





- (AudioBlock*)primitiveAudio_block;
- (void)setPrimitiveAudio_block:(AudioBlock*)value;



- (NSMutableSet*)primitiveContents;
- (void)setPrimitiveContents:(NSMutableSet*)value;



- (NSMutableSet*)primitiveCredits;
- (void)setPrimitiveCredits:(NSMutableSet*)value;



- (NSMutableSet*)primitiveLinks;
- (void)setPrimitiveLinks:(NSMutableSet*)value;



- (VideoBlock*)primitiveVideo_block;
- (void)setPrimitiveVideo_block:(VideoBlock*)value;


@end
