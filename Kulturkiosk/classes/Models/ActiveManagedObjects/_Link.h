// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Link.h instead.

#import <CoreData/CoreData.h>


extern const struct LinkAttributes {
	__unsafe_unretained NSString *localUrl;
	__unsafe_unretained NSString *type;
	__unsafe_unretained NSString *u_id;
	__unsafe_unretained NSString *url;
	__unsafe_unretained NSString *url2;
} LinkAttributes;

extern const struct LinkRelationships {
	__unsafe_unretained NSString *file;
	__unsafe_unretained NSString *record_image;
	__unsafe_unretained NSString *resource;
} LinkRelationships;

extern const struct LinkFetchedProperties {
} LinkFetchedProperties;

@class File;
@class RecordImage;
@class Resource;







@interface LinkID : NSManagedObjectID {}
@end

@interface _Link : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (LinkID*)objectID;





@property (nonatomic, strong) NSString* localUrl;



//- (BOOL)validateLocalUrl:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* type;



//- (BOOL)validateType:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* u_id;



//- (BOOL)validateU_id:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* url;



//- (BOOL)validateUrl:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* url2;



//- (BOOL)validateUrl2:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) File *file;

//- (BOOL)validateFile:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) RecordImage *record_image;

//- (BOOL)validateRecord_image:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) Resource *resource;

//- (BOOL)validateResource:(id*)value_ error:(NSError**)error_;





@end

@interface _Link (CoreDataGeneratedAccessors)

@end

@interface _Link (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveLocalUrl;
- (void)setPrimitiveLocalUrl:(NSString*)value;




- (NSString*)primitiveType;
- (void)setPrimitiveType:(NSString*)value;




- (NSString*)primitiveU_id;
- (void)setPrimitiveU_id:(NSString*)value;




- (NSString*)primitiveUrl;
- (void)setPrimitiveUrl:(NSString*)value;




- (NSString*)primitiveUrl2;
- (void)setPrimitiveUrl2:(NSString*)value;





- (File*)primitiveFile;
- (void)setPrimitiveFile:(File*)value;



- (RecordImage*)primitiveRecord_image;
- (void)setPrimitiveRecord_image:(RecordImage*)value;



- (Resource*)primitiveResource;
- (void)setPrimitiveResource:(Resource*)value;


@end
