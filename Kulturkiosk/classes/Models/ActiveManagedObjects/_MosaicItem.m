//
//  _MosaicItem.m
//  Kulturkiosk
//
//  Created by Dmitry on 2/14/17.
//
//

#import "_MosaicItem.h"

const struct MosaicItemAttributes MosaicItemAttributes = {
    .identifier = @"identifier",
    .u_id = @"u_id",
    .type = @"type",
    .presentationID = @"upresentationID",
    .position = @"position",
    .imageUrl = @"imageUrl",
};

const struct MosaicItemRelationships MosaicItemRelationships = {
    .mosaicPresentation = @"MosaicPresentation",
    .text = @"text"
};

const struct MosaicItemFetchedProperties MosaicItemFetchedProperties = {
};

@implementation MosaicItemID
@end

@implementation _MosaicItem

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
    NSParameterAssert(moc_);
    return [NSEntityDescription insertNewObjectForEntityForName:@"MosaicItem" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
    return @"MosaicItem";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
    NSParameterAssert(moc_);
    return [NSEntityDescription entityForName:@"MosaicItem" inManagedObjectContext:moc_];
}

- (MosaicItemID*)objectID {
    return (MosaicItemID*)[super objectID];
}


+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
    NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
    
    if ([key isEqualToString:@"identifierValue"]) {
        NSSet *affectingKey = [NSSet setWithObject:@"identifier"];
        keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
        return keyPaths;
    }
    if ([key isEqualToString:@"u_idValue"]) {
        NSSet *affectingKey = [NSSet setWithObject:@"u_id"];
        keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
        return keyPaths;
    }
    if ([key isEqualToString:@"presentationIDValue"]) {
        NSSet *affectingKey = [NSSet setWithObject:@"presentationID"];
        keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
        return keyPaths;
    }
    if ([key isEqualToString:@"positionValue"]) {
        NSSet *affectingKey = [NSSet setWithObject:@"position"];
        keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
        return keyPaths;
    }
    
    return keyPaths;
}


@dynamic identifier;

- (int16_t)identifierValue {
    NSNumber *result = [self identifier];
    return [result shortValue];
}

- (void)setIdentifierValue:(int16_t)value_ {
    [self setIdentifier:[NSNumber numberWithShort:value_]];
}

@dynamic type;

@dynamic u_id;

- (int32_t)u_idValue {
    NSNumber *result = [self u_id];
    return [result intValue];
}

- (void)setU_idValue:(int32_t)value_ {
    [self setU_id:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveU_idValue {
    NSNumber *result = [self primitiveU_id];
    return [result intValue];
}

- (void)setPrimitiveU_idValue:(int32_t)value_ {
    [self setPrimitiveU_id:[NSNumber numberWithInt:value_]];
}

@dynamic presentationID;

- (int16_t)presentationIDValue{
    NSNumber *result = [self presentationID];
    return [result intValue];
}

- (void)setPresentationIDValue:(int16_t)value_ {
    [self setPresentationID:[NSNumber numberWithInt:value_]];
}

- (int16_t)primitivePresentationIDValue {
    NSNumber *result = [self primitivePresentationID];
    return [result intValue];
}

- (void)setPrimitivePresentationIDValue:(int16_t)value_ {
    [self setPrimitivePresentationID:[NSNumber numberWithInt:value_]];
}

@dynamic position;

- (int16_t)positionValue{
    NSNumber *result = [self position];
    return [result intValue];
}

- (void)setPositionValue:(int16_t)value_ {
    [self setPosition:[NSNumber numberWithInt:value_]];
}

- (int16_t)primitivePositionValue {
    NSNumber *result = [self primitivePosition];
    return [result intValue];
}

- (void)setPrimitivePositionValue:(int16_t)value_ {
    [self setPrimitivePosition:[NSNumber numberWithInt:value_]];
}

@dynamic imageUrl;

@dynamic mosaicPresentation;

- (NSMutableSet*)mosaicPresentationSet {
    [self willAccessValueForKey:@"MosaicPresentation"];
    
    NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"MosaicPresentation"];
    
    [self didAccessValueForKey:@"MosaicPresentation"];
    return result;
}

@dynamic text;

- (NSMutableSet*)textSet {
    [self willAccessValueForKey:@"MosaicItemText"];
    
    NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"MosaicItemText"];
    
    [self didAccessValueForKey:@"MosaicItemText"];
    return result;
}

@end
