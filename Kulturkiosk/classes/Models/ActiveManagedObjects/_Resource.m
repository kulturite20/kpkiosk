// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Resource.m instead.

#import "_Resource.h"

const struct ResourceAttributes ResourceAttributes = {
	.identifier = @"identifier",
	.name = @"name",
	.type = @"type",
	.u_id = @"u_id",
};

const struct ResourceRelationships ResourceRelationships = {
	.device = @"device",
	.links = @"links",
	.presentation = @"presentation",
};

const struct ResourceFetchedProperties ResourceFetchedProperties = {
};

@implementation ResourceID
@end

@implementation _Resource

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Resource" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Resource";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Resource" inManagedObjectContext:moc_];
}

- (ResourceID*)objectID {
	return (ResourceID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"identifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"identifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic identifier;



- (int32_t)identifierValue {
	NSNumber *result = [self identifier];
	return [result intValue];
}

- (void)setIdentifierValue:(int32_t)value_ {
	[self setIdentifier:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveIdentifierValue {
	NSNumber *result = [self primitiveIdentifier];
	return [result intValue];
}

- (void)setPrimitiveIdentifierValue:(int32_t)value_ {
	[self setPrimitiveIdentifier:[NSNumber numberWithInt:value_]];
}





@dynamic name;






@dynamic type;






@dynamic u_id;






@dynamic device;

	

@dynamic links;

	
- (NSMutableSet*)linksSet {
	[self willAccessValueForKey:@"links"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"links"];
  
	[self didAccessValueForKey:@"links"];
	return result;
}
	

@dynamic presentation;

	






@end
