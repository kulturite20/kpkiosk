//
//  MosaicElementSize+CoreDataProperties.m
//  
//
//  Created by Vadim Osovets on 2/24/17.
//
//

#import "MosaicElementSize+CoreDataProperties.h"

@implementation MosaicElementSize (CoreDataProperties)

+ (NSFetchRequest<MosaicElementSize *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MosaicElementSize"];
}

@dynamic position;
@dynamic x;
@dynamic y;

@end
