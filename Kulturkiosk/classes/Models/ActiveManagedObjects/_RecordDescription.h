// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to RecordDescription.h instead.

#import <CoreData/CoreData.h>
#import "MapImage+CoreDataClass.h"
#import "MapText+CoreDataClass.h"


extern const struct RecordDescriptionAttributes {
	__unsafe_unretained NSString *code;
	__unsafe_unretained NSString *color;
	__unsafe_unretained NSString *position;
	__unsafe_unretained NSString *record_id;
	__unsafe_unretained NSString *u_id;
	__unsafe_unretained NSString *x;
	__unsafe_unretained NSString *y;
} RecordDescriptionAttributes;

extern const struct RecordDescriptionRelationships {
	__unsafe_unretained NSString *presentation;
    __unsafe_unretained NSString *map_image;
    __unsafe_unretained NSString *map_text;
} RecordDescriptionRelationships;

extern const struct RecordDescriptionFetchedProperties {
} RecordDescriptionFetchedProperties;

@class Presentation;



@interface RecordDescriptionID : NSManagedObjectID {}
@end

@interface _RecordDescription : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (RecordDescriptionID*)objectID;





@property (nonatomic, strong) NSString* code;



//- (BOOL)validateCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* color;

@property (nonatomic, strong) NSString* placement;



//- (BOOL)validateColor:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* position;



@property int32_t positionValue;
- (int32_t)positionValue;
- (void)setPositionValue:(int32_t)value_;

//- (BOOL)validatePosition:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* record_id;



@property int16_t record_idValue;
- (int16_t)record_idValue;
- (void)setRecord_idValue:(int16_t)value_;

//- (BOOL)validateRecord_id:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* u_id;



@property int32_t u_idValue;
- (int32_t)u_idValue;
- (void)setU_idValue:(int32_t)value_;

//- (BOOL)validateU_id:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* x;



@property int32_t xValue;
- (int32_t)xValue;
- (void)setXValue:(int32_t)value_;

//- (BOOL)validateX:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* y;



@property int32_t yValue;
- (int32_t)yValue;
- (void)setYValue:(int32_t)value_;

//- (BOOL)validateY:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *presentation;

- (NSMutableSet*)presentationSet;

@property (nonatomic, strong) MapImage *map_image;

@property (nonatomic, strong) NSSet *map_text;

//- (NSMutableSet*)map_textSet;


@end

@interface _RecordDescription (CoreDataGeneratedAccessors)

- (void)addPresentation:(NSSet*)value_;
- (void)removePresentation:(NSSet*)value_;
- (void)addPresentationObject:(Presentation*)value_;
- (void)removePresentationObject:(Presentation*)value_;

@end

@interface _RecordDescription (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveCode;
- (void)setPrimitiveCode:(NSString*)value;




- (NSString*)primitiveColor;
- (void)setPrimitiveColor:(NSString*)value;




- (NSNumber*)primitivePosition;
- (void)setPrimitivePosition:(NSNumber*)value;

- (int32_t)primitivePositionValue;
- (void)setPrimitivePositionValue:(int32_t)value_;




- (NSNumber*)primitiveRecord_id;
- (void)setPrimitiveRecord_id:(NSNumber*)value;

- (int16_t)primitiveRecord_idValue;
- (void)setPrimitiveRecord_idValue:(int16_t)value_;




- (NSNumber*)primitiveU_id;
- (void)setPrimitiveU_id:(NSNumber*)value;

- (int32_t)primitiveU_idValue;
- (void)setPrimitiveU_idValue:(int32_t)value_;




- (NSNumber*)primitiveX;
- (void)setPrimitiveX:(NSNumber*)value;

- (int32_t)primitiveXValue;
- (void)setPrimitiveXValue:(int32_t)value_;




- (NSNumber*)primitiveY;
- (void)setPrimitiveY:(NSNumber*)value;

- (int32_t)primitiveYValue;
- (void)setPrimitiveYValue:(int32_t)value_;





- (NSMutableSet*)primitivePresentation;
- (void)setPrimitivePresentation:(NSMutableSet*)value;


@end
