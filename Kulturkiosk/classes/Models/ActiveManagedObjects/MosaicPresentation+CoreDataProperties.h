//
//  MosaicPresentation+CoreDataProperties.h
//  
//
//  Created by Vadim Osovets on 2/24/17.
//
//

#import "MosaicPresentation+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MosaicPresentation (CoreDataProperties)

+ (NSFetchRequest<MosaicPresentation *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *identifier;
@property (nullable, nonatomic, copy) NSString *type;
@property (nullable, nonatomic, copy) NSString *updated_at;
@property (nullable, nonatomic, retain) NSSet<Content *> *contents;
@property (nullable, nonatomic, retain) NSSet<MosaicItem *> *mosaic_items;
@property (nullable, nonatomic, retain) MosaicTemplate *presentation_template;
@property (nullable, nonatomic, retain) NSSet<Resource *> *resources;

@end

@interface MosaicPresentation (CoreDataGeneratedAccessors)

- (void)addContentsObject:(Content *)value;
- (void)removeContentsObject:(Content *)value;
- (void)addContents:(NSSet<Content *> *)values;
- (void)removeContents:(NSSet<Content *> *)values;

- (void)addMosaic_itemsObject:(MosaicItem *)value;
- (void)removeMosaic_itemsObject:(MosaicItem *)value;
- (void)addMosaic_items:(NSSet<MosaicItem *> *)values;
- (void)removeMosaic_items:(NSSet<MosaicItem *> *)values;

- (void)addResourcesObject:(Resource *)value;
- (void)removeResourcesObject:(Resource *)value;
- (void)addResources:(NSSet<Resource *> *)values;
- (void)removeResources:(NSSet<Resource *> *)values;

@end

NS_ASSUME_NONNULL_END
