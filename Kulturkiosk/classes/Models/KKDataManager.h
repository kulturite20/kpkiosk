//
//  DataManager.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 23.07.14.
//
//

#import <Foundation/Foundation.h>
#import "KKLanguage.h"

typedef enum KKImageFormat {
    KKFormatHeader = 0,
    KKFormatOriginal,
    KKFormatThumb,
    KKFormatMain,
    KKFormatSmall
} KKImageFormat;

@interface KKDataManager : NSObject

+ (instancetype)sharedManager;

- (BOOL)hasContentForLanguage:(KKLanguageType)lang;
- (NSString *)stringForImageFormat:(KKImageFormat)format;
- (NSString *)localizedCreditTypeForTitle:(NSString *)title;

@end
