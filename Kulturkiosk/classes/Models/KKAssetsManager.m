//
//  KKAssetsManager.m
//  Kulturkiosk
//
//  Created by Martin Jensen on 18.07.12.
//
//

#import "KKAssetsManager.h"
#import "KKTimerManager.h"
#import "ProgressObserver.h"

static NSInteger const kMaxConcurrentDownloadingOperations = 2;
static NSInteger const kMaxConcurrentCheckingSizeTasks     = 3;
static NSInteger const kMaxAttemptsToLoadFailedFiles       = 10;
static NSInteger const kAvailableSizeDelta                 = 300 * 1000000;

@interface KKAssetsManager ()
<
NSURLSessionDataDelegate
>
@end

@implementation KKAssetsManager {
    NSURLSession        *_session;
    NSMutableArray      *_lastAssetsList;
    Progress            *_sizeCheckingProgress;
    NSOperationQueue    *_queue;
    
    dispatch_group_t _requestsToCheckSizeGroup;
    long long        _assetsGlobalSize;
    NSInteger        _attemptsToLoadFailedFiles;
}

#pragma mark - Singleton

+ (id)sharedManager {
    static dispatch_once_t pred = 0;
    __strong static id _sharedManager = nil;
    dispatch_once(&pred, ^{
        _sharedManager = [[self alloc] init]; // or some other init method
    });
    return _sharedManager;
}

#pragma mark - Init

- (id)init {
    self = [super init];
    if (self) {
        _queue = [[RKObjectManager sharedManager].HTTPClient operationQueue];
        [_queue setMaxConcurrentOperationCount:kMaxConcurrentDownloadingOperations];
      
        // Setting up encoding for images downloading
        [RKMIMETypeSerialization registerClass:[RKURLEncodedSerialization class] forMIMEType:@"image/jpeg"];
        [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
        _lastAssetsList = [NSMutableArray new];
        _mode           = AssetsManagerFirstLoadMode;
        
        [self setupSession];
    }
    return self;
}

#pragma mark - Public

- (void)reloadQueue {
    _queue = [[RKObjectManager sharedManager].HTTPClient operationQueue];
}

- (void)loadAssets:(NSArray *)assets withCompletionBlock:(DownloadingAssetsCompletionBlock)completionBlock {
    [self reloadQueue];
    
    // We have to download only that resources, that we don't have already.
    NSMutableArray *assetsThatWeNeed = [NSMutableArray new];
    for (NSDictionary *assetInfo in assets) {
        
        if (![self fileExistsWithName:assetInfo[@"fileName"]
                             inFolder:[self pathToDefaultFolder]] &&
            ![self fileExistsWithName:assetInfo[@"fileName"]
                            inFolder:[self pathToTempFolder]]) {
            [assetsThatWeNeed addObject:assetInfo];
        }
        [_lastAssetsList addObject:assetInfo[@"fileName"]];
    }
    
    _currentAssetsCount = [assetsThatWeNeed count];
    
    // If we have no new assets to download - delete unused resources (if app is launching) and execute completion block
    if (_currentAssetsCount == 0) {
        if (UIAppDelegate.hasNoSpace && _mode == AssetsManagerLaunchingMode) {
            [self deleteUnusedResources];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"enoughSpace"
                                                                object:nil];
        }
        completionBlock();
        _assetsGlobalSize = 0.0;
        return;
    }
    
    void (^canLoadBlock)();
    // Case #1: App is launching and we have no space.
    if (UIAppDelegate.hasNoSpace && _mode == AssetsManagerLaunchingMode) {
        [self deleteUnusedResources];
        canLoadBlock = ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"enoughSpace"
                                                                object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"loadingStarted"
                                                                object:self
                                                              userInfo:@{@"assetsCount" :
                                                                             [NSNumber numberWithInteger:[assetsThatWeNeed count]]}];
            [self loadAssets:assetsThatWeNeed usingMode:_mode withCompletionBlock:completionBlock];
        };
    }
    // Case #1: App is updating in background mode and we have no space.
    if (UIAppDelegate.hasNoSpace && _mode == AssetsManagerUpdatingMode) {
        _mode = AssetsManagerLaunchingMode;
        [self deleteUnusedResources];
        [self setUpSizeCheckingObserver];
        canLoadBlock = ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"enoughSpace"
                                                                object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"loadingInitialized"
                                                                object:self
                                                              userInfo:@{@"assetsCount" :
                                                                             [NSNumber numberWithInteger:[assetsThatWeNeed count]],
                                                                         @"needToShowHomeScreen" : @0}];
            [self loadAssets:assetsThatWeNeed usingMode:_mode withCompletionBlock:completionBlock];
        };
    }
    
    // Case #3: We have enough space to launch/update app
    if (!UIAppDelegate.hasNoSpace) {
        if ([self deleteUnusedResources] && _mode == AssetsManagerUpdatingMode) {
            _mode = AssetsManagerLaunchingMode;
            [self setUpSizeCheckingObserver];
            canLoadBlock = ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"loadingInitialized"
                                                                    object:self
                                                                  userInfo:@{@"assetsCount" :
                                                                                 [NSNumber numberWithInteger:[assetsThatWeNeed count]],
                                                                             @"needToShowHomeScreen" : @1}];
                [self loadAssets:assetsThatWeNeed usingMode:_mode withCompletionBlock:completionBlock];
            };

        } else {
            if (_mode != AssetsManagerUpdatingMode) {
                [self setUpSizeCheckingObserver];
            }
            canLoadBlock = ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"loadingStarted"
                                                                    object:self
                                                                  userInfo:@{@"assetsCount" :
                                                                                 [NSNumber numberWithInteger:[assetsThatWeNeed count]]}];
                [self loadAssets:assetsThatWeNeed usingMode:_mode withCompletionBlock:completionBlock];
            };
        }
    }
    
    [self canLoadAssets:assetsThatWeNeed withCompletionBlock:^(BOOL canLoadAssets) {
        if (canLoadAssets) {
            canLoadBlock();
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"notEnoughSpace"
                                                                    object:nil];
            _assetsGlobalSize = 0.0;
            [_lastAssetsList removeAllObjects];
        }
    }];
}

- (NSString *)pathToTempFolder {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *directory = [paths objectAtIndex:0];
    // Get caches folder
    NSString *cacheDir = [directory stringByAppendingPathComponent:@"TemporaryDownloadedMedia"];
    return cacheDir;
}

- (NSString *)pathToDefaultFolder {
    NSArray    *paths       = NSSearchPathForDirectoriesInDomains
    (NSCachesDirectory, NSUserDomainMask, YES);
    NSString   *directory   = [paths objectAtIndex:0];
    // Get caches folder
    NSString *cacheDir;
    cacheDir = [directory stringByAppendingPathComponent:@"DownloadedMedia"];
    
    return cacheDir;
}

- (BOOL)deleteUnusedResources {
    // Case #1: Resources were deleted on server side, so we have to delete them from our storage
    BOOL __block hasUnusedResources = NO;
    if ([_lastAssetsList count] == 0) {
        return hasUnusedResources;
    }
    NSMutableArray *currentResources = [[[NSFileManager defaultManager] contentsOfDirectoryAtPath:[self pathToDefaultFolder]
                                                                                            error:nil] mutableCopy];
    [currentResources enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *oldResourceName = (NSString *)obj;
        
        BOOL isContainObj = NO;
        
        for (NSString *str in _lastAssetsList) {
            
            // Due to specific of languages take only first part of url
            NSString *str1 = [NSString stringWithFormat:@"%@", [[str componentsSeparatedByString:@"."] firstObject]];
            NSString *str2 = [NSString stringWithFormat:@"%@", [[oldResourceName componentsSeparatedByString:@"."] firstObject]];
            
//            NSLog(@"\"%@\"|\"%@\"", str1, str2);
//            
//            NSLog(@"%d<>%d", [str1 isEqualToString:str2], strcmp(str1.UTF8String, str2.UTF8String));
            
            if ([str1 isEqualToString:str2]) {
                isContainObj = YES;
                break;
            }
        }
        
        if (!isContainObj) {
            NSError *error = nil;
            [[NSFileManager defaultManager] removeItemAtPath:[[self pathToDefaultFolder] stringByAppendingPathComponent:oldResourceName]
                                                       error:&error];
            hasUnusedResources = YES;
            NSLog(@"File at URL removed: %@", oldResourceName);
            if (error) {
                NSLog(@"Error deleting file: %@", [error localizedDescription]);
            }
        }
    }];

    [_lastAssetsList removeAllObjects];
    
    return hasUnusedResources;
}

#pragma mark - NSURLSession Delegate

- (void)URLSession:(NSURLSession *)session
          dataTask:(NSURLSessionDataTask *)dataTask
didReceiveResponse:(NSURLResponse *)response
 completionHandler:(void (^)(NSURLSessionResponseDisposition))completionHandler {
    _assetsGlobalSize += response.expectedContentLength;
    NSLog(@"Expected length: %lld", response.expectedContentLength);
    NSLog(@"Assets global size %lld", _assetsGlobalSize);
    if (_sizeCheckingProgress) {
        static int counter = 0;
        NSLog(@"Assets checked: %d", counter);
        [_sizeCheckingProgress subscribeForObserving:(float)counter/_currentAssetsCount];
        counter++;
        if (counter == _currentAssetsCount) {
            counter = 0;
        }
    }
    
    completionHandler(NSURLSessionResponseCancel);
    dispatch_group_leave(_requestsToCheckSizeGroup);
}

#pragma mark - Private

- (void)setUpSizeCheckingObserver {
    _sizeCheckingProgress = [[Progress alloc] initWithIdentifier:@"SizeCheckingProgress"
                                                   startingValue:17.5
                                                     endingValue:25.0];
    [[ProgressObserver sharedObserver] observeProgress:_sizeCheckingProgress
                                               partial:YES];
}

- (void)canLoadAssets:(NSArray *)assets withCompletionBlock:(void (^)(BOOL canLoadAssets))completionBlock {
    
    _requestsToCheckSizeGroup = dispatch_group_create();
    
    NSLog(@"Started calculating size");
    for (NSDictionary *assetInfo in assets) {
        dispatch_group_enter(_requestsToCheckSizeGroup);
        // Temp
        NSURL *url = [NSURL URLWithString:assetInfo[@"fileURL"]];
        NSURL *urlTemp = [NSURL URLWithString:assetInfo[@"fileURLTemp"]];
        NSMutableURLRequest *req = nil;
        if (urlTemp) {
            req = [NSMutableURLRequest requestWithURL:urlTemp];
        } else {
            req = [NSMutableURLRequest requestWithURL:url];
        }
        [req setHTTPMethod:@"HEAD"];
        [req setTimeoutInterval:[NSDate timeIntervalSinceReferenceDate]];
        [[_session dataTaskWithRequest:req] resume];
        
        NSLog(@"URL - %@", url ? url : urlTemp);
    }
    
    dispatch_group_notify(_requestsToCheckSizeGroup, dispatch_get_main_queue(), ^{
        long long freeDiskSpace = [self freeDiskspace];
        NSLog(@"Assets size: %.2f MB available: %.2f MB", (float)_assetsGlobalSize/1000000, (float)freeDiskSpace/1000000);
        // If we have no space on the device, we have to post notification about it
        if (((freeDiskSpace - _assetsGlobalSize) < kAvailableSizeDelta) && _assetsGlobalSize > 0) {
            completionBlock(NO);
        } else {
            completionBlock(YES);
        }
    });
}

- (void)loadAssets:(NSArray *)assets
         usingMode:(AssetsManagerMode)mode
withCompletionBlock:(DownloadingAssetsCompletionBlock)completionBlock {
    
    dispatch_group_t downloadingAssetsDispatchGroup = dispatch_group_create();
    NSInteger __block downloadedAssetsCounter = 0;
    
//#warning ONLY DEVS
//    
//    static int i = 0;
//    
    // If everything is OK, we can download assets
    for (NSDictionary *assetInfo in assets) {
        
//        i++;
//        
//        if (i > 5) {
//            break;
//        }
//        
        NSString *fileURL  = assetInfo[@"fileURL"];
        NSString *fileName = assetInfo[@"fileName"];
        NSString *fileLoadedNotificationName = nil;
        NSString *pathForSaving = nil;

        switch (_mode) {
            case AssetsManagerFirstLoadMode: {
                fileLoadedNotificationName = @"loadedFile";
                pathForSaving              = [self pathToDefaultFolder];
            }
                break;
            case AssetsManagerLaunchingMode: {
                fileLoadedNotificationName = @"loadedFile";
                pathForSaving              = [self pathToTempFolder];
            }
                break;
            case AssetsManagerUpdatingMode: {
                fileLoadedNotificationName = @"loadedUpdatingFile";
                pathForSaving              = [self pathToTempFolder];
            }
                break;
                
            default:
                break;
        }
        
        NSString *filePath = [pathForSaving stringByAppendingPathComponent:fileName];
        
        //Create folder
        if (![[NSFileManager defaultManager] fileExistsAtPath:pathForSaving]) {
            NSError *error;
            [[NSFileManager defaultManager] createDirectoryAtPath:pathForSaving
                                      withIntermediateDirectories:NO
                                                       attributes:nil
                                                            error:&error];
            if (error) {
                NSLog(@"Error while creating new saving folder: %@",[error localizedDescription]);
            }
        }
        
        fileURL = [fileURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableURLRequest *request = [[RKObjectManager sharedManager].HTTPClient requestWithMethod:@"GET"
                                                                                                path:fileURL
                                                                                          parameters:nil];
        [request setTimeoutInterval:[NSDate timeIntervalSinceReferenceDate]];
        dispatch_group_enter(downloadingAssetsDispatchGroup);
        NSLog(@"%@ IS IN STACK", fileName);
        AFHTTPRequestOperation *operation = [[RKObjectManager sharedManager].HTTPClient HTTPRequestOperationWithRequest:request
                                                                        success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                            
                                                                            [[NSNotificationCenter defaultCenter]
                                                                                       postNotificationName:fileLoadedNotificationName
                                                                                                     object:self
                                                                                                   userInfo:@{@"fileName" : fileName}];
                                                                                                NSLog(@"%@ loaded...", fileName);
                                                                            downloadedAssetsCounter++;
                                                                            dispatch_group_leave(downloadingAssetsDispatchGroup);
                                                                      } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                            dispatch_group_leave(downloadingAssetsDispatchGroup);
                                                                      }];
        [[RKObjectManager sharedManager].HTTPClient enqueueHTTPRequestOperation:operation];
        [operation setOutputStream:[NSOutputStream outputStreamToFileAtPath:filePath
                                                                     append:NO]];
    }
    
    dispatch_group_notify(downloadingAssetsDispatchGroup, dispatch_get_main_queue(), ^{
        if (downloadedAssetsCounter == [assets count]) {
            _assetsGlobalSize = 0.0;
            _attemptsToLoadFailedFiles = 0;
            _queue = nil;
            completionBlock();
        } else {
            _attemptsToLoadFailedFiles++;
            if (_attemptsToLoadFailedFiles == kMaxAttemptsToLoadFailedFiles) {
                _assetsGlobalSize = 0.0;
                _queue = nil;
                _attemptsToLoadFailedFiles = 0;
                completionBlock();
                return;
            }
            NSMutableArray *assetsThatWeNeed = [NSMutableArray new];
            for (NSDictionary *assetInfo in assets) {
                if (![self fileExistsWithName:assetInfo[@"fileName"]
                                     inFolder:[self pathToDefaultFolder]] &&
                    ![self fileExistsWithName:assetInfo[@"fileName"]
                                     inFolder:[self pathToTempFolder]]) {
                        [assetsThatWeNeed addObject:assetInfo];
                    }
            }
            [self loadAssets:assetsThatWeNeed
                   usingMode:_mode
         withCompletionBlock:completionBlock];
        }
    });
}

- (BOOL)fileExistsWithName:(NSString *)fileName
                  inFolder:(NSString *)folderName {
    
    NSString *filePathToFolder   = [folderName stringByAppendingPathComponent:fileName];
    long long sizeOfFileInFolder = [[[NSFileManager defaultManager] attributesOfItemAtPath:filePathToFolder
                                                                                     error:nil][NSFileSize] longLongValue];

    if ([[NSFileManager defaultManager] fileExistsAtPath:filePathToFolder]) {
        if (sizeOfFileInFolder < 2000) {
            [[NSFileManager defaultManager] removeItemAtPath:filePathToFolder
                                                       error:nil];
            return NO;
        }
        return YES;
    } else {
        return NO;
    }
}

- (void)setupSession {
    NSOperationQueue *queue = [NSOperationQueue new];
    [queue setMaxConcurrentOperationCount:kMaxConcurrentCheckingSizeTasks];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.HTTPMaximumConnectionsPerHost = kMaxConcurrentCheckingSizeTasks;
    _session = [NSURLSession sessionWithConfiguration:configuration
                                             delegate:self
                                        delegateQueue:queue];
}

- (uint64_t)freeDiskspace {
    uint64_t totalSpace = 0;
    uint64_t totalFreeSpace = 0;
    
    __autoreleasing NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    
    if (dictionary) {
        NSNumber *fileSystemSizeInBytes = [dictionary objectForKey: NSFileSystemSize];
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        totalSpace = [fileSystemSizeInBytes unsignedLongLongValue];
        totalFreeSpace = [freeFileSystemSizeInBytes unsignedLongLongValue];
    } else {
        NSLog(@"Error Obtaining System Memory Info: Domain = %@, Code = %ld", [error domain], (long)[error code]);
    }
    
    return totalFreeSpace;
}

@end
