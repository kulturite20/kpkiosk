//
//  KKAssetsManager.h
//  Kulturkiosk
//
//  Created by Martin Jensen on 18.07.12.
//
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import <CoreData/CoreData.h>

#import <RestKit/CoreData.h>
#import "Resource.h"
#import "Content.h"
#import "File.h"

typedef void (^DownloadingAssetsCompletionBlock)();
typedef void (^CheckingAssetsSizeCompletionBlock)(BOOL canLoad);

typedef enum {
    AssetsManagerFirstLoadMode,
    AssetsManagerLaunchingMode,
    AssetsManagerUpdatingMode
} AssetsManagerMode;

@interface KKAssetsManager : NSObject

@property (nonatomic) AssetsManagerMode mode;
@property (nonatomic) NSInteger currentAssetsCount;

+ (instancetype)sharedManager;
- (void)loadAssets:(NSArray *)assets withCompletionBlock:(DownloadingAssetsCompletionBlock)completionBlock;
- (BOOL)deleteUnusedResources;
- (void)reloadQueue;

- (NSString *)pathToTempFolder;
- (NSString *)pathToDefaultFolder;


@end
