//
//  DataManager.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 23.07.14.
//
//

#import "Presentation.h"
#import "Record.h"
#import "RecordDescription.h"
#import "Page.h"
#import "Block.h"
#import "Credit.h"
#import "File.h"
#import "Device.h"
#import "Content.h"
#import "Link.h"

#import "KKDataManager.h"

@implementation KKDataManager

#pragma mark - Singleton

+ (instancetype)sharedManager {
    static dispatch_once_t pred = 0;
    __strong static id _sharedManager = nil;
    dispatch_once(&pred, ^{
        _sharedManager = [[self alloc] init]; // or some other init method
    });
    return _sharedManager;
}

#pragma mark - Init

- (id)init {
    if (self = [super init]) {
        // Customize if needed
    }
    return self;
}

#pragma mark - Public

- (BOOL)hasContentForLanguage:(KKLanguageType)lang {
   /* NSString *language = nil;
    switch (lang) {
        case KKLanguageTypeEnglish: {
            language = @"en";
        }
            break;
        case KKLanguageTypeFrench: {
            language = @"fr";
        }
            break;
        case KKLanguageTypeGerman: {
            language = @"gr";
        }
            break;
        case KKLanguageTypeSwedish: {
            language = @"sw";
        }
            break;
        case KKLanguageTypeSpanish: {
            language = @"sp";
        }
            break;
        case KKLanguageTypeNorwegian: {
            language = @"no";
        }
            break;
            
        default:
            break;
    }*/
    
    for (Record *record in [Record allObjects]) {
        if ([record contentForLanguage:lang]) {
            return YES;
        }
    }
    return NO;
    
//    NSFetchRequest *fetch = [Content fetchRequest];
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"language == %@",language];
//    [fetch setPredicate:predicate];
//    
//    NSUInteger count = [Content countOfObjectsWithFetchRequest:fetch];
//    
//    if (count == 0) {
//        return NO;
//    }
//    return YES;
}

- (NSString *)stringForImageFormat:(KKImageFormat)format {
    NSString *str = @"original";
    switch (format) {
        case KKFormatHeader:
            str = @"header";
            break;
        case KKFormatMain:
            str = @"main";
            break;
        case KKFormatOriginal:
            str = @"original";
            break;
        case KKFormatSmall:
            str = @"small";
            break;
        case KKFormatThumb:
            str = @"thumb";
        default:
            break;
    }
    return str;
}

- (NSString *)localizedCreditTypeForTitle:(NSString *)title {
    if ([title isEqualToString:@"photographer"]) {
    /*English: Photographer
    Norwegian: Fotograf
    Swedish: Fotograf
    Spanish: Fotógrafo
    French: Photographe
    German: Fotograf*/
        switch ([KKLanguage currentLanguage]) {
            case KKLanguageTypeEnglish: {
                return @"Photographer";
            }
                break;
            case KKLanguageTypeFrench: {
                return @"Photographe";
            }
                break;
            case KKLanguageTypeGerman: {
                return @"Fotograf";
            }
                break;
            case KKLanguageTypeSwedish: {
                return @"Fotograf";
            }
                break;
            case KKLanguageTypeSpanish: {
                return @"Fotógrafo";
            }
                break;
            case KKLanguageTypeNorwegian: {
                return @"Fotograf";
            }
                break;
                
            default:
                return @"photographer";
                break;
        }
    } else if ([title isEqualToString:@"artist"]) {
        /*
         English: Artist
         Norwegian: Kunstner
         Swedish: Konstnär
         Spanish: Artista
         French: Artiste
         German: Künstler
         */
        switch ([KKLanguage currentLanguage]) {
            case KKLanguageTypeEnglish: {
                return @"Artist";
            }
                break;
            case KKLanguageTypeFrench: {
                return @"Artiste";
            }
                break;
            case KKLanguageTypeGerman: {
                return @"Künstler";
            }
                break;
            case KKLanguageTypeSwedish: {
                return @"Konstnär";
            }
                break;
            case KKLanguageTypeSpanish: {
                return @"Artista";
            }
                break;
            case KKLanguageTypeNorwegian: {
                return @"Kunstner";
            }
                break;
                
            default:
                return @"artist";
                break;
        }
    } else if ([title isEqualToString:@"author"]) {
   /* English: Author
    Norwegian: Forfatter
    Swedish: Författare
    Spanish: Autor
    French: Écrivain
    German: Autor */
        switch ([KKLanguage currentLanguage]) {
            case KKLanguageTypeEnglish: {
                return @"Author";
            }
                break;
            case KKLanguageTypeFrench: {
                return @"Écrivain";
            }
                break;
            case KKLanguageTypeGerman: {
                return @"Autor";
            }
                break;
            case KKLanguageTypeSwedish: {
                return @"Författare";
            }
                break;
            case KKLanguageTypeSpanish: {
                return @"Autor";
            }
                break;
            case KKLanguageTypeNorwegian: {
                return @"Forfatter";
            }
                break;
                
            default:
                return @"photographer";
                break;
        }
    } else if ([title isEqualToString:@"director"]) {
        /*
         English: Director
         Norwegian: Regissør
         Swedish: Regissör
         Spanish: Director
         French: Directeur
         German: Regisseur*/
        switch ([KKLanguage currentLanguage]) {
            case KKLanguageTypeEnglish: {
                return @"Director";
            }
                break;
            case KKLanguageTypeFrench: {
                return @"Directeur";
            }
                break;
            case KKLanguageTypeGerman: {
                return @"Regisseur";
            }
                break;
            case KKLanguageTypeSwedish: {
                return @"Regissör";
            }
                break;
            case KKLanguageTypeSpanish: {
                return @"Director";
            }
                break;
            case KKLanguageTypeNorwegian: {
                return @"Regissør";
            }
                break;
                
            default:
                return @"director";
                break;
        }
    } else if ([title isEqualToString:@"voice"]) {
        /*
         English: Voice
         Norwegian: Stemme
         Swedish: Röst
         Spanish: Voz
         French: Voix
         German: Stimme */
        switch ([KKLanguage currentLanguage]) {
            case KKLanguageTypeEnglish: {
                return @"Voice";
            }
                break;
            case KKLanguageTypeFrench: {
                return @"Voix";
            }
                break;
            case KKLanguageTypeGerman: {
                return @"Stimme";
            }
                break;
            case KKLanguageTypeSwedish: {
                return @"Röst";
            }
                break;
            case KKLanguageTypeSpanish: {
                return @"Voz";
            }
                break;
            case KKLanguageTypeNorwegian: {
                return @"Stemme";
            }
                break;
                
            default:
                return @"voice";
                break;
        }
    } else if ([title isEqualToString:@"owner"]) {
        /*
         English: Owner
         Norwegian: Eier
         Swedish: Ägare
         Spanish: Propietario
         French: Propriétaire
         German: Eigentümer*/
        switch ([KKLanguage currentLanguage]) {
            case KKLanguageTypeEnglish: {
                return @"Owner";
            }
                break;
            case KKLanguageTypeFrench: {
                return @"Propriétaire";
            }
                break;
            case KKLanguageTypeGerman: {
                return @"Eigentümer";
            }
                break;
            case KKLanguageTypeSwedish: {
                return @"Ägare";
            }
                break;
            case KKLanguageTypeSpanish: {
                return @"Propietario";
            }
                break;
            case KKLanguageTypeNorwegian: {
                return @"Eier";
            }
                break;
                
            default:
                return @"Owner";
                break;
        }
    } else {
        return @"";
    }
}


@end