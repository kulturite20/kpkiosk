//
//  KKTimerManager.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 4/17/14.
//
//

#import "KKTimerManager.h"

// TODO: Clarify issues with timeout's

static NSInteger const kTimeToUpdateContent = 900;

@implementation KKTimerManager {
    NSMutableArray *_timers;
    NSMutableArray *_timersMetaData;
}

#pragma mark - Singleton

+ (instancetype)sharedManager {
    static dispatch_once_t pred = 0;
    __strong static id _sharedManager = nil;
    dispatch_once(&pred, ^{
        _sharedManager = [[self alloc] init]; // or some other init method
    });
    return _sharedManager;
}

#pragma mark - Init

- (id)init {
    if (self = [super init]) {
        _timers = [NSMutableArray new];
        _timersMetaData = [NSMutableArray new];
    }
    return self;
}

#pragma mark - Timers management

- (void)startTimerWithIdentifier:(NSString *)name
                   timeout:(NSInteger)seconds {
    
    if ([self timerWithIdentifier:name]) {
        [self stopTimerWithIdentifier:name];
    }
    NSDictionary *userInfo = @{kTimerIdentifierKey   : name,
                               kTimerInitialTimeKey  : [NSNumber numberWithInteger:seconds]};
    
    NSTimer *timer = [NSTimer timerWithTimeInterval:seconds
                                             target:self
                                           selector:@selector(handleTimer:)
                                           userInfo:userInfo
                                            repeats:NO];
    [[NSRunLoop currentRunLoop] addTimer:timer
                                 forMode:NSRunLoopCommonModes];
    [_timers addObject:timer];
    if (![self timerMetadataWithIdentifier:name]) {
        [_timersMetaData addObject:userInfo];
    }
    if ([_delegate respondsToSelector:@selector(timerDidStart:)]) {
        [_delegate timerDidStart:timer];
    }
    NSLog(@"%@ STARTED >> ++++ %@: %@", [timer identifier], timer, @(seconds));
}

- (void)stopTimerWithIdentifier:(NSString *)name {
    NSTimer *timer = [self timerWithIdentifier:name];
    if (timer) {
//        NSLog(@"%@ STOPPED", [timer identifier]);
        NSLog(@"%@ STOPPED >> ---- %@", [timer identifier], timer);
        [timer invalidate];
        [_timers removeObject:timer];
        [_timersMetaData removeObject:[self timerMetadataWithIdentifier:name]];
        timer = nil;
    }
}

- (void)restartTimerWithIdentifier:(NSString *)name {
    if ([self timerWithIdentifier:name]) {
        NSInteger timeout = [[self timerWithIdentifier:name] initialTime];
        [self stopTimerWithIdentifier:name];
        [self startTimerWithIdentifier:name
                         timeout:timeout];
    } else {
        NSDictionary *timerMeta = [self timerMetadataWithIdentifier:name];
        
        NSInteger timeout = [timerMeta[kTimerInitialTimeKey] integerValue];
        
        if (timeout == 0) {
            timeout = kTimeToUpdateContent;
        }
        
        [self startTimerWithIdentifier:name
                               timeout:timeout];
        
    }
}

- (NSTimer *)timerWithIdentifier:(NSString *)name {
    for (NSTimer *timer in _timers) {
        if ([[timer identifier] isEqualToString:name]) {
            return timer;
        }
    }
    return nil;
}

- (NSArray *)activeTimers {
    return _timers;
}

#pragma mark - Private

- (void)handleTimer:(NSTimer *)timer {
    NSLog(@"%@ FIRED >>> %@", [timer identifier], timer);
    [_delegate timerTimeoutFinished:timer];
    [_timers removeObject:timer];
}

- (NSDictionary *)timerMetadataWithIdentifier:(NSString *)name {
    for (NSDictionary *meta in _timersMetaData) {
        if ([meta[kTimerIdentifierKey] isEqualToString:name]) {
            return meta;
        }
    }
    return nil;
}

@end
