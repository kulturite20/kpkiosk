//
//  UIView+Simplified.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 6/16/17.
//
//

#import "UIView+Simplified.h"

@implementation UIView (Simplified)

- (CGFloat)halfWidth {
    return self.frame.size.width / 2;
}

- (CGFloat)halfHeight {
    return self.frame.size.height / 2;
}

- (CGFloat)width {
    return self.frame.size.width;
}

- (CGFloat)height {
    return self.frame.size.height;
}

- (CGPoint)leftTopPoint {
    return self.frame.origin;
}

- (CGPoint)leftBottomPoint {
    CGPoint leftBottomPoint = CGPointMake(self.frame.origin.x,
                                          self.frame.origin.y + [self height]);
    return leftBottomPoint;
}

- (CGPoint)rightTopPoint {
    CGPoint rightTopPoint = CGPointMake(self.frame.origin.x + [self width],
                                        self.frame.origin.y);
    return rightTopPoint;

}
- (CGPoint)rightBottomPoint {
    CGPoint rightBottomPoint = CGPointMake(self.frame.origin.x + [self width],
                                           self.frame.origin.y);
    return rightBottomPoint;
}


@end
