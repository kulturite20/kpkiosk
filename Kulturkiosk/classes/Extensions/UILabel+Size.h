//
//  UILabel+UILabel_Size.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 6/19/17.
//
//

#import <UIKit/UIKit.h>

@interface UILabel (Size)

- (void)resizeForExpectedSize:(CGSize) expectedSize andPosition:(CGPoint) point;

@end
