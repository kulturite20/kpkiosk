//
//  UILabel+UILabel_Size.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 6/19/17.
//
//

#import "UILabel+Size.h"

@implementation UILabel (Size)

- (void)resizeForExpectedSize:(CGSize) expectedSize andPosition:(CGPoint) point {
    self.lineBreakMode = NSLineBreakByWordWrapping;
    self.numberOfLines = 0;
    CGSize sizeForLabel =  [self.text boundingRectWithSize:expectedSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : self.font} context:nil].size;
    self.frame = (CGRect){.origin = point, .size = sizeForLabel};
}


@end
