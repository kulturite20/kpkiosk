//
//  UIView+Simplified.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 6/16/17.
//
//

#import <UIKit/UIKit.h>

@interface UIView (Simplified)

- (CGFloat)halfWidth;
- (CGFloat)halfHeight;
- (CGFloat)width;
- (CGFloat)height;

- (CGPoint)leftTopPoint;
- (CGPoint)leftBottomPoint;
- (CGPoint)rightTopPoint;
- (CGPoint)rightBottomPoint;


@end
