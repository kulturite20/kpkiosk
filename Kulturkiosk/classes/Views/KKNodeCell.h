//
//  KKNodeCell.h
//  Kulturkiosk
//
//  Created by Martin Jensen on 16.07.12.
//
//

#import <UIKit/UIKit.h>
#import "KKTopAlignImageView.h"

@interface KKNodeCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *title, *accessoryLabel;
@property (weak, nonatomic) IBOutlet KKTopAlignImageView *image;

@end
