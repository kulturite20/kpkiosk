//
//  KKImageCell.h
//  Kulturkiosk
//
//  Created by Rune Botten on 11.12.12.
//
//

#import <UIKit/UIKit.h>
#import "KKZoomingImageView.h"

@interface KKImageCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (assign) BOOL zoomEnabled;
@property (strong) File *imageFile;

- (void)updateWithImageFile:(File *)imageFile;

@end
