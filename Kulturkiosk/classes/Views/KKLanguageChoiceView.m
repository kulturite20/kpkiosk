//
//  KKLanguageChoiceView.m
//  Kulturkiosk
//
//  Created by Rune Botten on 23.08.12.
//
//

#import "KKLanguageChoiceView.h"

@implementation KKLanguageChoiceView

@synthesize title, flag, checkMark;

-(void) drawRect:(CGRect)rect
{
  [super drawRect:rect];
  
  // We are drawing a line, embossed in the background for this view
  // This effect is created by drawing two lines, 1 px wide each with different colors.
  
  CGContextRef context    = UIGraphicsGetCurrentContext();
  CGContextSaveGState(context);
  
  CGContextSetStrokeColorWithColor(context, [UIColor colorWithWhite:0.400 alpha:1.000].CGColor);
  CGContextSetLineWidth(context, 1.0);
  CGContextMoveToPoint(context, 0,0);
  CGContextAddLineToPoint(context, rect.size.width, 0);
  CGContextStrokePath(context);

  CGContextSetStrokeColorWithColor(context, [UIColor colorWithWhite:0.596 alpha:1.000].CGColor);
  CGContextSetLineWidth(context, 1.0);
  CGContextMoveToPoint(context, 0,1);
  CGContextAddLineToPoint(context, rect.size.width, 1);
  CGContextStrokePath(context);
  
  CGContextRestoreGState(context);
}

@end
