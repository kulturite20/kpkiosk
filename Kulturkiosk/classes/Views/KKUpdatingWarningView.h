//
//  KKUpdatingWarningView.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 5/27/14.
//

#import <UIKit/UIKit.h>

@interface KKUpdatingWarningView : UIView

@property (weak, nonatomic) IBOutlet UILabel *updatingContentLabel;

@end
