//
//  KKSelectableNodeCell.m
//  Kulturkiosk
//
//  Created by Rune Botten on 22.08.12.
//
//

#import "KKSelectableNodeCell.h"
#import "UIColor+CustomColors.h"
#import <QuartzCore/QuartzCore.h>

@implementation KKSelectableNodeCell

-(void) setSelected:(BOOL)selected
{
  self.layer.borderColor = [UIColor selectedColor].CGColor;
  
  if(selected)
    self.layer.borderWidth = 4.0f;
  else
    self.layer.borderWidth = 0.0f;
}

@end
