//
//  KKResourceCollectionViewCell.h
//  Kulturkiosk
//
//  Created by Rune Botten on 21.08.12.
//
//

#import "KKResourceCell.h"
#import "KKTopAlignImageView.h"

@interface KKResourceCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet KKTopAlignImageView *image;
@end
