//
//  KKResourceCreditView.h
//  Kulturkiosk
//
//  Created by Rune Botten on 04.10.12.
//
//

#import <UIKit/UIKit.h>

@interface KKResourceCreditView : UIView

@property (weak, nonatomic) IBOutlet UILabel *descsLabel, *creditsLabel;
// Size of the "i" handle, used to open and close the view
-(CGSize) handleSize;

-(void) hide;
-(void) show;

@end
