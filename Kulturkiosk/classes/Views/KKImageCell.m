//
//  KKImageCell.m
//  Kulturkiosk
//
//  Created by Rune Botten on 11.12.12.
//
//

#import "KKImageCell.h"

@implementation KKImageCell {
    UIImageView *_thumbImageView;
}

- (void)awakeFromNib {
    _thumbImageView = [[UIImageView alloc] initWithFrame:self.frame];
    _thumbImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    [self addSubview:_thumbImageView];
}

-(BOOL) zoomEnabled {
  return NO;
}

-(void) setZoomEnabled:(BOOL)zoomEnabled
{
//  self.imageView.zoomEnabled = zoomEnabled;
}

- (void)updateWithImageFile:(File *)imageFile {
    _imageFile = imageFile;
    
    UIImage *img = [UIImage imageWithContentsOfFile:[imageFile pathToThumbnailItem]];
    
    _thumbImageView.image = img;
}

@end
