//
//  KKResourceCreditView.m
//  Kulturkiosk
//
//  Created by Rune Botten on 04.10.12.
//
//

#import "KKResourceCreditView.h"
#import "Credit.h"

@interface KKResourceCreditView() {
  BOOL isOpen, isHidden;
}

@property (weak, nonatomic) IBOutlet UIView *handle;

@end

@implementation KKResourceCreditView

-(id) initWithCoder:(NSCoder *)aDecoder
{
  self = [super initWithCoder:aDecoder];
  if(self) {
    isOpen = NO;
    isHidden = YES;
  }
  return self;
}

-(void) hide
{
  if(!isOpen && isHidden)
    return;

  CGRect newFrame = self.frame;
  newFrame.origin.x = -self.frame.size.width;
  self.frame = newFrame;

  isOpen = NO;
  isHidden = YES;
}

-(void) show
{
  if(!isOpen && !isHidden)
    return;
  
  CGRect newFrame = self.frame;
  newFrame.origin.x = -self.frame.size.width + self.handleSize.width;
// TODO: only temporary fix until we found most appropriate solution for layout issue:
    
    if (newFrame.origin.y == 1000) {
        newFrame.origin.y = 62;
    }
  
    
  self.frame = newFrame;

  isOpen = NO;
  isHidden = NO;
}

-(void) open
{
  if(isOpen)
    return;
  
  [UIView animateWithDuration:0.2 animations:^{
    CGRect newFrame = self.frame;
    newFrame.origin.x = 0;
    self.frame = newFrame;
  } completion:^(BOOL finished) {
    isOpen = YES;
    isHidden = NO;
    //self.handle.backgroundColor = [UIColor blackColor];
  }];
}

-(void) close
{
  if(!isOpen)
    return;
  
  [UIView animateWithDuration:0.2 animations:^{
    CGRect newFrame = self.frame;
    newFrame.origin.x = -self.frame.size.width + self.handleSize.width;
    self.frame = newFrame;
  } completion:^(BOOL finished) {
    isOpen = NO;
    //self.handle.backgroundColor = [UIColor grayColor];
  }];
}

-(IBAction)openClose
{
  if(isOpen)
    [self close];
  else
    [self open];
}

-(CGSize) handleSize
{
  return self.handle.frame.size;
}

@end
