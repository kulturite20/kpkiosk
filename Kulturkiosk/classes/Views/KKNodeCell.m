//
//  KKNodeCell.m
//  Kulturkiosk
//
//  Created by Martin Jensen on 16.07.12.
//
//

#import "KKNodeCell.h"

@implementation KKNodeCell


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
@synthesize title, accessoryLabel;
@synthesize image;

@end
