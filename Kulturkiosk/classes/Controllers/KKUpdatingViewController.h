//
//  KKUpdatingViewController.h
//  Kulturkiosk
//
//  Created by Roman Ivchenko on 4/28/14.
//
//

#import <UIKit/UIKit.h>

@interface KKUpdatingViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *contentView;

@end
