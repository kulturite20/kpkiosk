//
//  KKUpdatingViewController.m
//  Kulturkiosk
//
//  Created by Roman Ivchenko on 4/28/14.
//
//

#import "KKUpdatingViewController.h"

@interface KKUpdatingViewController ()

@end

@implementation KKUpdatingViewController {
    __weak IBOutlet UILabel *_updatingContentLabel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _updatingContentLabel.font  = [UIFont fontWithName:@"MyriadPro-Cond"
                                                  size:27];
}

@end
