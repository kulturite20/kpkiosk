//
//  KKTextContentViewController.h
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 29.05.12.
//  Copyright (c) 2012 Nordaaker Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Content.h"
#import "DMLazyScrollView.h"
#import "KKGalleryViewController.h"

@interface KKTextContentViewController : KKGalleryViewController <UIWebViewDelegate, DMLazyScrollViewDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet DMLazyScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;


@end
