//
//  KKTimeLineViewController2.m
//  Kulturkiosk
//
//  Created by Rune Botten on 06.09.12.
//
//

#import "KKTimeLineViewController.h"
#import "KKNavigationViewController.h"
#import "KKTimeLineCell.h"
#import "KKNodeViewController.h"
#import "KKNavBarController.h"
#import "KKContainerViewController.h"
#import "KKItemNavigationViewController.h"
#import "KKVideoGalleryViewController.h"
#import "KKImageGalleryViewController.h"
#import "KKTextContentViewController.h"
#import "UIColor+HexColor.h"
#import <QuartzCore/QuartzCore.h>
#import "KKLanguage.h"
#import "KKRecordContainerViewController.h"
#import "KKRecordPageViewController.h"

#define SPLIT_INSET 0
#define VIEWPORT CGRectMake(0,0,1024,708)
#define NAV_CONTROLLER_RECT CGRectMake(SPLIT_INSET, 0, VIEWPORT.size.width - (SPLIT_INSET*2), VIEWPORT.size.height)

@interface KKTimeLineViewController ()
<
UIAlertViewDelegate,
KKRecordContainerDelegate
>

@property (assign, nonatomic) BOOL warningDisplayed;
@property (strong, nonatomic) UIViewController *nodeViewController;
@property (strong, nonatomic) UIScrollView *backgroundView, *middleLayerView;
@property (strong, nonatomic) UIImageView *backgroundImageView, *middleLayerImageView;
@property (strong, nonatomic) UISwipeGestureRecognizer *closeSwipeLeft, *closeSwipeRight;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@property (strong, nonatomic) NSIndexPath *splitIndexPath;

@property (strong, nonatomic) NSMutableArray *recordsContent;

- (void)updateAnnotationForCell: (KKTimeLineCell*)cell withIndexPath: (NSIndexPath*) path;

@end



@implementation KKTimeLineViewController

#pragma mark - KKNavBarDelegate

-(void) goBack
{
  if(!self.navigationController)
    return;
  
  if([self.navigationController.viewControllers count] == 1)
    [self restoreScreen];
  else
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Custom methods
-(void) languageChange
{
  [self prepareContent];
  [self.collectionView reloadData];
}

-(void) prepareContent {
    self.recordsContent = [NSMutableArray new];

    [[self.presentation records] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Record *record = (Record *)obj;
        RecordDescription *recordDesc = [record recordDescriptionWithPresentation:self.presentation];
        record.position = [recordDesc.position integerValue];
    }];
    NSArray *sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"position"
                                                               ascending:YES]];
    NSArray *sortedRecords = [[self.presentation records] sortedArrayUsingDescriptors:sortDescriptors];
    for (Record *record in sortedRecords) {
        if ([record contentForLanguage:[KKLanguage currentLanguage]]) {
            [self.recordsContent addObject:[record contentForLanguage:[KKLanguage currentLanguage]]];
        }
    }
}

- (UIImageView*) imageViewWithImage:(UIImage *)image {

  UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
  [imageView setClipsToBounds:NO];
  [imageView setContentMode:UIViewContentModeTopLeft];
  
  return imageView;
}

- (UIScrollView*) scrollViewWithImageView:(UIImageView*) imageView {
    
  UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:VIEWPORT];
  scrollView.userInteractionEnabled = NO;
  [scrollView addSubview:imageView];
    
  return scrollView;
}

-(void) addBackground
{
  UIImage *image = [self.presentation timelineBackground];
  
  if(!image)
    return;
  
  self.backgroundImageView = [self imageViewWithImage:image];
  self.backgroundView      = [self scrollViewWithImageView:self.backgroundImageView];
  CGSize contentSize = self.backgroundImageView.frame.size;
  self.backgroundView.contentSize = contentSize;
  [self.view insertSubview:self.backgroundView belowSubview:self.collectionView];
}

-(void) addMiddleLayer
{
  UIImage *image = [self.presentation timelineInterLayer];
  
  if (!image)
    return;
  
  self.middleLayerImageView = [self imageViewWithImage:image];
  self.middleLayerView      = [self scrollViewWithImageView:self.middleLayerImageView];
  
  // Match the background image
  CGSize contentSize = self.middleLayerImageView.frame.size;
  
  self.middleLayerView.contentSize = contentSize;
  [self.view insertSubview:self.middleLayerView aboveSubview:self.backgroundView];
}

#pragma mark - UIViewController

- (void)dealloc {
  [[NSNotificationCenter defaultCenter] removeObserver:self];
  [self.collectionView removeObserver:self forKeyPath:@"contentSize"];
}

- (void)viewDidLoad {
  [super viewDidLoad];
    
    self.collectionView.backgroundColor = [UIColor clearColor];
    
  //Remodelling
 // self.rootNode = [Nodes rootNode];
  
  [self prepareContent];
  
  // Always add background first. Crappy code.
  [self addBackground];
  [self addMiddleLayer];
  
  // Observers
  /*[[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(resetViewState)
                                               name:TimerDidEndNotification
                                             object:nil];*/
  
  [self.collectionView addObserver:self
                        forKeyPath:@"contentSize"
                           options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
                           context:nil];
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(languageChange)
                                               name:@"languageChange"
                                             object:nil];
    
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.presentation.isDefault && UIAppDelegate.needToShowDefaultRecord && [self.presentation defaultRecord]) {
        NSInteger defaultRecordPosition = [self.presentation defaultRecord].position;
        NSIndexPath *defaultRecordIndexPath = [NSIndexPath indexPathForItem:defaultRecordPosition inSection:0];
        [self.collectionView scrollToItemAtIndexPath:defaultRecordIndexPath
                                    atScrollPosition:UICollectionViewScrollPositionRight
                                            animated:YES];
        [self collectionView:self.collectionView didSelectItemAtIndexPath:defaultRecordIndexPath];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.view layoutSubviews];
    [UIAppDelegate startIdleTimers];
}

#pragma mark - UIDeviceOrientation

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    return orientation;
}

- (BOOL) shouldAutorotate {
    return  YES;
}

#pragma mark - TimerActions

- (void)resetViewState {
//    KKRecordContainerViewController *recordContainer = (KKRecordContainerViewController *)[self presentedViewController];
//    [recordContainer dismissViewControllerAnimated:YES
//                                        completion:NULL];
    /*
    [recordContainer dismissViewControllerAnimated:YES
                                        completion:NULL];
    for (KKRecordPageViewController *pageController in recordContainer.controllersForPages) {
        if (pageController.presentedViewController) {
            [pageController.presentedViewController dismissViewControllerAnimated:YES
                                                                       completion:NULL];
        }
    } */
    /*
    
    if (vc.presentedViewController){
        if ([vc.presentedViewController isKindOfClass:[KKVideoGalleryViewController class]]) {
            KKVideoGalleryViewController *videoGalleryController = (KKVideoGalleryViewController*)vc.presentedViewController;
            if (videoGalleryController.moviePlayer.fullscreen) {
                videoGalleryController.moviePlayer.fullscreen = NO;
            }
        }
    }
    
    [vc dismissViewControllerAnimated:YES
                           completion:^{
                               if ([[self.navigationController viewControllers] count] > 1) {
                                   [self.navigationController popToRootViewControllerAnimated:YES];
                               }
                           }];*/
    
  /*if (self.presentedViewController && [self.presentedViewController respondsToSelector:@selector(toggleNavigation)]) {
    
    KKNavBarController *navbarController = [(KKItemNavigationViewController*)self.presentedViewController navBarController];
    
    if ([navbarController languageMenuOpen]) {
      [navbarController closeLanguageMenu];
    }
    
    if ([[self.presentedViewController childViewControllers] count]) {
      UIViewController *rowVc = [[[[self.presentedViewController childViewControllers] objectAtIndex:0] childViewControllers] lastObject];
      
      if (rowVc.presentedViewController) {
        if ([rowVc.presentedViewController isKindOfClass:[KKVideoGalleryViewController class]]) {
          KKVideoGalleryViewController *videoGalleryController = (KKVideoGalleryViewController*)rowVc.presentedViewController;
          if (videoGalleryController.moviePlayer.fullscreen) {
            videoGalleryController.moviePlayer.fullscreen = NO;
          }
        }
        [rowVc dismissViewControllerAnimated:YES completion:^{
          [(KKItemNavigationViewController*)self.presentedViewController toggleNavigation];
        }];
      }else{
        [(KKItemNavigationViewController*)self.presentedViewController toggleNavigation];
      }
    }
  }*/
    
    //REF
    /*
  BOOL validNode = NO;
  
  NSNumber *default_timeline_identifier = [[NSUserDefaults standardUserDefaults] objectForKey:@"default_timeline_identifier"];
  if ([default_timeline_identifier integerValue]) {

    for (NSUInteger i = 0; i < [self.nodes count]; i++) {
      Nodes *node = self.nodes[i];
      if ([node.item_id intValue] == [default_timeline_identifier intValue]) {
        validNode = YES;
        self.selectedIndexPath = [NSIndexPath indexPathForItem:(NSInteger)i inSection:0];
        [self.collectionView scrollToItemAtIndexPath:self.selectedIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
      }
    }
  
    if ([RKObjectManager sharedManager].HTTPClient.networkReachabilityStatus != AFNetworkReachabilityStatusNotReachable) {
      if (!validNode && [self.nodes count] && !self.warningDisplayed) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ikke gyldig utstillingspunkt!" message:@"Du har valgt et utstillingspunkt som ikke er i bruk. En oversikt over gyldige utstillingspunkt finnes i EKP" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        self.warningDisplayed = YES;
      }
    }
    [self.collectionView reloadData];

  }
     */

}


#pragma mark - UICollectionViewDataSource
-(NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
  return 1;
}

-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
  return (NSInteger)[self.recordsContent count];
}

-(UICollectionViewCell*) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
  KKTimeLineCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TimeLineCell" forIndexPath:indexPath];
  Content *c = self.recordsContent[(NSUInteger)indexPath.row];

  [self updateAnnotationForCell: cell withIndexPath: indexPath];

//  Content *c = [record contentForLanguage:[KKLanguage currentLanguage]];
  cell.titleLabel.text = [NSString stringWithFormat:@" %@",[c.title stringByReplacingOccurrencesOfString:@":" withString:@":\n"]];
  cell.titleLabel.font = [UIFont fontWithName:@"MyriadPro-Bold" size:20];
  [cell.annotationView setNeedsDisplay];
  
  if ([indexPath isEqual:self.selectedIndexPath]) {
    cell.titleLabel.textColor = [UIColor blackColor];
    cell.titleLabel.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.7];
  } else {
    cell.titleLabel.textColor = [UIColor whiteColor];
    cell.titleLabel.backgroundColor = [UIColor colorWithRed:0.588 green:0.576 blue:0.549 alpha:0.270];
  }
  
  return cell;
}

- (void)updateAnnotationForCell:(KKTimeLineCell*)cell withIndexPath:(NSIndexPath *)path {
    BOOL canBeShownInOtherPresentations = NO;
    Content *content = self.recordsContent[(NSUInteger)path.row];
    for (Presentation *presentaton in [Presentation allObjects]) {
        if ([presentaton.type isEqualToString:@"map_image"]) {
            if ([[content record] recordDescriptionWithPresentation:presentaton]) {
                canBeShownInOtherPresentations = YES;
            }
        }
    }
    if (canBeShownInOtherPresentations) {
        //NSLog(@"Enabled for %@",record);
        if ([[content.record recordDescriptionWithPresentation:self.presentation] color]) {
            cell.annotationView.bgColor = [UIColor colorWithHexColor:[[content.record recordDescriptionWithPresentation:self.presentation] color]];
        }
        cell.annotationView.code = [[content.record recordDescriptionWithPresentation:self.presentation] code];
        cell.annotationView.hidden = NO;
    } else {
        cell.annotationView.hidden = YES;
    }
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    //REF
    /*
    Nodes *node = self.nodes[(NSUInteger)indexPath.row];
    KKTimeLineCell *cell = (KKTimeLineCell*)[collectionView cellForItemAtIndexPath:indexPath];
    self.splitIndexPath = indexPath;
    
    // We want to split at the dividing line in the cell. This is inset (36/2) from origin.x of the cell
    CGPoint splitPoint = cell.frame.origin;
    CGPoint contentOffset = collectionView.contentOffset;
    
    if (self.presentation.presentationFilter == KKNoFilter) {
        [self showDefaultTimelinePresentationWithNode:node
                                           splitPoint:splitPoint
                                        contentOffset:contentOffset
                                           annotation:cell.annotationView];
    } else {
        [self showFilteredPresentationWithNode:node
                                    splitPoint:splitPoint
                                 contentOffset:contentOffset
                                    annotation:cell.annotationView];
    }
     */
    Content *content = _recordsContent[indexPath.row];
    KKTimeLineCell *cell = (KKTimeLineCell*)[collectionView cellForItemAtIndexPath:indexPath];
    self.splitIndexPath = indexPath;
    
    // We want to split at the dividing line in the cell. This is inset (36/2) from origin.x of the cell
    CGPoint splitPoint = cell.frame.origin;
    CGPoint contentOffset = collectionView.contentOffset;
    
    [self showRecord:content.record
          splitPoint:splitPoint
       contentOffset:contentOffset
          annotation:cell.annotationView];
}
/*
- (void)highlightNode:(Nodes *)_node
{
  [self prepareContent];
  [self.collectionView reloadData];
  for (NSUInteger i = 0; i < [self.nodes count]; i++) {
    Nodes *node = self.nodes[i];
    if ([node.item_id intValue] == [_node.item_id intValue]) {
      self.selectedIndexPath = [NSIndexPath indexPathForItem:(NSInteger)i inSection:0];
      [self.collectionView scrollToItemAtIndexPath:self.selectedIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    }
  }
}
*/
#pragma mark - Screen-split

// Take a picture of our view
- (UIImage *) screenshot {
  UIGraphicsBeginImageContextWithOptions(VIEWPORT.size, NO, 1);
  [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return image;
}

- (void)restoreScreen {
  
  UIView *left = [self.view viewWithTag:100];
  UIView *right = [self.view viewWithTag:200];

  [left removeGestureRecognizer:self.closeSwipeLeft];
  [right removeGestureRecognizer:self.closeSwipeRight];
    
  self.closeSwipeLeft = nil;
  self.closeSwipeRight = nil;
  
  [UIView animateWithDuration:0.35
                   animations:^{
    [self.nodeViewController willMoveToParentViewController:nil];
    [self.nodeViewController.view removeFromSuperview];
    [self.nodeViewController removeFromParentViewController];
    self.nodeViewController = nil;
    
    left.frame  = CGRectOffset( left.frame,   left.frame.size.width - SPLIT_INSET, 0);
    right.frame = CGRectOffset(right.frame, -right.frame.size.width + SPLIT_INSET, 0);
    
  } completion:^(BOOL finished) {
    for(UIView *v in self.view.subviews)
      v.hidden = NO;
    
    [left removeFromSuperview];
    [right removeFromSuperview];
    
    KKTimeLineCell *cell = (KKTimeLineCell*)[self.collectionView cellForItemAtIndexPath:self.splitIndexPath];
    [self updateAnnotationForCell:cell
                    withIndexPath:self.splitIndexPath];
  }];
}

-(void) splitScreenAt:(CGPoint) point complete:(void(^)(BOOL finished)) callback
{
  CGFloat scale = [[UIScreen mainScreen] scale];
  
  // Change this to change the image used. For instance only self.backgroundImageView.image
  UIImage *image = [self screenshot];
  CGImageRef imageRef = image.CGImage;
  
  // Left side
  // Size of left side = Everything to the left of the tap, that is visible on screen
  CGSize leftSize = VIEWPORT.size;
  leftSize.width -= MAX(VIEWPORT.size.width - point.x, SPLIT_INSET);
  
  // Frame of left side starts at visible rect origin
  CGRect leftFrame = CGRectZero;
  leftFrame.origin.x = 0;
  leftFrame.origin.y = 0;
  leftFrame.size = leftSize;
  
  // Make the left side
  CGImageRef leftImageRef = CGImageCreateWithImageInRect(imageRef, leftFrame);
  UIImage *leftImage = [UIImage imageWithCGImage:leftImageRef scale:scale orientation:UIImageOrientationUp];
  CGImageRelease(leftImageRef);
  
  UIImageView *leftImageView = [[UIImageView alloc] initWithFrame:leftFrame];
  leftImageView.image = leftImage;

  // Size of right side = Everything on visible screen minus the left side
  CGSize rightSize = VIEWPORT.size;
  rightSize.width -= leftSize.width;
  rightSize.height = VIEWPORT.size.height;
  
  // Frame of right side starts at the splitLocationInFrame point
  CGRect rightFrame = CGRectZero;
  rightFrame.origin.x = point.x;
  rightFrame.size = rightSize;
  
  // Make the right side
  CGImageRef rightImageRef = CGImageCreateWithImageInRect(imageRef, rightFrame);
  UIImage *rightImage = [UIImage imageWithCGImage:rightImageRef scale:scale orientation:UIImageOrientationUp];
  CGImageRelease(rightImageRef);
  
  UIImageView *rightImageView = [[UIImageView alloc] initWithFrame:rightFrame];
  rightImageView.image = rightImage;
  
  leftImageView.tag  = 100;
  rightImageView.tag = 200;
  
    self.closeSwipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(restoreScreen)];
    self.closeSwipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    
  self.closeSwipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(restoreScreen)];
    self.closeSwipeRight.direction = UISwipeGestureRecognizerDirectionRight;
  
  // Add the images
  //TODO: Insert subviews at correct position in stack
  [self.view addSubview:leftImageView];
  [self.view addSubview:rightImageView];

  // Hide the other views, except the left, right and navbar
  for(UIView *v in self.view.subviews)
    if(v.tag != 100 && v.tag != 200 )
      v.hidden = YES;
  
  // On the screen it will appear as if nothing has happened
  // Then we animate the two images out
  [UIView animateWithDuration:0.350 animations:^{
    leftImageView.frame = CGRectOffset(leftFrame, -(leftFrame.size.width - SPLIT_INSET), 0);
    rightImageView.frame = CGRectOffset(rightFrame, (rightFrame.size.width - SPLIT_INSET), 0);
  } completion:^(BOOL finished) {
    leftImageView.userInteractionEnabled = YES;
    rightImageView.userInteractionEnabled = YES;

    [rightImageView addGestureRecognizer:self.closeSwipeLeft];
    [leftImageView addGestureRecognizer:self.closeSwipeRight];
    callback(finished);
  }];
}

#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
  if(object == self.collectionView && [keyPath isEqualToString:@"contentSize"]) {
    CGSize old = [[change valueForKey:@"old"] CGSizeValue];
    CGSize new = [[change valueForKey:@"new"] CGSizeValue];
    if(!CGSizeEqualToSize(old, new)) {
      
      // The content size of the collectionView changed, so we need to update the
      // background and middleLayer views and their imageViews if the collectionView
      // is now larger than the screen width
      
      if(new.width < VIEWPORT.size.width)
        // Its not, so just return
        return;
      
      if(self.backgroundView) {
        // Change the backgroundView to match it
        [self.backgroundView setContentSize:new];
        
        // Increase the image frame so the image gets larger
        CGRect backgroundImageViewFrame = self.backgroundImageView.frame;
        backgroundImageViewFrame.size = new;
        self.backgroundImageView.frame = backgroundImageViewFrame;
      }

      if(self.middleLayerView) {
        // Change the middleLayerView to match it
        [self.middleLayerView setContentSize:new];
        
        // Increase the image frame so the image gets larger
        CGRect middleLayerImageViewFrame = self.middleLayerImageView.frame;
        middleLayerImageViewFrame.size = new;
        self.middleLayerImageView.frame = middleLayerImageViewFrame;
      }
    }
  }
}

#pragma mark - Private

- (void)showRecord:(Record *)record
        splitPoint:(CGPoint)splitPoint
     contentOffset:(CGPoint)contentOffset
        annotation:(KKAnnotationView *)annotationView {
    
    annotationView.hidden = YES;
    
    splitPoint.x += 22;
    splitPoint.y = 0; // Y doesnt matter here
    splitPoint.x -= contentOffset.x;
    
    if (splitPoint.x < SPLIT_INSET) {
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        
        CGFloat diff = splitPoint.x - SPLIT_INSET;
        
        contentOffset.x += diff;
        splitPoint.x -= diff;
        
        [UIView animateWithDuration:0.4 animations:^{
            [self.collectionView setContentOffset:contentOffset];
        } completion:^(BOOL finished) {
            dispatch_semaphore_signal(semaphore);
        }];
        
        while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW))
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                     beforeDate:[NSDate dateWithTimeIntervalSinceNow:5]];
        
    }
    

    [self splitScreenAt:splitPoint
               complete:^(BOOL finished) {
                   KKRecordContainerViewController *recordContainer = [KKRecordContainerViewController new];
                   recordContainer.record   = record;
                   recordContainer.timeLineDelegate = self;
                   [self presentViewController:recordContainer
                                      animated:NO
                                    completion:NULL];
               }];
}

/*
-(void)showFilteredPresentationWithNode:(Nodes *)node
                             splitPoint:(CGPoint)splitPoint
                          contentOffset:(CGPoint)contentOffset
                             annotation:(KKAnnotationView *)annotationView {
    // Nav controller
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    UIViewController *viewControllerToPresent = nil;
    switch (self.presentation.presentationFilter) {
        case KKArticleFilter: {
            KKTextContentViewController *textContentView = [storyboard instantiateViewControllerWithIdentifier:@"textView"];
            textContentView.node = node;
            viewControllerToPresent = textContentView;
        }
            break;
        case KKImagesFilter: {
            NSArray *imageResources = [node resourcesWithType:KKResourceTypeImage forLanguage:[KKLanguage currentLanguage]];
            if ([imageResources count] == 0) {
                [self showDefaultTimelinePresentationWithNode:node
                                                   splitPoint:splitPoint
                                                contentOffset:contentOffset
                                                   annotation:annotationView];
                return;
            }
            KKImageGalleryViewController *imageGalleryView = [storyboard instantiateViewControllerWithIdentifier:@"imageGalleryView"];
            NSArray *sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"page_id" ascending:YES],[NSSortDescriptor sortDescriptorWithKey:@"position" ascending:YES]];
            imageGalleryView.resources = [imageResources sortedArrayUsingDescriptors:sortDescriptors];
            viewControllerToPresent = imageGalleryView;
        }
            break;
        case KKVideoFilter: {
            NSArray *videoResources = [node resourcesWithType:KKResourceTypeVideo forLanguage:[KKLanguage currentLanguage]];
            if ([videoResources count] == 0) {
                [self showDefaultTimelinePresentationWithNode:node
                                                   splitPoint:splitPoint
                                                contentOffset:contentOffset
                                                   annotation:annotationView];
                return;
            }
            KKVideoGalleryViewController *videoGalleryView = [storyboard instantiateViewControllerWithIdentifier:@"videoGalleryView"];
            videoGalleryView.resources = [videoResources sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"identifier" ascending:YES]]];
            viewControllerToPresent = videoGalleryView;
        }
            break;
            
        default:
            break;
    }
    
    [self presentViewController:viewControllerToPresent
                       animated:YES
                     completion:NULL];
}

- (void)showDefaultTimelinePresentationWithNode:(Nodes *)node
                                    splitPoint:(CGPoint)splitPoint
                                  contentOffset:(CGPoint)contentOffset
                                     annotation:(KKAnnotationView *)annotationView {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    annotationView.hidden = YES;
    
    splitPoint.x += 22;
    splitPoint.y = 0; // Y doesnt matter here
    splitPoint.x -= contentOffset.x;
    
    if (splitPoint.x < SPLIT_INSET) {
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        
        CGFloat diff = splitPoint.x - SPLIT_INSET;
        
        contentOffset.x += diff;
        splitPoint.x -= diff;
        
        [UIView animateWithDuration:0.4 animations:^{
            [self.collectionView setContentOffset:contentOffset];
        } completion:^(BOOL finished) {
            dispatch_semaphore_signal(semaphore);
        }];
        
        while (dispatch_semaphore_wait(semaphore, DISPATCH_TIME_NOW))
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                     beforeDate:[NSDate dateWithTimeIntervalSinceNow:5]];
        
    }
    
    
    [self splitScreenAt:splitPoint complete:^(BOOL finished) {
        if (node.nodeType == KKTypeFolder)
            self.nodeViewController = [storyboard instantiateViewControllerWithIdentifier:@"FolderView"];
        else if(node.nodeType == KKTypeRecord)
            self.nodeViewController = [storyboard instantiateViewControllerWithIdentifier:@"NodeViewController"];
        [self.nodeViewController.view setFrame:NAV_CONTROLLER_RECT];
        [self.view addSubview:self.nodeViewController.view];
        [self addChildViewController:self.nodeViewController];
        ((KKNodeViewController*)self.nodeViewController).node = node;
        
        [self.nodeViewController didMoveToParentViewController:self];
        
        //    int64_t delayInSeconds = 2.0;
        //    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        //    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //      [self restoreScreen];
        //    });
        
    }];
    
}
*/
#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
  if(scrollView == self.collectionView) {
    
    // Do parallax scrolling with backgroundView and middleLayerView
    
    // Copy-pasted the following two lines from previous implementation
    CGFloat parallaxRatio= scrollView.contentOffset.x / (scrollView.contentSize.width -scrollView.frame.size.width);
    CGFloat x = -self.collectionView.backgroundView.frame.size.width * parallaxRatio + (self.collectionView.frame.size.width * parallaxRatio);
    
    // Move the middleLayerView
    if(self.middleLayerView)
      self.middleLayerView.contentOffset = CGPointMake(x, 0);
    
    // Move the backgroundView twice as slow
    if(self.backgroundView)
      self.backgroundView.contentOffset = CGPointMake(x/2.0, 0);
  }
}

#pragma mark - KKRecordContainerDelegate

- (void)recordContainerDidDismiss:(KKRecordContainerViewController *)recordContainer {
    [self restoreScreen];
}

- (void)toggleNavigation {
    
}

@end
