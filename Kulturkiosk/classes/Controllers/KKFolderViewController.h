//
//  KKFolderViewController.h
//  Kulturkiosk
//
//  Created by Martin Jensen on 16.07.12.
//  Copyright (c) 2012 Nordaaker Ltd.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KKNodeViewController.h"

@interface KKFolderViewController : KKNodeViewController <UICollectionViewDataSource, UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSArray *childs;
@end
