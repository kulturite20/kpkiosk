//
//  KKNavBarController.m
//  Kulturkiosk
//
//  Created by Rune Botten on 29.08.12.
//
//

#import "KKNavBarController.h"
#import "KKLanguageViewController.h"
#import "KKLanguage.h"

@interface KKNavBarController ()

@property (strong, nonatomic) IBOutlet KKLanguageViewController *languageView;
@property (weak, nonatomic) IBOutlet UIButton *backButton, *languageButton;
@property (weak, nonatomic) IBOutlet UILabel *backButtonLabel, *overViewButtonLabel, *languageButtonLabel;
@property (assign, nonatomic) BOOL isAnimating;

- (IBAction)toggleLanguageMenu:(id)sender;
- (IBAction)toggleNavigation:(id)sender;

@end

@implementation KKNavBarController

@synthesize delegate, languageView, backButton, languageButton, backButtonLabel, languageMenuOpen, navButton;

-(void) toggleNavigation:(id)sender
{
  if(self.languageMenuOpen)
    [self closeLanguageMenu];
  
  if(self.delegate && [(NSObject*)self.delegate respondsToSelector:@selector(toggleNavigation)])
    [self.delegate toggleNavigation];
}

-(bool) backEnabled
{
  return self.backButton.enabled;
}

- (void) setBackEnabled:(BOOL) state
{
  self.backButton.enabled = state;
  self.backButtonLabel.enabled = state;
}

-(void) closeLanguageMenu
{ 
  
  if(!self.languageView || self.isAnimating)
    return;
  
  self.languageButton.selected = NO;
  [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
    CGRect newFrame = CGRectOffset(self.languageView.view.frame, self.languageView.view.frame.size.width, 0);
    self.languageView.view.frame = newFrame;
    self.isAnimating = YES;
  } completion:^(BOOL finished) {
    self.isAnimating = NO;
    [self.languageView.view removeFromSuperview];
    self.languageView = nil;
    
  }];
  self.languageMenuOpen = NO;
}

- (IBAction)toggleLanguageMenu:(id)sender {
  
  if ([sender isKindOfClass:[NSNotification class]]) {
    UIEvent *event = (UIEvent*)[[sender userInfo] objectForKey:@"event"];
    UITouch *touch = [[event allTouches] anyObject];
    
    if (![touch.view isEqual:self.languageView.view] && ![touch.view isEqual:self.languageButton]) {
      [self closeLanguageMenu];
      return;
    }
  
  }
    
  UIButton *langBtn = (UIButton*)sender;
  
  if (self.isAnimating) return;
  
  if(self.languageMenuOpen) {
    // Extracted this method so segues and other things can close it.
    // The button state will be handled there aswell.
    [self closeLanguageMenu];
    
  } else {
    
    langBtn.selected = YES;
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    self.languageView = (KKLanguageViewController *)[storyBoard instantiateViewControllerWithIdentifier:@"§"];
    
    CGRect frame = self.languageView.view.frame;
    frame.origin.x = CGRectGetWidth(self.view.frame);
    self.languageView.view.frame = frame;
    
    [[self.view superview] insertSubview:self.languageView.view belowSubview:self.view];
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
      self.isAnimating = YES;
      self.languageView.view.frame = CGRectOffset(frame, -CGRectGetWidth(frame), 0);
      
    } completion:^(BOOL finished) {
      self.isAnimating = NO;
    }];
    
    self.languageMenuOpen = YES;
  }
}

-(IBAction) goBack:(id)sender
{
  if(self.delegate && [(NSObject*)self.delegate respondsToSelector:@selector(goBack)])
    [self.delegate goBack];
}

-(void) localize
{
  self.backButtonLabel.text     = [KKLanguage localizedString:@"Back"];
  self.overViewButtonLabel.text = [KKLanguage localizedString:@"Overview"];
  self.languageButtonLabel.text = [KKLanguage localizedString:@"Language"];
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  [self localize];
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(localize)
                                               name:@"languageChange"
                                             object:nil];
  
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeLanguageMenu) name:ApplicationShouldHideLanguageMenu object:nil];

  self.languageMenuOpen = NO;
  
}

- (void)dealloc{
  
  [[NSNotificationCenter defaultCenter] removeObserver:self name:ApplicationShouldHideLanguageMenu object:nil];
  [[NSNotificationCenter defaultCenter] removeObserver:self name:@"languageChange" object:nil];
  
}

@end
