//
//  KKImageGalleryViewController.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 29.05.12.
//  Copyright (c) 2012 Nordaaker Ltd. All rights reserved.
//

#import "KKImageGalleryViewController.h"
#import "File.h"
#import "Resource.h"
#import "Credit.h"
#import "KKDataManager.h"

#import "KKTopAlignImageView.h"
#import "PhotoViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface KKImageGalleryViewController () <UIPageViewControllerDataSource, UIPageViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *thumbnailContainer;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (assign) CGRect originalImageContainerSize;
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (assign) BOOL fullscreen;

- (IBAction)goBack:(id)sender;
@end

@implementation KKImageGalleryViewController

#pragma mark UIPageViewControllerDelegate

- (void)pageViewController:(UIPageViewController *)pageViewController
        didFinishAnimating:(BOOL)finished
   previousViewControllers:(NSArray *)previousViewControllers
       transitionCompleted:(BOOL)completed
{
    PhotoViewController *vc = pageViewController.viewControllers[0];
    [vc resetZoom];
    vc.view.userInteractionEnabled = self.fullscreen;
    [self didScrollToPage:(NSInteger)vc.pageIndex];
    
}

- (PhotoViewController *)photoViewControllerForPage:(NSUInteger) pageIndex {
    PhotoViewController *p = [[PhotoViewController alloc] initWithPageIndex:pageIndex];
    NSString *imagePath = [(NSDictionary*)[self.resourceDetails objectAtIndex:p.pageIndex] objectForKey:@"original"];
    p.image = [UIImage imageWithContentsOfFile:imagePath];
    p.view.userInteractionEnabled = NO;
    return p;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pvc
      viewControllerBeforeViewController:(PhotoViewController *)vc
{
    NSInteger index = vc.pageIndex;
    vc = nil;

  if(index > 0) {
    return [self photoViewControllerForPage:index-1];
  }
  
    return [self photoViewControllerForPage:[self.resourceDetails count]-1];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pvc
       viewControllerAfterViewController:(PhotoViewController *)vc
{
    NSInteger index = vc.pageIndex;
    vc = nil;

    if(index < [self.resourceDetails count] - 1) {
        return [self photoViewControllerForPage:index +1];
    }
    
    return [self photoViewControllerForPage:0];
}

#pragma mark View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.fullscreen = NO;
    
    self.pageViewController = [[UIPageViewController alloc]
                               initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll
                               navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
                                             options:nil];
  
  self.pageViewController.dataSource = self;
  self.pageViewController.delegate = self;
  
  [self.pageViewController willMoveToParentViewController:self];
  [self addChildViewController:self.pageViewController];
  CGRect frame=self.imageContainer.frame;
  frame.origin.x=frame.origin.y=0;
  self.pageViewController.view.frame=frame; 
  
  [self.imageContainer addSubview:self.pageViewController.view];
  
  
  UITapGestureRecognizer *galleryTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(galleryTap:)];
  galleryTap.numberOfTouchesRequired = 1;
  [self.imageContainer addGestureRecognizer:galleryTap];
  
  UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(galleryPinch:)];
  [self.imageContainer addGestureRecognizer:pinchGesture];
  
  [self addObserver:self
         forKeyPath:@"fullscreen"
            options:NSKeyValueObservingOptionNew
            context:nil];
}

-(void) galleryPinch:(UIPinchGestureRecognizer*) gesture
{
    if(gesture.scale > 1 && !self.fullscreen)
        self.fullscreen = YES;
}

-(void) galleryTap:(UITapGestureRecognizer*) gesture
{
    // Toggle fullscreen. Handled in observeValueForKeyPath:ofObject:change:context
    self.fullscreen = !self.fullscreen;
}

-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if([keyPath isEqualToString:@"fullscreen"]) {
        if(self.fullscreen) {
            [self.imageContainer setFrame:CGRectMake(0, 0, 1024, 768)];
            [self.imageContainer setBackgroundColor:[UIColor blackColor]];
            [self hideInfo];
        } else {
            [self.imageContainer setFrame:self.originalImageContainerSize];
            [self.imageContainer setBackgroundColor:[UIColor clearColor]];
        }
        
        for(PhotoViewController *vc in self.pageViewController.viewControllers) {
            [vc resetZoom];
            vc.view.userInteractionEnabled = self.fullscreen;
        }
        
        [self showInfo:self.currentIndex];
    }
}

-(void) setupCreditView {
    NSArray *array = [[UINib nibWithNibName:@"KKResourceCreditView" bundle:nil] instantiateWithOwner:self options:nil];
    for(id obj in array) {
        if([obj class] == [KKResourceCreditView class]) {
            self.infoView = (KKResourceCreditView*)obj;
        }
    }
    
    if(self.infoView) {
        CGRect creditFrame = self.infoView.frame;
        // Hidden initially
        creditFrame.origin.x = -creditFrame.size.width;
        self.infoView.frame = creditFrame;
        [self.imageContainer addSubview:self.infoView];
    }
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.originalImageContainerSize = self.imageContainer.frame;
    
    [self setupCreditView];
    
	// Do any additional setup after loading the view
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    for (File *file in self.resources) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        
        Content *content = [file contentForLanguage:[KKLanguage currentLanguage]];
    
        // Calculating label size for credits.
        NSMutableString *allCreditsString = [@"" mutableCopy];
        for (Credit *credit in file.credits) {
            if (credit.name.length > 0) {
                [allCreditsString appendFormat:@"%@: %@\n", [[KKDataManager sharedManager] localizedCreditTypeForTitle:credit.credit_type], credit.name];
            }
        }
        
        [dict setObject:allCreditsString
                 forKey:@"credit"];
        if (content) {
            [dict setObject:content.desc ? content.desc : @""
                     forKey:@"description"];
            [dict setObject:content.title ? content.title : @""
                     forKey:@"title"];
        }
        
        NSString *thumbImagePath = [file pathToThumbnailItem];
        NSString *originalImagePath = [file pathToOriginalItem];

        if(thumbImagePath && originalImagePath) {
            [dict setObject:thumbImagePath forKey:@"thumbnail"];
            [dict setObject:originalImagePath forKey:@"original"];
            [tempArray addObject:dict];
        }
    }
    
    if([tempArray count] < 1) {
        return;
    }
    
    self.resourceDetails = [NSArray arrayWithArray:tempArray];
    
    PhotoViewController *first = [self photoViewControllerForPage:0];
    
    [self.pageViewController setViewControllers:@[first]
                                      direction:UIPageViewControllerNavigationDirectionForward
                                       animated:NO
                                     completion:NULL];
    
    // BEGIN
    CGSize thumbnailContainerSize = CGSizeMake(106, 106);
    CGRect thumbnailContainerFrame = CGRectMake(0, 0, thumbnailContainerSize.width, thumbnailContainerSize.height);
    
    for( NSUInteger i = 0; i < [self.resourceDetails count]; i++) {
        id object = [self.resourceDetails objectAtIndex:i];
        UIView *container = [[UIView alloc] initWithFrame:thumbnailContainerFrame];
        
        KKTopAlignImageView *imageView = [[KKTopAlignImageView alloc] initWithFrame:CGRectInset(thumbnailContainerFrame, 8, 8)];
        imageView.image = [UIImage imageWithContentsOfFile:[object objectForKey:@"thumbnail"]];
        
        [self.thumbnailContainer addSubview:imageView];
        
        // Move the frame to the right
        thumbnailContainerFrame = CGRectOffset(thumbnailContainerFrame, thumbnailContainerFrame.size.width, 0);
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTap:)];
        [container addGestureRecognizer:tapGesture];
        [container setUserInteractionEnabled:YES];
        container.tag = i;
        
        [self.thumbViews addObject:container];
        [self.thumbnailContainer addSubview:container];
    }
    
    // END
    
    if([self.resourceDetails count]) {
        [self selectThumb:0];
        
        CGSize contentSize = CGSizeMake(thumbnailContainerFrame.origin.x, self.thumbnailContainer.frame.size.height);
        
        [self.thumbnailContainer setContentSize:contentSize];
    }
    self.titleLabel.text = self.resourceDetails[0][@"title"];
    [self.titleLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:48]];
    
    [self.spinner stopAnimating];
}

-(void) dealloc
{
    [self removeObserver:self
              forKeyPath:@"fullscreen"];
    [self setThumbnailContainer:nil];
    [self setTitleLabel:nil];
    [self setImageContainer:nil];
    [self setImageContainer:nil];
    [self setResourceDetails:nil];
    
    [self.pageViewController willMoveToParentViewController:nil];
    [self.pageViewController removeFromParentViewController];
    self.pageViewController = nil;
}

- (IBAction)goBack:(id)sender {
    [self dismissViewControllerAnimated:YES
                             completion:NULL];
}

-(void)didScrollToPage:(NSInteger)page {
    self.titleLabel.text = self.resourceDetails[page][@"title"];
    [self selectThumb:page];
}

- (void)didTap:(UITapGestureRecognizer *)tapGesture
{
    [super didTap:tapGesture];
    
    NSInteger index = [self indexOfItemView:tapGesture.view];
    
    //Scroll to that index
    self.titleLabel.text = self.resourceDetails[index][@"title"];
    [self.pageViewController setViewControllers:@[[self photoViewControllerForPage:(NSUInteger)index]]
                                      direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    NSLog(@"ImageGallery received a memory warning");
}

@end