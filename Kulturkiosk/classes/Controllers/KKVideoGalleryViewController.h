//
//  KKVideoGalleryViewController.h
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 29.05.12.
//  Copyright (c) 2012 Nordaaker Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

#import "KKGalleryViewController.h"

@interface KKVideoGalleryViewController : KKGalleryViewController

- (void)goBackWithCompletionBlock:(void (^)())completionBlock;

@end