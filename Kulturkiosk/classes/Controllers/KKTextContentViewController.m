//
//  KKTextContentViewController.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 29.05.12.
//  Copyright (c) 2012 Nordaaker Ltd. All rights reserved.
//

#import "KKTextContentViewController.h"
#import "KKResourceCreditView.h"
#import "GRMustache.h"
#import "Credit.h"
#import "ImageScrollView.h"

@interface KKImageRenderObject : NSObject
@property (nonatomic, strong) NSString *urlString;
@property (nonatomic, strong) NSArray *credits, *descs;
@end

@implementation KKImageRenderObject
@end

@interface KKRenderObject : NSObject
@property (nonatomic, strong) NSString *title, *ingress, *body;
@property (nonatomic, strong) NSArray *images;
@end

@implementation KKRenderObject
@end

@interface NSString (containsCategory)
- (BOOL) containsString: (NSString*) substring;
@end

@implementation NSString (containsCategory)
- (BOOL) containsString: (NSString*) substring {
    NSRange range = [self rangeOfString : substring];
    BOOL found = ( range.location != NSNotFound );
    return found;
}
@end

@interface KKTextContentViewController () {
  int numPages, numPagesLoaded;
}

- (IBAction)goBack:(id)sender;
//- (IBAction)changePage:(id)sender;

@end

@implementation KKTextContentViewController {
    __weak IBOutlet UIView *_imageContainer;
                    KKResourceCreditView   *_infoView;
    
                    NSMutableArray         *_imageDataSource;
                    NSMutableArray         *_htmlDataSource;
                    NSMutableArray         *_pageContainers;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.pageControl.numberOfPages = numPages = numPagesLoaded = 0;
    self.pageControl.currentPage = 0;

    _imageContainer.hidden = YES;
    [self setupCreditView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //REF
    /*
    NSArray *pages = [self.node pagesForLanguage:[KKLanguage currentLanguage]];
     
    if([pages count] == 0) {
        [self.spinner stopAnimating];
        return;
    }
    // Loading all data that we need for displaying pages
    [self loadDataForPages:pages];
    
    // DMLazyScrollView data source & delegate
    __weak __typeof(&*self)weakSelf = self;
    self.scrollView.dataSource = ^(NSUInteger index) {
        return [weakSelf presentationForPageAtIndex:index];
    };
    self.scrollView.numberOfPages = [_htmlDataSource count];
    self.scrollView.controlDelegate = self;
   
    // Setting number of pages for page controller to show pages indication
    self.pageControl.numberOfPages = [_htmlDataSource count];
     */
}

#pragma mark - UIGestureRecognizer Delegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

#pragma mark - UIWebViewController Delegate

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    UITapGestureRecognizer *imageTap = [[UITapGestureRecognizer alloc]
                                        initWithTarget:self
                                        action:@selector(imageTappedWithRecognizer:)];
    imageTap.delegate = self;
    
    [webView addGestureRecognizer:imageTap];
    webView.userInteractionEnabled = YES;
    webView.opaque = YES;
}

#pragma mark - UIScrollViewController Delegate

- (void)lazyScrollView:(DMLazyScrollView *)pagingView currentPageChanged:(NSInteger)currentPageIndex {
    self.pageControl.currentPage = currentPageIndex;
}

//- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
//        return [self.scrollView.subviews objectAtIndex:[self currentPage]];
//}

//-(void) scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
//{
//  if(scrollView != self.scrollView)
//    return;
//
//  if(scale == 1.0) {
//    for(UIView *v in self.scrollView.subviews)
//      v.hidden = NO;
//    self.scrollView.pagingEnabled = YES;
//  }
//}

#pragma mark - Callbacks

- (void)dismissMaximizedImage:(UIButton *)sender {
    [sender removeFromSuperview];
    [UIView animateWithDuration:0.3 animations:^{
        _imageContainer.alpha = 0.0f;
    } completion:^(BOOL finished) {
        _imageContainer.hidden  = YES;
        [[[_imageContainer subviews] lastObject] removeFromSuperview];
    }];
    [_infoView hide];
}

- (void)imageTappedWithRecognizer:(UIGestureRecognizer *)recognizer {
    UIWebView  *webView = (UIWebView *)recognizer.view;
    
    CGPoint     point   = [recognizer locationInView:webView];
    NSString   *imgURL  = [NSString stringWithFormat:
                           @"document.elementFromPoint(%f,%f).src", point.x, point.y];
    
    NSString   *urlToSave  = [webView stringByEvaluatingJavaScriptFromString:imgURL];
    NSURL      *imageURL   = [NSURL URLWithString:urlToSave];
    NSString   *imagePath  = [imageURL relativePath];
    
    if (imagePath) {
        NSData *imageData  = [NSData dataWithContentsOfURL:[self pathToOriginalImageWithURL:imageURL]];
        UIImage *image     = [UIImage imageWithData:imageData];
        [self setupFullScreenWithImage:image];

        [UIView animateWithDuration:0.3 animations:^{
            _imageContainer.alpha = 1.0f;
        } completion:^(BOOL finished) {
            [self showInfoWithImageURL:imagePath];
        }];
    }
}

#pragma mark - Private

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
  //  UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)setupFullScreenWithImage:(UIImage *)image {
    ImageScrollView *scrollView = [[ImageScrollView alloc] initWithFrame:_imageContainer.frame];
    scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    scrollView.image = image;
    [self.view addSubview:[self closeButton]];
    
    if (scrollView.image.size.height < 1046)
    {
        CGFloat scaleFactor = 1046 / image.size.height;
        CGSize imageSize = CGSizeMake(floorf(image.size.width*scaleFactor), floorf(image.size.height*scaleFactor));
        scrollView.image = [self imageWithImage:image scaledToSize:imageSize];
    }
    
    [_imageContainer addSubview:scrollView];
    _imageContainer.hidden = NO;
}

- (NSURL *)pathToOriginalImageWithURL:(NSURL *)url {
    NSString *pathToLibrary = [NSSearchPathForDirectoriesInDomains(
                                                                   NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *path = [pathToLibrary stringByAppendingString:@"/Caches/DownloadedMedia/"];
    
    NSString *smallImageName  = [[url lastPathComponent] stringByDeletingPathExtension];
    NSString *originalImageID = [[smallImageName componentsSeparatedByString:@"-"][0] stringByAppendingString:@"-original"];
    
    NSError *error;
    NSArray *imagesInFolder = [[NSFileManager defaultManager]
                               contentsOfDirectoryAtPath:path
                               error:&error];
    if (error) {
        NSLog(@"Error: %@", [error localizedDescription]);
        return nil;
    }
    
    NSString *originalImageName;
    for (NSString *imageName in imagesInFolder) {
        if ([imageName containsString:originalImageID]) {
            originalImageName = imageName;
        }
    }
    if (originalImageName) {
        NSString *originalImageURL = [path stringByAppendingString:originalImageName];
        return [NSURL fileURLWithPath:originalImageURL];
    } else {
        return nil;
    }
}

- (IBAction)goBack:(id)sender {
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

//- (IBAction)changePage:(id)sender {
//    int x = self.pageControl.currentPage * (int)self.scrollView.frame.size.width;
//    [self.scrollView setContentOffset:CGPointMake(x, 0) animated:YES];
//}

- (void)setupCreditView {
    NSArray *array = [[UINib nibWithNibName:@"KKResourceCreditView" bundle:nil] instantiateWithOwner:self options:nil];
    for(id obj in array) {
        if([obj class] == [KKResourceCreditView class]) {
            _infoView = (KKResourceCreditView*)obj;
        }
    }
    
    if(_infoView) {
        CGRect creditFrame = _infoView.frame;
        // Hidden initially
        creditFrame.origin.x = -creditFrame.size.width;
        _infoView.frame = creditFrame;
        [self.view addSubview:_infoView];
    }
}

- (void)showInfoWithImageURL:(NSString *)path {
    _infoView.creditsLabel.hidden = YES;
    
    for (KKImageRenderObject *obj in _imageDataSource) {
        if ([obj.urlString isEqualToString:path]) {
            if ([obj.descs count] == 0) {
                _infoView.descsLabel.text = @"";
                break;
            }
            //REF
            /*
            Description *description = obj.descs[0];
            _infoView.descsLabel.text = description.desc;
             */
        }
    }
    if ([_infoView.descsLabel.text length] > 0) {
        [_infoView show];
    } else {
        [_infoView hide];
    }
}

//- (NSUInteger) currentPage {
//    return (NSUInteger) round(self.scrollView.contentOffset.x / 1024);
//}

- (UIButton *)closeButton {
    
    UIImage *closeButtonImage = [UIImage imageNamed:@"kryss-icon"];
    UIButton *closeButton = [[UIButton alloc] initWithFrame:
                             CGRectMake(972.0f,14.0f,
                                        closeButtonImage.size.width,
                                        closeButtonImage.size.height)];
    [closeButton setImage:closeButtonImage
                 forState:UIControlStateNormal];
    [closeButton addTarget:self
                    action:@selector(dismissMaximizedImage:)
          forControlEvents:UIControlEventTouchUpInside];
    
    return closeButton;
}

#pragma mark - Dealloc

- (void)dealloc {

}

- (void)loadDataForPages:(NSArray *)pages {
    
    _imageDataSource = [NSMutableArray new];
    _pageContainers  = [NSMutableArray new];
    _htmlDataSource = [NSMutableArray new];

//    NSString *template = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"article" ofType:@"html"] encoding:NSUTF8StringEncoding error:nil];
    //REF
    /*
    for(Content *page in pages) {
        KKRenderObject *renderObj = [[KKRenderObject alloc] init];
        renderObj.title = page.title;
        renderObj.ingress = page.ingress;
        renderObj.body = page.body;
        
        NSArray *images = [page resourcesWithType:KKResourceTypeImage];
        NSMutableArray *array = [NSMutableArray arrayWithCapacity:[images count] ];
        for(Resource *image in images) {
            File *file = [image imageWithFormat:KKFormatSmall];
            if(file) {
                KKImageRenderObject *imageRenderObject = [[KKImageRenderObject alloc] init];
                
                NSMutableArray *credits = [NSMutableArray arrayWithCapacity:[image.credits count]];
                
                for(Credit *c in [image.credits allObjects]) {
                    [credits addObject:[c localizedCredit]];
                }
                
                imageRenderObject.urlString = file.localUrl;
                imageRenderObject.descs = [image descriptionsForLanguage:[KKLanguage currentLanguage]];
                imageRenderObject.credits = credits;
                
                [array addObject:imageRenderObject];
                [_imageDataSource addObject:imageRenderObject];
            }
        }
        renderObj.images = array;
        
        NSError *error = nil;
        NSString *rendering = [GRMustacheTemplate renderObject:renderObj
                                                    fromString:template
                                                         error:&error];
        [_htmlDataSource addObject:rendering];
        // Array that will store containers for displaying pages
        [_pageContainers addObject:[NSNull null]];
    }
    [self.spinner stopAnimating];
    [self.scrollView setHidden:NO];
    _imageContainer.backgroundColor = [UIColor blackColor];
     */
}

- (UIViewController *)presentationForPageAtIndex:(NSInteger)index {
    id existingContainer = [_pageContainers objectAtIndex:index];
    if (existingContainer == [NSNull null]) {
        UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 1024, 768)];
        
        webView.delegate = self;
        webView.backgroundColor = [UIColor clearColor];
        webView.opaque = NO;

        [webView loadHTMLString:_htmlDataSource[index]
                    baseURL:[NSURL fileURLWithPath:@"/"]];
        
        // Initializing UIViewController as container for UIWebView, and storing it in the containers array, in order to not create container twice
        UIViewController *container = [[UIViewController alloc] init];
        [container.view addSubview:webView];
        [_pageContainers replaceObjectAtIndex:index withObject:container];
        
        return container;
    }
    return existingContainer;
}

@end
