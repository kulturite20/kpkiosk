//
//  KKRecordCollectionViewController.m
//  Kulturkiosk
//
//  Created by Rune Botten on 13.08.12.
//
//

#import "KKTreeContentRowController.h"

#import "KKTextContentViewController.h"
#import "KKImageGalleryViewController.h"
#import "KKVideoGalleryViewController.h"
#import "KKResourceCollectionViewCell.h"

#import "File.h"
#import "Resource.h"

@implementation KKTreeContentRowController


static NSString *KKTextContent = @"text";
static NSString *KKImageContent = @"image";
static NSString *KKVideoContent = @"video";

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  // Set which NIB will be used to create cells
  self.cellNib =[UINib nibWithNibName:@"ContentCell" bundle:nil];
  [[self collectionView] registerNib:self.cellNib forCellWithReuseIdentifier:@"ContentCell"];
}

-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    //REF
    /*
  NSMutableArray *a = [[NSMutableArray alloc] initWithCapacity:3];
  
  if([self.node hasContentForLanguage:[KKLanguage currentLanguage]]) {
    [a addObject:KKTextContent];
  }
  if([self.node hasImagesForLanguage:[KKLanguage currentLanguage]]) {
    [a addObject:KKImageContent];
  }
  if([self.node hasVideosForLanguage:[KKLanguage currentLanguage]]) {
    [a addObject:KKVideoContent];
  }
  
  self.nodes = a;
  return (NSInteger)[self.nodes count];
     */
    return 1;
}

-(UICollectionViewCell*) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
  KKResourceCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ContentCell" forIndexPath:indexPath];
  //REF
    /*
  // Find the content type requested
  NSString *content = [self.nodes objectAtIndex:(NSUInteger)indexPath.row];
  
  if(content == KKVideoContent)
  {
    NSArray *vids = [self.node resourcesWithType:KKResourceTypeVideo forLanguage:[KKLanguage currentLanguage]];
    if([vids count]) {
      Resource *video = [vids objectAtIndex:0];
      File *v = [[video.files allObjects] objectAtIndex:0];
      
      CGSize thumbSize = CGSizeMake(cell.frame.size.width, cell.frame.size.height);
      UIImage *image = [v thumbnailWithSize:&thumbSize];
      
      cell.image.image = image;
      cell.title.text = [[KKLanguage localizedString:@"Videos"] uppercaseString];
    }
  } else {

    
    if(content == KKImageContent) {
      cell.title.text = [[KKLanguage localizedString:@"Photos"] uppercaseString];
      // Set the first image as a thumbnail on the cell
      NSArray *images = [self.node imagesWithType:KKFormatMain];
      if(images.count != 0)
      {
        File *f = [images objectAtIndex:0];
        cell.image.image = [UIImage imageWithContentsOfFile:f.localUrl];
      }
    } else if(content == KKTextContent) {
      cell.title.text = [[KKLanguage localizedString:@"Read more"] uppercaseString];
      cell.image.image = [self.node mainImage];
    }
  }
  */
  return cell;
}

// The user wants to view some media now.
// TODO: Should we report this to our delegate and let that handle this?
-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //REF
    /*
  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
  UIViewController *contentView;
  
  NSString *content = [self.nodes objectAtIndex:(NSUInteger)indexPath.row];
  
  if(content == KKTextContent){
    contentView = [storyboard instantiateViewControllerWithIdentifier:@"textView"];
    ((KKTextContentViewController*)contentView).node = self.node;
  } else if(content == KKImageContent) {
    contentView = [storyboard instantiateViewControllerWithIdentifier:@"imageGalleryView"];
    ((KKImageGalleryViewController*)contentView).resources = [self.node resourcesWithType:KKResourceTypeImage forLanguage:[KKLanguage currentLanguage]];
  } else if(content == KKVideoContent) {
    contentView = [storyboard instantiateViewControllerWithIdentifier:@"videoGalleryView"];
    ((KKVideoGalleryViewController*)contentView).resources = [self.node resourcesWithType:KKResourceTypeVideo forLanguage:[KKLanguage currentLanguage]];
  }
  
  [self presentViewController:contentView animated:YES completion:nil];
     */
}


@end
