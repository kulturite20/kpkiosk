//
//  KKNavBarController.h
//  Kulturkiosk
//
//  Created by Rune Botten on 29.08.12.
//
//

#import <UIKit/UIKit.h>

@protocol KKNavBarControllerDelegate

-(void) goBack;
-(void) toggleNavigation;

@end

@interface KKNavBarController : UIViewController

@property (weak) id<KKNavBarControllerDelegate> delegate;
@property (assign) BOOL languageMenuOpen;
@property (weak, nonatomic) IBOutlet UIButton *navButton;

-(void) closeLanguageMenu;
- (void) setBackEnabled:(BOOL) state;
-(bool) backEnabled;

@end
