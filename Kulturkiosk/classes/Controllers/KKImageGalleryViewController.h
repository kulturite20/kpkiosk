//
//  KKImageGalleryViewController.h
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 29.05.12.
//  Copyright (c) 2012 Nordaaker Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KKGalleryViewController.h"

@interface KKImageGalleryViewController : KKGalleryViewController

@property (strong, nonatomic) IBOutlet UIView *imageContainer;

@end
