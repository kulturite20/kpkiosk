
//
//  KKVideoGalleryViewController.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 29.05.12.
//  Copyright (c) 2012 Nordaaker Ltd. All rights reserved.
//

#import "KKVideoGalleryViewController.h"
#import "Resource.h"
#import "File.h"
#import "Credit.h"
#import "KKDataManager.h"

// Controllers
#import "KKMediaItemViewController.h"

@interface KKVideoGalleryViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageContainer;
@property (weak, nonatomic) IBOutlet UIScrollView *thumbnailContainer;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *videoTitle;
@property (weak, nonatomic) IBOutlet UIView *videoWrapper;
@property (strong, nonatomic) KKMediaItemViewController *videoPlayerController;

@end

@implementation KKVideoGalleryViewController {
    NSInteger _currentVideoIndex;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    UIAppDelegate.shouldRegisterTouch = YES;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    [self.titleLabel setText:nil];
    [self.videoTitle setText:nil];
    [self.titleLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:36]];
    [self.videoTitle setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:18]];
    
    UIAppDelegate.shouldRegisterTouch = NO;

     NSMutableArray *tempArray = [[NSMutableArray alloc] init];
     for (File *file in self.resources) {
     NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
     
     NSString *descString = @"", *titleString = @"";
     
     
     KKLanguageType current = [KKLanguage currentLanguage];
     Content *content = [file contentForLanguage:current];
         
         NSMutableString *allCreditsString = [@"" mutableCopy];
         for (Credit *credit in file.credits) {
             if (credit.name.length > 0) {
                 [allCreditsString appendFormat:@"%@: %@\n", [[KKDataManager sharedManager] localizedCreditTypeForTitle:credit.credit_type], credit.name];
             }
         }
         
         descString = content.desc;
         titleString = content.title;
         if ([allCreditsString length] > 0) {
             [dict setObject:allCreditsString
                      forKey:@"credit"];
         }
         if ([descString length] > 0) {
             [dict setObject:descString
                      forKey:@"description"];
         }
         if ([titleString length] > 0) {
             [dict setObject:titleString
                      forKey:@"title"];
         }
     [tempArray addObject:dict];
     
     }
     
     if([tempArray count] < 1) {
         return;
     }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showVideoInFullscreen:)
                                                 name:@"showGalleryVideoFullscreen"
                                               object:nil];
    
     self.resourceDetails = [NSArray arrayWithArray:tempArray];
    [self setupCreditView];
    self.videoTitle.text = self.resourceDetails[0][@"title"];
    if ([self.resources count] != 0) {
        NSInteger index = [self indexOfItemView:0];
        
        _currentVideoIndex = index;
        File *video = [self.resources objectAtIndex:(NSUInteger)index];
        _videoPlayerController = [KKMediaItemViewController new];
        
        [_videoPlayerController willMoveToParentViewController:self];
        [self addChildViewController:_videoPlayerController];
        
        _videoPlayerController.mediaItem = video;
        _videoPlayerController.isInVideoGalleryMode = YES;
        
        [_videoPlayerController didMoveToParentViewController:self];
        [self.videoWrapper addSubview:_videoPlayerController.view];
    }
    CGRect videoFrame = self.videoWrapper.frame;
    videoFrame.origin.x = videoFrame.origin.y = 0;
    
    CGSize thumbSize = CGSizeMake(300, 200);
    NSUInteger offset = 10, horizontalMargin = 10, verticalMargin = 10;
    
    _videoPlayerController.view.frame = (CGRect){0.0,0.0,self.videoWrapper.frame.size.width,self.videoWrapper.frame.size.height};
    if (!self.thumbViews) {
        self.thumbViews = [NSMutableArray new];
    }
    for(NSUInteger i = 0; i < [self.resources count]; i++) {
        
        File *video = [self.resources objectAtIndex:i];
        
        UIImage *thumb = [video thumbnailWithSize:&thumbSize];
        
        UIImageView *thumbnail = [[UIImageView alloc] initWithImage:thumb];
        
        CGRect thumbFrame = CGRectMake(offset, verticalMargin/2, thumbSize.width, thumbSize.height);
        [thumbnail setFrame:thumbFrame];
        offset += (thumbSize.width + horizontalMargin);
        
        NSString *thumbTitle = [(NSDictionary*)[self.resourceDetails objectAtIndex:i] objectForKey:@"title"];
        if ([thumbTitle length] > 0) {
            CGRect  titleFrame = CGRectMake(0, ((thumbSize.height/5)*4), thumbSize.width, thumbSize.height/5);
            UILabel *titleLabel = [[UILabel alloc] initWithFrame: titleFrame];
            titleLabel.text = thumbTitle;
            titleLabel.backgroundColor = [UIColor colorWithRed:0.50 green:0.50 blue:0.50 alpha:0.60];
            titleLabel.textColor = [UIColor whiteColor];
            titleLabel.textAlignment = NSTextAlignmentCenter;
            [titleLabel setFont: [UIFont fontWithName:@"MyriadPro-Bold" size:18]];
            [thumbnail addSubview:titleLabel];
        }
        [thumbnail setContentMode:UIViewContentModeScaleAspectFill];
        [thumbnail setAutoresizesSubviews:YES];
        [thumbnail setClipsToBounds:YES];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTap:)];
        thumbnail.tag = i;
        [thumbnail addGestureRecognizer:tapGesture];
        [thumbnail setUserInteractionEnabled:YES];
        
        [self.thumbnailContainer addSubview:thumbnail];
        [self.thumbViews addObject:thumbnail];
    }
    
    // Set the scroll view content size to fit all added thumbs (accumulated 'offset' variable), plus their height + vertical margin
    [self.thumbnailContainer setContentSize:CGSizeMake(offset, thumbSize.height + verticalMargin)];
    [self.titleLabel setText:self.resourceDetails[0][@"title"]];
    [self.spinner stopAnimating];
    
    [self selectThumb:0];
}

- (void) setupCreditView {
    NSArray *array = [[UINib nibWithNibName:@"KKResourceCreditView" bundle:nil] instantiateWithOwner:self options:nil];
    for(id obj in array) {
        if([obj class] == [KKResourceCreditView class]) {
            self.infoView = (KKResourceCreditView*)obj;
        }
    }
    
    if(self.infoView) {
        CGRect creditFrame = self.infoView.frame;
        // Hidden initially
        creditFrame.origin.x = -creditFrame.size.width;
        self.infoView.frame = creditFrame;
        [self.view addSubview:self.infoView];
    }
}

- (IBAction)goBack:(id)sender {
    [UIAppDelegate startIdleTimers];
    UIAppDelegate.shouldRegisterTouch = YES;
    self.videoWrapper = nil;
    [self.view.window makeKeyAndVisible];
    [self setResources:nil];
    [self dismissViewControllerAnimated:YES
                             completion:NULL];
}

- (void)didTap:(UITapGestureRecognizer *)tapGesture {
    [super didTap:tapGesture];
    [UIView animateWithDuration:0.3 animations:^{
        [self.thumbnailContainer scrollRectToVisible:tapGesture.view.frame animated:NO];
    } completion:^(BOOL finished) {
        NSInteger index = [self indexOfItemView:tapGesture.view];
        if (_currentVideoIndex == index) {
            return;
        }
        _currentVideoIndex = index;
        [self.titleLabel setText:self.resourceDetails[index][@"title"]];
        
        File *video = [self.resources objectAtIndex:(NSUInteger)index];
        _videoPlayerController.mediaItem = video;
    }];
}

- (void)showVideoInFullscreen:(NSNotification *)notification {
    File *videoFile = notification.userInfo[@"videoFile"];
    KKMediaItemViewController *videoController = [KKMediaItemViewController new];
    videoController.mediaItem = videoFile;
    videoController.isInFullscreenMode = YES;
    videoController.isInVideoGalleryMode = YES;
    videoController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:videoController
                       animated:YES
                     completion:NULL];
}

//#pragma mark - Observers
//
//- (void)didFinishPlaying {
//    [UIAppDelegate startIdleTimers];
//}
//
//- (void)didChangePlayingMovie {
//    if (self.moviePlayer.playbackState == MPMoviePlaybackStatePlaying) {
//        [UIAppDelegate stopIdleTimers];
//    } else {
//        [UIAppDelegate startIdleTimers];
//    }
//}

#pragma mark - Public

- (void)goBackWithCompletionBlock:(void (^)())completionBlock {
    self.videoWrapper = nil;
    [self.view.window makeKeyAndVisible];
    [self setResources:nil];
    [self dismissViewControllerAnimated:YES
                             completion:completionBlock];
}

@end
