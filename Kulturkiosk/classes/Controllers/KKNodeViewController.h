//
//  KKNodeViewController.h
//  Kulturkiosk
//
//  Created by Martin Jensen on 17.07.12.
//
//

#import <UIKit/UIKit.h>
/*
    Super-class for content controllers
 */
@interface KKNodeViewController : UIViewController

@end
