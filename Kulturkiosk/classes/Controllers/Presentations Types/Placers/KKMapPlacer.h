//
//  KKMapPlacer.h
//  Kulturkiosk
//
//  Created by Vadim Osovets on 6/19/17.
//
//

#import <Foundation/Foundation.h>

@interface KKMapPlacer : NSObject

- (instancetype)initWithBackground:(UIView *)backgroundView circlePointView:(UIView *)circlePointView withZoomFactor:(CGFloat)zoomFactor;

- (BOOL)applyActionToView:(UIView *)view;

@end

@interface KKLeftMapPlacer : KKMapPlacer
@end

@interface KKRightMapPlacer : KKMapPlacer
@end

@interface KKTopMapPlacer : KKMapPlacer
@end

@interface KKBottomMapPlacer : KKMapPlacer
@end


// If view cutting from left or right side we are moving to left or right.
@interface KKMoveLeftMapPlacer : KKMapPlacer
@end

@interface KKMoveRightMapPlacer : KKMapPlacer
@end

@interface KKCenterPlacer : KKMapPlacer
@end
