//
//  KKMapPlacer.m
//  Kulturkiosk
//
//  Created by Vadim Osovets on 6/19/17.
//
//

#import "KKMapPlacer.h"

#import "UILabel+Size.h"
#import "UIView+Simplified.h"

//MARK: Constants
//const CGFloat kDiamentrCircle = 24;
static const CGFloat kDiamentrCircle = 24;
static const CGFloat kbuttonCenterDeviation = 5.5;

static const CGFloat kDistanceBetweenViews = 20.0;
static const CGFloat kOpacity = 0.5;
static const CGFloat kMaxLabelWidth = 400.0f;

@interface KKMapPlacer ()

@property (nonatomic) UIView *backgroundView;
@property (nonatomic) UIView *circlePointView;
@property (nonatomic) CGFloat zoomFactor;

@end

@implementation KKMapPlacer

- (instancetype)initWithBackground:(UIView *)backgroundView circlePointView:(UIView *)circlePointView withZoomFactor:(CGFloat)zoomFactor {
    
    if (self = [super init]) {
        self.backgroundView = backgroundView;
        self.circlePointView = circlePointView;
        self.zoomFactor = zoomFactor;
    }
    
    return self;
}

- (BOOL)applyActionToView:(UIView *)view {
    [self doesNotRecognizeSelector:_cmd];
    return NO;
}

@end

@implementation KKLeftMapPlacer

- (BOOL)applyActionToView:(UIView *)view {
    
    BOOL canPutOnLeftSide = [view width] < self.circlePointView.frame.origin.x - kDistanceBetweenViews * 2 * self.zoomFactor;
    
    if (canPutOnLeftSide) {
        
        CGFloat x = self.circlePointView.frame.origin.x - kDistanceBetweenViews * self.zoomFactor - [view width];
        CGFloat y = self.circlePointView.center.y - [view halfHeight];
        
        CGPoint point = CGPointMake(x, y);
        
        view.frame = (CGRect){.origin = point,
            .size   = view.frame.size};
        
        return YES;
    }
    
    return NO;
}
    
@end

@implementation KKRightMapPlacer

- (BOOL)applyActionToView:(UIView *)view {
    
    BOOL canPutOnRightSide = [view width] < self.backgroundView.frame.size.width * self.zoomFactor - self.circlePointView.frame.origin.x - kDistanceBetweenViews * self.zoomFactor * 2;
    
    if (canPutOnRightSide) {
        
        CGFloat x = [self.circlePointView rightTopPoint].x + kDistanceBetweenViews * self.zoomFactor;
        CGFloat y = self.circlePointView.center.y - [view halfHeight];
        
        CGPoint point = CGPointMake(x, y);
        
        view.frame = (CGRect){.origin = point,
            .size   = view.frame.size};
        
        return YES;
    }
    
    return NO;
}

@end

@implementation KKTopMapPlacer

- (BOOL)applyActionToView:(UIView *)view {
    
    BOOL canPutOnTopSide = [view height] < self.circlePointView.center.y - kDistanceBetweenViews * self.zoomFactor;
    
    if (canPutOnTopSide) {
        
        CGFloat x = self.circlePointView.center.x - [view halfWidth];
        CGFloat y = self.circlePointView.center.y - [self.circlePointView halfHeight] - kDistanceBetweenViews * self.zoomFactor - [view height];
        
        CGPoint point = CGPointMake(x, y);
        
        view.frame = (CGRect){.origin = point,
            .size   = view.frame.size};
        
        return YES;
    }
    
    return NO;
}

@end

@implementation KKBottomMapPlacer

- (BOOL)applyActionToView:(UIView *)view {
    
    BOOL canPutOnBottomSide = [view height] < self.backgroundView.frame.size.height * self.zoomFactor - self.circlePointView.frame.origin.y - self.circlePointView.frame.size.height - kDistanceBetweenViews * 2 * self.zoomFactor;
    if (canPutOnBottomSide) {
        
        CGFloat x = self.circlePointView.center.x - [view halfWidth];
        CGFloat y = [self.circlePointView leftBottomPoint].y + kDistanceBetweenViews * self.zoomFactor;
        
        CGPoint point = CGPointMake(x, y);
        
        view.frame = (CGRect){.origin = point,
            .size   = view.frame.size};
        
        return YES;
    }
    
    return NO;
}

@end

@implementation KKMoveLeftMapPlacer

- (BOOL)applyActionToView:(UIView *)view {
    
    BOOL canMoveLeft = [view width] <= self.circlePointView.frame.origin.x;
    
    if (canMoveLeft) {
        
        CGFloat moveValueX = -([view halfWidth] - ([self.backgroundView width] * self.zoomFactor - [self.circlePointView rightTopPoint].x));
        
        CGFloat x = self.circlePointView.center.x - [view halfWidth] + moveValueX;
        CGFloat y = view.frame.origin.y;
        
        CGPoint point = CGPointMake(x, y);
        
        view.frame = (CGRect){.origin = point,
            .size   = view.frame.size};
        
        return YES;
    }
    
    return NO;
}

@end


@implementation KKMoveRightMapPlacer

- (BOOL)applyActionToView:(UIView *)view {
    
    BOOL canMoveRight = [view width] >= self.circlePointView.frame.origin.x - kDistanceBetweenViews * 2 * self.zoomFactor;
    
    if (canMoveRight) {
        
        CGFloat moveValueX = [view halfWidth] - self.circlePointView.frame.origin.x;
        
        CGFloat x = self.circlePointView.center.x - [view halfWidth] + moveValueX;
        CGFloat y = view.frame.origin.y;
        
        CGPoint point = CGPointMake(x, y);
        
        view.frame = (CGRect){.origin = point,
            .size   = view.frame.size};
        
        return YES;
    }
    
    return NO;
}

@end

@implementation KKCenterPlacer

- (BOOL)applyActionToView:(UIView *)view {
    
    CGFloat x = self.circlePointView.center.x - [view halfWidth];
    CGFloat y = self.circlePointView.center.x - [view halfHeight];
    
    CGPoint point = CGPointMake(x, y);
    
    view.frame = (CGRect){.origin = point,
        .size   = view.frame.size};
    
    return YES;
    
}

@end
