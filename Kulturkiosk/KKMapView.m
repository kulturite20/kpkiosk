//
//  KKMapView.m
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 04.06.13.
//
//

#import "KKMapView.h"
#import "KKCalloutView.h"
#import "KKAnnotation.h"
#import "KKAnnotationView.h"

#import <QuartzCore/QuartzCore.h>

#define KK_PIN_ANIMATION_DURATION     0.5f
#define KK_CALLOUT_ANIMATION_DURATION 0.1f
#define KK_ZOOM_STEP                  1.5f

@interface KKMapView()

-(void)viewSetup;
-(void)addAnimatedAnnontation:(KKAnnotation *)annontation;
-(void)handleDoubleTap:(UIGestureRecognizer *)gestureRecognizer;
-(void)handleTwoFingerTap:(UIGestureRecognizer *)gestureRecognizer;

@end


@implementation KKMapView

- (void)viewSetup {
    self.delegate = self;
    
    UITapGestureRecognizer *doubleTap    = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    UITapGestureRecognizer *twoFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTwoFingerTap:)];
    
    [doubleTap setNumberOfTapsRequired:2];
    [twoFingerTap setNumberOfTouchesRequired:2];
    
    [self addGestureRecognizer:doubleTap];
    [self addGestureRecognizer:twoFingerTap];
    
    // Disable bouncing after zooming
    self.bounces = NO;
    self.bouncesZoom = NO;
    
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    self.containerView = [[UIView alloc] initWithFrame:CGRectZero];
    [self addSubview:self.containerView];
    [self.containerView addSubview:self.imageView];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self viewSetup];
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self viewSetup];
    }
    return self;
}

- (void)displayMap:(UIImage *)map shouldDisableZoom: (BOOL)shouldDisableZoom annotateWithLinePoints: (BOOL)annotateWithLinePoints {

    //Disable zoom if we have dots with line on mapView
    if (annotateWithLinePoints) {
        
        // 768, CGSizeMake(1024, 688)
        
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        CGSize mapSize = map.size;
        CGFloat zoomFactor = screenSize.width/mapSize.width;//screenSize.width/mapSize.width;

        self.annotationOffset = 1.0f;
        CGFloat maxZoomFactor = 1.0f ;
        
        self.containerView.frame = CGRectMake(0.0f, 0.0f, mapSize.width, mapSize.height);
        self.imageView.frame = CGRectMake(0.0f, 0.0f, mapSize.width, mapSize.height);
        self.imageView.image = map;
        self.orignalSize = mapSize;
        self.contentSize = mapSize;
        
        [self setMaximumZoomScale:maxZoomFactor];
        [self setMinimumZoomScale:zoomFactor];
        [self setZoomScale:zoomFactor];
        
        // Turn off zooming here
        if (shouldDisableZoom) {
            // mapSize = CGSizeMake(screenSize.width, screenSize.height - 60);
            zoomFactor = screenSize.width/mapSize.width;
            
            self.minimumZoomScale = zoomFactor;
            self.maximumZoomScale = zoomFactor;
            
            self.pinchGestureRecognizer.enabled = NO;
        }
        
    } else {
        
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        CGSize mapSize = map.size;
        
        CGFloat zoomFactor = screenSize.width/mapSize.width;
        self.annotationOffset = 1.0f;
        CGFloat maxZoomFactor = 1.0f;
        
        self.containerView.frame = CGRectMake(0.0f, 0.0f, map.size.width, map.size.height);
        self.imageView.frame = CGRectMake(0.0f, 0.0f, map.size.width, map.size.height);
        self.imageView.image = map;
        self.orignalSize = map.size;
        self.contentSize = map.size;
        
        [self setMaximumZoomScale:maxZoomFactor];
        [self setMinimumZoomScale:zoomFactor];
        [self setZoomScale:zoomFactor];
    }
}

-(void)addAnimatedAnnontation:(KKAnnotation *)annontation {
    [self addAnnotation:annontation animated:YES];
}

- (void)addAnnotation:(KKAnnotation *)annotation animated:(BOOL)animate {
    
    KKAnnotationView *annotationView = [[KKAnnotationView alloc] initWithAnnotation:annotation
                                                                          onMapView:self];
    [annotationView addGestureRecognizer: [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showCallOut:) ]];
    [self addObserver:annotationView forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
    [annotation addObserver:annotationView forKeyPath:@"selected" options:NSKeyValueObservingOptionPrior context:nil];
    
    if(animate){
        annotationView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0.0f, -annotationView.center.y);
    }
    
    [self.containerView addSubview:annotationView];
    
    CGAffineTransform transform = CGAffineTransformMakeScale(1.0/self.zoomScale, 1.0/self.zoomScale);
    annotationView.transform = transform;
    
    
    if(animate){
        annotationView.animating = YES;
        [UIView animateWithDuration:KK_PIN_ANIMATION_DURATION animations:^{
            annotationView.transform = CGAffineTransformIdentity;
        }
                         completion:^ (BOOL finished) {
                             annotationView.animating = NO;
                         }];
    }
    
    if(!self.annotationViews){
        self.annotationViews = [[NSMutableArray alloc] init];
    }
    
    [self.annotationViews addObject:annotationView];
    
    [self replaceLeadingAndTopWithCenterConstraints:annotationView];
}

- (void)addAnnotations:(NSArray *)annotations animated:(BOOL)animate {
    int i = 0;
    for (KKAnnotation *annotation in annotations) {
        if(animate){
            [self performSelector:@selector(addAnimatedAnnontation:) withObject:annotation afterDelay:(KK_PIN_ANIMATION_DURATION * (i++ / 2.0f))];
        }
        else{
            [self addAnnotation:annotation animated:NO];
        }
        
    }
}

-(void)removeAnnotation:(KKAnnotation *)annotation
{
    for(KKAnnotationView *annotationView in self.annotationViews){
        if (annotationView.annotation == annotation) {
            [annotationView removeFromSuperview];
            [annotation removeObserver:annotationView forKeyPath:@"selected"];
            [self removeObserver:annotationView forKeyPath:@"contentSize"];
            [self.annotationViews removeObject:annotationView];
            break;
        }
    }
}

-(void)removeAnnotations
{
    for (KKAnnotationView *annotationView in [self.annotationViews copy]) {
        [annotationView removeFromSuperview];
        [annotationView.annotation removeObserver:annotationView forKeyPath:@"selected"];
        [self removeObserver:annotationView forKeyPath:@"contentSize"];
        [self.annotationViews removeObject:annotationView];
    }
}


- (void)replaceLeadingAndTopWithCenterConstraints:(UIView *)subview
{
    CGPoint center = subview.center;
    
    NSLayoutConstraint *leadingConstraint = [self findConstraintOnItem:subview
                                                             attribute:NSLayoutAttributeLeading];
    if (leadingConstraint)
    {
        NSLog(@"Found leading constraint");
        
        [subview.superview removeConstraint:leadingConstraint];
        
        [subview.superview addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                                      attribute:NSLayoutAttributeCenterX
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:subview.superview
                                                                      attribute:NSLayoutAttributeTop
                                                                     multiplier:1.0
                                                                       constant:center.x]];
    }
    
    NSLayoutConstraint *topConstraint = [self findConstraintOnItem:subview
                                                         attribute:NSLayoutAttributeTop];
    
    if (topConstraint)
    {
        NSLog(@"Found top constraint");
        
        [subview.superview removeConstraint:topConstraint];
        
        [subview.superview addConstraint:[NSLayoutConstraint constraintWithItem:subview
                                                                      attribute:NSLayoutAttributeCenterY
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:subview.superview
                                                                      attribute:NSLayoutAttributeLeft
                                                                     multiplier:1.0
                                                                       constant:center.y]];
    }
}

- (NSLayoutConstraint *)findConstraintOnItem:(UIView *)item attribute:(NSLayoutAttribute)attribute
{
    // since we're looking for the item's constraints to the superview, let's
    // iterate through the superview's constraints
    
    for (NSLayoutConstraint *constraint in item.superview.constraints)
    {
        // I believe that the constraints to a superview generally have the
        // `firstItem` equal to the subview, so we'll try that first.
        
        if (constraint.firstItem == item && constraint.firstAttribute == attribute)
            return constraint;
        
        // While it always appears that the constraint to a superview uses the
        // subview as the `firstItem`, theoretically it's possible that the two
        // could be flipped around, so I'll check for that, too:
        
        if (constraint.secondItem == item && constraint.secondAttribute == attribute)
            return constraint;
    }
    
    return nil;
}

- (void)showCallOut:(UITapGestureRecognizer*)sender
{
    NSLog(@"Gesture recognized: %@", sender);
    if(![sender.view isKindOfClass:[KKAnnotationView class]]) return;
    
    KKAnnotationView *annontationView = (KKAnnotationView *)[sender view];
    KKAnnotation        *annotation      = annontationView.annotation;
    
    if(!annotation || !annotation.title) return;
    
    if (annotation.selected){
        [self.mapViewdelegate didDeselectAnnotation:annotation];
        [annotation setSelected:NO];
        return;
    }
    
    [self.mapViewdelegate willSelectAnnotation:annotation];
    [annotation setSelected:YES];
    [self.mapViewdelegate didSelectAnnotation:annotation];
    
    [self bringSubviewToFront:annontationView];
}

- (void)centreOnPoint:(CGPoint)point animated:(BOOL)animate {
    CGFloat x = (CGFloat) round((point.x * self.zoomScale) - (self.frame.size.width / 2.0f));
    CGFloat y = (CGFloat) round((point.y * self.zoomScale) - (self.frame.size.height / 2.0f));
    [self setContentOffset:CGPointMake(x, y) animated:animate];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (!self.dragging) {
        for (KKAnnotationView *aView in self.annotationViews)
        {
            [aView.annotation setSelected:NO];
        }
    }
    
    
    [super touchesEnded:touches withEvent:event];
}

-(CGPoint)zoomRelativePoint:(CGPoint)point{
    CGFloat x = (CGFloat) round((self.contentSize.width / self.orignalSize.width) * point.x);
    CGFloat y = (CGFloat) round((self.contentSize.height / self.orignalSize.height) * point.y);
    return CGPointMake(x, y);
}

- (void)dealloc {
    for(KKAnnotationView *annotationView in self.annotationViews){
        [self removeObserver:annotationView forKeyPath:@"contentSize"];
        [annotationView.annotation removeObserver:annotationView forKeyPath:@"selected"];
    }
    
}

#pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.containerView;
}


- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    UIView *subView = [scrollView.subviews objectAtIndex:0];
    
    CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)?
    (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0.0;
    
    subView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                                 scrollView.contentSize.height * 0.5);
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    for (KKAnnotationView *annotationView in self.annotationViews) {
        [annotationView.annotation setSelected:NO];
    }
}

#pragma mark - Tap to Zoom

- (void)handleDoubleTap:(UIGestureRecognizer *)gestureRecognizer {
    // double tap zooms in, but returns to normal zoom level if it reaches max zoom
    float  newScale = self.zoomScale >= self.maximumZoomScale ? self.minimumZoomScale : self.zoomScale * KK_ZOOM_STEP;
    [self setZoomScale:newScale animated:YES];
}

- (void)handleTwoFingerTap:(UIGestureRecognizer *)gestureRecognizer {
    // two-finger tap zooms out, but returns to normal zoom level if it reaches min zoom
    float  newScale = self.zoomScale <= self.minimumZoomScale ? self.maximumZoomScale : self.zoomScale / KK_ZOOM_STEP;
    [self setZoomScale:newScale animated:YES];
}

@end

#undef KK_PIN_ANIMATION_DURATION
#undef KK_CALLOUT_ANIMATION_DURATION
#undef KK_ZOOM_STEP
