//
//  KKVideoPresentationViewController.m
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 05.11.13.
//
//

#import "KKVideoPresentationViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "KKVideoThumbCell.h"
#import "KKDataManager.h"
#import "Page.h"

NSString *kTracksKey        = @"tracks";
NSString *kStatusKey        = @"status";
NSString *kRateKey          = @"rate";
NSString *kPlayableKey      = @"playable";
NSString *kCurrentItemKey   = @"currentItem";

static void *KKMovieViewControllerRateObservationContext = &KKMovieViewControllerRateObservationContext;
static void *KKMovieViewControllerCurrentItemObservationContext = &KKMovieViewControllerCurrentItemObservationContext;
static void *KKMovieViewControllerPlayerItemStatusObserverContext = &KKMovieViewControllerPlayerItemStatusObserverContext;

@interface KKVideoPresentationViewController () {
    double restoreAfterScrubbingRate;
    BOOL seekToZero;
}

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet UIView *videoContainer;
@property (weak, nonatomic) IBOutlet UILabel *videotitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *videoDescriptionLabel;
@property (retain, nonatomic) AVPlayer *avPlayer;
@property (retain, nonatomic) AVPlayerItem *avPlayerItem;
@property (weak, nonatomic) IBOutlet UICollectionView *videoThumbs;
@property (weak, nonatomic) IBOutlet UISlider *progressSlider;
@property (strong, nonatomic) NSArray *videos;
@property (strong, nonatomic) UIWindow *externalWindow;
@property (weak, nonatomic) IBOutlet UIButton *pauseButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (strong, nonatomic)  id timeObserver;
@property (strong, nonatomic) NSArray *videoThumbImgs;

- (void)syncScrubber;
-(void)removePlayerTimeObserver;
- (IBAction)beginScrubbing:(id)sender;
- (IBAction)endScrubbing:(id)sender;

@end

@implementation KKVideoPresentationViewController

- (IBAction)dismissController:(id)sender {
    UIAppDelegate.needToShowDefaultRecord = NO;
    [self.avPlayer pause];
    self.externalWindow.hidden = YES;
    [self dismissViewControllerAnimated:NO
                             completion:^{
                                 
                                 [[NSNotificationCenter defaultCenter] removeObserver:self
                                                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                                                               object:nil];
                                 [self.avPlayer removeObserver:self forKeyPath:kCurrentItemKey];
                                 
                                 [self.avPlayer removeObserver:self forKeyPath:kRateKey];
                                 [self.avPlayerItem removeObserver:self forKeyPath:kStatusKey];
                             }];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.titleLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:36]];
    [self.descriptionLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:20]];
    self.descriptionLabel.numberOfLines = 0;
    [self.videotitleLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:24]];
    [self.videoDescriptionLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:20]];
    self.videoDescriptionLabel.numberOfLines = 0;
    
    [self gatherAllVideos];
    
    if ([self.videos count]) {
        [self loadVideo:[self.videos firstObject]];
    }
    
    NSMutableArray *thumbs = [NSMutableArray array];
    
    for (File *videoFile in self.videos) {
        CGSize size = CGSizeMake(250.0, 250.0);
        
        UIImage *thumb = [videoFile thumbnailWithSize: &size];
        
        if (!thumb) {
            thumb=[[UIImage alloc] init];
        }
        
        [thumbs addObject:thumb];
    }
    
    self.videoThumbImgs = (NSArray*)thumbs;
    
    Content *content = [self.recordToPresent contentForLanguage:[KKLanguage currentLanguage]];
    self.titleLabel.text = content.title;
    self.descriptionLabel.text = content.desc;
    [self.descriptionLabel sizeToFit];
}

- (void)viewWillDisappear:(BOOL)animated {
    // FIXME: Probably hubris?
    [self.avPlayer pause];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

- (void)gatherAllVideos {
    NSMutableArray *videos = [NSMutableArray new];
    
    Content *content = [self.recordToPresent contentForLanguage:[KKLanguage currentLanguage]];
    for (Page *page in content.pages) {
        for (VideoBlock *block in [page videoBlocks]) {
            for (File *file in [(VideoBlock *)block files]) {
                if ([file.type isEqualToString:@"video"]) {
                    [videos addObject:file];
                }
            }
        }
    }
    
//    NSSortDescriptor *pageIdDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"page_id"
//                                                                       ascending:YES];
    NSSortDescriptor *positionSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"position"
                                                                             ascending:YES];
    NSSortDescriptor *identifierSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"identifier"
                                                                               ascending:YES];
    
    
    self.videos = [videos sortedArrayUsingDescriptors:@[positionSortDescriptor,
                                                        identifierSortDescriptor]];
}

#pragma mark - Collection view for bottom

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.videos count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    KKVideoThumbCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"thumbCell" forIndexPath:indexPath];
    [cell setThumbnail: [self.videoThumbImgs objectAtIndex:indexPath.row]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self loadVideo: [self.videos objectAtIndex:indexPath.row]];
}

#pragma mark - Video Player

- (void)loadVideo:(File *)videoFile {
    
    self.videotitleLabel.text = [videoFile contentForLanguage:[KKLanguage currentLanguage]].title;
    
    NSString *videoDescription = [videoFile contentForLanguage:[KKLanguage currentLanguage]].desc;
    
    NSMutableString *allCreditsString = [@"" mutableCopy];
    for (Credit *credit in videoFile.credits) {
        if (credit.name.length > 0) {
            [allCreditsString appendFormat:@"%@: %@\n", [[KKDataManager sharedManager] localizedCreditTypeForTitle:credit.credit_type], credit.name];
        }
    }
    
    NSString *finalDescription = [videoDescription stringByAppendingString:[NSString stringWithFormat:@"\n\n%@", allCreditsString]];
    
    self.videoDescriptionLabel.text = finalDescription;
    [self.videoDescriptionLabel sizeToFit];
    seekToZero = NO;
    
    NSURL *url = [videoFile pathToOriginalItem] ? [NSURL fileURLWithPath:[videoFile pathToOriginalItem]] : nil;
    
    if (!url) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invalid video"
                                                        message:@"There is no internal link for this video"
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        return;
        
    }
    
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:url
                                            options:nil];
    if (!asset.playable) {
        
    // uncomment this to show alert when is not possible to play madia item.
        
    //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invalid video"
    //                                                        message:@"The specified video could not be loaded"
    //                                                       delegate:self
    //                                              cancelButtonTitle:@"Ok"
    //                                              otherButtonTitles: nil];
    //        [alert show];
        
        return;
    }
    
    if (self.avPlayerItem) {
        [self.avPlayerItem removeObserver:self forKeyPath:kStatusKey];
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:AVPlayerItemDidPlayToEndTimeNotification
                                                      object:self.avPlayerItem];
    }
    
    self.avPlayerItem = [AVPlayerItem playerItemWithAsset:asset];
    
    [self.avPlayerItem addObserver:self
                        forKeyPath:kStatusKey
                           options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                           context:KKMovieViewControllerPlayerItemStatusObserverContext];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:self.avPlayerItem];
    if (!self.avPlayer) {
        self.avPlayer = [AVPlayer playerWithPlayerItem:self.avPlayerItem];
        
        [self.avPlayer addObserver:self
                        forKeyPath:kCurrentItemKey
                           options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                           context:KKMovieViewControllerCurrentItemObservationContext];
        
        /* Observe the AVPlayer "rate" property to update the scrubber control. */
        [self.avPlayer addObserver:self
                        forKeyPath:kRateKey
                           options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                           context:KKMovieViewControllerRateObservationContext];
        
        [self setupControls];
    }
    
    if (self.avPlayer.currentItem != self.avPlayerItem) {
        [self.avPlayer replaceCurrentItemWithPlayerItem:self.avPlayerItem];
    }

    self.progressSlider.value = 0.0;
    [self.avPlayer play];
    [self syncPlayPauseButtons];

    // Add sublayer to device internal screen.
    AVPlayerLayer *layer = [AVPlayerLayer playerLayerWithPlayer:self.avPlayer];
    CGRect frame = self.videoContainer.frame;
    frame.origin.x = frame.origin.y = 0;
    layer.frame = frame;
    [self.videoContainer.layer addSublayer:layer];

    // Configure external display if present.
    CGRect videoFrame = self.externalWindow.frame;
    videoFrame.origin.x = videoFrame.origin.y = 0;
    self.externalWindow.frame = videoFrame;
    
    if ([[UIScreen screens] count] > 1) {
        UIScreen *externalDisplay = [[UIScreen screens] objectAtIndex:1];
        CGRect bounds = externalDisplay.bounds;
        
        self.externalWindow = [[UIWindow alloc] initWithFrame:bounds];
        self.externalWindow.screen = externalDisplay;
        self.externalWindow.hidden = NO;
        
        AVPlayerLayer *layer2 = [AVPlayerLayer playerLayerWithPlayer:self.avPlayer];
        layer2.frame = self.externalWindow.frame;

        self.externalWindow.rootViewController = [UIViewController new];
        
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [UIAlertView showMessage:[NSString stringWithFormat:@"Layer exist: %@?", layer2]];
        [self.externalWindow.rootViewController.view.layer addSublayer:layer2];
//        }
    }
}

- (void)setupControls {
    double interval = .1f;
    CMTime playerDuration = [self playerItemDuration]; // return player duration.
    if (CMTIME_IS_INVALID(playerDuration))
    {
        return;
    }
    double duration = CMTimeGetSeconds(playerDuration);
    if (isfinite(duration))
    {
        CGFloat width = CGRectGetWidth([self.progressSlider bounds]);
        interval = 0.5f * duration / width;
    }
    /* Update the scrubber during normal playback. */
    __weak typeof(self) weakSelf = self;
    self.timeObserver = [self.avPlayer addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(interval, NSEC_PER_SEC)
                                                                    queue:NULL
                                                               usingBlock:^(CMTime time) {
                                                                   [weakSelf syncScrubber];
                                                               }];
}

- (void)syncScrubber {
    CMTime playerDuration = [self playerItemDuration];
    if (CMTIME_IS_INVALID(playerDuration)) {
        self.progressSlider.minimumValue = 0.0;
        return;
    }
    
    double duration = CMTimeGetSeconds(playerDuration);
    if (isfinite(duration) && (duration > 0)) {
        float minValue = [self.progressSlider minimumValue];
        float maxValue = [self.progressSlider maximumValue];
        double time = CMTimeGetSeconds([self.avPlayer currentTime]);
        [self.progressSlider setValue:(maxValue - minValue) * time / duration + minValue];
    }
}

/* The user is dragging the movie controller thumb to scrub through the movie. */
- (IBAction)beginScrubbing:(id)sender {
    restoreAfterScrubbingRate = [self.avPlayer rate];
    [self.avPlayer setRate:0.f];
    
    /* Remove previous timer. */
    [self removePlayerTimeObserver];
}

/* The user has released the movie thumb control to stop scrubbing through the movie. */
- (IBAction)endScrubbing:(id)sender {
    [self syncPlayPauseButtons];
    if (!self.timeObserver) {
        CMTime playerDuration = [self playerItemDuration];
        if (CMTIME_IS_INVALID(playerDuration)) {
            return;
        }
                
        double duration = CMTimeGetSeconds(playerDuration);
        if (isfinite(duration)) {
            CGFloat width = CGRectGetWidth([self.progressSlider bounds]);
            double tolerance = 0.5f * duration / width;
            
            __weak typeof(self) weakSelf = self;
            
            self.timeObserver = [self.avPlayer addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(tolerance, NSEC_PER_SEC) queue:dispatch_get_main_queue() usingBlock:
                                 ^(CMTime time)
                                 {
                                     [weakSelf syncScrubber];
                                 }];
        }
    }
    
    if (restoreAfterScrubbingRate) {
        [self.avPlayer setRate:restoreAfterScrubbingRate];
        restoreAfterScrubbingRate = 0.f;
    }
}

- (IBAction)scrub:(id)sender {
    if ([sender isKindOfClass:[UISlider class]]) {
        UISlider *slider = sender;
        
        CMTime playerDuration = [self playerItemDuration];
        if (CMTIME_IS_INVALID(playerDuration)) {
            return;
        }
        
        double duration = CMTimeGetSeconds(playerDuration);
        if (isfinite(duration)) {
            float minValue = [slider minimumValue];
            float maxValue = [slider maximumValue];
            float value = [slider value];
            
            double time = duration * (value - minValue) / (maxValue - minValue);
            
            [self.avPlayer seekToTime:CMTimeMakeWithSeconds(time, NSEC_PER_SEC)];
        }
    }
}

- (void)removePlayerTimeObserver {
    if (self.timeObserver) {
        [self.avPlayer removeTimeObserver:self.timeObserver];
        self.timeObserver = nil;
    }
}

- (CMTime)playerItemDuration {
    AVPlayerItem *thePlayerItem = [self.avPlayer currentItem];
    if (thePlayerItem.status == AVPlayerItemStatusReadyToPlay) {
        return([thePlayerItem duration]);
    }
    
    return(kCMTimeInvalid);
}

- (IBAction)playButtonClicked:(id)sender {
    if(seekToZero) {
        [self.avPlayer seekToTime:kCMTimeZero];
        seekToZero = NO;
    }
    
    [self.avPlayer play];
    [self syncPlayPauseButtons];
}

- (IBAction)pauseButtonClicked:(id)sender {
    [self.avPlayer pause];
    [self syncPlayPauseButtons];
}

- (void)syncPlayPauseButtons {
    if ([self.avPlayer rate] != 0.0) {
        self.pauseButton.hidden = NO;
        self.playButton.hidden = YES;
        
        [self performSelector:@selector(stopTimer)
                   withObject:self
                   afterDelay:1.0];
    } else {
        self.pauseButton.hidden = YES;
        self.playButton.hidden = NO;
        
        [self performSelector:@selector(startTimer)
                   withObject:self
                   afterDelay:1.0];
    }
}

/*
- (void)didFinishPlaying {
    [UIAppDelegate startIdleTimers];
}

- (void)didChangePlayingMovie {
    if (self.moviePlayer.playbackState == MPMoviePlaybackStatePlaying) {
        [UIAppDelegate stopIdleTimers];
    } else {
        [UIAppDelegate startIdleTimers];
    }
}
*/

- (void)stopTimer {
    [UIAppDelegate stopIdleTimers];
    UIAppDelegate.shouldRegisterTouch = NO;
}

- (void)startTimer {
    UIAppDelegate.shouldRegisterTouch = YES;
    [UIAppDelegate startIdleTimers];
}

- (void)initScrubberTimer {
    double interval = .1f;
    
    CMTime playerDuration = [self playerItemDuration];
    if (CMTIME_IS_INVALID(playerDuration)) {
        return;
    }
    
    double duration = CMTimeGetSeconds(playerDuration);
    if (isfinite(duration)) {
        CGFloat width = CGRectGetWidth([self.progressSlider bounds]);
        interval = 0.5f * duration / width;
    }
    
    /* Update the scrubber during normal playback. */
    __weak typeof(self) weakSelf = self;
    
    self.timeObserver = [self.avPlayer addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(interval, NSEC_PER_SEC)
                                                                    queue:NULL
                                                               usingBlock:^(CMTime time) {
                                                                   [weakSelf syncScrubber];
                                                               }];
}

- (void)observeValueForKeyPath:(NSString*) path
                      ofObject:(id)object
                        change:(NSDictionary*)change
                       context:(void*)context {
    /* AVPlayerItem "status" property value observer. */
    if (context == KKMovieViewControllerPlayerItemStatusObserverContext) {
        [self syncPlayPauseButtons];
        
        AVPlayerStatus status = [[change objectForKey:NSKeyValueChangeNewKey] integerValue];
        
        switch (status) {
                /* Indicates that the status of the player is not yet known because
                 it has not tried to load new media resources for playback */
            case AVPlayerStatusUnknown: {
                [self removePlayerTimeObserver];
                [self syncScrubber];
                self.progressSlider.enabled = NO;
            }
                break;
                
            case AVPlayerStatusReadyToPlay: {
                /* Once the AVPlayerItem becomes ready to play, i.e.
                 [playerItem status] == AVPlayerItemStatusReadyToPlay,
                 its duration can be fetched from the item. */
                
                /* Show the movie slider control since the movie is now ready to play. */
                self.progressSlider.enabled = YES;
                [self initScrubberTimer];
            }
                break;
                
            case AVPlayerStatusFailed: {
                //     AVPlayerItem *thePlayerItem = (AVPlayerItem *)object;
                //      [self assetFailedToPrepareForPlayback:thePlayerItem.error];
            }
                break;
        }
    } else if (context == KKMovieViewControllerRateObservationContext) {
        /* AVPlayer "rate" property value observer. */
        [self syncPlayPauseButtons];
    } else if (context == KKMovieViewControllerCurrentItemObservationContext) {
        /* AVPlayer "currentItem" property observer.
         Called when the AVPlayer replaceCurrentItemWithPlayerItem:
         replacement will/did occur. */
        
        AVPlayerItem *newPlayerItem = [change objectForKey:NSKeyValueChangeNewKey];
        
        /* New player item null? */
        if (newPlayerItem == (id)[NSNull null]) {
            //   [self disablePlayerButtons];
            //   [self disableScrubber];
            
        } else {
            /* Replacement of player currentItem has occurred */
            
            /* Set the AVPlayer for which the player layer displays visual output. */
            //   [playerLayerView.playerLayer setPlayer:self.player];
            
            /* Specifies that the player should preserve the video’s aspect ratio and
             fit the video within the layer’s bounds. */
            //   [playerLayerView setVideoFillMode:AVLayerVideoGravityResizeAspect];
            
            [self syncPlayPauseButtons];
        }
    } else {
        [super observeValueForKeyPath:path
                             ofObject:object
                               change:change
                              context:context];
    }
    
    return;
}

/* Called when the player item has played to its end time. */
- (void) playerItemDidReachEnd:(NSNotification*) aNotification {
    /* Hide the 'Pause' button, show the 'Play' button in the slider control */
    [self syncPlayPauseButtons];
    
    /* After the movie has played to its end time, seek back to time zero
     to play it again */
    seekToZero = YES;
}

@end
