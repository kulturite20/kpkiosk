//
//  KKNavigationViewController.h
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 6/11/13.
//
//

#import <UIKit/UIKit.h>
#import "MosaicPresentation+CoreDataClass.h"

extern const CGFloat kNavigationBarDefaultHeight;

@interface KKNavigationViewController : UIViewController

@property (nonatomic) NSArray *presentations;
@property (assign, nonatomic) BOOL shouldEnterRestmode;
@property (weak, nonatomic) IBOutlet UIView *buttonContainer;
@property (nonatomic, strong) UIViewController	 *activeViewController;
@property (weak, nonatomic) IBOutlet UIView *activeView;
@property (strong,nonatomic) Presentation *currentPresentation;
@property (strong, nonatomic) MosaicPresentation *mosaicPresentation;
@property (weak, nonatomic) IBOutlet UIButton *helpButton;

// For testing
- (void)fileLoaded:(NSNotification *)notification;
- (void)showSinglePresentationFromHomeScreen:(Presentation *)presentation;

@end
